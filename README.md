DDLib - Advanced Dungeons & Dragons (2nd Edition) library and toolkit
=====================================================================

Written by Marco Carosio [https://marcocarosio.it/](https://marcocarosio.it/)

Ddlib is an open C++ role playing game library. It aims to mimic the ruleset of Advanced Dungeons & Dragons 2nd Edition.
Ddlib hides the burden of dealing with the complexity of AD&D ruleset, simplifying the creation of applications like role playing games,
character generators, simulators, and things like that.
It may sound an ambitious project, and it is indeed. I'm working on it in my spare time, that's why the project progress at a slow pace...

For a brief introduction to the library features, please refer to the [DDLib Wiki](https://gitlab.com/mcarosio/ddlib/-/wikis/home)

You may also be interested in the reason why I decided to implement a legacy ruleset:
* because I enjoyed it so much when I was a teenager;
* because I still consider it as one of the best framework ever conceived;
* because I've always been waiting for [Eye of the Beholder IV](https://en.wikipedia.org/wiki/Eye_of_the_Beholder_(video_game)) and I may eventually need a framework to program it myself.

Last but not least, as far as I know AD&D 2nd Edition is not copyrighted or maintained anymore, so there's no copyright infringment.
I did research about it and discovered that D&D rules are copyrighted to the extreme, that'd explain why many companies have recently opted for custom rulesets instead of the official one.
Moreover, AD&D 2nd Edition cannot be considered the core business of the current right holder, the [Wizards of the Coast](https://company.wizards.com/)/[Hasbro](https://products.hasbro.com/it-it), since it's not even mentioned in their website.
They seem to focus on the third edition on, so I hope they won't mind if an AD&D enthusiast decided to bring back to life what he still considers a milestone in role playing game history.

That said, [WOTC](https:://wizards.com) has a [Fan Content Policy](https://company.wizards.com/it/fancontentpolicy) that allows for amatorial contents to be created.
Ddlib is released under that policy, and according to it:
* it's free of charge;
* it's not an official WOTC product;
* it doesn't make use of copyrighted material;
* it's not sponsored by third parties.

Have fun and long life to AD&D!