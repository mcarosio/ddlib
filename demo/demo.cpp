// demo.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "stdafx.h"
#include <iostream>
#include <filesystem>

using namespace std;

#include "adnd.h"
#include "market.h"
#include "treasure.h"
#include "monster.h"

using namespace adnd;

void print_help(int argc, char* argv[]);

void dump_classes();
std::string print_split(const defs::character_class& cls);
void print_character(character& c, bool printStats = false, bool printSkills = false, bool printExperience = false, bool printMoney = false);
void print_hp(hp_sequence& hp);
void print_hps(hit_points& hps);
void print_store(const uint32_t& mktId);
void print_money_bag(money::money_bag& bag);
void print_spell_book(spells::spell_book& spellBook);
void print_holy_symbol(spells::holy_symbol& holySymbol);
void check_attack_result(const attack_result& result);
void check_attack_result(const hit_result& result);

void test_skill();
void test_class();
void test_xp();
void test_multiclass();
void test_experience();
void test_hp_sequence();
void test_hit_points(short constAdj);
void test_hit_points_single_class();
void test_experience_single_class();
void test_turn_undead();
void test_weapons();
void test_class_2();
void test_class_creation();
void test_money_bag();
void test_bonus_xp();
void test_market();
void test_market_2();
void test_market_3();
void test_weight_and_encumbrance();
void test_attack();
void test_multiclass_template();
void test_missile_specialisation();
void test_treasure();
void test_ageing();
void test_wizard_spells();
void test_priest_spells();
void test_monster();

using namespace std::chrono;

int main(int argc, char* argv[])
{
	//if (argc == 1)
	//{
	//	print_help(argc, argv);
	//	exit(0);
	//}

	std::string dbFilePath;// = "..\\..\\..\\..\\SQL\\adnd.db";
	std::string sqlCreateFilePath;// = "..\\..\\..\\..\\SQL\\adnd.sql";
	std::string sqlInitFilePath;// = "..\\..\\..\\..\\SQL\\adnd_init.sql";
	adnd::configuration::error_handler_style errHdlStyle = adnd::configuration::error_handler_style::exception;
	adnd::configuration::database_source dbSrc = adnd::configuration::database_source::memory;

	for (int a = 1; a < argc; ++a)
	{
		size_t argLen = strlen(argv[a]);
		if (std::memcmp(argv[a], "-p", argLen) == 0 || std::memcmp(argv[a], "--path", argLen) == 0)
		{
			std::experimental::filesystem::path dir(argv[a + 1]);
			if (!std::experimental::filesystem::exists(dir))
			{
				std::cout << "Parameter " << argv[a] << " specified an invalid value: " << argv[a + 1] << "\n";
				exit(1);
			}
			if (dir.string().at(dir.string().length() - 1) != std::experimental::filesystem::path::preferred_separator)
				dir += std::experimental::filesystem::path::preferred_separator;

			dbFilePath.append(dir.string().c_str()).append("adnd.db");
			sqlCreateFilePath.append(dir.string().c_str()).append("adnd.sql");
			sqlInitFilePath.append(dir.string().c_str()).append("adnd_init.sql");
			a++;
		}
		else if (std::memcmp(argv[a], "--db", argLen) == 0 || std::memcmp(argv[a], "-d", argLen) == 0)
		{
			std::experimental::filesystem::path dir(argv[a + 1]);
			if (!std::experimental::filesystem::exists(dir))
			{
				std::cout << "Parameter " << argv[a] << " specified an invalid value: " << argv[a + 1] << "\n";
			}
			dbFilePath.append(dir.string().c_str());
			a++;
		}
		else if (std::memcmp(argv[a], "--sql", argLen) == 0 || std::memcmp(argv[a], "-s", argLen) == 0)
		{
			std::experimental::filesystem::path dir(argv[a + 1]);
			if (!std::experimental::filesystem::exists(dir))
			{
				std::cout << "Parameter " << argv[a] << " specified an invalid value: " << argv[a + 1] << "\n";
			}
			if (dir.string().at(dir.string().length() - 1) != std::experimental::filesystem::path::preferred_separator)
				dir += std::experimental::filesystem::path::preferred_separator;

			sqlCreateFilePath.append(dir.string().c_str());
			a++;
		}
		else if (std::memcmp(argv[a], "--init", argLen) == 0 || std::memcmp(argv[a], "-i", argLen) == 0)
		{
			std::experimental::filesystem::path dir(argv[a + 1]);
			if (!std::experimental::filesystem::exists(dir))
			{
				std::cout << "Parameter " << argv[a] << " specified an invalid value: " << argv[a + 1] << "\n";
			}
			if (dir.string().at(dir.string().length() - 1) != std::experimental::filesystem::path::preferred_separator)
				dir += std::experimental::filesystem::path::preferred_separator;

			sqlInitFilePath.append(dir.string().c_str());
			a++;
		}
		else if (std::memcmp(argv[a], "--err", argLen) == 0 || std::memcmp(argv[a], "-e", argLen) == 0)
		{
			if (std::memcmp(argv[a + 1], "exception", std::strlen(argv[a + 1])) == 0)
				errHdlStyle = adnd::configuration::error_handler_style::exception;
			else if (std::memcmp(argv[a + 1], "internal", std::strlen(argv[a + 1])) == 0)
				errHdlStyle = adnd::configuration::error_handler_style::internal;
			a++;
		}
		else if (std::memcmp(argv[a], "--source", argLen) == 0 || std::memcmp(argv[a], "-r", argLen) == 0)
		{
			if (std::memcmp(argv[a + 1], "file", std::strlen(argv[a + 1])) == 0)
				dbSrc = adnd::configuration::database_source::file;
			else if (std::memcmp(argv[a + 1], "memory", std::strlen(argv[a + 1])) == 0)
				dbSrc = adnd::configuration::database_source::memory;
			a++;
		}
		else if (std::memcmp(argv[a], "--help", argLen) == 0 || std::memcmp(argv[a], "-h", argLen) == 0)
		{
			print_help(argc, argv);
			exit(0);
		}
	}

	adnd::configuration::get_instance().get_database_path() = dbFilePath;
	adnd::configuration::get_instance().get_definition_script_path() = sqlCreateFilePath;
	adnd::configuration::get_instance().get_init_script_path() = sqlInitFilePath;
	adnd::configuration::get_instance().get_error_handler_style() = errHdlStyle;
	adnd::configuration::get_instance().get_database_source() = dbSrc;

	templates::metadata::init();
	configuration::get_instance().init();

	try
	{
		//configuration::get_instance().definition_script_path() = "..\\..\\..\\..\\SQL\\adnd.db";
		//configuration::get_instance().definition_script_path() = "..\\..\\..\\..\\SQL\\adnd.sql";
		//configuration::get_instance().init_script_path() = "..\\..\\..\\..\\SQL\\adnd_init.sql";
		//configuration::get_instance().error_handler_style() = db::sqlite_db::error_handler_style::exception;
		//configuration::get_instance().database_source() = db::sqlite_db::source::file;


		/*{
			std::chrono::time_point<std::chrono::high_resolution_clock> beg;
			beg = std::chrono::high_resolution_clock::now();

			auto t = templates::metadata::get_instance().get_coin_template(defs::coin::gold);

			auto micro = std::chrono::duration_cast<std::chrono::duration<long, std::nano>>(std::chrono::high_resolution_clock::now() - beg).count();

			std::cout << "Description: " << t.get_description() << "\n";
			std::cout << "It took " << micro << " microseconds\n";
		}

		auto t = templates::metadata::get_instance().get_coin_template(defs::coin::gold);
		{
			std::chrono::time_point<std::chrono::high_resolution_clock> beg;
			beg = std::chrono::high_resolution_clock::now();

			std::string desc = t.get_description();

			auto micro = std::chrono::duration_cast<std::chrono::duration<long, std::nano>>(std::chrono::high_resolution_clock::now() - beg).count();

			std::cout << "Description: " << desc << "\n";
			std::cout << "It took " << micro << " microseconds\n";
		}*/

		//test_skill();							//OK
		//test_class();							//OK
		//test_xp();							//OK
		//test_multiclass();					//OK
		//test_experience();					//OK
		//test_hp_sequence();					//OK
		//test_hit_points(4);					//OK
		//test_hit_points(-2);					//OK
		//test_hit_points_single_class();		//OK
		//test_experience_single_class();		//OK
		//test_turn_undead();					//OK
		//test_weapons();						//OK
		//test_class_2();						//OK
		//test_class_creation();				//OK
		//test_money_bag();						//OK
		//test_bonus_xp();						//OK
		//test_market();						//OK
		//test_market_2();						//OK
		//test_market_3();						//OK
		//test_weight_and_encumbrance();		//OK
		//test_attack();						//OK
		//test_multiclass_template();			//OK
		//test_missile_specialisation();		//OK
		//test_treasure();						//
		//test_ageing();						//OK
		//test_wizard_spells();					//OK
		//test_priest_spells();					//OK
		//test_monster();
	}
	catch (std::exception &e)
	{
		cout << e.what() << endl;
	}

	cout << "Press a key to terminate" << endl;
	char c;
	cin.get(c);

	return 0;
}


void test_wizard_spells()
{
	auto cls = defs::character_class::invoker;
	character chr("Palantir", cls, defs::character_race::human, defs::character_sex::male,
					defs::moral_alignment::lawful_neutral, true);
	chr.set_skill(defs::character_skill::intelligence, 11);
	auto& spellBook = chr.spell_book();
	
	print_spell_book(spellBook);

	std::cout << "Adding Magic missile to the book\n";
	spellBook.add(defs::spells::wizard_spell::magic_missile);

	print_spell_book(spellBook);

	std::cout << "Adding Fireball to the book\n";
	try
	{
		spellBook.add(defs::spells::wizard_spell::fireball);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << "\n";
	}

	std::cout << "Adding Audible glamer e to the book\n";
	try
	{
		spellBook.add(defs::spells::wizard_spell::audible_glamer);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << "\n";
	}

	std::cout << "Selecting Magic missile to the book\n";
	spellBook.select(defs::spells::wizard_spell::magic_missile);
	print_spell_book(spellBook);

	std::cout << "Selecting Magic missile to the book\n";
	try
	{
		spellBook.select(defs::spells::wizard_spell::magic_missile);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << "\n";
	}

	print_spell_book(spellBook);

	std::cout << "Selecting Cantrip to the book\n";
	try
	{
		spellBook.select(defs::spells::wizard_spell::cantrip);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << "\n";
	}

	print_spell_book(spellBook);

	std::cout << "Adding 20000 XP\n";
	chr.add_xp(20000);

	try
	{
		spellBook.select(defs::spells::wizard_spell::cantrip);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << "\n";
	}

	std::cout << "Adding Fireball to the book\n";
	try
	{
		spellBook.add(defs::spells::wizard_spell::fireball);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << "\n";
	}

	print_spell_book(spellBook);

	std::cout << "Selecting Magic missile to the book\n";
	spellBook.select(defs::spells::wizard_spell::magic_missile);
	print_spell_book(spellBook);

	std::cout << "Selecting Magic missile to the book\n";
	spellBook.select(defs::spells::wizard_spell::magic_missile);
	std::cout << "Selecting Magic missile to the book\n";
	spellBook.select(defs::spells::wizard_spell::magic_missile);
	print_spell_book(spellBook);

	std::cout << "Selecting Magic missile to the book\n";
	try
	{
		spellBook.select(defs::spells::wizard_spell::magic_missile);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << "\n";
	}

	print_spell_book(spellBook);
}

void test_monster()
{
	adnd::monster::monster* m = adnd::monster::monsters_den::get_instance().spawn(defs::monsters::monster::ghost, "Angus Roy");

	character chr("Father Jon", defs::character_class::cleric, defs::character_race::human, defs::character_sex::male,
		defs::moral_alignment::lawful_neutral, true, defs::deities::deity::lathander);
	chr.gain_level(11);

	auto res = chr.turn_undead(m->turnable_as());
	std::cout << chr.get_name() << " tries to turn " << m->name() << ": " << res << "\n";
}

void test_priest_spells()
{
	auto cls = defs::character_class::cleric;
	character chr("Father Jon", cls, defs::character_race::human, defs::character_sex::male,
		defs::moral_alignment::lawful_neutral, true, defs::deities::deity::lathander);
	chr.set_skill(defs::character_skill::wisdom, 11);
	auto& holySymbol = chr.holy_symbol();

	print_holy_symbol(holySymbol);

	holySymbol.select(defs::spells::priest_spell::cure_light_wounds);

	print_holy_symbol(holySymbol);

	try
	{
		holySymbol.select(defs::spells::priest_spell::cure_light_wounds);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << "\n";
	}

	print_holy_symbol(holySymbol);

	chr.add_xp(3000);

	holySymbol.select(defs::spells::priest_spell::shillelagh);

	print_holy_symbol(holySymbol);

	holySymbol.pray(1);
	print_holy_symbol(holySymbol);

	holySymbol.pray(5);
	print_holy_symbol(holySymbol);

	chr.add_xp(10000);
	print_holy_symbol(holySymbol);

	chr.set_skill(defs::character_skill::wisdom, 15);

	holySymbol.select(defs::spells::priest_spell::purify_food_and_drink);
	holySymbol.select(defs::spells::priest_spell::detect_magic);
	holySymbol.select(defs::spells::priest_spell::detect_magic);
	holySymbol.select(defs::spells::priest_spell::slow_poison);
	holySymbol.select(defs::spells::priest_spell::hold_person);
	holySymbol.select(defs::spells::priest_spell::know_alignment);
	holySymbol.select(defs::spells::priest_spell::spiritual_hammer);
	holySymbol.select(defs::spells::priest_spell::prayer);

	print_holy_symbol(holySymbol);

	auto h = holySymbol.pray_all();
	print_holy_symbol(holySymbol);

	holySymbol.cast(defs::spells::priest_spell::cure_light_wounds);
	print_holy_symbol(holySymbol);
}

void test_ageing()
{
	character chr("Anya", defs::character_class::fighter, defs::character_race::human, defs::character_sex::female,
		defs::moral_alignment::chaotic_good, true);

	chr.set_skill(defs::character_skill::strength, 15);
	chr.set_skill(defs::character_skill::dexterity, 15);
	chr.set_skill(defs::character_skill::constitution, 15);
	chr.set_skill(defs::character_skill::intelligence, 15);
	chr.set_skill(defs::character_skill::wisdom, 15);
	chr.set_skill(defs::character_skill::charisma, 15);

	print_character(chr, true, true);

	auto toMiddle = 45 - chr.age();
	chr.grow_old(toMiddle);
	print_character(chr, true, true);

	auto toOld = 60 - chr.age();
	chr.grow_old(toOld);
	print_character(chr, true, true);

	auto toVenerable = 90 - chr.age();
	chr.grow_old(toVenerable);
	print_character(chr, true, true);
}

void test_treasure()
{
	defs::treasure::treasure_class treasureClass = defs::treasure::treasure_class::R;
	auto t = templates::metadata::get_instance().get_treasure_template(treasureClass);
	std::cout << "Treasure class: " << t.acronym() << "\n";

	for (auto tc : t.components())
	{
		auto comp = static_cast<short>(t[tc].component);
		std::cout << "Component: " << comp << ", Count: " << t[tc].countFrom << " - " << t[tc].countTo
			<< " (" << t[tc].probability << "%), Nature: " << t[tc].nature
			<< ", Add. Nature: " << t[tc].additionalNature << " (" << t[tc].additionalCount << ")\n";
	}

	auto tr = treasure::treasure_pool::get_instance().create(treasureClass);
	defs::coin cur = defs::coin::gold;
	auto ct = templates::metadata::get_instance().get_coin_template(cur);

	for (auto c : tr.coins())
	{
		auto coinTempl = templates::metadata::get_instance().get_coin_template(c);
		std::cout << " >> " << coinTempl.get_description() << " (" << coinTempl.get_acronym() << ") = "
			<< tr.coins(c) << "\n";
	}

	for (auto g : tr.gems())
	{
		auto gemTempl = templates::metadata::get_instance().get_gem_template(g.id());
		std::cout << " >> " << gemTempl.get_name() << " (" << gemTempl.get_type_name() << ") = "
			<< g.value() << "\n";
	}

	for (auto o : tr.objects_of_art())
	{
		std::cout << " >> " << o.name() << " = " << o.value() << "\n";
	}

	std::cout << "Total treasure value (" << ct.get_acronym() << ") = " << tr.total_value(cur) << "\n";
}

void test_multiclass_template()
{
	defs::character_class cls = defs::character_class::cleric_ranger;
	auto templ = templates::metadata::get_instance().get_class_template(cls);

	std::cout << "Retrieving template for " << templ.get_long_name() << "\n";
}

void test_missile_specialisation()
{
	std::cout << "Testing missile specialisation bonuses...\n\n\n";

	defs::equipment::item it = defs::equipment::item::heavy_crossbow;
	defs::equipment::item projectileID = defs::equipment::item::heavy_quarrel;
	defs::equipment::item itQuiver = defs::equipment::item::quiver;
	auto *heavyCrossBow = equipment::items_pool::get_instance().create_as<equipment::weapon>(it);
	auto *quiver = equipment::items_pool::get_instance().create_as<equipment::miscellaneous_equipment>(itQuiver);

	auto wTempl = templates::metadata::get_instance().get_weapon_template(it);

	{
		character chr("Eltan", defs::character_class::paladin, defs::character_race::human, defs::character_sex::male, defs::moral_alignment::lawful_good, true);
		print_character(chr, true);

		std::cout << chr.get_name() << " wants to specialise in " << wTempl->description() << "\n";
		chr.add_proficiency(it);
		std::cout << chr.get_name() << " wants to further specialise in " << wTempl->description() << "\n";
		try
		{
			chr.add_proficiency(it);
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << "\n";
		}
	}

	{
		character chr("Khalid", defs::character_class::fighter, defs::character_race::human, defs::character_sex::male, defs::moral_alignment::neutral_good, true);
		print_character(chr, true);

		std::cout << chr.get_name() << " grabs a " << wTempl->description() << "\n";
		chr.grab(defs::hand_slot_type::primary, heavyCrossBow);
		chr.inventory_bag() += quiver;
		for (int i = 0; i < 10; ++i)
		{
			auto p = equipment::items_pool::get_instance().create_as<equipment::projectile>(projectileID);
			chr.inventory_bag().add(p);
		}
		heavyCrossBow->select_projectile(projectileID);


		std::cout << chr.get_name() << " wants to specialise in " << wTempl->description() << "\n";
		chr.add_proficiency(it);

		short tgtArmourClass = 8;
		short tgtDistance = 10;

		std::cout << chr.get_name() << " tries to hit AC " << tgtArmourClass << " from " << tgtDistance << " metres...\n";
		try
		{
			attack_result res = chr.attack(defs::hand_slot_type::primary, tgtArmourClass, tgtDistance);
			check_attack_result(res);
		}
		catch (adnd::exceptions::target_out_of_range& e)
		{
			std::cout << e.what() << "\n";
		}
		catch (std::exception &e)
		{
			cout << e.what() << endl;
		}

		std::cout << chr.get_name() << " wants to further specialise in " << wTempl->description() << "\n";
		chr.add_proficiency(it);

		std::cout << chr.get_name() << " tries to hit AC " << tgtArmourClass << " from " << tgtDistance << " metres...\n";
		try
		{
			attack_result res = chr.attack(defs::hand_slot_type::primary, tgtArmourClass, tgtDistance);
			check_attack_result(res);
		}
		catch (adnd::exceptions::target_out_of_range& e)
		{
			std::cout << e.what() << "\n";
		}
		catch (std::exception &e)
		{
			cout << e.what() << endl;
		}
	}
}

std::string print_split(const defs::character_class& cls)
{
	std::stringstream ss;
	ss << static_cast<uint32_t>(cls) << " [ " << (static_cast<uint32_t>(cls) >> 24) << " - " << (static_cast<uint32_t>(cls) & 0x00FFFFFF) << "]";
	return ss.str();
}

void dump_classes()
{
	std::ofstream f;
	f.open("c:\\temp\\.dump.txt", std::ios::binary);

	f << "Fighter = " << print_split(defs::character_class::fighter) << "\n";
	f << "Paladin = " << print_split(defs::character_class::paladin) << "\n";
	f << "Ranger = " << print_split(defs::character_class::ranger) << "\n";
	f << "Mage = " << print_split(defs::character_class::mage) << "\n";
	f << "Abjurer = " << print_split(defs::character_class::abjurer) << "\n";
	f << "Conjurer = " << print_split(defs::character_class::conjurer) << "\n";
	f << "Diviner = " << print_split(defs::character_class::diviner) << "\n";
	f << "Enchanter = " << print_split(defs::character_class::enchanter) << "\n";
	f << "Illusionist = " << print_split(defs::character_class::illusionist) << "\n";
	f << "Invoker = " << print_split(defs::character_class::invoker) << "\n";
	f << "Necromancer = " << print_split(defs::character_class::necromancer) << "\n";
	f << "Transmuter = " << print_split(defs::character_class::transmuter) << "\n";
	f << "Cleric = " << print_split(defs::character_class::cleric) << "\n";
	f << "Druid = " << print_split(defs::character_class::druid) << "\n";
	f << "Preist of specific mythos = " << print_split(defs::character_class::priest_of_specific_mythos) << "\n";
	f << "Thief = " << print_split(defs::character_class::thief) << "\n";
	f << "Bard = " << print_split(defs::character_class::bard) << "\n";
	f << "Fighter Mage = " << print_split(defs::character_class::fighter_mage) << "\n";
	f << "Fighter Cleric = " << print_split(defs::character_class::fighter_cleric) << "\n";
	f << "Fighter Thief = " << print_split(defs::character_class::fighter_thief) << "\n";
	f << "Mmage Thief = " << print_split(defs::character_class::mage_thief) << "\n";
	f << "Mage Cleric = " << print_split(defs::character_class::mage_cleric) << "\n";
	f << "Cleric Thief = " << print_split(defs::character_class::cleric_thief) << "\n";
	f << "Fighter Mage Cleric = " << print_split(defs::character_class::fighter_mage_cleric) << "\n";
	f << "Fighter Mage Thief = " << print_split(defs::character_class::fighter_mage_thief) << "\n";
	f << "Fighter Druid = " << print_split(defs::character_class::fighter_druid) << "\n";
	f << "Fighter Illusionist = " << print_split(defs::character_class::fighter_illusionist) << "\n";
	f << "Cleric Illusionist = " << print_split(defs::character_class::cleric_illusionist) << "\n";
	f << "Illusionist Thief = " << print_split(defs::character_class::illusionist_thief) << "\n";
	f << "Cleric Ranger = " << print_split(defs::character_class::cleric_ranger) << "\n";
	f << "Mage Druid = " << print_split(defs::character_class::mage_druid) << "\n";
	f << "Fighter Mage Druid = " << print_split(defs::character_class::fighter_mage_druid) << "\n";
	f.close();
}

void print_store(const uint32_t& mktId)
{
	std::cout << "\n\n" << market::market::get_instance()[mktId].name() << " items:\n";
	for (auto s : market::market::get_instance()[mktId])
	{
		auto t = templates::metadata::get_instance().get_item_template(s.first);
		std::cout << t->description() << ": "
			<< market::market::get_instance()[mktId].get_price(s.first, market::store::buy_sell_side::buy)
			<< " - " << market::market::get_instance()[mktId].get_price(s.first, market::store::buy_sell_side::sell)
			<< " (count " << market::market::get_instance()[mktId].count(s.first) << ")\n";
	}
}

void check_attack_result(const attack_result& result)
{
	if (result.hitResult == hit_result::hit)
		std::cout << "Success! " << result.damage << " damage caused\n";
	else if (result.hitResult == hit_result::critical_hit)
		std::cout << "Critical hit! " << result.damage << " damage caused\n";
	else if (result.hitResult == hit_result::critical_failure)
		std::cout << "Critical failure!\n";
	else if (result.hitResult == hit_result::missed)
		std::cout << "Missed!\n";
}

void check_attack_result(const hit_result& result)
{
	if (result == hit_result::hit)
		std::cout << "Success!\n";
	else if (result == hit_result::critical_hit)
		std::cout << "Critical hit!\n";
	else if (result == hit_result::critical_failure)
		std::cout << "Critical failure!\n";
	else if (result == hit_result::missed)
		std::cout << "Missed!\n";
}

void test_attack()
{
	character chr("Jacques de Molay", defs::character_class::fighter, defs::character_race::human, defs::character_sex::male, defs::moral_alignment::lawful_good);
	chr.generate_skills(skills::generation_method::best_of_four);
	chr.set_skill(defs::character_skill::strength, 18, 21);
	print_character(chr, false, true);

	short targetArmourClass = 10;
	std::cout << "\n\n" << chr.get_name() << " tries to hit AC " << targetArmourClass << ".\n";

	try
	{
		short targetDistance = 2;
		std::cout << "Distance = " << targetDistance << "\n";
		hit_result result = chr.try_hit(defs::hand_slot_type::primary, targetArmourClass, targetDistance);
		check_attack_result(result);

		defs::equipment::item w = defs::equipment::item::khopesh;
		auto wt = templates::metadata::get_instance().get_weapon_template(w);
		std::cout << "\nThen grabs a " << wt->description() << "...\n";

		equipment::weapon *weaponItem = equipment::items_pool::get_instance().create_as<equipment::weapon>(w);
		chr.grab(defs::hand_slot_type::primary, weaponItem);

		std::cout << "Distance = " << targetDistance << "\n";
		result = chr.try_hit(defs::hand_slot_type::primary, targetArmourClass, targetDistance);
		check_attack_result(result);

		std::cout << chr.get_name() << " gets proficient with " << wt->description() << "\n";
		chr.add_proficiency(w);

		result = chr.try_hit(defs::hand_slot_type::primary, targetArmourClass, targetDistance);
		check_attack_result(result);

		std::cout << chr.get_name() << " gets specialised with " << wt->description() << "\n";
		chr.add_proficiency(w);

		result = chr.try_hit(defs::hand_slot_type::primary, targetArmourClass, targetDistance);
		check_attack_result(result);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << "\n";
	}
}

void test_market_3()
{
	std::cout << "Testing money bag: subtraction with currency change...\n\n\n";

	{
		money::money_bag bag;
		for (auto cn : { defs::coin::copper, defs::coin::silver, defs::coin::electrum, defs::coin::gold, defs::coin::platinum })
			bag.get(cn) = 0;
		bag.get(defs::coin::electrum) = 1;
		bag.get(defs::coin::gold) = 1;
		bag.get(defs::coin::platinum) = 1;

		print_money_bag(bag);

		money::amount amt(defs::coin::silver, 19);
		std::cout << "Subtracting " << amt << "\n";
		bag.subtract(amt);

		print_money_bag(bag);
	}

	{
		money::money_bag bag;
		for (auto cn : { defs::coin::copper, defs::coin::silver, defs::coin::electrum, defs::coin::gold, defs::coin::platinum })
			bag.get(cn) = 0;
		bag.get(defs::coin::electrum) = 1;
		bag.get(defs::coin::gold) = 1;
		bag.get(defs::coin::platinum) = 1;

		print_money_bag(bag);

		money::amount amt(defs::coin::silver, 14);
		std::cout << "Subtracting " << amt << "\n";
		bag.subtract(amt);

		print_money_bag(bag);

		money::amount amt2(defs::coin::silver, 52);
		std::cout << "Subtracting " << amt2 << "\n";
		try
		{
			bag.subtract(amt2);
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << "\n";
		}

		print_money_bag(bag);
	}
}

void test_market_2()
{
	std::cout << "Testing multi-store market and buy/sell features, unlimited budget...\n\n\n";

	character chr("Gandalf", defs::character_class::mage, defs::character_race::elf, defs::character_sex::male, defs::moral_alignment::neutral_good, true);
	chr.generate_skills(skills::generation_method::standard);

	chr.money().get(defs::coin::gold) = 0;
	chr.money().add(defs::coin::electrum, 10);

	print_character(chr, false, false, false, true);

	market::market::init();

	std::string storeName = "Friendly Arm's Store";
	std::cout << "Creating store: " << storeName << "\n";
	std::cout << "\tBudget model: unlimited (spread = buy/sell)\n";
	uint32_t mktId = market::market::get_instance().create_store(storeName, market::store::budget_model::unlimited, true, true);


	for (auto i : { defs::equipment::item::quarterstaff, defs::equipment::item::knife,
				defs::equipment::item::full_plate, defs::equipment::item::hide,
				defs::equipment::item::plate_mail, defs::equipment::item::chain_mail,
				defs::equipment::item::robe_common, defs::equipment::item::tabard,
				defs::equipment::item::cloak_fine_fur })
	{
		auto t = templates::metadata::get_instance().get_item_template(i);
		std::cout << "Adding " << t->description() << " to " << storeName << ";\n";
		market::market::get_instance()[mktId].add<equipment::item>(i);
	}

	print_store(mktId);

	defs::equipment::item itemID = defs::equipment::item::robe_common;
	auto itemTempl = templates::metadata::get_instance().get_item_template(itemID);
	std::cout << "\n\n" << chr.get_name() << " wants to buy a " << itemTempl->description() << " from " << storeName << "\n";
	auto price = market::market::get_instance()[mktId].get_price(itemID, market::store::buy_sell_side::buy);

	std::cout << "Price = " << price << "\n";
	auto it = market::market::get_instance()[mktId].buy(itemID);
	chr.money().subtract(price);
	chr.inventory_bag() += it;

	print_character(chr, false, false, false, true);

	print_store(mktId);
}

void test_weight_and_encumbrance()
{
	character chr("Philippe Gaston", defs::character_class::thief, defs::character_race::human, defs::character_sex::male, defs::moral_alignment::chaotic_good, true);
	chr.generate_skills(skills::generation_method::standard);
	chr.set_skill(defs::character_skill::strength, 14);
	print_character(chr, false, true);

	std::cout << chr.get_name() << " current encumbrance is " << chr.encumbrance() << "\n";
	std::cout << chr.get_name() << " current movement rate is " << chr.movement_rate() << "\n";

	equipment::armour* hide = equipment::items_pool::get_instance().create_as<equipment::armour>(defs::equipment::item::hide);
	std::cout << chr.get_name() << " wears a " << hide->name() << " (weight = " << hide->weight() << ")\n";
	chr.inventory_bag().wear(defs::body_slot::body, hide);

	std::cout << chr.get_name() << " current encumbrance is " << chr.encumbrance() << "\n";
	std::cout << chr.get_name() << " current movement rate is " << chr.movement_rate() << "\n";

	equipment::weapon* shortSword = equipment::items_pool::get_instance().create_as<equipment::weapon>(defs::equipment::item::short_sword);
	std::cout << chr.get_name() << " grabs a " << shortSword->name() << " (weight = " << shortSword->weight() << ")\n";
	chr.grab(defs::hand_slot_type::primary, shortSword);

	std::cout << chr.get_name() << " current encumbrance is " << chr.encumbrance() << "\n";
	std::cout << chr.get_name() << " current movement rate is " << chr.movement_rate() << "\n";

	for (auto i : { defs::equipment::item::chain_per_ft_light, defs::equipment::item::dagger, defs::equipment::item::halberd,
					defs::equipment::item::heavy_crossbow, defs::equipment::item::mancatcher, defs::equipment::item::water_clock
		})
	{
		equipment::item* it = equipment::items_pool::get_instance().create(i);
		std::cout << chr.get_name() << " gains a " << it->name() << " (weight = " << it->weight() << ")\n";
		chr.inventory_bag() += it;

		std::cout << chr.get_name() << " current encumbrance is " << chr.encumbrance() << "\n";
		std::cout << chr.get_name() << " current movement rate is " << chr.movement_rate() << "\n";
	}
}

void test_market()
{
	std::cout << "Testing multi-store market and buy/sell features, random budget...\n\n\n";

	character chr("Lancelot", defs::character_class::fighter, defs::character_race::human, defs::character_sex::male, defs::moral_alignment::lawful_neutral, true);
	chr.generate_skills(skills::generation_method::standard);

	print_character(chr, false, false, false, true);

	market::market::init();

	std::string storeName = "Winthrop's Store";
	std::cout << "Creating store: " << storeName << "\n";
	std::cout << "\tBudget model: random (spread = buy/sell)\n";
	uint32_t mktId = market::market::get_instance().create_store(storeName, market::store::budget_model::random, true, true);

	for (auto w : { defs::equipment::item::battle_axe, defs::equipment::item::long_sword,
				defs::equipment::item::broad_sword, defs::equipment::item::composite_long_bow,
				defs::equipment::item::morning_star })
	{
		auto wt = templates::metadata::get_instance().get_weapon_template(w);
		std::cout << "Adding " << wt->description() << " to " << storeName << ";\n";
		market::market::get_instance()[mktId].add<equipment::weapon>(w);
	}
	for (auto a : { defs::equipment::item::full_plate,
				defs::equipment::item::hide, defs::equipment::item::plate_mail, defs::equipment::item::chain_mail })
	{
		auto at = templates::metadata::get_instance().get_armour_template(a);
		std::cout << "Adding " << at->description() << " to " << storeName << ";\n";
		market::market::get_instance()[mktId].add<equipment::armour>(a);
	}

	print_store(mktId);

	defs::equipment::item itemID = defs::equipment::item::broad_sword;
	auto itemTempl = templates::metadata::get_instance().get_item_template(itemID);
	std::cout << "\n\n" << chr.get_name() << " wants to buy a " << itemTempl->description() << " from " << storeName << "\n";
	auto price = market::market::get_instance()[mktId].get_price(itemID, market::store::buy_sell_side::buy);

	std::cout << "Price = " << price << "\n";
	auto it = market::market::get_instance()[mktId].buy(itemID);
	chr.money().subtract(price);
	chr.inventory_bag() += it;

	print_character(chr, false, false, false, true);

	print_store(mktId);

	std::cout << "\n\n" << chr.get_name() << " wants to buy another " << itemTempl->description() << " from " << storeName << "\n";
	price = market::market::get_instance()[mktId].get_price(itemID, market::store::buy_sell_side::buy);

	std::cout << "Price = " << price << "\n";
	if (!market::market::get_instance()[mktId].in_stock(itemID))
	{
		std::cout << itemTempl->description() << " is out of stock at " << storeName << "\n";
	}
	else if (!chr.money().check_availability(price))
	{
		std::cout << chr.get_name() << " cannot afford buying a " << itemTempl->description() << " from " << storeName << "\n";
	}
	else
	{
		auto it = market::market::get_instance()[mktId].buy(itemID);
		chr.money().subtract(price);
		chr.inventory_bag() += it;
	}

	print_character(chr, false, false, false, true);

	print_store(mktId);

	itemID = defs::equipment::item::full_plate;
	itemTempl = templates::metadata::get_instance().get_item_template(itemID);
	std::cout << "\n\n" << chr.get_name() << " wants to buy a " << itemTempl->description() << " from " << storeName << "\n";

	price = market::market::get_instance()[mktId].get_price(itemID, market::store::buy_sell_side::buy);
	std::cout << "Price = " << price << "\n";
	it = nullptr;
	try
	{
		it = market::market::get_instance()[mktId].buy(itemID);
		chr.money().subtract(price);
		chr.inventory_bag() += it;
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << "\n";
		market::market::get_instance()[mktId].undo_buy(it);
	}

	print_character(chr, false, false, false, true);

	print_store(mktId);

	itemID = defs::equipment::item::broad_sword;
	itemTempl = templates::metadata::get_instance().get_item_template(itemID);
	std::cout << "\n\n" << chr.get_name() << " sells his " << itemTempl->description() << " to " << storeName << "\n";
	try
	{
		auto price = market::market::get_instance()[mktId].get_price(itemID, market::store::buy_sell_side::sell);
		std::cout << "Price = " << price << "\n";
		it = chr.inventory_bag().find(itemID);
		auto prc = market::market::get_instance()[mktId].sell(it);
		chr.money().add(price);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << "\n";
	}

	print_character(chr, false, false, false, true);

	print_store(mktId);
}

void test_weapons()
{
	defs::character_class cls = defs::character_class::ranger;
	auto templ = templates::metadata::get_instance().get_class_template(cls);
	character chr(templ.get_long_name(), templ.get_class(), defs::character_race::elf, defs::character_sex::male, defs::moral_alignment::chaotic_good, true);
	chr.generate_skills(skills::generation_method::best_of_four);

	auto& pool = equipment::items_pool::get_instance();
	equipment::weapon *elvenLongBow = pool.create_as<equipment::weapon>(defs::equipment::item::long_bow, "Elven long bow");
	cout << "Creating " << elvenLongBow->name() << " (" << elvenLongBow->uid() << "). Pool size is " << pool.size() << endl;

	defs::equipment::item projectileId = defs::equipment::item::sheaf_arrow;
	for (int i = 0; i < 10; ++i)
	{
		auto p = equipment::items_pool::get_instance().create_as<equipment::projectile>(projectileId);
		chr.inventory_bag().add(p);
	}
	elvenLongBow->select_projectile(projectileId);

	equipment::weapon *dwarvenBattleAxe = pool.create_as<equipment::weapon>(defs::equipment::item::battle_axe, "Dwarven battle axe");
	cout << "Creating " << dwarvenBattleAxe->name() << " (" << dwarvenBattleAxe->uid() << "). Pool size is " << pool.size() << endl;
	equipment::weapon *handAxe = pool.create_as<equipment::weapon>(defs::equipment::item::hand_axe);
	cout << "Creating " << handAxe->name() << " (" << handAxe->uid() << "). Pool size is " << pool.size() << endl;

	cout << chr.get_name() << " tries to grab " << elvenLongBow->name() << " with primary hand" << endl;
	chr.grab(defs::hand_slot_type::primary, elvenLongBow);

	cout << chr.get_name() << " tries to grab " << dwarvenBattleAxe->name() << " with primary hand" << endl;
	try
	{
		chr.grab(defs::hand_slot_type::primary, dwarvenBattleAxe);
	}
	catch (std::exception &e)
	{
		cout << e.what() << endl;
	}

	cout << chr.get_name() << " tries to grab " << dwarvenBattleAxe->name() << " with secondary hand" << endl;
	try
	{
		chr.grab(defs::hand_slot_type::secondary, dwarvenBattleAxe);
	}
	catch (std::exception &e)
	{
		cout << e.what() << endl;
	}

	cout << chr.get_name() << " tries to grab " << handAxe->name() << " with secondary hand" << endl;
	try
	{
		chr.grab(defs::hand_slot_type::secondary, handAxe);
	}
	catch (std::exception &e)
	{
		cout << e.what() << endl;
	}

	cout << chr.get_name() << " releases primary hand" << endl;
	try
	{
		equipment::item *it = nullptr;
		chr.release(defs::hand_slot_type::primary, it);

		cout << chr.get_name() << " releases a " << it->name() << ". Pool size is " << pool.size() << endl;

		cout << "Removing " << it->name() << " from pool" << endl;
		pool -= it;
		cout << "Pool size is " << pool.size() << endl;
	}
	catch (std::exception &e)
	{
		cout << e.what() << endl;
	}

	cout << chr.get_name() << " tries to grab " << dwarvenBattleAxe->name() << " with primary hand" << endl;
	try
	{
		chr.grab(defs::hand_slot_type::primary, dwarvenBattleAxe);
	}
	catch (std::exception &e)
	{
		cout << e.what() << endl;
	}

	cout << chr.get_name() << " releases primary hand" << endl;
	try
	{
		equipment::item *it = nullptr;
		chr.release(defs::hand_slot_type::primary, it);

		cout << chr.get_name() << " releases a " << it->name() << ". Pool size is " << pool.size() << endl;

		cout << "Removing " << it->name() << " from pool" << endl;
		pool -= it;
		cout << "Pool size is " << pool.size() << endl;
	}
	catch (std::exception &e)
	{
		cout << e.what() << endl;
	}

	cout << chr.get_name() << " tries to grab " << elvenLongBow->name() << " with primary hand" << endl;
	try
	{
		chr.grab(defs::hand_slot_type::primary, elvenLongBow);
	}
	catch (std::exception &e)
	{
		cout << e.what() << endl;
	}

	for (int i = 0; i < 10; ++i)
	{
		short tgtAC = 5;
		short tgtDistance = 36;
		defs::weapons::target_size tgtSize = defs::weapons::target_size::small_medium;
		std::cout << chr.get_name() << " attacks with " << elvenLongBow->name() << " on primary hand.\n";
		std::cout << "Target: AC = " << tgtAC << ", distance = " << tgtDistance << ", size = medium...";
		try
		{
			auto res = chr.attack(defs::hand_slot_type::primary, tgtAC, tgtDistance, tgtSize);
			if (res.hitResult == hit_result::hit)
				std::cout << " delivering " << res.damage << " wounds" << endl;
			else if (res.hitResult == hit_result::critical_hit)
				std::cout << " delivering " << res.damage << " wounds (critical hit!)" << endl;
			else if (res.hitResult == hit_result::missed)
				std::cout << " missing the target..." << endl;
			else if (res.hitResult == hit_result::critical_failure)
				std::cout << " missing the target with critical failure!" << endl;
		}
		catch (std::exception &e)
		{
			cout << e.what() << endl;
		}
	}

	long xp = 150000;
	cout << chr.get_name() << " gains " << xp << " XP and tries again...\n";
	chr.add_xp(xp);

	for (int i = 0; i < 10; ++i)
	{
		short tgtAC = 5;
		short tgtDistance = 36;
		defs::weapons::target_size tgtSize = defs::weapons::target_size::small_medium;
		std::cout << chr.get_name() << " attacks with " << elvenLongBow->name() << " on primary hand.\n";
		std::cout << "Target: AC = " << tgtAC << ", distance = " << tgtDistance << ", size = medium...";
		try
		{
			auto res = chr.attack(defs::hand_slot_type::primary, tgtAC, tgtDistance, tgtSize);
			if (res.hitResult == hit_result::hit)
				std::cout << " delivering " << res.damage << " wounds" << endl;
			else if (res.hitResult == hit_result::critical_hit)
				std::cout << " delivering " << res.damage << " wounds (critical hit!)" << endl;
			else if (res.hitResult == hit_result::missed)
				std::cout << " missing the target..." << endl;
			else if (res.hitResult == hit_result::critical_failure)
				std::cout << " missing the target with critical failure!" << endl;
		}
		catch (std::exception &e)
		{
			cout << e.what() << endl;
		}
	}
}

void test_turn_undead()
{
	{
		defs::character_class cls = defs::character_class::cleric;
		auto templ = templates::metadata::get_instance().get_class_template(cls);
		character chr("Ileria", cls, defs::character_race::half_elf, defs::character_sex::female,
			defs::moral_alignment::chaotic_good, true);
		short lvl = chr.get_level();
		defs::turn_ability turnable = defs::turn_ability::turn_hd2;

		std::cout << chr.get_name() << ": " << templ.get_short_name() << ", level " << lvl << " tries to turn a 2HD monster..." << "\n";
		auto result = chr.turn_undead(turnable);
		std::cout << "\t result: " << result << "\n";

		long xp = 460000;
		chr.add_xp(xp);
		lvl = chr.get_level();
		std::cout << chr.get_name() << " gains " << xp << " XP, reaching level " << lvl << "\n";

		turnable = defs::turn_ability::turn_ghoul;
		std::cout << chr.get_name() << ": " << templ.get_short_name() << ", level " << lvl << " tries to turn a ghoul..." << "\n";
		result = chr.turn_undead(turnable);
		std::cout << "\t result: " << result << "\n";

		turnable = defs::turn_ability::turn_vampire;
		std::cout << chr.get_name() << ": " << templ.get_short_name() << ", level " << lvl << " tries to turn a vampire..." << "\n";
		result = chr.turn_undead(turnable);
		std::cout << "\t result: " << result << "\n";

		turnable = defs::turn_ability::turn_lich;
		std::cout << chr.get_name() << ": " << templ.get_short_name() << ", level " << lvl << " tries to turn a lich..." << "\n";
		result = chr.turn_undead(turnable);
		std::cout << "\t result: " << result << "\n";
	}

	{
		defs::character_class cls = defs::character_class::fighter;
		auto templ = templates::metadata::get_instance().get_class_template(cls);
		character chr("Calandra", cls, defs::character_race::human, defs::character_sex::female,
			defs::moral_alignment::lawful_neutral, true);
		short lvl = chr.get_level();
		defs::turn_ability turnable = defs::turn_ability::turn_hd2;

		std::cout << chr.get_name() << ": " << templ.get_short_name() << ", level " << lvl << " tries to turn a 2HD monster..." << "\n";
		try
		{
			auto result = chr.turn_undead(turnable);
			std::cout << "\t result: " << result << "\n";
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << "\n";
		}
	}
}

void test_experience_single_class()
{
	defs::character_class cls = defs::character_class::ranger;
	auto templ = templates::metadata::get_instance().get_class_template(cls);
	character chr(templ.get_long_name(), templ.get_class(), defs::character_race::elf, defs::character_sex::male, defs::moral_alignment::chaotic_good, true);

	chr.generate_skills(skills::generation_method::best_of_four);
	print_character(chr, false, true);

	long px = 24000;
	cout << "Gaining experience: " << px << endl;
	chr.add_xp(px);
	print_character(chr, false, true);

	px = 9000;
	cout << "Gaining experience: " << px << endl;
	chr.add_xp(px);
	print_character(chr, false, true);

	short lvl = 1;
	cout << "Gaining level: " << lvl << endl;
	chr.gain_level(lvl);
	print_character(chr, false, true);

	lvl = 2;
	cout << "Losing level: " << lvl << endl;
	chr.drain_level(lvl);
	print_character(chr, false, true);

	lvl = 3;
	cout << "Gaining level: " << lvl << endl;
	chr.gain_level(lvl);
	print_character(chr, false, true);

	px = 30000;
	cout << "Losing experience: " << px << endl;
	chr.add_xp(-px);
	print_character(chr, false, true);

	lvl = 2;
	cout << "Losing level: " << lvl << endl;
	chr.drain_level(lvl);
	print_character(chr, false, true);
}

void test_hit_points_single_class()
{
	hit_points hp(defs::character_class::paladin);

	print_hps(hp);
	short val = 10;
	cout << "Adding " << val << " HPs: " << endl;
	hp.add(defs::character_class::paladin, val);
	print_hps(hp);

	val = 7;
	cout << "Adding " << val << " HPs: " << endl;
	hp.add(defs::character_class::paladin, val);
	print_hps(hp);

	val = 9;
	cout << "Adding " << val << " HPs: " << endl;
	hp.add(defs::character_class::paladin, val);
	print_hps(hp);

	val = 10;
	cout << "Causing " << val << " wounds: " << endl;
	hp.wound(val);
	print_hps(hp);

	val = 7;
	cout << "Curing " << val << " wounds: " << endl;
	hp.heal(val);
	print_hps(hp);

	val = 7;
	cout << "Curing " << val << " wounds: " << endl;
	hp.heal(val);
	print_hps(hp);

	val = 30;
	cout << "Causing " << val << " wounds: " << endl;
	hp.wound(val);
	print_hps(hp);

	try
	{
		val = 7;
		cout << "Curing " << val << " wounds: " << endl;
		hp.heal(val);
		print_hps(hp);
	}
	catch (std::exception &e)
	{
		cout << e.what() << endl;
	}

	cout << "Raising from death: " << endl;
	hp.raise();
	print_hps(hp);

	try
	{
		cout << "Raising from death: " << endl;
		hp.raise();
		print_hps(hp);
	}
	catch (std::exception &e)
	{
		cout << e.what() << endl;
	}

	cout << "Draining two levels: " << endl;
	hp.drain(2);
	print_hps(hp);

	cout << "Draining five levels: " << endl;
	hp.drain(5);
	print_hps(hp);

	try
	{
		cout << "Restore from death: " << endl;
		hp.raise_full();
		print_hps(hp);
	}
	catch (std::exception &e)
	{
		cout << e.what() << endl;
	}

	val = 10;
	cout << "Adding " << val << " HPs: " << endl;
	hp.add(defs::character_class::paladin, val);
	print_hps(hp);

	val = 8;
	cout << "Adding " << val << " HPs: " << endl;
	hp.add(defs::character_class::paladin, val);
	print_hps(hp);

	val = 7;
	cout << "Adding " << val << " HPs: " << endl;
	hp.add(defs::character_class::paladin, val);
	print_hps(hp);

	val = 100;
	cout << "Causing " << val << " wounds: " << endl;
	hp.wound(val);
	print_hps(hp);

	try
	{
		cout << "Restore from death: " << endl;
		hp.raise_full();
		print_hps(hp);
	}
	catch (std::exception &e)
	{
		cout << e.what() << endl;
	}
}

void test_hit_points(short constAdj)
{
	std::cout << "Testing HP sequence for multiclass characters when wounds are caused/cured, death and resurrection conditions...\n\n\n";

	defs::character_class cls = defs::character_class::fighter_mage_cleric;
	auto t = templates::metadata::get_instance().get_class_template(cls);
	std::cout << t.get_long_name() << ":\n\n";
	hit_points hp(cls);

	cout << "Setting HP Adjustment (constitution bonus) = " << constAdj << "\n";
	hp.set_constitution_adjustment(constAdj);

	short val_f = 10;
	short val_m = 4;
	short val_c = 8;
	cout << "Adding " << val_f << ", " << val_m << ", " << val_c << " HPs:\n";
	hp.add(cls, { val_f, val_m, val_c });
	print_hps(hp);

	val_f = 7;
	val_m = 3;
	val_c = 6;
	cout << "Adding " << val_f << ", " << val_m << ", " << val_c << " HPs:\n";
	hp.add(defs::character_class::fighter, val_f);
	hp.add(defs::character_class::mage, val_m);
	hp.add(defs::character_class::cleric, val_c);
	print_hps(hp);

	val_f = 9;
	val_m = 2;
	val_c = 5;
	cout << "Adding " << val_f << ", " << val_m << ", " << val_c << " HPs:\n";
	hp.add(defs::character_class::fighter, val_f);
	hp.add(defs::character_class::mage, val_m);
	hp.add(defs::character_class::cleric, val_c);
	print_hps(hp);

	short val = 10;
	cout << "Causing " << val << " wounds:\n";
	hp.wound(val);
	print_hps(hp);

	val = 7;
	cout << "Curing " << val << " wounds:\n";
	hp.heal(val);
	print_hps(hp);

	val = 7;
	cout << "Curing " << val << " wounds:\n";
	hp.heal(val);
	print_hps(hp);

	val = 30;
	cout << "Causing " << val << " wounds:\n";
	hp.wound(val);
	print_hps(hp);

	try
	{
		val = 7;
		cout << "Curing " << val << " wounds:\n";
		hp.heal(val);
		print_hps(hp);
	}
	catch (std::exception &e)
	{
		cout << e.what() << endl;
	}

	cout << "Raising from death:\n";
	hp.raise();
	print_hps(hp);

	try
	{
		cout << "Raising from death:\n";
		hp.raise();
		print_hps(hp);
	}
	catch (std::exception &e)
	{
		cout << e.what() << endl;
	}

	short levelDrain = 2;
	cout << "Draining " << levelDrain << " levels:\n";
	hp.drain(levelDrain);
	print_hps(hp);

	levelDrain = 5;
	cout << "Draining " << levelDrain << " levels:\n";
	hp.drain(levelDrain);
	print_hps(hp);

	try
	{
		cout << "Restore from death:\n";
		hp.raise_full();
		print_hps(hp);
	}
	catch (std::exception &e)
	{
		cout << e.what() << "\n";
	}

	val_f = 10;
	val_m = 4;
	val_c = 8;
	cout << "Adding " << val_f << ", " << val_m << ", " << val_c << " HPs:\n";
	hp.add(cls, { val_f, val_m, val_c });
	print_hps(hp);

	val_f = 6;
	val_m = 3;
	val_c = 6;
	cout << "Adding " << val_f << ", " << val_m << ", " << val_c << " HPs:\n";
	hp.add(defs::character_class::fighter, val_f);
	hp.add(defs::character_class::mage, val_m);
	hp.add(defs::character_class::cleric, val_c);
	print_hps(hp);

	val_f = 7;
	val_m = 2;
	val_c = 5;
	cout << "Adding " << val_f << ", " << val_m << ", " << val_c << " HPs:\n";
	hp.add(defs::character_class::fighter, val_f);
	hp.add(defs::character_class::mage, val_m);
	hp.add(defs::character_class::cleric, val_c);
	print_hps(hp);

	val = 100;
	cout << "Causing " << val << " wounds:\n";
	hp.wound(val);
	print_hps(hp);

	try
	{
		cout << "Restore from death:\n";
		hp.raise_full();
		print_hps(hp);
	}
	catch (std::exception &e)
	{
		cout << e.what() << endl;
	}
}

void test_hp_sequence()
{
	std::cout << "Testing HP sequence when wounds are caused/cured, death and resurrection conditions...\n\n\n";

	hp_sequence hp;

	print_hp(hp);
	short val = 10;
	cout << "Adding " << val << " HPs: " << endl;
	hp += val;
	print_hp(hp);

	val = 5;
	cout << "Adding " << val << " HPs: " << endl;
	hp += val;
	print_hp(hp);

	val = 5;
	cout << "Adding " << val << " HPs: " << endl;
	hp += val;
	print_hp(hp);

	val = 7;
	cout << "Causing " << val << " wounds: " << endl;
	hp.wound(val);
	print_hp(hp);

	val = 7;
	cout << "Causing " << val << " wounds: " << endl;
	hp.wound(val);
	print_hp(hp);

	val = 10;
	cout << "Curing " << val << " wounds: " << endl;
	hp.heal(val);
	print_hp(hp);

	val = 10;
	cout << "Curing " << val << " wounds: " << endl;
	hp.heal(val);
	print_hp(hp);

	cout << "Draining one levels: " << endl;
	hp.drain();
	print_hp(hp);

	val = 20;
	cout << "Causing " << val << " wounds: " << endl;
	hp.wound(val);
	print_hp(hp);

	val = 10;
	try
	{
		cout << "Curing " << val << " wounds: " << endl;
		hp.heal(val);
		print_hp(hp);
	}
	catch (std::exception &e)
	{
		cout << e.what() << endl;
	}

	cout << "Raising from death: " << endl;
	hp.raise();
	print_hp(hp);

	try
	{
		cout << "Raising from death: " << endl;
		hp.raise();
		print_hp(hp);
	}
	catch (std::exception &e)
	{
		cout << e.what() << endl;
	}

	cout << "Draining two levels: " << endl;
	hp.drain(2);
	print_hp(hp);

	try
	{
		cout << "Restore from death: " << endl;
		hp.raise_full();
		print_hp(hp);
	}
	catch (std::exception &e)
	{
		cout << e.what() << endl;
	}

	val = 10;
	cout << "Adding " << val << " HPs: " << endl;
	hp += val;
	print_hp(hp);

	val = 10;
	cout << "Adding " << val << " HPs: " << endl;
	hp += val;
	print_hp(hp);

	val = 10;
	cout << "Adding " << val << " HPs: " << endl;
	hp += val;
	print_hp(hp);

	val = 31;
	cout << "Causing " << val << " wounds: " << endl;
	hp.wound(val);
	print_hp(hp);

	try
	{
		cout << "Restore from death: " << endl;
		hp.raise_full();
		print_hp(hp);
	}
	catch (std::exception &e)
	{
		cout << e.what() << endl;
	}
}

void test_experience()
{
	std::cout << "Testing multiclass character gaining/losing level(s)...\n\n\n";

	defs::character_class cls = defs::character_class::fighter_mage_thief;
	character chr("Elu Thingol", cls, defs::character_race::elf, defs::character_sex::male, defs::moral_alignment::lawful_neutral, true);
	chr.generate_skills(skills::generation_method::best_of_four);
	print_character(chr, false, false, true);

	long px = 24000;
	cout << "Gaining experience: " << px << endl;
	chr.add_xp(px);
	print_character(chr, false, false, true);

	px = 9000;
	cout << "Gaining experience: " << px << endl;
	chr.add_xp(px);
	print_character(chr, false, false, true);

	short lvl = 1;
	cout << "Gaining level: " << lvl << endl;
	chr.gain_level(lvl);
	print_character(chr, false, false, true);

	lvl = 2;
	cout << "Losing level: " << lvl << endl;
	chr.drain_level(lvl);
	print_character(chr, false, false, true);

	lvl = 3;
	cout << "Gaining level: " << lvl << endl;
	chr.gain_level(lvl);
	print_character(chr, false, false, true);

	px = 30000;
	cout << "Losing experience: " << px << endl;
	chr.add_xp(-px);
	print_character(chr, false, false, true);

	lvl = 2;
	cout << "Losing level: " << lvl << endl;
	chr.drain_level(lvl);
	print_character(chr, false, false, true);
}

void test_multiclass()
{
	std::cout << "Testing multiclass character experience progression...\n\n\n";

	defs::character_class cls = defs::character_class::fighter_mage;
	character chr("Fingolfin", cls, defs::character_race::elf, defs::character_sex::male,
		defs::moral_alignment::chaotic_neutral, true);

	std::cout << chr.get_name() << " level is " << chr.level() << " px[" << chr.get_xp(defs::character_class::fighter) << ", " << chr.get_xp(defs::character_class::mage) << "]" << endl;

	long px = 9000;
	cout << "Adding " << px << " px" << endl;
	chr.add_xp(px);
	std::cout << chr.get_name() << " level is " << chr.level() << " px[" << chr.get_xp(defs::character_class::fighter) << ", " << chr.get_xp(defs::character_class::mage) << "]" << endl;

	px = 3000;
	cout << "Adding " << px << " px" << endl;
	chr.add_xp(px);
	std::cout << chr.get_name() << " level is " << chr.level() << " px[" << chr.get_xp(defs::character_class::fighter) << ", " << chr.get_xp(defs::character_class::mage) << "]" << endl;

	px = 12000;
	cout << "Adding " << px << " px" << endl;
	chr.add_xp(px);
	std::cout << chr.get_name() << " level is " << chr.level() << " px[" << chr.get_xp(defs::character_class::fighter) << ", " << chr.get_xp(defs::character_class::mage) << "]" << endl;

	px = 7000;
	cout << "Subtracting " << px << " px" << endl;
	chr.add_xp(-px);
	std::cout << chr.get_name() << " level is " << chr.level() << " px[" << chr.get_xp(defs::character_class::fighter) << ", " << chr.get_xp(defs::character_class::mage) << "]" << endl;

	px = 70000;
	cout << "Subtracting " << px << " px" << endl;
	try
	{
		chr.add_xp(-px);
		std::cout << chr.get_name() << " level is " << chr.level() << " px[" << chr.get_xp(defs::character_class::fighter) << ", " << chr.get_xp(defs::character_class::mage) << "]" << endl;
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << "\n";
	}
}

//void test_xp()
//{
//	std::cout << "Testing character class experience progression...\n\n\n";
//	{
//		defs::character_class cls = defs::character_class::paladin;
//		auto templ = templates::metadata::get_instance().get_class_template(cls);
//
//		std::cout << "Testing " << templ.get_long_name() << ":\n";
//
//		long px = 135675;
//		short level = templ.get_level(px, cls);
//		cout << " >> " << templ.get_long_name() << ": px = " << px << " -> level = " << level << endl;
//
//		px = 2700000;
//		level = templ.get_level(px, cls);
//		cout << " >> " << templ.get_long_name() << ": px = " << px << " -> level = " << level << endl;
//
//		px = 2100;
//		level = templ.get_level(px, cls);
//		cout << " >> " << templ.get_long_name() << ": px = " << px << " -> level = " << level << endl;
//	}
//
//	{
//		defs::character_class cls = defs::character_class::druid;
//		auto templ = templates::metadata::get_instance().get_class_template(cls);
//
//		std::cout << "Testing " << templ.get_long_name() << ":\n";
//
//		long px = 2700000;
//		short level = templ.get_level(px, cls);
//		cout << " >> " << templ.get_long_name() << ": px = " << px << " -> level = " << level << endl;
//
//		px = 700000;
//		level = templ.get_level(px, cls);
//		cout << " >> " << templ.get_long_name() << ": px = " << px << " -> level = " << level << endl;
//	}
//
//	{
//		defs::character_class cls = defs::character_class::thief;
//		auto templ = templates::metadata::get_instance().get_class_template(cls);
//
//		std::cout << "Testing " << templ.get_long_name() << ":\n";
//
//		long px = 1200000;
//		short level = templ.get_level(px, cls);
//		cout << " >> " << templ.get_long_name() << ": px = " << px << " -> level = " << level << endl;
//	}
//
//	{
//		defs::character_class cls = defs::character_class::fighter_illusionist;
//		auto templ = templates::metadata::get_instance().get_class_template(cls);
//
//		std::cout << "Testing " << templ.get_long_name() << ":\n";
//
//		long px = 55345;
//		auto levels = templ.get_levels(px);
//		for (auto l : levels)
//		{
//			auto t = templates::metadata::get_instance().get_class_template(l.first);
//			cout << " >> " << t.get_long_name() << ": px = " << px/levels.size() << " -> level = " << l.second << endl;
//		}
//	}
//
//	{
//		defs::character_class cls = defs::character_class::fighter_mage_thief;
//		auto templ = templates::metadata::get_instance().get_class_template(cls);
//
//		std::cout << "Testing " << templ.get_long_name() << ":\n";
//
//		long px = 5500;
//		auto levels = templ.get_levels(px);
//		for (auto l : levels)
//		{
//			auto t = templates::metadata::get_instance().get_class_template(l.first);
//			cout << " >> " << t.get_long_name() << ": px = " << px / levels.size() << " -> level = " << l.second << endl;
//		}
//	}
//
//	{
//		defs::character_class cls = defs::character_class::fighter_mage_druid;
//		auto templ = templates::metadata::get_instance().get_class_template(cls);
//
//		std::cout << "Testing " << templ.get_long_name() << ":\n";
//
//		long px = 257033;
//		auto levels = templ.get_levels(px);
//		for (auto l : levels)
//		{
//			auto t = templates::metadata::get_instance().get_class_template(l.first);
//			cout << " >> " << t.get_long_name() << ": px = " << px / levels.size() << " -> level = " << l.second << endl;
//		}
//	}
//}

void test_xp()
{
	std::cout << "Testing character class experience progression...\n\n\n";
	{
		defs::character_class cls = defs::character_class::paladin;
		auto templ = templates::metadata::get_instance().get_class_template(cls);
		character chr("Behoram", cls, defs::character_race::human, defs::character_sex::male,
			defs::moral_alignment::lawful_good, true);

		std::cout << "Testing " << chr.get_name() << " (" << templ.get_long_name() << "):\n";

		long px = 135675;
		std::cout << "Adding " << px << " XP (new total is " << chr.get_xp() + px << ")\n";
		chr.add_xp(px);
		short level = chr.get_level();
		cout << " >> " << chr.get_name() << ": px = " << px << " -> level = " << level << endl;

		px = 2564325;
		std::cout << "Adding " << px << " XP (new total is " << chr.get_xp() + px << ")\n";
		chr.add_xp(px);
		level = chr.get_level();
		cout << " >> " << templ.get_long_name() << ": px = " << px << " -> level = " << level << endl;

		px = -2697900;
		std::cout << "subtracting " << std::abs(px) << " XP (new total is " << chr.get_xp() + px << ")\n";
		chr.add_xp(px);
		level = chr.get_level();
		cout << " >> " << templ.get_long_name() << ": px = " << px << " -> level = " << level << endl;
	}

	{
		defs::character_class cls = defs::character_class::druid;
		auto templ = templates::metadata::get_instance().get_class_template(cls);
		character chr("Panoramix", cls, defs::character_race::human, defs::character_sex::male,
			defs::moral_alignment::true_neutral, true);

		std::cout << "\nTesting " << templ.get_long_name() << ":\n";

		long px = 2700000;
		std::cout << "Adding " << px << " XP (new total is " << chr.get_xp() + px << ")\n";
		chr.add_xp(px);
		short level = chr.get_level();
		cout << " >> " << templ.get_long_name() << ": px = " << px << " -> level = " << level << endl;

		px = -2000000;
		std::cout << "Adding " << px << " XP (new total is " << chr.get_xp() + px << ")\n";
		chr.add_xp(px);
		level = chr.get_level();
		cout << " >> " << templ.get_long_name() << ": px = " << px << " -> level = " << level << endl;
	}

	{
		defs::character_class cls = defs::character_class::thief;
		auto templ = templates::metadata::get_instance().get_class_template(cls);
		character chr("Imoen", cls, defs::character_race::human, defs::character_sex::female,
			defs::moral_alignment::chaotic_neutral, true);

		std::cout << "\nTesting " << templ.get_long_name() << ":\n";

		long px = 1200000;
		std::cout << "Adding " << px << " XP (new total is " << chr.get_xp() + px << ")\n";
		chr.add_xp(px);
		short level = chr.get_level();
		cout << " >> " << templ.get_long_name() << ": px = " << px << " -> level = " << level << endl;
	}

	{
		defs::character_class cls = defs::character_class::fighter_illusionist;
		auto templ = templates::metadata::get_instance().get_class_template(cls);
		character chr("Amber", cls, defs::character_race::gnome, defs::character_sex::female,
			defs::moral_alignment::lawful_neutral, true);

		std::cout << "\nTesting " << templ.get_long_name() << ":\n";

		long px = 55345;
		std::cout << "Adding " << px << " XP (new total is " << chr.get_xp() + px << ")\n";
		chr.add_xp(px);
		auto levels = chr.get_levels();
		for (auto l : levels)
		{
			auto t = templates::metadata::get_instance().get_class_template(l.first);
			cout << " >> " << t.get_long_name() << ": px = " << px / levels.size() << " -> level = " << l.second << endl;
		}
	}

	{
		defs::character_class cls = defs::character_class::fighter_mage_thief;
		auto templ = templates::metadata::get_instance().get_class_template(cls);
		character chr("Tyrra", cls, defs::character_race::half_elf, defs::character_sex::female,
			defs::moral_alignment::lawful_neutral, true);

		std::cout << "\nTesting " << templ.get_long_name() << ":\n";

		long px = 5500;
		std::cout << "Adding " << px << " XP (new total is " << chr.get_xp() + px << ")\n";
		chr.add_xp(px);
		auto levels = chr.get_levels();
		for (auto l : levels)
		{
			auto t = templates::metadata::get_instance().get_class_template(l.first);
			cout << " >> " << t.get_long_name() << ": px = " << px / levels.size() << " -> level = " << l.second << endl;
		}
	}

	{
		defs::character_class cls = defs::character_class::fighter_mage_druid;
		auto templ = templates::metadata::get_instance().get_class_template(cls);
		character chr("Delmair", cls, defs::character_race::half_elf, defs::character_sex::male,
			defs::moral_alignment::lawful_neutral, true);

		std::cout << "\nTesting " << templ.get_long_name() << ":\n";

		long px = 257033;
		std::cout << "Adding " << px << " XP (new total is " << chr.get_xp() + px << ")\n";
		chr.add_xp(px);
		auto levels = chr.get_levels();
		for (auto l : levels)
		{
			auto t = templates::metadata::get_instance().get_class_template(l.first);
			cout << " >> " << t.get_long_name() << ": px = " << px / levels.size() << " -> level = " << l.second << endl;
		}
	}
}

void test_class()
{
	std::cout << "Testing character/skills generation...\n\n\n";

	{
		defs::character_class cls = defs::character_class::paladin;
		auto templ = templates::metadata::get_instance().get_class_template(cls);

		std::cout << "Creating a " << templ.get_long_name() << " and setting custom skill values\n";

		character chr("Eomer", cls, defs::character_race::human, defs::character_sex::male, defs::moral_alignment::lawful_good);
		skills::character_skill_value str(defs::character_skill::strength, 18);
		str.set_exceptional(85);
		skills::character_skill_value dex(defs::character_skill::dexterity);
		dex = 13;
		skills::character_skill_value con(defs::character_skill::constitution, 16);
		skills::character_skill_value inl(defs::character_skill::intelligence, 12);
		skills::character_skill_value wis(defs::character_skill::wisdom, 13);
		skills::character_skill_value cha(defs::character_skill::charisma, 17);

		chr.set_skills(str, dex, con, inl, wis, cha);
		print_character(chr, true, true);
	}

	{
		defs::character_class cls = defs::character_class::mage;
		auto templ = templates::metadata::get_instance().get_class_template(cls);

		std::cout << "Creating a " << templ.get_long_name() << " and generating skill values\n";

		character chr("Elrond", cls, defs::character_race::elf, defs::character_sex::male, defs::moral_alignment::neutral_evil);
		chr.generate_skills();
		print_character(chr, true, true);
	}

	{
		defs::character_class cls = defs::character_class::paladin;
		std::string name = "Elven Paladin";

		std::cout << "Creating a " << name << " and handling errors\n";

		try
		{
			character elvenPaladin(name, cls, defs::character_race::elf, defs::character_sex::male, defs::moral_alignment::lawful_good);
			print_character(elvenPaladin, true, true);
		}
		catch (exceptions::class_not_allowed_for_race& e)
		{
			cout << e.what() << "\n";
		}
	}

	{
		defs::character_class cls = defs::character_class::paladin;
		std::string name = "Evil Paladin";

		std::cout << "Creating a " << name << " and handling errors\n";

		try
		{
			character evilPaladin(name, defs::character_class::paladin, defs::character_race::human, defs::character_sex::male, defs::moral_alignment::chaotic_evil);
			print_character(evilPaladin, true, true);
		}
		catch (std::exception& e)
		{
			cout << e.what() << "\n";
		}
	}

	cout << "Testing generation with standard method" << endl;
	character thagor("Thagor", defs::character_class::fighter_cleric, defs::character_race::dwarf, defs::character_sex::male, defs::moral_alignment::chaotic_good);
	thagor.generate_skills();
	print_character(thagor, true, true);

	cout << "Testing generation with best_of_four method" << endl;
	character tonglor("Tonglor", defs::character_class::fighter, defs::character_race::dwarf, defs::character_sex::male, defs::moral_alignment::chaotic_good);
	tonglor.generate_skills(skills::generation_method::best_of_four);
	print_character(tonglor, true, true);

	cout << "Testing generation with best_of_each method" << endl;
	character keirghar("Keirgar", defs::character_class::fighter_thief, defs::character_race::dwarf, defs::character_sex::male, defs::moral_alignment::chaotic_good, true);
	keirghar.generate_skills(skills::generation_method::best_of_each);
	print_character(keirghar, true, true);
}

void test_class_2()
{
	character chr("Tonglor", defs::character_class::fighter_thief, defs::character_race::dwarf, defs::character_sex::male, defs::moral_alignment::chaotic_neutral, true);
	chr.generate_skills(skills::generation_method::best_of_four);
	chr.set_skill(defs::character_skill::constitution, 17);
	print_character(chr, true, true, true);

	cout << "Gaining one level..." << endl;
	chr.gain_level(1);
	print_character(chr, true, true, true);

	short lvlUp = 2;
	cout << "Gaining " << lvlUp << " levels as a thief" << endl;
	chr.gain_level(defs::character_class::thief, lvlUp);
	print_character(chr, true, true, true);
}

void test_money_bag()
{
	std::cout << "Testing money management...\n\n\n";

	character chr("Boromir", defs::character_class::fighter, defs::character_race::human, defs::character_sex::male, defs::moral_alignment::lawful_neutral, true);
	chr.generate_skills(skills::generation_method::best_of_four);

	print_character(chr, false, false, false, true);

	uint32_t amt = 105;
	std::cout << chr.get_name() << " gains " << amt << " EP\n";
	chr.money().add(defs::coin::electrum, amt);

	print_character(chr, false, false, false, true);

	std::cout << chr.get_name() << " empties his money bag\n";
	for (auto cn : { defs::coin::copper, defs::coin::silver, defs::coin::electrum, defs::coin::gold, defs::coin::platinum })
		chr.money().get(cn) = 0;

	amt = 46;
	std::cout << chr.get_name() << " gains " << amt << " SP\n";
	chr.money().get(defs::coin::silver) = amt;

	amt = 19;
	std::cout << chr.get_name() << " gains " << amt << " EP\n";
	chr.money().get(defs::coin::electrum) = amt;

	print_character(chr, false, false, false, true);

	std::cout << chr.get_name() << " money bag is normalised\n";
	chr.money().normalise();

	print_character(chr, false, false, false, true);
}

void test_class_creation()
{
	std::cout << "Testing full automatic character generation...\n\n\n";

	skills::skill_generator sklGen(skills::generation_method::best_of_four);
	skills::character_skill_set skills;

	sklGen.generate(skills);

	cout << "Generated skills:\n";
	cout << "	Strength: " << skills.strength() << "\n";
	cout << "	Dexterity: " << skills.dexterity() << "\n";
	cout << "	Constitution: " << skills.constitution() << "\n";
	cout << "	Intelligence: " << skills.intelligence() << "\n";
	cout << "	Wisdom: " << skills.wisdom() << "\n";
	cout << "	Charisma: " << skills.charisma() << "\n";

	std::cout << "Available races:\n";
	auto races = templates::metadata::get_instance().get_available_races(skills);
	for (auto r : races)
	{
		auto rt = templates::metadata::get_instance().get_race_template(r);
		std::cout << "\t" << rt.race_name() << ";\n";
	}
	if (races.size() == 0)
	{
		std::cout << "No races available...\n";
		return;
	}

	std::cout << "Selecting race...\n";
	defs::character_race chosenRace;
	{
		std::random_device rd;
		std::mt19937 generator(rd());
		std::uniform_int_distribution<int> distribution(0, static_cast<int>(races.size()) - 1);

		auto choice = distribution(generator);
		auto it = races.begin();
		for (int i = 0; i < choice; ++i, ++it);

		chosenRace = *it;
		auto rt = templates::metadata::get_instance().get_race_template(chosenRace);
		std::cout << "Selected race: " << rt.race_name() << "\n";
	}

	std::cout << "Available classes:\n";
	auto raceTempl = templates::metadata::get_instance().get_race_template(chosenRace);
	auto classes = raceTempl.get_available_classes(skills);
	for (auto c : classes)
	{
		auto ct = templates::metadata::get_instance().get_class_template(c);
		std::cout << "\t" << ct.get_long_name() << ";\n";
	}
	if (classes.size() == 0)
	{
		std::cout << "No classes available...\n";
		return;
	}

	std::cout << "Selecting class...\n";
	defs::character_class chosenClass;
	{
		std::random_device rd;
		std::mt19937 generator(rd());
		std::uniform_int_distribution<int> distribution(0, static_cast<int>(classes.size()) - 1);

		auto choice = distribution(generator);
		auto it = classes.begin();
		for (int i = 0; i < choice; ++i, ++it);

		chosenClass = *it;
		auto ct = templates::metadata::get_instance().get_class_template(chosenClass);
		std::cout << "Selected race: " << ct.get_long_name() << "\n";
	}

	std::cout << "Available moral alignments:\n";
	auto classTempl = templates::metadata::get_instance().get_class_template(chosenClass);
	auto aligns = classTempl.get_moral_alignments();
	for (auto a : aligns)
	{
		auto mat = templates::metadata::get_instance().get_moral_alignment_template(a);
		std::cout << "\t" << mat.get_name() << " (" << mat.get_acronym() << ");\n";
	}
	if (aligns.size() == 0)
	{
		std::cout << "No moral alignments available...\n";
		return;
	}

	std::cout << "Selecting moral alignment...\n";
	defs::moral_alignment chosenAlign;
	{
		std::random_device rd;
		std::mt19937 generator(rd());
		std::uniform_int_distribution<int> distribution(0, static_cast<int>(aligns.size()) - 1);

		auto choice = distribution(generator);
		auto it = aligns.begin();
		for (int i = 0; i < choice; ++i, ++it);

		chosenAlign = *it;
		auto mat = templates::metadata::get_instance().get_moral_alignment_template(chosenAlign);
		std::cout << "Selected race: " << mat.get_name() << "\n";
	}

	std::cout << "Selecting sex...\n";
	std::string charName;
	defs::character_sex chosenSex;
	{
		std::random_device rd;
		std::mt19937 generator(rd());
		std::uniform_int_distribution<int> distribution(0, 1);

		auto choice = distribution(generator);
		if (choice % 2 == 0)
		{
			chosenSex = defs::character_sex::male;
			std::cout << "Selected sex: male;\n";
			charName = "Sigmar";
		}
		else
		{
			chosenSex = defs::character_sex::female;
			std::cout << "Selected sex: female;\n";
			charName = "Eowin";
		}
	}

	std::cout << "\n\nGenerating character: " << charName << "...\n";

	character chr(charName, chosenClass, chosenRace, chosenSex, chosenAlign, true);
	chr.set_skills(skills.strength(), skills.dexterity(), skills.constitution(), skills.intelligence(), skills.wisdom(), skills.charisma());

	print_character(chr, true, true, true, true);
}

void test_skill()
{
	std::cout << "Testing skill creation, modify and comparison...\n\n\n";

	defs::character_skill skl = defs::character_skill::strength;
	auto t = templates::metadata::get_instance().get_skill_template(skl);
	skills::character_skill_value val(skl);
	val = 18;

	std::cout << "Creating skill [" << t.get_name() << "] = " << val << "\n";

	uint8_t excVal = 60;
	std::cout << "Setting exceptional strength = " << static_cast<short>(excVal) << "\n";
	val.set_exceptional(excVal);

	std::cout << t.get_name() << " = " << val << "\n";

	/*int8_t modifier = -3;
	std::cout << "Setting skill modifier = " << static_cast<short>(modifier) << "\n";
	skills::character_skill_value::skill_modifier mod = [&](const skills::character_skill_value& val) -> int8_t
	{
		return modifier;
	};
	val.set_modifier(mod);*/

	cout << "Skill value: " << val << endl;

	//modifier = -2;
	//std::cout << "Setting skill modifier = " << static_cast<short>(modifier) << "\n";
	//cout << "Skill value: " << val << endl;
	//
	//modifier = 2;
	//std::cout << "Setting skill modifier = " << static_cast<short>(modifier) << "\n";
	//cout << "Skill value: " << val << endl;
	//
	//modifier = 1;
	//std::cout << "Setting skill modifier = " << static_cast<short>(modifier) << "\n";
	//cout << "Skill value: " << val << endl;

	uint8_t testValue = 19;
	std::cout << "Testing equals to " << static_cast<short>(testValue) << ": ";
	if (val == skills::character_skill_value(defs::character_skill::strength, testValue))
		cout << "Yes\n";
	else
		cout << "No\n";

	//modifier = 0;
	//std::cout << "Setting skill modifier = " << static_cast<short>(modifier) << "\n";
	//std::cout << "Skill value: " << val << endl;

	{
		skills::character_skill_value val2(defs::character_skill::strength, 18, 93);
		std::cout << "Testing against skill value " << val2 << "\n";

		if (val < val2) cout << val << " < " << val2 << endl;
		if (val <= val2) cout << val << " <= " << val2 << endl;
		if (val > val2) cout << val << " > " << val2 << endl;
		if (val >= val2) cout << val << " >= " << val2 << endl;
		if (val == val2) cout << val << " == " << val2 << endl;
		if (val != val2) cout << val << " != " << val2 << endl;
	}

	{
		skills::character_skill_value val2(defs::character_skill::strength, 18, 33);
		std::cout << "Testing against skill value " << val2 << "\n";

		if (val < val2) cout << val << " < " << val2 << endl;
		if (val <= val2) cout << val << " <= " << val2 << endl;
		if (val > val2) cout << val << " > " << val2 << endl;
		if (val >= val2) cout << val << " >= " << val2 << endl;
		if (val == val2) cout << val << " == " << val2 << endl;
		if (val != val2) cout << val << " != " << val2 << endl;
	}

	{
		skills::character_skill_value val2(defs::character_skill::strength);
		val2 = val.value();
		val2.set_exceptional(val.exceptional());
		std::cout << "Testing against skill value " << val2 << "\n";

		if (val < val2) cout << val << " < " << val2 << endl;
		if (val <= val2) cout << val << " <= " << val2 << endl;
		if (val > val2) cout << val << " > " << val2 << endl;
		if (val >= val2) cout << val << " >= " << val2 << endl;
		if (val == val2) cout << val << " == " << val2 << endl;
		if (val != val2) cout << val << " != " << val2 << endl;
	}
}

void test_bonus_xp()
{
	std::cout << "Testing bonus experience for primary skill...\n\n\n";

	character chr("Boromir", defs::character_class::fighter, defs::character_race::human, defs::character_sex::male, defs::moral_alignment::lawful_neutral, true);
	chr.generate_skills(skills::generation_method::standard);
	skills::character_skill_value str(defs::character_skill::strength, 18, 60);
	chr.set_skill(defs::character_skill::strength, str);

	print_character(chr, false, false, true);

	long xp = 1900;
	std::cout << "Adding " << xp << " XP\n";
	chr.add_xp(xp);

	print_character(chr, false, false, true);

	uint8_t newStrengthVal = 15;
	std::cout << "Strength changed to " << static_cast<short>(newStrengthVal) << "\n";
	skills::character_skill_value newStrength(defs::character_skill::strength, newStrengthVal);
	chr.set_skill(defs::character_skill::strength, newStrength);

	print_character(chr, false, false, true);

	xp = 1900;
	std::cout << "Adding " << xp << " XP\n";
	chr.add_xp(xp);

	print_character(chr, false, false, true);
}

void print_character(character& c, bool printStats, bool printSkills, bool printExperience, bool printMoney)
{
	auto templ = templates::metadata::get_instance().get_class_template(c.get_class());
	auto raceTempl = templates::metadata::get_instance().get_race_template(c.get_race());
	auto constTempl = templates::metadata::get_instance().get_skill_template(defs::character_skill::constitution);
	auto stats = constTempl.get_constitution_stats(c.constitution_value());

	std::cout << "--- " << c.get_name() << " (" << raceTempl.race_name() << ")---\n";
	std::cout << "Level [" << templ.get_short_name() << "]: " << c.level() << endl;

	if (printStats)
	{
		auto ac = c.armour_class();
		auto thaco = c.thaco();
		auto age = c.age();
		auto exp = c.life_expectancy();
		std::cout << "AC: " << ac << ", THAC0: " << thaco << ";\n";
		std::cout << "Age: " << age << " (";
		auto ageDesc = c.ageing();
		if (ageDesc == defs::character_age::young)
			std::cout << "young";
		else if (ageDesc == defs::character_age::middle_age)
			std::cout << "middle_age";
		else if (ageDesc == defs::character_age::old)
			std::cout << "old";
		else if (ageDesc == defs::character_age::venerable)
			std::cout << "venerable";
		std::cout << "), expectancy: " << exp << ";\n";

		auto wgt = c.weight();
		auto hgt = c.height();
		std::cout << "Weight: " << wgt << " Kg, height: " << hgt << " cm;\n";

		auto dice = templ.hit_dice();
		for (auto d : dice)
		{
			auto t = templates::metadata::get_instance().get_class_template(d.first);
			std::cout << "\t" << t.get_long_name() << " HD(" << d.second << ")" << endl;
		}
		std::cout << "HP: " << c.get_current_hp() << " (max: " << c.max_hp() << "), constitution adjustment: " << stats.hitPointAdjustmentWarriors << "\n";
	}

	if (printSkills)
	{
		short strength, dexterity, constitution, intelligence, wisdom, charisma, excStrength;
		c.get_skill_values(strength, dexterity, constitution, intelligence, wisdom, charisma, excStrength);
		cout << "Character \'" << c.get_name() << "\' has skills (total " << c.points() << "):" << endl;
		cout << "  strength = " << strength;
		if (excStrength > 0)
			cout << "/" << excStrength;
		cout << endl;
		cout << "  dexterity = " << dexterity << endl;
		cout << "  constitution = " << constitution << endl;
		cout << "  intelligence = " << intelligence << endl;
		cout << "  wisdom = " << wisdom << endl;
		cout << "  charisma = " << charisma << endl;
	}

	if (printExperience)
	{
		std::cout << "Experience:\n";
		for (auto& l : c.level())
		{
			auto t = templates::metadata::get_instance().get_class_template(l.first);
			cout << "\t" << t.get_short_name() << " = " << l.second.level() << " (" << l.second.get_xp() << ")" << endl;
		}
	}

	if (printMoney)
	{
		print_money_bag(c.money());
	}
}

void print_hp(hp_sequence& hp)
{
	cout << "HP = " << hp << "/" << hp.max() << "(" << hp.health() * 100.0 << "%) (size: " << hp.size() << ", alive: " << (hp.alive() ? "yes" : "no") << ")" << endl;
}

void print_hps(hit_points& hps)
{
	cout << "HP = " << hps.current_hps() << "/" << hps.max() << "(" << hps.health() * 100.0 << "%) (limit: " << hps.limit() << ", alive: " << (hps.alive() ? "yes" : "no") << ")" << endl;
	for (auto hp : hps)
	{
		auto t = templates::metadata::get_instance().get_class_template(hp.first);
		std::cout << "\t" << t.get_long_name() << " (size = " << hps.size(hp.first) << ") :";
		for (auto h : hp.second)
		{
			std::cout << " [" << h << "]";
		}
		std::cout << " = Total (" << hp.second.max() << ");\n";
	}
}


void print_money_bag(money::money_bag& bag)
{
	std::cout << "Money bag: availability:\n";
	for (auto cn : { defs::coin::copper, defs::coin::silver, defs::coin::electrum, defs::coin::gold, defs::coin::platinum })
	{
		auto t = templates::metadata::get_instance().get_coin_template(cn);
		std::cout << ">> " << bag.get(cn) << " (" << t.get_description() << ")\n";
	}
}


void print_spell_book(spells::spell_book& spellBook)
{
	short levels = spellBook.max_level();
	std::cout << "SPELL BOOK [1 - " << levels << "]\n";
	for (short lvl = 1; lvl <= levels; ++lvl)
	{
		std::cout << "  Level " << lvl << ": count = " << spellBook[lvl].count() << ", capacity = " << spellBook.capacity(lvl) << ";\n";
		std::cout << "    #spells: " << spellBook[lvl].count_distinct();

		short spellLimit = spellBook.limit(lvl);
		if (spellLimit < 0)
			std::cout << " (no limit)\n";
		else
			std::cout << " " << spellLimit << "\n";

		auto it = spellBook[lvl].cbegin();
		while (it != spellBook[lvl].cend())
		{
			auto spellTempl = templates::metadata::get_instance().get_wizard_spell_template(it->first);
			std::cout << "    > " << spellTempl.name() << ": castable = " << it->second.count_castable << ", total = " << it->second.count_total << ";\n";
			++it;
		}
	}
}

void print_holy_symbol(spells::holy_symbol& holySymbol)
{
	short levels = holySymbol.max_level();
	std::cout << "HOLY SYMBOL [1 - " << levels << "]\n";
	for (short lvl = 1; lvl <= levels; ++lvl)
	{
		std::cout << "  Level " << lvl << ": full (" << holySymbol[lvl].count_castable() << ") " << holySymbol[lvl].count() << "/" << holySymbol.capacity(lvl) << ";\n";
		std::cout << "    #spells: " << holySymbol[lvl].count_distinct() << "\n";

		auto it = holySymbol[lvl].cbegin();
		while (it != holySymbol[lvl].cend())
		{
			if (it->second.count_total > 0)
			{
				auto spellTempl = templates::metadata::get_instance().get_priest_spell_template(it->first);
				std::cout << "    > " << spellTempl.name() << ": castable = " << it->second.count_castable << ", total = " << it->second.count_total << ";\n";
			}
			++it;
		}
	}
}

void print_help(int argc, char* argv[])
{
	std::cout << "Ddlib " << adnd::library::version << " demo tool, author: " << adnd::library::author << "\n";
	std::cout << "------------------------------------------------\n";
	std::cout << adnd::library::desc << "\n\n";

	std::cout << "INFO:\n";
	std::cout << "    First created:	" << adnd::library::first_created << "\n";
	std::cout << "    Last commit:	" << adnd::library::last_commit << "\n";
	std::cout << "    Home:		" << adnd::library::link_home << "\n";
	std::cout << "    Doc:		" << adnd::library::link_doc << "\n\n";

	std::cout << "Usage: " << argv[0] << " [OPTIONS]...\n";
	std::cout << "------------------------------------------------\n";
	std::cout << "OPTIONS:\n";//"\033[1;31mbold red text\033[0m\n";
	std::cout << "\t -p, --path\n";
	std::cout << "\t\t path to the SQLite database and script files\n\n";
	std::cout << "\t -d, --db\n";
	std::cout << "\t\t path to the SQLite database file\n\n";
	std::cout << "\t -s, --sql\n";
	std::cout << "\t\t path to the SQL script that creates the SQLite database\n\n";
	std::cout << "\t -i, --init\n";
	std::cout << "\t\t path to the SQL script that initialises the SQLite database\n\n";
	std::cout << "\t -e, --err\n";
	std::cout << "\t\t error handler mode (exception | internal)\n\n";
	std::cout << "\t -r, --source\n";
	std::cout << "\t\t SQLite database source (file | memory)\n\n";
	std::cout << "\t -h, --help\n";
	std::cout << "\t\t displays this help\n\n";

	std::cout << "Examples:\n";
	std::cout << argv[0] << " --db C:\\temp\\adnd.db --sql C:\\temp\\adnd.sql --init C:\\temp\\adnd_init.sql --err exception --source file\n";
	std::cout << argv[0] << " --path C:\\temp\\ --err exception --source file\n";
	std::cout << argv[0] << " --path C:\\temp\\ --err exception --source memory\n";
	std::cout << argv[0] << " -s C:\\temp\\adnd.sql -i C:\\temp\\adnd_init.sql -e exception -r memory\n";
}