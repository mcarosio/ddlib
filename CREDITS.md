Credits
=====================

In this file I would like to mention those who have contributed to the project.
This list includes both people and projects or library I used, even indirectly, to build Ddlib.
Thanks you all guys, you are amazing.

Marco


=====================

#	SQLite (version 3.25.2)
	https://www.sqlite.org
	Ddlib stores the core information and tabular data in a SQLite database. 

#	Catch2 (version 2.12.2)
	https://github.com/catchorg/Catch2
	Used to implement the test cases againt which the library's funcionalities are thoroughly tested.