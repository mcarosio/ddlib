// test.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <filesystem>

#include "adnd.h"
#include "market.h"
#include "money.h"
#include "monster.h"

using namespace adnd;

#define CATCH_CONFIG_RUNNER
#include "catch.hpp"

void print_help(int argc, char* argv[]);

int main(int argc, char* argv[])
{
	if (argc == 1)
	{
		print_help(argc, argv);
		exit(0);
	}

	std::string dbFilePath;// = "..\\..\\..\\..\\SQL\\adnd.db";
	std::string sqlCreateFilePath;// = "..\\..\\..\\..\\SQL\\adnd.sql";
	std::string sqlInitFilePath;// = "..\\..\\..\\..\\SQL\\adnd_init.sql";
	adnd::configuration::error_handler_style errHdlStyle = adnd::configuration::error_handler_style::exception;
	adnd::configuration::database_source dbSrc = adnd::configuration::database_source::memory;

	for (int a = 1; a < argc; ++a)
	{
		size_t argLen = strlen(argv[a]);
		if (std::memcmp(argv[a], "-p", argLen) == 0 || std::memcmp(argv[a], "--path", argLen) == 0)
		{
			std::experimental::filesystem::path dir(argv[a+1]);
			if (!std::experimental::filesystem::exists(dir))
			{
				std::cout << "Parameter " << argv[a] << " specified an invalid value: " << argv[a + 1] << "\n";
				exit(1);
			}
			if (dir.string().at(dir.string().length() - 1) != std::experimental::filesystem::path::preferred_separator)
				dir += std::experimental::filesystem::path::preferred_separator;

			dbFilePath.append(dir.string().c_str()).append("adnd.db");
			sqlCreateFilePath.append(dir.string().c_str()).append("adnd.sql");
			sqlInitFilePath.append(dir.string().c_str()).append("adnd_init.sql");
			a++;
		}
		else if (std::memcmp(argv[a], "--db", argLen) == 0 || std::memcmp(argv[a], "-d", argLen) == 0)
		{
			std::experimental::filesystem::path dir(argv[a+1]);
			if (!std::experimental::filesystem::exists(dir))
			{
				std::cout << "Parameter " << argv[a] << " specified an invalid value: " << argv[a + 1] << "\n";
			}
			if (dir.string().at(dir.string().length() - 1) != std::experimental::filesystem::path::preferred_separator)
				dir += std::experimental::filesystem::path::preferred_separator;
			dbFilePath.append(dir.string().c_str());
			a++;
		}
		else if (std::memcmp(argv[a], "--sql", argLen) == 0 || std::memcmp(argv[a], "-s", argLen) == 0)
		{
			std::experimental::filesystem::path dir(argv[a + 1]);
			if (!std::experimental::filesystem::exists(dir))
			{
				std::cout << "Parameter " << argv[a] << " specified an invalid value: " << argv[a + 1] << "\n";
			}
			if (dir.string().at(dir.string().length() - 1) != std::experimental::filesystem::path::preferred_separator)
				dir += std::experimental::filesystem::path::preferred_separator;
			sqlCreateFilePath.append(dir.string().c_str());
			a++;
		}
		else if (std::memcmp(argv[a], "--init", argLen) == 0 || std::memcmp(argv[a], "-i", argLen) == 0)
		{
			std::experimental::filesystem::path dir(argv[a + 1]);
			if (!std::experimental::filesystem::exists(dir))
			{
				std::cout << "Parameter " << argv[a] << " specified an invalid value: " << argv[a + 1] << "\n";
			}
			if (dir.string().at(dir.string().length() - 1) != std::experimental::filesystem::path::preferred_separator)
				dir += std::experimental::filesystem::path::preferred_separator;
			sqlInitFilePath.append(dir.string().c_str());
			a++;
		}
		else if (std::memcmp(argv[a], "--err", argLen) == 0 || std::memcmp(argv[a], "-e", argLen) == 0)
		{
			if (std::memcmp(argv[a+1], "exception", std::strlen(argv[a+1])) == 0)
				errHdlStyle = adnd::configuration::error_handler_style::exception;
			else if (std::memcmp(argv[a + 1], "internal", std::strlen(argv[a + 1])) == 0)
					errHdlStyle = adnd::configuration::error_handler_style::internal;
			a++;
		}
		else if (std::memcmp(argv[a], "--source", argLen) == 0 || std::memcmp(argv[a], "-r", argLen) == 0)
		{
			if (std::memcmp(argv[a + 1], "file", std::strlen(argv[a + 1])) == 0)
				dbSrc = adnd::configuration::database_source::file;
			else if (std::memcmp(argv[a + 1], "memory", std::strlen(argv[a + 1])) == 0)
				dbSrc = adnd::configuration::database_source::memory;
			a++;
		}
		else if (std::memcmp(argv[a], "--help", argLen) == 0 || std::memcmp(argv[a], "-h", argLen) == 0)
		{
			print_help(argc, argv);
			exit(0);
		}
	}

	configuration::get_instance().init();
	adnd::configuration::get_instance().get_database_path() = dbFilePath;
	adnd::configuration::get_instance().get_definition_script_path() = sqlCreateFilePath;
	adnd::configuration::get_instance().get_init_script_path() = sqlInitFilePath;
	adnd::configuration::get_instance().get_error_handler_style() = errHdlStyle;
	adnd::configuration::get_instance().get_database_source() = dbSrc;

	templates::metadata::init();

	int result = Catch::Session().run(1, argv);

	std::cout << "Press a key to terminate\n";
	char c;
	std::cin.get(c);

	return result;
}

void print_help(int argc, char* argv[])
{
	std::cout << "Ddlib " << adnd::library::version << " test tool, author: " << adnd::library::author << "\n";
	std::cout << "------------------------------------------------\n";
	std::cout << adnd::library::desc << "\n\n";

	std::cout << "INFO:\n";
	std::cout << "    First created:	" << adnd::library::first_created << "\n";
	std::cout << "    Last commit:	" << adnd::library::last_commit << "\n";
	std::cout << "    Home:		" << adnd::library::link_home << "\n";
	std::cout << "    Doc:		" << adnd::library::link_doc << "\n\n";

	std::cout << "Usage: " << argv[0] << " [OPTIONS]...\n";
	std::cout << "------------------------------------------------\n";
	std::cout << "OPTIONS:\n";//"\033[1;31mbold red text\033[0m\n";
	std::cout << "\t -p, --path\n";
	std::cout << "\t\t path to the SQLite database and script files\n\n";
	std::cout << "\t -d, --db\n";
	std::cout << "\t\t path to the SQLite database file\n\n";
	std::cout << "\t -s, --sql\n";
	std::cout << "\t\t path to the SQL script that creates the SQLite database\n\n";
	std::cout << "\t -i, --init\n";
	std::cout << "\t\t path to the SQL script that initialises the SQLite database\n\n";
	std::cout << "\t -e, --err\n";
	std::cout << "\t\t error handler mode (exception | internal)\n\n";
	std::cout << "\t -r, --source\n";
	std::cout << "\t\t SQLite database source (file | memory)\n\n";
	std::cout << "\t -h, --help\n";
	std::cout << "\t\t displays this help\n\n";

	std::cout << "Examples:\n";
	std::cout << argv[0] << " --db C:\\temp\\adnd.db --sql C:\\temp\\adnd.sql --init C:\\temp\\adnd_init.sql --err exception --source file\n";
	std::cout << argv[0] << " --path C:\\temp\\ --err exception --source file\n";
	std::cout << argv[0] << " --path C:\\temp\\ --err exception --source memory\n";
	std::cout << argv[0] << " -s C:\\temp\\adnd.sql -i C:\\temp\\adnd_init.sql -e exception -r memory\n";
}

TEST_CASE("TC001 - Retrieving templates", "[template]")
{
	SECTION("Single class has class size = 1")
	{
		defs::character_class cls = defs::character_class::paladin;
		auto templ = templates::metadata::get_instance().get_class_template(cls);
		auto cc = templ.get_class_composition();

		REQUIRE(templ.get_class() == cls);
		REQUIRE(cc.classes().size() == 1);
	}

	SECTION("Double class has class size = 2")
	{
		defs::character_class cls = defs::character_class::cleric_ranger;
		auto templ = templates::metadata::get_instance().get_class_template(cls);
		auto cc = templ.get_class_composition();

		REQUIRE(templ.get_class() == cls);
		REQUIRE(cc.classes().size() == 2);
	}

	SECTION("Triple class has class size = 3")
	{
		defs::character_class cls = defs::character_class::fighter_mage_thief;
		auto templ = templates::metadata::get_instance().get_class_template(cls);
		auto cc = templ.get_class_composition();

		REQUIRE(templ.get_class() == cls);
		REQUIRE(cc.classes().size() == 3);
	}
}

TEST_CASE("TC002 - Testing money")
{
	money::money_bag bag;
	for (auto cn : { defs::coin::copper, defs::coin::silver, defs::coin::electrum, defs::coin::gold, defs::coin::platinum })
		bag.get(cn) = 0;

	REQUIRE(bag.copper().value() == 0);
	REQUIRE(bag.silver().value() == 0);
	REQUIRE(bag.electrum().value() == 0);
	REQUIRE(bag.gold().value() == 0);
	REQUIRE(bag.platinum().value() == 0);

	SECTION("Subtraction with change back: bag(1 PP + 1 GP + 1 EP) - 19 SP = bag(46 SP)")
	{
		bag.get(defs::coin::electrum) = 1;
		bag.get(defs::coin::gold) = 1;
		bag.get(defs::coin::platinum) = 1;

		REQUIRE(bag.copper().value() == 0);
		REQUIRE(bag.silver().value() == 0);
		REQUIRE(bag.electrum().value() == 1);
		REQUIRE(bag.gold().value() == 1);
		REQUIRE(bag.platinum().value() == 1);

		money::amount amt(defs::coin::silver, 19);
		bag.subtract(amt);

		REQUIRE(bag.copper().value() == 0);
		REQUIRE(bag.silver().value() == 46);
		REQUIRE(bag.electrum().value() == 0);
		REQUIRE(bag.gold().value() == 0);
		REQUIRE(bag.platinum().value() == 0);
	}

	SECTION("Subtraction impossible: bag(1 PP + 1 SP) - 52 SP")
	{
		bag.get(defs::coin::silver) = 1;
		bag.get(defs::coin::platinum) = 1;

		money::amount amt(defs::coin::silver, 52);
		REQUIRE(bag.check_availability(amt) == false);
		REQUIRE_THROWS_AS(bag.subtract(amt), std::exception);

		REQUIRE(bag.copper().value() == 0);
		REQUIRE(bag.silver().value() == 1);
		REQUIRE(bag.electrum().value() == 0);
		REQUIRE(bag.gold().value() == 0);
		REQUIRE(bag.platinum().value() == 1);
	}

	SECTION("Normalising money bag: bag(19 EP, 46 SP) = bag(2 PP, 4 GP, 1 SP)")
	{
		bag.get(defs::coin::silver) = 46;
		bag.get(defs::coin::electrum) = 19;
	
		REQUIRE(bag.get(defs::coin::copper).value() == 0);
		REQUIRE(bag.get(defs::coin::silver).value() == 46);
		REQUIRE(bag.get(defs::coin::electrum).value() == 19);
		REQUIRE(bag.get(defs::coin::gold).value() == 0);
		REQUIRE(bag.get(defs::coin::platinum).value() == 0);
	
		bag.normalise();
	
		REQUIRE(bag.get(defs::coin::copper).value() == 0);
		REQUIRE(bag.get(defs::coin::silver).value() == 1);
		REQUIRE(bag.get(defs::coin::electrum).value() == 0);
		REQUIRE(bag.get(defs::coin::gold).value() == 4);
		REQUIRE(bag.get(defs::coin::platinum).value() == 2);
	}

	SECTION("Normalising 1.5 GP yields 3 EP")
	{
		double amout = 1.5;
		auto normalised = money::normalise(defs::coin::gold, amout);
	
		REQUIRE(normalised.currency() == defs::coin::electrum);
		REQUIRE(normalised.value() == 3);
	}
	
	SECTION("1.35665 PP cannot be normalised")
	{
		double amout = 1.35665;
		REQUIRE_THROWS_AS(money::normalise(defs::coin::platinum, amout), std::exception);
	}

	SECTION("Normalising 1.27 GP yields 127 CP")
	{
		double amout = 1.27;
		auto normalised = money::normalise(defs::coin::gold, amout);
	
		REQUIRE(normalised.currency() == defs::coin::copper);
		REQUIRE(normalised.value() == 127);
	}
	
	SECTION("Splitting 1.5 GP yields 1 GP + 1 EP")
	{
		double amout = 1.5;
		auto curr = adnd::money::split(defs::coin::gold, amout);
	
		REQUIRE(curr.size() == 2);
		REQUIRE(curr[defs::coin::gold].value() == 1);
		REQUIRE(curr[defs::coin::electrum].value() == 1);
	}

	SECTION("1.5054 GP cannot be split")
	{
		double amout = 1.5054;
		REQUIRE_THROWS_AS(adnd::money::split(defs::coin::gold, amout), std::exception);
	}
}

TEST_CASE("TC003 - Testing ageing")
{
	character chr("Anya", defs::character_class::fighter, defs::character_race::human, defs::character_sex::female,
		defs::moral_alignment::chaotic_good, true);

	REQUIRE(chr.age() >= 15);
	REQUIRE(chr.age() <= 19);

	chr.set_skill(defs::character_skill::strength, 15);
	chr.set_skill(defs::character_skill::dexterity, 15);
	chr.set_skill(defs::character_skill::constitution, 15);
	chr.set_skill(defs::character_skill::intelligence, 15);
	chr.set_skill(defs::character_skill::wisdom, 15);
	chr.set_skill(defs::character_skill::charisma, 15);

	SECTION("At 45 humans are middle aged: STR -1, CON -1, INT +1 WIS +1")
	{
		auto toMiddle = 45 - chr.age();
		chr.grow_old(toMiddle);
		REQUIRE(chr.age() == 45);

		REQUIRE(chr.ageing() == defs::character_age::middle_age);
		REQUIRE(chr.strength_value().value() == 14);
		REQUIRE(chr.dexterity_value().value() == 15);
		REQUIRE(chr.constitution_value().value() == 14);
		REQUIRE(chr.intelligence_value().value() == 16);
		REQUIRE(chr.wisdom_value().value() == 16);
		REQUIRE(chr.charisma_value().value() == 15);
	}

	SECTION("At 60 humans are old: STR -2, DEX -2, CON -1, WIS +1")
	{
		auto toMiddle = 45 - chr.age();
		chr.grow_old(toMiddle);
		chr.grow_old(15);
		REQUIRE(chr.age() == 60);

		REQUIRE(chr.ageing() == defs::character_age::old);
		REQUIRE(chr.strength_value().value() == 12);
		REQUIRE(chr.dexterity_value().value() == 13);
		REQUIRE(chr.constitution_value().value() == 13);
		REQUIRE(chr.intelligence_value().value() == 16);
		REQUIRE(chr.wisdom_value().value() == 17);
		REQUIRE(chr.charisma_value().value() == 15);
	}

	SECTION("At 90 humans are venerable: STR -1, DEX -1, CON -1, INT +1, WIS +1")
	{
		auto toMiddle = 45 - chr.age();
		chr.grow_old(toMiddle);
		chr.grow_old(15);
		chr.grow_old(30);
		REQUIRE(chr.age() == 90);

		REQUIRE(chr.ageing() == defs::character_age::venerable);
		REQUIRE(chr.strength_value().value() == 11);
		REQUIRE(chr.dexterity_value().value() == 12);
		REQUIRE(chr.constitution_value().value() == 12);
		REQUIRE(chr.intelligence_value().value() == 17);
		REQUIRE(chr.wisdom_value().value() == 18);
		REQUIRE(chr.charisma_value().value() == 15);
	}

	SECTION("Rejuvenating venerable (90) to middle age (50): STR +3, DEX +3, CON +2, INT -1, WIS -2")
	{
		auto toVenerable = 90 - chr.age();
		chr.grow_old(toVenerable);
		REQUIRE(chr.age() == 90);

		short str = 0, dex = 0, con = 0, itl = 0, wis = 0, cha = 0, exc = 0;
		chr.get_skill_values(str, dex, con, itl, wis, cha, exc);

		chr.rejuvenate(40);
		REQUIRE(chr.age() == 50);

		REQUIRE(chr.ageing() == defs::character_age::middle_age);
		REQUIRE(chr.strength_value().value() == str + 3);
		REQUIRE(chr.dexterity_value().value() == dex + 3);
		REQUIRE(chr.constitution_value().value() == con + 2);
		REQUIRE(chr.intelligence_value().value() == itl - 1);
		REQUIRE(chr.wisdom_value().value() == wis - 2);
		REQUIRE(chr.charisma_value().value() == cha);
	}
}

TEST_CASE("TC004 - Testing paladin", "[paladin]")
{
	defs::character_class cls = defs::character_class::paladin;
	character chr("Behoram", cls, defs::character_race::human, defs::character_sex::male,
		defs::moral_alignment::lawful_good, true);

	REQUIRE(chr.get_level() == 1);
	REQUIRE(chr.get_xp(defs::character_class::paladin) == 0);

	REQUIRE(chr.strength_value().value() >= 12);
	REQUIRE(chr.constitution_value().value() >= 9);
	REQUIRE(chr.wisdom_value().value() >= 13);
	REQUIRE(chr.charisma_value().value() >= 17);

	// This ensures the character does not benefit from 10% extra XP for high prime requisite values
	chr.set_skill(defs::character_skill::strength, 15);

	SECTION("Elven paladins are not allowed")
	{
		REQUIRE_THROWS_AS(character("Elven Paladin", defs::character_class::paladin, defs::character_race::elf, defs::character_sex::male, defs::moral_alignment::lawful_good),
						adnd::exceptions::class_not_allowed_for_race);
	}
	
	SECTION("Evil paladins are not allowed")
	{
		REQUIRE_THROWS_AS(character("Evil Paladin", defs::character_class::paladin, defs::character_race::human, defs::character_sex::male, defs::moral_alignment::chaotic_evil), std::exception);
	}

	SECTION("Gaining 135675 XP gets to level 7")
	{
		long px = 135675;
		chr.add_xp(px);
		REQUIRE(chr.get_level() == 7);
	}

	SECTION("Gaining 2700000 XP and losing 2697900 gets to level 17 then back to 1st")
	{
		long px = 2700000;
		chr.add_xp(px);
		REQUIRE(chr.get_level() == 17);

		px = -2697900;
		chr.add_xp(px);
		REQUIRE(chr.get_level() == 1);
	}

	SECTION("Paladins can get proficient but cannot specialise")
	{
		character chr("Eltan", defs::character_class::paladin, defs::character_race::human, defs::character_sex::male, defs::moral_alignment::lawful_good, true);
		defs::equipment::item it = defs::equipment::item::heavy_crossbow;

		chr.add_proficiency(it);
		REQUIRE_THROWS_AS(chr.add_proficiency(it), std::exception);
	}

	SECTION("Paladins cannot worship evil gods")
	{
		REQUIRE_THROWS_AS(character("Paladin", defs::character_class::paladin,
			defs::character_race::human, defs::character_sex::male, defs::moral_alignment::lawful_good,
			true, defs::deities::deity::bane),
			std::exception);
	}
}

TEST_CASE("TC005 - Testing druid", "[druid]")
{
	defs::character_class cls = defs::character_class::druid;
	character chr("Panoramix", cls, defs::character_race::human, defs::character_sex::male,
		defs::moral_alignment::true_neutral, true);

	REQUIRE(chr.get_level() == 1);
	REQUIRE(chr.get_xp(defs::character_class::druid) == 0);

	REQUIRE(chr.wisdom_value().value() >= 12);
	REQUIRE(chr.charisma_value().value() >= 15);

	// This ensures the character does not benefit from 10% extra XP for high prime requisite values
	chr.set_skill(defs::character_skill::wisdom, 15);

	SECTION("Gaining 2700000 XP and losing 2000000 gets to level 14 then back to 12th")
	{
		long px = 2700000;
		chr.add_xp(px);
		REQUIRE(chr.get_level() == 14);

		px = -2000000;
		chr.add_xp(px);
		REQUIRE(chr.get_level() == 12);
	}

	SECTION("Druids cannot turn undead")
	{
		auto res = chr.turn_undead(defs::turn_ability::turn_skeleton);
		REQUIRE(res.type() == defs::turn_effect::no_effect);
		REQUIRE(res.chance() == 0);
	}
}

TEST_CASE("TC006 - Testing cleric", "[cleric]")
{
	defs::character_class cls = defs::character_class::cleric;
	character chr("Ileria", cls, defs::character_race::half_elf, defs::character_sex::female,
		defs::moral_alignment::chaotic_good, true);

	REQUIRE(chr.get_level() == 1);
	REQUIRE(chr.wisdom_value().value() >= 9);

	chr.set_skill(defs::character_skill::wisdom, 11);
	auto& holySymbol = chr.holy_symbol();

	SECTION("A 1st level cleric requires 16 to turn a 2HD undead monster")
	{
		auto result = chr.turn_undead(defs::turn_ability::turn_hd2);
		REQUIRE(result.type() == defs::turn_effect::percentage);
		REQUIRE(result.chance() == 16);
	}

	SECTION("Cleric holy symbol is automatically populated with available spells")
	{
		chr.gain_level(13);
		REQUIRE(chr.get_level() == 14);

		REQUIRE(holySymbol[1].count_distinct() == 24);
		REQUIRE(holySymbol[2].count_distinct() == 28);
		REQUIRE(holySymbol[3].count_distinct() == 30);
		REQUIRE(holySymbol[4].count_distinct() == 25);
		REQUIRE(holySymbol[5].count_distinct() == 23);
		REQUIRE(holySymbol[6].count_distinct() == 22);
		REQUIRE(holySymbol[7].count_distinct() == 22);
	}

	SECTION("When a 10th level cleric tries to turn a ghoul, the result is D*")
	{
		chr.add_xp(460000);

		REQUIRE(chr.get_level() == 10);
		auto result = chr.turn_undead(defs::turn_ability::turn_ghoul);
		REQUIRE(result.type() == defs::turn_effect::destroy_more);
	}

	SECTION("A 10th level cleric requires 10 to turn a vampire")
	{
		chr.add_xp(460000);

		auto result = chr.turn_undead(defs::turn_ability::turn_vampire);
		REQUIRE(result.type() == defs::turn_effect::percentage);
		REQUIRE(result.chance() == 10);
	}

	SECTION("A 10th level cleric requires 16 to turn a lich")
	{
		chr.add_xp(460000);

		auto result = chr.turn_undead(defs::turn_ability::turn_lich);
		REQUIRE(result.type() == defs::turn_effect::percentage);
		REQUIRE(result.chance() == 16);
	}

	SECTION("Clerics can get proficient but cannot specialise")
	{
		defs::equipment::item it = defs::equipment::item::morning_star;

		chr.add_proficiency(it);
		REQUIRE_THROWS_AS(chr.add_proficiency(it), std::exception);
	}

	SECTION("At 1st level clerics access 1st level spells and are granted access to 24 spells, only one slot")
	{
		REQUIRE(holySymbol.max_level() == 1);
		REQUIRE(holySymbol[1].count_castable() == 0);
		REQUIRE(holySymbol[1].count() == 0);
		REQUIRE(holySymbol.capacity(1) == 1);
		REQUIRE(holySymbol[1].count_distinct() == 24);
	}

	SECTION("A 1st level cleric can select only one first level spell")
	{
		holySymbol.select(defs::spells::priest_spell::cure_light_wounds);
		REQUIRE_THROWS_AS(holySymbol.select(defs::spells::priest_spell::cure_light_wounds), std::exception);
	}

	SECTION("A 3rd level cleric can select 2 1st level and 1 2nd level spell")
	{
		chr.add_xp(3000);
		REQUIRE(chr.get_level(defs::character_class::cleric) == 3);

		REQUIRE(holySymbol.max_level() == 2);
		REQUIRE(holySymbol.capacity(1) == 2);
		REQUIRE(holySymbol.capacity(2) == 1);
		REQUIRE(holySymbol.capacity(3) == 0);
		REQUIRE(holySymbol.capacity(4) == 0);
		REQUIRE(holySymbol.capacity(5) == 0);
		REQUIRE(holySymbol.capacity(6) == 0);
		REQUIRE(holySymbol.capacity(7) == 0);

		holySymbol.select(defs::spells::priest_spell::cure_light_wounds);
		holySymbol.select(defs::spells::priest_spell::shillelagh);
		REQUIRE_THROWS_AS(holySymbol.select(defs::spells::priest_spell::cure_light_wounds), std::exception);
	}

	SECTION("Praying less than 4 hours doesn't grant any spells")
	{
		holySymbol.select(defs::spells::priest_spell::cure_light_wounds);
		holySymbol.pray(1);
		REQUIRE(holySymbol[1].count_castable() == 0);
	}

	SECTION("Praying more than 4 hours does grant spells")
	{
		holySymbol.select(defs::spells::priest_spell::cure_light_wounds);
		holySymbol.pray(5);
		REQUIRE(holySymbol[1].count_castable() == 1);
	}

	SECTION("Wisdom 15 gives 2 additional spells at 1st level")
	{
		chr.set_skill(defs::character_skill::wisdom, 15);

		REQUIRE(holySymbol.capacity(1) == 3);
		REQUIRE(holySymbol.capacity(2) == 0);
		REQUIRE(holySymbol.capacity(3) == 0);
		REQUIRE(holySymbol.capacity(4) == 0);
		REQUIRE(holySymbol.capacity(5) == 0);
		REQUIRE(holySymbol.capacity(6) == 0);
		REQUIRE(holySymbol.capacity(7) == 0);
	}

	SECTION("At 5th level, it takes 36 hours to pray and get granted all castable spells")
	{
		chr.add_xp(13000);
		REQUIRE(chr.get_level(defs::character_class::cleric) == 5);
		chr.set_skill(defs::character_skill::wisdom, 15);

		holySymbol.select(defs::spells::priest_spell::purify_food_and_drink);
		holySymbol.select(defs::spells::priest_spell::detect_magic);
		holySymbol.select(defs::spells::priest_spell::detect_magic);
		holySymbol.select(defs::spells::priest_spell::slow_poison);
		holySymbol.select(defs::spells::priest_spell::hold_person);
		holySymbol.select(defs::spells::priest_spell::know_alignment);
		holySymbol.select(defs::spells::priest_spell::spiritual_hammer);
		holySymbol.select(defs::spells::priest_spell::prayer);

		REQUIRE(holySymbol[1].count_castable() == 0);
		REQUIRE(holySymbol[2].count_castable() == 0);
		REQUIRE(holySymbol[3].count_castable() == 0);
		REQUIRE(holySymbol.pray_all() == 32);
		REQUIRE(holySymbol[1].count_castable() == 3);
		REQUIRE(holySymbol[2].count_castable() == 4);
		REQUIRE(holySymbol[3].count_castable() == 1);
	}

	SECTION("Clerics need praying to cast spells")
	{
		auto spellId = defs::spells::priest_spell::purify_food_and_drink;
		holySymbol.select(spellId);
		REQUIRE_THROWS_AS(holySymbol.cast(spellId), std::exception);
	}

	SECTION("Casting spells decrease the number of castable spells but doesn't change their availability")
	{
		auto spellId = defs::spells::priest_spell::cure_light_wounds;
		holySymbol.select(spellId);
		holySymbol.pray(4);

		REQUIRE(holySymbol[1].count_castable() == 1);
		holySymbol.cast(spellId);
		REQUIRE(holySymbol[1].count_castable() == 0);
	}
}

TEST_CASE("TC007 - Testing thief", "[thief]")
{
	defs::character_class cls = defs::character_class::thief;
	character chr("Tod Uphill", cls, defs::character_race::human, defs::character_sex::male,
		defs::moral_alignment::chaotic_neutral, true);

	REQUIRE(chr.get_level() == 1);
	REQUIRE(chr.get_xp(defs::character_class::thief) == 0);

	// This ensures the character does not benefit from 10% extra XP for high prime requisite values
	chr.set_skill(defs::character_skill::dexterity, 15);

	SECTION("Gaining 1200000 XP and losing 1100000 gets to level 15 then back to 8th")
	{
		long px = 1200000;
		chr.add_xp(px);
		REQUIRE(chr.get_level() == 15);

		px = -1100000;
		chr.add_xp(px);
		REQUIRE(chr.get_level() == 8);
	}
}

TEST_CASE("TC008 - Testing ranger", "[ranger]")
{
	defs::character_class cls = defs::character_class::ranger;
	character chr("Rex", cls, defs::character_race::elf, defs::character_sex::male,
		defs::moral_alignment::chaotic_good, true);
	chr.generate_skills(skills::generation_method::best_of_four);

	// This ensures the character does not benefit from 10% extra XP for high prime requisite values
	chr.set_skill(defs::character_skill::strength, 15);

	REQUIRE(chr.get_level() == 1);

	SECTION("Gaining 24000 XP gets to level 5")
	{
		chr.add_xp(24000);
		REQUIRE(chr.get_level() == 5);
	}

	SECTION("Gaining 33000 XP and one level gets to level 6 with 36000 XP")
	{
		chr.add_xp(33000);
		REQUIRE(chr.get_level() == 5);
		chr.gain_level(1);
		REQUIRE(chr.get_level() == 6);
		REQUIRE(chr.get_xp() == 36000);
	}

	SECTION("Gaining 33000 XP and one level gets to level 6 with 36000 XPs, losing two levels gets to 4th with 13500 XP")
	{
		chr.add_xp(33000);
		chr.gain_level(1);
		chr.drain_level(2);
		REQUIRE(chr.get_level() == 4);
		REQUIRE(chr.get_xp() == 13500);
	}

	SECTION("Gaining 75000 XP, losing 30000 XP and two levels gets to 4th with 13500 XP")
	{
		chr.add_xp(75000);
		REQUIRE(chr.get_level() == 7);

		chr.add_xp(-30000);
		REQUIRE(chr.get_level() == 6);
		REQUIRE(chr.get_xp() == 45000);

		chr.drain_level(2);
		REQUIRE(chr.get_level() == 4);
		REQUIRE(chr.get_xp() == 13500);
	}
}


TEST_CASE("TC009 - Testing fighter")
{
	character chr("Khalid", defs::character_class::fighter, defs::character_race::human, defs::character_sex::male,
				defs::moral_alignment::neutral_good, false);

	chr.generate_skills(skills::generation_method::best_of_four);

	REQUIRE(chr.strength_value().value() >= 9);

	defs::equipment::item it = defs::equipment::item::heavy_crossbow;
	defs::equipment::item projectileID = defs::equipment::item::heavy_quarrel;
	defs::equipment::item itQuiver = defs::equipment::item::quiver;

	auto *heavyCrossBow = equipment::items_pool::get_instance().create_as<equipment::weapon>(it);
	auto *quiver = equipment::items_pool::get_instance().create_as<equipment::miscellaneous_equipment>(itQuiver);

	chr.grab(defs::hand_slot_type::primary, heavyCrossBow);
	chr.inventory_bag() += quiver;
	for (int i = 0; i < 10; ++i)
	{
		auto p = equipment::items_pool::get_instance().create_as<equipment::projectile>(projectileID);
		chr.inventory_bag().add(p);
	}
	heavyCrossBow->select_projectile(projectileID);

	chr.add_proficiency(it);

	short tgtArmourClass = 8;
	short tgtDistance = 10;

	SECTION("Distance 10 is unreachable")
	{
		REQUIRE_THROWS_AS(chr.attack(defs::hand_slot_type::primary, tgtArmourClass, tgtDistance), std::exception);
	}

	SECTION("Distance 10 is reachable for specialised fighters")
	{
		chr.add_proficiency(it);

		REQUIRE_NOTHROW(chr.attack(defs::hand_slot_type::primary, tgtArmourClass, tgtDistance));
	}

	equipment::items_pool::get_instance().erase();
}

TEST_CASE("TC010 - Testing fighter/illusionist", "[fighter][illusionist]")
{
	defs::character_class cls = defs::character_class::fighter_illusionist;
	character chr("Amber", cls, defs::character_race::gnome, defs::character_sex::female,
		defs::moral_alignment::lawful_neutral, true);

	REQUIRE(chr.strength_value().value() >= 9);
	REQUIRE(chr.intelligence_value().value() >= 9);
	REQUIRE(chr.dexterity_value().value() >= 16);

	// This ensures the character does not benefit from 10% extra XP for high prime requisite values
	chr.set_skill(defs::character_skill::strength, 15);
	chr.set_skill(defs::character_skill::intelligence, 15);

	REQUIRE(chr.get_levels()[defs::character_class::fighter] == 1);
	REQUIRE(chr.get_levels()[defs::character_class::illusionist] == 1);
	REQUIRE(chr.get_xp(defs::character_class::fighter) == 0);
	REQUIRE(chr.get_xp(defs::character_class::illusionist) == 0);

	SECTION("Gaining 55345 XP gets to level 5/5")
	{
		long px = 55345;
		chr.add_xp(px);

		REQUIRE(chr.get_levels()[defs::character_class::fighter] == 5);
		REQUIRE(chr.get_levels()[defs::character_class::illusionist] == 5);
		REQUIRE(chr.get_xp(defs::character_class::fighter) == 27672);
		REQUIRE(chr.get_xp(defs::character_class::illusionist) == 27672);
	}
}

TEST_CASE("TC011 - Testing fighter/mage/thief", "[fighter][mage][thief]")
{
	defs::character_class cls = defs::character_class::fighter_mage_thief;
	character chr("Tyrra", cls, defs::character_race::half_elf, defs::character_sex::female,
		defs::moral_alignment::lawful_neutral, true);

	// This ensures the character does not benefit from 10% extra XP for high prime requisite values
	chr.set_skill(defs::character_skill::strength, 15);
	chr.set_skill(defs::character_skill::intelligence, 15);
	chr.set_skill(defs::character_skill::dexterity, 15);

	REQUIRE(chr.get_levels()[defs::character_class::fighter] == 1);
	REQUIRE(chr.get_levels()[defs::character_class::mage] == 1);
	REQUIRE(chr.get_levels()[defs::character_class::thief] == 1);
	REQUIRE(chr.get_xp(defs::character_class::fighter) == 0);
	REQUIRE(chr.get_xp(defs::character_class::mage) == 0);
	REQUIRE(chr.get_xp(defs::character_class::thief) == 0);

	SECTION("Gaining 5500, 18501 and 9000 XP gets to level 1/1/2, 4/3/4 and 4/4/5")
	{
		chr.add_xp(5500);
		REQUIRE(chr.get_levels()[defs::character_class::fighter] == 1);
		REQUIRE(chr.get_levels()[defs::character_class::mage] == 1);
		REQUIRE(chr.get_levels()[defs::character_class::thief] == 2);
		REQUIRE(chr.get_xp(defs::character_class::fighter) == 1833);
		REQUIRE(chr.get_xp(defs::character_class::mage) == 1833);
		REQUIRE(chr.get_xp(defs::character_class::thief) == 1833);

		chr.add_xp(18501);
		REQUIRE(chr.get_levels()[defs::character_class::fighter] == 4);
		REQUIRE(chr.get_levels()[defs::character_class::mage] == 3);
		REQUIRE(chr.get_levels()[defs::character_class::thief] == 4);
		REQUIRE(chr.get_xp(defs::character_class::fighter) == 8000);
		REQUIRE(chr.get_xp(defs::character_class::mage) == 8000);
		REQUIRE(chr.get_xp(defs::character_class::thief) == 8000);

		chr.add_xp(9000);
		REQUIRE(chr.get_levels()[defs::character_class::fighter] == 4);
		REQUIRE(chr.get_levels()[defs::character_class::mage] == 4);
		REQUIRE(chr.get_levels()[defs::character_class::thief] == 5);
		REQUIRE(chr.get_xp(defs::character_class::fighter) == 11000);
		REQUIRE(chr.get_xp(defs::character_class::mage) == 11000);
		REQUIRE(chr.get_xp(defs::character_class::thief) == 11000);
	}

	//SECTION("Test bonus experience for high prime requisite values", "[xp]")
	//{
	//	character chr("Boromir", defs::character_class::fighter, defs::character_race::human,
	//		defs::character_sex::male, defs::moral_alignment::lawful_neutral, true);
	//	chr.generate_skills(skills::generation_method::standard);
	//	skills::character_skill_value str(defs::character_skill::strength, 18, 60);
	//	chr.set_skill(defs::character_skill::strength, str);

	//	chr.add_xp(1900);
	//	REQUIRE(chr.get_level() == 2);
	//	REQUIRE(chr.get_xp() == 2090);

	//	uint8_t newStrengthVal = 15;
	//	chr.set_skill(defs::character_skill::strength, 15);

	//	chr.add_xp(1900);
	//	REQUIRE(chr.get_level() == 2);
	//	REQUIRE(chr.get_xp() == 3990);
	//}
}

TEST_CASE("TC012 - Testing priest of specific mythos", "[priest][specific][mythos]")
{
	SECTION("Priests of specific mythos must be assigned a deity to worship")
	{
		REQUIRE_THROWS_AS(character("Godless priest", defs::character_class::priest_of_specific_mythos,
			defs::character_race::human, defs::character_sex::male, defs::moral_alignment::lawful_good),
			std::exception);
	}

	SECTION("Priests of specific mythos and theis god's moral alignment mush agree")
	{
		REQUIRE_THROWS_AS(character("Bane's priest", defs::character_class::priest_of_specific_mythos,
			defs::character_race::human, defs::character_sex::male, defs::moral_alignment::lawful_good,
			true, defs::deities::deity::bane),
			std::exception);
	}

	SECTION("An evil priests of specific mythos can worship an evil god")
	{
		REQUIRE_NOTHROW(character("Bane's priest", defs::character_class::priest_of_specific_mythos,
			defs::character_race::human, defs::character_sex::male, defs::moral_alignment::lawful_evil,
			true, defs::deities::deity::bane));
	}
}

TEST_CASE("TC013 - Testing fighter/cleric", "[fighter][cleric]")
{
	SECTION("Generating a Fighter/Cleric with default method")
	{
		character thagor("Thagor", defs::character_class::fighter_cleric, defs::character_race::dwarf, defs::character_sex::male, defs::moral_alignment::chaotic_good);
		thagor.generate_skills();

		REQUIRE(thagor.strength_value().value() >= 9);
		REQUIRE(thagor.wisdom() >= 9);
	}
}

TEST_CASE("TC014 - Testing fighter", "[fighter]")
{

	SECTION("Generating a Fighter/Thief with Best of Each method")
	{
		character keirghar("Keirgar", defs::character_class::fighter_thief, defs::character_race::dwarf, defs::character_sex::male, defs::moral_alignment::chaotic_good, true);
		keirghar.generate_skills(skills::generation_method::best_of_each);

		REQUIRE(keirghar.strength_value().value() >= 9);
		REQUIRE(keirghar.dexterity() >= 9);
	}
}

TEST_CASE("TC015 - Testing fighter/mage", "[fighter][mage]")
{
	defs::character_class cls = defs::character_class::fighter_mage;
	character chr("Fingolfin", cls, defs::character_race::elf, defs::character_sex::male,
		defs::moral_alignment::chaotic_neutral, true);

	// This ensures the character does not benefit from 10% extra XP for high prime requisite values
	chr.set_skill(defs::character_skill::strength, 15);
	chr.set_skill(defs::character_skill::intelligence, 15);

	REQUIRE(chr.get_levels()[defs::character_class::fighter] == 1);
	REQUIRE(chr.get_levels()[defs::character_class::mage] == 1);
	REQUIRE(chr.get_xp(defs::character_class::fighter) == 0);
	REQUIRE(chr.get_xp(defs::character_class::mage) == 0);

	SECTION("Gaining 9000 XP gets to level 3/2")
	{
		chr.add_xp(9000);
		REQUIRE(chr.get_levels()[defs::character_class::fighter] == 3);
		REQUIRE(chr.get_levels()[defs::character_class::mage] == 2);
		REQUIRE(chr.get_xp(defs::character_class::fighter) == 4500);
		REQUIRE(chr.get_xp(defs::character_class::mage) == 4500);
	}

	SECTION("Gaining 9000 and 3000 XP gets to level 3/3")
	{
		chr.add_xp(9000);
		chr.add_xp(3000);
		REQUIRE(chr.get_levels()[defs::character_class::fighter] == 3);
		REQUIRE(chr.get_levels()[defs::character_class::mage] == 3);
		REQUIRE(chr.get_xp(defs::character_class::fighter) == 6000);
		REQUIRE(chr.get_xp(defs::character_class::mage) == 6000);
	}

	SECTION("Gaining 9000, 3000 and 12000 XP gets to level 4/4")
	{
		chr.add_xp(9000);
		chr.add_xp(3000);
		chr.add_xp(12000);
		REQUIRE(chr.get_levels()[defs::character_class::fighter] == 4);
		REQUIRE(chr.get_levels()[defs::character_class::mage] == 4);
		REQUIRE(chr.get_xp(defs::character_class::fighter) == 12000);
		REQUIRE(chr.get_xp(defs::character_class::mage) == 12000);
	}

	SECTION("Gaining 9000, 3000 and 12000 XP and losing 7000 XP gets to level 4/3")
	{
		chr.add_xp(9000);
		chr.add_xp(3000);
		chr.add_xp(12000);
		chr.add_xp(-7000);
		REQUIRE(chr.get_levels()[defs::character_class::fighter] == 4);
		REQUIRE(chr.get_levels()[defs::character_class::mage] == 3);
		REQUIRE(chr.get_xp(defs::character_class::fighter) == 8500);
		REQUIRE(chr.get_xp(defs::character_class::mage) == 8500);
	}

	SECTION("Experience loos cannot exceed the XP amount")
	{
		chr.add_xp(17000);
		REQUIRE_THROWS_AS(chr.add_xp(-70000), std::exception);
		REQUIRE(chr.get_levels()[defs::character_class::fighter] == 4);
		REQUIRE(chr.get_levels()[defs::character_class::mage] == 3);
		REQUIRE(chr.get_xp(defs::character_class::fighter) == 8500);
		REQUIRE(chr.get_xp(defs::character_class::mage) == 8500);
	}
}

TEST_CASE("TC016 - Testing fighter/mage/druid", "[fighter][mage][druid]")
{
	defs::character_class cls = defs::character_class::fighter_mage_druid;
	character chr("Delmair", cls, defs::character_race::half_elf, defs::character_sex::male,
		defs::moral_alignment::lawful_neutral, true);

	// This ensures the character does not benefit from 10% extra XP for high prime requisite values
	chr.set_skill(defs::character_skill::strength, 15);
	chr.set_skill(defs::character_skill::intelligence, 15);
	chr.set_skill(defs::character_skill::wisdom, 15);

	REQUIRE(chr.get_levels()[defs::character_class::fighter] == 1);
	REQUIRE(chr.get_levels()[defs::character_class::mage] == 1);
	REQUIRE(chr.get_levels()[defs::character_class::druid] == 1);

	SECTION("Gaining 257033 XP gets to level 7/7/8")
	{
		chr.add_xp(257033);

		REQUIRE(chr.get_levels()[defs::character_class::fighter] == 7);
		REQUIRE(chr.get_levels()[defs::character_class::mage] == 7);
		REQUIRE(chr.get_levels()[defs::character_class::druid] == 8);
		REQUIRE(chr.get_xp(defs::character_class::fighter) == 85677);
		REQUIRE(chr.get_xp(defs::character_class::mage) == 85677);
		REQUIRE(chr.get_xp(defs::character_class::druid) == 85677);
	}
}

TEST_CASE("TC017 - Test his points (gain/loss) for single class", "[hp][single class]")
{
	defs::character_class cls = defs::character_class::paladin;
	hit_points hp(cls);
	hp_sequence& hpSeq = hp[cls];
	
	REQUIRE(hp.current_hps() == 0);
	REQUIRE(hp.max() == 0);
	REQUIRE(hp.alive() == false);
	REQUIRE(hp.limit() == 0);
	REQUIRE(hp.health() == 0.0);
	REQUIRE(hp.size(cls) == 0);
	REQUIRE(hp[cls].max() == 0);

	hp.add(cls, 10);
	REQUIRE(hp.current_hps() == 10);
	REQUIRE(hp.max() == 10);
	REQUIRE(hp.alive() == true);
	REQUIRE(hp.limit() == 10);
	REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
	REQUIRE(hp.size(cls) == 1);
	REQUIRE(hp[cls].max() == 10);
	REQUIRE(hp[cls][0] == 10);

	SECTION("HPs can be gained and lost")
	{
		hp.add(cls, 7);
		REQUIRE(hp.current_hps() == 17);
		REQUIRE(hp.max() == 17);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp.limit() == 20);
		REQUIRE(hp.health() == 1.0);
		REQUIRE(hp.size(cls) == 2);
		REQUIRE(hp[cls].max() == 17);
		REQUIRE(hp[cls][0] == 10);
		REQUIRE(hp[cls][1] == 7);

		hp.add(cls, 9);
		REQUIRE(hp.current_hps() == 26);
		REQUIRE(hp.max() == 26);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp.limit() == 30);
		REQUIRE(hp.health() == 1.0);
		REQUIRE(hp.size(cls) == 3);
		REQUIRE(hp[cls].max() == 26);
		REQUIRE(hp[cls][0] == 10);
		REQUIRE(hp[cls][1] == 7);
		REQUIRE(hp[cls][2] == 9);

		hp.wound(10);
		REQUIRE(hp.current_hps() == 16);
		REQUIRE(hp.max() == 26);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp.limit() == 30);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.61);
		REQUIRE(hp.size(cls) == 3);
		REQUIRE(hp[cls].max() == 26);
		REQUIRE(hp[cls][0] == 10);
		REQUIRE(hp[cls][1] == 7);
		REQUIRE(hp[cls][2] == 9);

		hp.heal(7);
		REQUIRE(hp.current_hps() == 23);
		REQUIRE(hp.max() == 26);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp.limit() == 30);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.88);
		REQUIRE(hp.size(cls) == 3);
		REQUIRE(hp[cls].max() == 26);
		REQUIRE(hp[cls][0] == 10);
		REQUIRE(hp[cls][1] == 7);
		REQUIRE(hp[cls][2] == 9);

		hp.heal(7);
		REQUIRE(hp.current_hps() == 26);
		REQUIRE(hp.max() == 26);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp.limit() == 30);
		REQUIRE(hp.health() == 1.0);
		REQUIRE(hp.size(cls) == 3);
		REQUIRE(hp[cls].max() == 26);
		REQUIRE(hp[cls][0] == 10);
		REQUIRE(hp[cls][1] == 7);
		REQUIRE(hp[cls][2] == 9);

		hp.wound(30);
		REQUIRE(hp.current_hps() == 0);
		REQUIRE(hp.max() == 26);
		REQUIRE(hp.alive() == false);
		REQUIRE(hp.limit() == 30);
		REQUIRE(hp.health() == 0.0);
		REQUIRE(hp.size(cls) == 3);
		REQUIRE(hp[cls].max() == 26);
		REQUIRE(hp[cls][0] == 10);
		REQUIRE(hp[cls][1] == 7);
		REQUIRE(hp[cls][2] == 9);
	}

	SECTION("Once dead, a character cannot be healed any more")
	{
		hp.wound(30);

		REQUIRE_THROWS_AS(hp.heal(7), std::exception);

		REQUIRE(hp.current_hps() == 0);
		REQUIRE(hp.max() == 10);
		REQUIRE(hp.alive() == false);
		REQUIRE(hp.limit() == 10);
		REQUIRE(hp.health() == 0.0);
		REQUIRE(hp.size(cls) == 1);
		REQUIRE(hp[cls].max() == 10);
		REQUIRE(hp[cls][0] == 10);
	}

	SECTION("Dead characters can be resurrected and start at 1 HP")
	{
		hp.wound(30);

		hp.raise();
		REQUIRE(hp.current_hps() == 1);
		REQUIRE(hp.max() == 10);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp.limit() == 10);
		REQUIRE(hp.health() == 0.1);
		REQUIRE(hp.size(cls) == 1);
		REQUIRE(hp[cls].max() == 10);
		REQUIRE(hp[cls][0] == 10);
	}

	SECTION("Raising a living characters results in error")
	{
		hp.wound(30);

		hp.raise();

		REQUIRE_THROWS_AS(hp.raise(), std::exception);
		REQUIRE(hp.current_hps() == 1);
		REQUIRE(hp.max() == 10);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp.limit() == 10);
		REQUIRE(hp.health() == 0.1);
		REQUIRE(hp.size(cls) == 1);
		REQUIRE(hp[cls].max() == 10);
		REQUIRE(hp[cls][0] == 10);
	}

	SECTION("Character dead by energy drain cannot be raised from death using 5th level clerics spell")
	{
		hp.drain(2);
		REQUIRE(hp.current_hps() == 0);
		REQUIRE(hp.max() == 0);
		REQUIRE(hp.alive() == false);
		REQUIRE(hp.limit() == 0);
		REQUIRE(hp.health() == 0.0);
		REQUIRE(hp.size(cls) == 0);
		REQUIRE(hp[cls].max() == 0);

		REQUIRE_THROWS_AS(hp.raise(), std::exception);

		REQUIRE(hp.current_hps() == 0);
		REQUIRE(hp.max() == 0);
		REQUIRE(hp.alive() == false);
		REQUIRE(hp.limit() == 0);
		REQUIRE(hp.health() == 0.0);
		REQUIRE(hp.size(cls) == 0);
		REQUIRE(hp[cls].max() == 0);
	}

	SECTION("Character dead by energy drain cannot be raised from death using 7th level clerics spell")
	{
		hp.drain(2);
		REQUIRE(hp.current_hps() == 0);
		REQUIRE(hp.max() == 0);
		REQUIRE(hp.alive() == false);
		REQUIRE(hp.limit() == 0);
		REQUIRE(hp.health() == 0.0);
		REQUIRE(hp.size(cls) == 0);
		REQUIRE(hp[cls].max() == 0);

		REQUIRE_THROWS_AS(hp.raise_full(), std::exception);

		REQUIRE(hp.current_hps() == 0);
		REQUIRE(hp.max() == 0);
		REQUIRE(hp.alive() == false);
		REQUIRE(hp.limit() == 0);
		REQUIRE(hp.health() == 0.0);
		REQUIRE(hp.size(cls) == 0);
		REQUIRE(hp[cls].max() == 0);
	}

	SECTION("Using the 7th level clerics spell a character is completely restored")
	{
		hp.add(cls, 8);
		REQUIRE(hp.current_hps() == 18);
		REQUIRE(hp.max() == 18);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp.limit() == 20);
		REQUIRE(hp.health() == 1.0);
		REQUIRE(hp.size(cls) == 2);
		REQUIRE(hp[cls].max() == 18);
		REQUIRE(hp[cls][0] == 10);
		REQUIRE(hp[cls][1] == 8);

		hp.wound(100);

		hp.raise_full();
		REQUIRE(hp.current_hps() == 18);
		REQUIRE(hp.max() == 18);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp.limit() == 20);
		REQUIRE(hp.health() == 1.0);
		REQUIRE(hp.size(cls) == 2);
		REQUIRE(hp[cls].max() == 18);
		REQUIRE(hp[cls][0] == 10);
		REQUIRE(hp[cls][1] == 8);
	}
}

TEST_CASE("TC018 - Weight, strength and encumbrance does affect movement capabilities", "[encumbrance]")
{
	character chr("Philippe Gaston", defs::character_class::thief, defs::character_race::human,
		defs::character_sex::male, defs::moral_alignment::chaotic_good, true);
	chr.generate_skills(skills::generation_method::standard);
	chr.set_skill(defs::character_skill::strength, 14);

	SECTION("When encumbrance optional rule applies")
	{
		configuration::get_instance().set(defs::config::optional::encumbrance, true);
		
		REQUIRE(chr.encumbrance() == 2.5);
		REQUIRE(chr.movement_rate() == 12);

		equipment::armour* hide = equipment::items_pool::get_instance().create_as<equipment::armour>(defs::equipment::item::hide);
		chr.inventory_bag().wear(defs::body_slot::body, hide);
		REQUIRE(chr.encumbrance() == 17.5);
		REQUIRE(chr.movement_rate() == 12);

		equipment::weapon* shortSword = equipment::items_pool::get_instance().create_as<equipment::weapon>(defs::equipment::item::short_sword);
		chr.grab(defs::hand_slot_type::primary, shortSword);
		REQUIRE(chr.encumbrance() == 19);
		REQUIRE(chr.movement_rate() == 12);

		std::vector<equipment::item*> items;
		for (auto itemId : { defs::equipment::item::chain_per_ft_light, defs::equipment::item::dagger,
						defs::equipment::item::halberd, defs::equipment::item::heavy_crossbow,
						defs::equipment::item::mancatcher, defs::equipment::item::water_clock })
		{
			items.push_back(equipment::items_pool::get_instance().create(itemId));
		}

		chr.inventory_bag() += items[0];
		REQUIRE(chr.encumbrance() == 19.5);
		REQUIRE(chr.movement_rate() == 12);

		chr.inventory_bag() += items[1];
		REQUIRE(chr.encumbrance() == 20);
		REQUIRE(chr.movement_rate() == 12);

		chr.inventory_bag() += items[2];
		REQUIRE(chr.encumbrance() == 27);
		REQUIRE(chr.movement_rate() == 11);

		chr.inventory_bag() += items[3];
		REQUIRE(chr.encumbrance() == 34);
		REQUIRE(chr.movement_rate() == 10);

		chr.inventory_bag() += items[4];
		REQUIRE(chr.encumbrance() == 38);
		REQUIRE(chr.movement_rate() == 9);

		chr.inventory_bag() += items[5];
		REQUIRE(chr.encumbrance() == 128);
		REQUIRE(chr.movement_rate() == 0);

		equipment::items_pool::get_instance().erase();
	}

	SECTION("When optional rule is disabled encumbrance does not affect movement capability")
	{
		configuration::get_instance().set(defs::config::optional::encumbrance, false);
		equipment::item* waterClock = equipment::items_pool::get_instance().create(defs::equipment::item::water_clock);

		REQUIRE(chr.encumbrance() == 0.0);
		REQUIRE(chr.movement_rate() == 12);

		chr.inventory_bag() += waterClock;
		REQUIRE(chr.encumbrance() == 0);
		REQUIRE(chr.movement_rate() == 12);

		equipment::items_pool::get_instance().erase();
	}
}

TEST_CASE("TC019 - Testing skill creation and comparison", "[skills]")
{
	defs::character_skill skl = defs::character_skill::strength;
	skills::character_skill_value val(skl, 18, 60);

	REQUIRE(val.value() == 18);
	REQUIRE(val.exceptional() == 60);

	SECTION("18/93 is bigger than 18/60")
	{
		skills::character_skill_value val2(defs::character_skill::strength, 18, 93);

		REQUIRE(val < val2);
		REQUIRE(val <= val2);
		REQUIRE_FALSE(val > val2);
		REQUIRE_FALSE(val >= val2);
		REQUIRE_FALSE(val == val2);
		REQUIRE(val != val2);
	}

	SECTION("18/33 is smaller than 18/60")
	{
		skills::character_skill_value val2(defs::character_skill::strength, 18, 33);

		REQUIRE_FALSE(val < val2);
		REQUIRE_FALSE(val <= val2);
		REQUIRE(val > val2);
		REQUIRE(val >= val2);
		REQUIRE_FALSE(val == val2);
		REQUIRE(val != val2);
	}

	SECTION("18/60 is equals to 18/60")
	{
		skills::character_skill_value val2(defs::character_skill::strength);
		val2 = val.value();
		val2.set_exceptional(val.exceptional());

		REQUIRE_FALSE(val < val2);
		REQUIRE(val <= val2);
		REQUIRE_FALSE(val > val2);
		REQUIRE(val >= val2);
		REQUIRE(val == val2);
		REQUIRE_FALSE(val != val2);
	}
}

TEST_CASE("TC020 - Testing items pool", "[items][pool]")
{
	auto& pool = equipment::items_pool::get_instance();
	equipment::items_pool::get_instance().erase();

	SECTION("At the beginning the pool is empty")
	{
		REQUIRE(pool.size() == 0);
	}

	SECTION("Creating an item sets the pool size to 1. The new item has unique ID = 0")
	{
		equipment::weapon *elvenLongBow = pool.create_as<equipment::weapon>(defs::equipment::item::long_bow, "Elven long bow");
		REQUIRE(elvenLongBow->uid() == 0);
		REQUIRE(pool.size() == 1);
	}

	SECTION("When an item is removed from the pool, the pool size decrease")
	{
		equipment::armour *plateMail = pool.create_as<equipment::armour>(defs::equipment::item::plate_mail);
		REQUIRE(pool.size() == 1);
		pool -= plateMail;
		REQUIRE(pool.size() == 0);
	}

	equipment::items_pool::get_instance().erase();
}

TEST_CASE("TC021 - Testing characters grab and release items", "[items][grab][release]")
{
	defs::character_class cls = defs::character_class::ranger;
	character chr("Feanor", cls, defs::character_race::elf, defs::character_sex::male,
		defs::moral_alignment::chaotic_good, true);

	equipment::items_pool::get_instance().erase();

	auto& pool = equipment::items_pool::get_instance();
	equipment::weapon *elvenLongBow = pool.create_as<equipment::weapon>(defs::equipment::item::long_bow, "Elven long bow");
	equipment::weapon *dwarvenBattleAxe = pool.create_as<equipment::weapon>(defs::equipment::item::battle_axe, "Dwarven battle axe");
	equipment::weapon *handAxe = pool.create_as<equipment::weapon>(defs::equipment::item::hand_axe);

	SECTION("Grabbing an item when the hand is busy results in error")
	{
		chr.grab(defs::hand_slot_type::primary, elvenLongBow);

		REQUIRE_THROWS_AS(chr.grab(defs::hand_slot_type::primary, dwarvenBattleAxe), std::exception);
	}

	SECTION("Grabbing a medium sized item with secondary hand results in error")
	{
		REQUIRE_THROWS_AS(chr.grab(defs::hand_slot_type::secondary, dwarvenBattleAxe), std::exception);
	}

	SECTION("Characters can grab another item after having released the one they hold")
	{
		chr.grab(defs::hand_slot_type::primary, elvenLongBow);

		equipment::item *it = nullptr;
		chr.release(defs::hand_slot_type::primary, it);
		REQUIRE_NOTHROW(chr.grab(defs::hand_slot_type::primary, dwarvenBattleAxe));
	}
}

TEST_CASE("TC022 - Testing treasure generation")
{
	defs::treasure::treasure_class treasureClass = defs::treasure::treasure_class::R;
	auto t = templates::metadata::get_instance().get_treasure_template(treasureClass);

	REQUIRE(t[defs::treasure::component::copper].countFrom == 0);
	REQUIRE(t[defs::treasure::component::copper].countTo == 0);
	REQUIRE(t[defs::treasure::component::silver].countFrom == 0);
	REQUIRE(t[defs::treasure::component::silver].countTo == 0);
	REQUIRE(t[defs::treasure::component::gold].countFrom == 2);
	REQUIRE(t[defs::treasure::component::gold].countTo == 20);
	REQUIRE(t[defs::treasure::component::gold].probability == 100);
	REQUIRE(t[defs::treasure::component::gold].additionalNature == -1);
	REQUIRE(t[defs::treasure::component::gold].additionalCount == 0);
	REQUIRE(t[defs::treasure::component::platinum_or_electrum].countFrom == 10);
	REQUIRE(t[defs::treasure::component::platinum_or_electrum].countTo == 60);
	REQUIRE(t[defs::treasure::component::platinum_or_electrum].probability == 100);
	REQUIRE(t[defs::treasure::component::platinum_or_electrum].additionalNature == -1);
	REQUIRE(t[defs::treasure::component::platinum_or_electrum].additionalCount == 0);
	REQUIRE(t[defs::treasure::component::gems].countFrom == 2);
	REQUIRE(t[defs::treasure::component::gems].countTo == 8);
	REQUIRE(t[defs::treasure::component::gems].probability == 100);
	REQUIRE(t[defs::treasure::component::gems].additionalNature == -1);
	REQUIRE(t[defs::treasure::component::gems].additionalCount == 0);
	REQUIRE(t[defs::treasure::component::art_objects].countFrom == 1);
	REQUIRE(t[defs::treasure::component::art_objects].countTo == 3);
	REQUIRE(t[defs::treasure::component::art_objects].probability == 100);
	REQUIRE(t[defs::treasure::component::art_objects].additionalNature == -1);
	REQUIRE(t[defs::treasure::component::art_objects].additionalCount == 0);

	SECTION("Creating an instance of the treasure class R")
	{
		auto tr = treasure::treasure_pool::get_instance().create(treasureClass);

		REQUIRE(tr.coins(defs::coin::copper).value() == 0);
		REQUIRE(tr.coins(defs::coin::silver).value() == 0);
		REQUIRE(tr.coins(defs::coin::gold).value() >= 2);
		REQUIRE(tr.coins(defs::coin::gold).value() <= 20);

		bool checkEpAndPp = tr.coins(defs::coin::electrum).value() >= 10 && tr.coins(defs::coin::electrum).value() <= 60
			|| tr.coins(defs::coin::platinum).value() >= 10 && tr.coins(defs::coin::platinum).value() <= 60;
		REQUIRE(checkEpAndPp == true);

		REQUIRE(tr.gems().size() >= 2);
		REQUIRE(tr.gems().size() <= 8);
		REQUIRE(tr.objects_of_art().size() >= 1);
		REQUIRE(tr.objects_of_art().size() <= 3);
	}
}

TEST_CASE("TC023 - Testing Specialist wizard", "[specialist]")
{
	auto cls = defs::character_class::invoker;
	character chr("Palantir", cls, defs::character_race::human, defs::character_sex::male,
		defs::moral_alignment::lawful_neutral, true);

	// Chance to learn spell = 85% + 15% (bonus as specialist) = 100%
	chr.set_skill(defs::character_skill::intelligence, 18);
	auto& spellBook = chr.spell_book();

	short capacity[defs::spell_book_max_level]{ 2, 0, 0, 0, 0, 0, 0, 0, 0 };
	short count[defs::spell_book_max_level]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	short count_distinct[defs::spell_book_max_level]{ 1, 0, 0, 0, 0, 0, 0, 0, 0 };

	SECTION("Intelligence 18 gives access to all 9 spell levels")
	{
		auto st = templates::metadata::get_instance().get_skill_template(defs::character_skill::intelligence);
		auto stats = st.get_intelligence_stats(chr.intelligence_value());
		REQUIRE(stats.spellLevel == 9);
	}

	SECTION("Intelligence 11 gives access to spells up to the 5th level, no matter the caster's level")
	{
		chr.gain_level(19);
		REQUIRE(chr.get_level() == 20);

		chr.set_skill(defs::character_skill::intelligence, 11);
		auto st = templates::metadata::get_instance().get_skill_template(defs::character_skill::intelligence);
		auto stats = st.get_intelligence_stats(chr.intelligence_value());
		REQUIRE(stats.spellLevel == 5);
		REQUIRE(spellBook.max_level() == 5);
	}

	SECTION("A 1st level mage has access to")
	{
		auto st = templates::metadata::get_instance().get_skill_template(defs::character_skill::intelligence);
		auto stats = st.get_intelligence_stats(chr.intelligence_value());
		REQUIRE(stats.spellLevel == 9);
		REQUIRE(spellBook.max_level() == 1);
	}

	SECTION("Invoker cannot select spells which are not scribed in the book")
	{
		auto spellId = defs::spells::wizard_spell::shocking_grasp;
		REQUIRE_THROWS_AS(spellBook.select(spellId), std::exception);
	}

	SECTION("Invoker begins with two slots at level 1 and a random Invocation/Evocation spell in his book")
	{
		for (int i = 1; i <= spellBook.max_level(); ++i)
		{
			REQUIRE(spellBook.limit(i) == 18);
			REQUIRE(spellBook.capacity(i) == capacity[i - 1]);
			REQUIRE(spellBook[i].count() == count[i - 1]);
			REQUIRE(spellBook[i].count_distinct() == count_distinct[i - 1]);
		}
		REQUIRE(spellBook[1].has_specialist_spell() == true);
		auto spellId = (*spellBook[1].cbegin()).first;
		auto t = templates::metadata::get_instance().get_wizard_spell_template(spellId);
		REQUIRE(t.schools().find(defs::spells::school::invocation_evocation) != t.schools().end());
	}

	SECTION("At 5-th level, invoker has 5-3-2 slots available at levels 1, 2 and 3 and a specialist spell in each level")
	{
		chr.add_xp(20000);
		REQUIRE(chr.level()[cls].level() == 5);

		REQUIRE(spellBook.capacity(1) == 5);
		REQUIRE(spellBook.capacity(2) == 3);
		REQUIRE(spellBook.capacity(3) == 2);

		REQUIRE(spellBook[1].has_specialist_spell() == true);
		REQUIRE(spellBook[2].has_specialist_spell() == true);
		REQUIRE(spellBook[3].has_specialist_spell() == true);

		for (int i = 1; i <= 3; ++i)
		{
			auto spellId = (*spellBook[1].cbegin()).first;
			auto t = templates::metadata::get_instance().get_wizard_spell_template(spellId);
			REQUIRE(t.schools().find(defs::spells::school::invocation_evocation) != t.schools().end());
		}
	}

	SECTION("Enchantment/Charm spells cannot be added to the spell book")
	{
		auto spellId = defs::spells::wizard_spell::charm_person;
		REQUIRE_THROWS_AS(spellBook.add(spellId), std::exception);
	}

	SECTION("Advanced Evocation spells are not available to 1st level invoker")
	{
		auto spellId = defs::spells::wizard_spell::fireball;
		REQUIRE_THROWS_AS(spellBook.add(spellId), std::exception);
	}

	SECTION("Adding a spell changes count/count_distinct")
	{
		auto spellId1 = defs::spells::wizard_spell::magic_missile;
		auto spellId2 = defs::spells::wizard_spell::shield;
		defs::spells::wizard_spell spellId = spellBook.find(spellId1) ? spellId2 : spellId1;

		spellBook.add(spellId);
		count_distinct[0] = 2;

		for (int i = 1; i <= spellBook.max_level(); ++i)
		{
			REQUIRE(spellBook.capacity(i) == capacity[i - 1]);
			REQUIRE(spellBook[i].count() == count[i - 1]);
			REQUIRE(spellBook[i].count_distinct() == count_distinct[i - 1]);
		}
	}

	SECTION("Selecting more than two spells at level 1 is not allowed")
	{
		auto spellId = (*spellBook[1].cbegin()).first;

		REQUIRE(spellBook.capacity(1) == 2);
		REQUIRE(spellBook[1].count() == 0);
		REQUIRE(spellBook[1].count_distinct() == 1);

		spellBook.select(spellId);

		REQUIRE(spellBook[1].count() == 1);

		spellBook.select(spellId);

		REQUIRE(spellBook.capacity(1) == 2);
		REQUIRE(spellBook[1].count() == 2);
		REQUIRE(spellBook[1].count_distinct() == 1);

		REQUIRE_THROWS_AS(spellBook.select(spellId), std::exception);
	}

	SECTION("Adding a spell already present in the book is not allowed")
	{
		auto spellId1 = defs::spells::wizard_spell::magic_missile;
		auto spellId2 = defs::spells::wizard_spell::shield;
		defs::spells::wizard_spell spellId = spellBook.find(spellId1) ? spellId2 : spellId1;

		spellBook.add(spellId);
		REQUIRE_THROWS_AS(spellBook.add(spellId), std::exception);

		spellId = (*spellBook[1].cbegin()).first;
		REQUIRE_THROWS_AS(spellBook.add(spellId), std::exception);
	}

	SECTION("Studying increases the number of castable spells")
	{
		chr.add_xp(20000);
		REQUIRE(chr.level()[cls].level() == 5);

		REQUIRE(spellBook.capacity(1) == 5);
		REQUIRE(spellBook.capacity(2) == 3);
		REQUIRE(spellBook.capacity(3) == 2);

		auto level1SpellId = spellBook[1].cbegin()->first;
		auto level2SpellId = spellBook[2].cbegin()->first;
		auto level3SpellId = spellBook[3].cbegin()->first;

		spellBook.select(level1SpellId);
		spellBook.select(level1SpellId);
		spellBook.select(level1SpellId);
		spellBook.select(level1SpellId);
		spellBook.select(level2SpellId);
		spellBook.select(level2SpellId);
		spellBook.select(level3SpellId);

		REQUIRE(spellBook[1].count_castable() == 0);
		REQUIRE(spellBook[2].count_castable() == 0);
		REQUIRE(spellBook[3].count_castable() == 0);

		spellBook.study(1);

		REQUIRE(spellBook[1].count_castable() == 0);
		REQUIRE(spellBook[2].count_castable() == 0);
		REQUIRE(spellBook[3].count_castable() == 0);

		REQUIRE(spellBook.study_all() == 28);
		REQUIRE(spellBook[1].count_castable() == 4);
		REQUIRE(spellBook[2].count_castable() == 2);
		REQUIRE(spellBook[3].count_castable() == 1);
	}
}

TEST_CASE("TC024 - Testing Mage ", "[mage]")
{
	auto cls = defs::character_class::mage;
	character chr("Palantir", cls, defs::character_race::human, defs::character_sex::male,
		defs::moral_alignment::lawful_neutral, true);
	
	REQUIRE(chr.intelligence() >= 9);

	auto t = templates::metadata::get_instance().get_class_template(cls);

	auto& spellBook = chr.spell_book();

	SECTION("Mages can access all schools of magic")
	{
		auto p = templates::metadata::get_character_type_and_id(cls);
		auto res = templates::metadata::get_instance().fetch<short>(templates::metadata::query::schools_per_class);
		auto schools = res.filter<uint16_t>(p.classType).filter<uint32_t>(p.classId).run();

		for (auto s : schools)
		{
			auto schoolId = static_cast<defs::spells::school>(std::get<0>(s));
			REQUIRE(t.get_school_access(schoolId) == defs::spells::school_access::neutral);
		}
	}

	SECTION("Mage begins with one slots at level 1 and no spells in his book")
	{
		short capacity[defs::spell_book_max_level]{ 1, 0, 0, 0, 0, 0, 0, 0, 0 };
		
		auto st = templates::metadata::get_instance().get_skill_template(defs::character_skill::intelligence);
		auto stats = st.get_intelligence_stats(chr.intelligence_value());

		for (int i = 1; i <= spellBook.max_level(); ++i)
		{
			REQUIRE(spellBook.limit(i) == stats.maxNumberOfSpellsPerLevel);
			REQUIRE(spellBook.capacity(i) == capacity[i - 1]);
			REQUIRE(spellBook[i].count() == 0);
			REQUIRE(spellBook[i].count_distinct() == 0);
		}
	}

	SECTION("At 5-th level, a mage has 4-2-1 slots available at levels 1, 2 and 3 and no spells in his spell book")
	{
		chr.add_xp(20000);
		REQUIRE(chr.level()[cls].level() == 5);

		REQUIRE(spellBook.capacity(1) == 4);
		REQUIRE(spellBook.capacity(2) == 2);
		REQUIRE(spellBook.capacity(3) == 1);

		for (int i = 1; i <= spellBook.max_level(); ++i)
		{
			REQUIRE(spellBook[i].count() == 0);
			REQUIRE(spellBook[i].count_distinct() == 0);
		}
	}
}

TEST_CASE("TC025 - Testing HP sequence", "[hp]")
{
	hp_sequence hp;

	REQUIRE(static_cast<short>(hp) == 0);
	REQUIRE(hp.max() == 0);
	REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.0);
	REQUIRE(hp.size() == 0);
	REQUIRE(hp.alive() == false);

	hp += 10;
	REQUIRE(static_cast<short>(hp) == 10);
	REQUIRE(hp.max() == 10);
	REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
	REQUIRE(hp.size() == 1);
	REQUIRE(hp.alive() == true);

	hp += 5;
	REQUIRE(static_cast<short>(hp) == 15);
	REQUIRE(hp.max() == 15);
	REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
	REQUIRE(hp.size() == 2);
	REQUIRE(hp.alive() == true);

	hp += 5;
	REQUIRE(static_cast<short>(hp) == 20);
	REQUIRE(hp.max() == 20);
	REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
	REQUIRE(hp.size() == 3);
	REQUIRE(hp.alive() == true);

	hp.wound(7);
	REQUIRE(static_cast<short>(hp) == 13);
	REQUIRE(hp.max() == 20);
	REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.65);
	REQUIRE(hp.size() == 3);
	REQUIRE(hp.alive() == true);

	hp.wound(7);
	REQUIRE(static_cast<short>(hp) == 6);
	REQUIRE(hp.max() == 20);
	REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.3);
	REQUIRE(hp.size() == 3);
	REQUIRE(hp.alive() == true);

	hp.heal(10);
	REQUIRE(static_cast<short>(hp) == 16);
	REQUIRE(hp.max() == 20);
	REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.8);
	REQUIRE(hp.size() == 3);
	REQUIRE(hp.alive() == true);

	hp.heal(10);
	REQUIRE(static_cast<short>(hp) == 20);
	REQUIRE(hp.max() == 20);
	REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
	REQUIRE(hp.size() == 3);
	REQUIRE(hp.alive() == true);

	hp.drain();
	REQUIRE(static_cast<short>(hp) == 15);
	REQUIRE(hp.max() == 15);
	REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
	REQUIRE(hp.size() == 2);
	REQUIRE(hp.alive() == true);

	hp.wound(20);
	REQUIRE(static_cast<short>(hp) == 0);
	REQUIRE(hp.max() == 15);
	REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.0);
	REQUIRE(hp.size() == 2);
	REQUIRE(hp.alive() == false);

	REQUIRE_THROWS_AS(hp.heal(10), std::exception);
	REQUIRE(static_cast<short>(hp) == 0);
	REQUIRE(hp.max() == 15);
	REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.0);
	REQUIRE(hp.size() == 2);
	REQUIRE(hp.alive() == false);

	hp.raise();
	REQUIRE(static_cast<short>(hp) == 1);
	REQUIRE(hp.max() == 15);
	REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.066);
	REQUIRE(hp.size() == 2);
	REQUIRE(hp.alive() == true);

	REQUIRE_THROWS_AS(hp.raise(), std::exception);
	REQUIRE(static_cast<short>(hp) == 1);
	REQUIRE(hp.max() == 15);
	REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.066);
	REQUIRE(hp.size() == 2);
	REQUIRE(hp.alive() == true);

	hp.drain(2);
	REQUIRE(static_cast<short>(hp) == 0);
	REQUIRE(hp.max() == 0);
	REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.0);
	REQUIRE(hp.size() == 0);
	REQUIRE(hp.alive() == false);

	REQUIRE_THROWS_AS(hp.raise_full(), std::exception);
	REQUIRE(static_cast<short>(hp) == 0);
	REQUIRE(hp.max() == 0);
	REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.0);
	REQUIRE(hp.size() == 0);
	REQUIRE(hp.alive() == false);

	hp += 10;
	REQUIRE(static_cast<short>(hp) == 10);
	REQUIRE(hp.max() == 10);
	REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
	REQUIRE(hp.size() == 1);
	REQUIRE(hp.alive() == true);

	hp += 10;
	REQUIRE(static_cast<short>(hp) == 20);
	REQUIRE(hp.max() == 20);
	REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
	REQUIRE(hp.size() == 2);
	REQUIRE(hp.alive() == true);

	hp += 10;
	REQUIRE(static_cast<short>(hp) == 30);
	REQUIRE(hp.max() == 30);
	REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
	REQUIRE(hp.size() == 3);
	REQUIRE(hp.alive() == true);

	hp.wound(31);
	REQUIRE(static_cast<short>(hp) == 0);
	REQUIRE(hp.max() == 30);
	REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.0);
	REQUIRE(hp.size() == 3);
	REQUIRE(hp.alive() == false);

	hp.raise_full();
	REQUIRE(static_cast<short>(hp) == 30);
	REQUIRE(hp.max() == 30);
	REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
	REQUIRE(hp.size() == 3);
	REQUIRE(hp.alive() == true);
}

TEST_CASE("TC026 - Testing HP sequence with constitution adjustment", "[hp]")
{
	defs::character_class cls = defs::character_class::fighter_mage_cleric;
	hit_points hp(cls);

	SECTION("When constitution bonus applies")
	{
		hp.set_constitution_adjustment(4);

		hp.add(cls, { 10, 4, 8 });
		REQUIRE(hp.current_hps() == 11);
		REQUIRE(hp.max() == 11);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
		REQUIRE(hp.limit() == 11);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 1);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::mage].size() == 1);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::cleric].size() == 1);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);

		hp.add(defs::character_class::fighter, 7);
		hp.add(defs::character_class::mage, 3);
		hp.add(defs::character_class::cleric, 6);
		REQUIRE(hp.current_hps() == 19);
		REQUIRE(hp.max() == 19);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
		REQUIRE(hp.limit() == 20);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 2);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 7);
		REQUIRE(hp[defs::character_class::mage].size() == 2);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::cleric].size() == 2);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);

		hp.add(defs::character_class::fighter, 9);
		hp.add(defs::character_class::mage, 2);
		hp.add(defs::character_class::cleric, 5);
		REQUIRE(hp.current_hps() == 27);
		REQUIRE(hp.max() == 27);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
		REQUIRE(hp.limit() == 29);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 3);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 7);
		REQUIRE(hp[defs::character_class::fighter][2] == 9);
		REQUIRE(hp[defs::character_class::mage].size() == 3);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::mage][2] == 2);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);

		hp.wound(10);
		REQUIRE(hp.current_hps() == 17);
		REQUIRE(hp.max() == 27);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.6296);
		REQUIRE(hp.limit() == 29);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 3);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 7);
		REQUIRE(hp[defs::character_class::fighter][2] == 9);
		REQUIRE(hp[defs::character_class::mage].size() == 3);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::mage][2] == 2);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);

		hp.heal(7);
		REQUIRE(hp.current_hps() == 24);
		REQUIRE(hp.max() == 27);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.8888);
		REQUIRE(hp.limit() == 29);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 3);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 7);
		REQUIRE(hp[defs::character_class::fighter][2] == 9);
		REQUIRE(hp[defs::character_class::mage].size() == 3);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::mage][2] == 2);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);

		hp.heal(7);
		REQUIRE(hp.current_hps() == 27);
		REQUIRE(hp.max() == 27);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
		REQUIRE(hp.limit() == 29);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 3);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 7);
		REQUIRE(hp[defs::character_class::fighter][2] == 9);
		REQUIRE(hp[defs::character_class::mage].size() == 3);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::mage][2] == 2);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);

		hp.wound(30);
		REQUIRE(hp.current_hps() == 0);
		REQUIRE(hp.max() == 27);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.0);
		REQUIRE(hp.limit() == 29);
		REQUIRE(hp.alive() == false);
		REQUIRE(hp[defs::character_class::fighter].size() == 3);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 7);
		REQUIRE(hp[defs::character_class::fighter][2] == 9);
		REQUIRE(hp[defs::character_class::mage].size() == 3);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::mage][2] == 2);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);

		REQUIRE_THROWS_AS(hp.heal(7), std::exception);
		REQUIRE(hp.current_hps() == 0);
		REQUIRE(hp.max() == 27);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.0);
		REQUIRE(hp.limit() == 29);
		REQUIRE(hp.alive() == false);
		REQUIRE(hp[defs::character_class::fighter].size() == 3);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 7);
		REQUIRE(hp[defs::character_class::fighter][2] == 9);
		REQUIRE(hp[defs::character_class::mage].size() == 3);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::mage][2] == 2);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);

		hp.raise();
		REQUIRE(hp.current_hps() == 1);
		REQUIRE(hp.max() == 27);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.037);
		REQUIRE(hp.limit() == 29);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 3);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 7);
		REQUIRE(hp[defs::character_class::fighter][2] == 9);
		REQUIRE(hp[defs::character_class::mage].size() == 3);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::mage][2] == 2);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);

		REQUIRE_THROWS_AS(hp.raise(), std::exception);
		REQUIRE(hp.current_hps() == 1);
		REQUIRE(hp.max() == 27);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.037);
		REQUIRE(hp.limit() == 29);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 3);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 7);
		REQUIRE(hp[defs::character_class::fighter][2] == 9);
		REQUIRE(hp[defs::character_class::mage].size() == 3);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::mage][2] == 2);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);

		hp.drain(2);
		REQUIRE(hp.current_hps() == 1);
		REQUIRE(hp.max() == 21);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.0476);
		REQUIRE(hp.limit() == 23);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 2);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 7);
		REQUIRE(hp[defs::character_class::mage].size() == 2);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);

		hp.drain(5);
		REQUIRE(hp.current_hps() == 0);
		REQUIRE(hp.max() == 0);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.0);
		REQUIRE(hp.limit() == 0);
		REQUIRE(hp.alive() == false);
		REQUIRE(hp[defs::character_class::fighter].size() == 0);
		REQUIRE(hp[defs::character_class::mage].size() == 0);
		REQUIRE(hp[defs::character_class::cleric].size() == 0);

		REQUIRE_THROWS_AS(hp.raise_full(), std::exception);
		REQUIRE(hp.current_hps() == 0);
		REQUIRE(hp.max() == 0);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.0);
		REQUIRE(hp.limit() == 0);
		REQUIRE(hp.alive() == false);
		REQUIRE(hp[defs::character_class::fighter].size() == 0);
		REQUIRE(hp[defs::character_class::mage].size() == 0);
		REQUIRE(hp[defs::character_class::cleric].size() == 0);

		hp.add(cls, { 10, 4, 8 });
		REQUIRE(hp.current_hps() == 11);
		REQUIRE(hp.max() == 11);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
		REQUIRE(hp.limit() == 11);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 1);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::mage].size() == 1);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::cleric].size() == 1);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);

		hp.add(defs::character_class::fighter, 6);
		hp.add(defs::character_class::mage, 3);
		hp.add(defs::character_class::cleric, 6);
		REQUIRE(hp.current_hps() == 19);
		REQUIRE(hp.max() == 19);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
		REQUIRE(hp.limit() == 20);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 2);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 6);
		REQUIRE(hp[defs::character_class::mage].size() == 2);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::cleric].size() == 2);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);

		hp.add(defs::character_class::fighter, 7);
		hp.add(defs::character_class::mage, 2);
		hp.add(defs::character_class::cleric, 5);
		REQUIRE(hp.current_hps() == 26);
		REQUIRE(hp.max() == 26);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
		REQUIRE(hp.limit() == 29);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 3);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 6);
		REQUIRE(hp[defs::character_class::fighter][2] == 7);
		REQUIRE(hp[defs::character_class::mage].size() == 3);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::mage][2] == 2);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);

		hp.wound(100);
		REQUIRE(hp.current_hps() == 0);
		REQUIRE(hp.max() == 26);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.0);
		REQUIRE(hp.limit() == 29);
		REQUIRE(hp.alive() == false);
		REQUIRE(hp[defs::character_class::fighter].size() == 3);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 6);
		REQUIRE(hp[defs::character_class::fighter][2] == 7);
		REQUIRE(hp[defs::character_class::mage].size() == 3);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::mage][2] == 2);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);

		hp.raise_full();
		REQUIRE(hp.current_hps() == 26);
		REQUIRE(hp.max() == 26);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
		REQUIRE(hp.limit() == 29);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 3);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 6);
		REQUIRE(hp[defs::character_class::fighter][2] == 7);
		REQUIRE(hp[defs::character_class::mage].size() == 3);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::mage][2] == 2);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);
	}

	SECTION("When constitution malus applies")
	{
		hp.set_constitution_adjustment(-2);

		hp.add(cls, { 10, 4, 8 });
		REQUIRE(hp.current_hps() == 5);
		REQUIRE(hp.max() == 5);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
		REQUIRE(hp.limit() == 5);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 1);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::mage].size() == 1);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::cleric].size() == 1);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);

		hp.add(defs::character_class::fighter, 7);
		hp.add(defs::character_class::mage, 3);
		hp.add(defs::character_class::cleric, 6);
		REQUIRE(hp.current_hps() == 10);
		REQUIRE(hp.max() == 10);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
		REQUIRE(hp.limit() == 11);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 2);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 7);
		REQUIRE(hp[defs::character_class::mage].size() == 2);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::cleric].size() == 2);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);

		hp.add(defs::character_class::fighter, 9);
		hp.add(defs::character_class::mage, 2);
		hp.add(defs::character_class::cleric, 5);
		REQUIRE(hp.current_hps() == 15);
		REQUIRE(hp.max() == 15);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
		REQUIRE(hp.limit() == 17);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 3);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 7);
		REQUIRE(hp[defs::character_class::fighter][2] == 9);
		REQUIRE(hp[defs::character_class::mage].size() == 3);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::mage][2] == 2);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);

		hp.wound(10);
		REQUIRE(hp.current_hps() == 5);
		REQUIRE(hp.max() == 15);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.3333);
		REQUIRE(hp.limit() == 17);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 3);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 7);
		REQUIRE(hp[defs::character_class::fighter][2] == 9);
		REQUIRE(hp[defs::character_class::mage].size() == 3);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::mage][2] == 2);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);

		hp.heal(7);
		REQUIRE(hp.current_hps() == 12);
		REQUIRE(hp.max() == 15);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.80);
		REQUIRE(hp.limit() == 17);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 3);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 7);
		REQUIRE(hp[defs::character_class::fighter][2] == 9);
		REQUIRE(hp[defs::character_class::mage].size() == 3);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::mage][2] == 2);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);

		hp.heal(7);
		REQUIRE(hp.current_hps() == 15);
		REQUIRE(hp.max() == 15);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
		REQUIRE(hp.limit() == 17);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 3);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 7);
		REQUIRE(hp[defs::character_class::fighter][2] == 9);
		REQUIRE(hp[defs::character_class::mage].size() == 3);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::mage][2] == 2);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);

		hp.wound(30);
		REQUIRE(hp.current_hps() == 0);
		REQUIRE(hp.max() == 15);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.0);
		REQUIRE(hp.limit() == 17);
		REQUIRE(hp.alive() == false);
		REQUIRE(hp[defs::character_class::fighter].size() == 3);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 7);
		REQUIRE(hp[defs::character_class::fighter][2] == 9);
		REQUIRE(hp[defs::character_class::mage].size() == 3);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::mage][2] == 2);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);

		REQUIRE_THROWS_AS(hp.heal(7), std::exception);
		REQUIRE(hp.max() == 15);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.0);
		REQUIRE(hp.limit() == 17);
		REQUIRE(hp.alive() == false);
		REQUIRE(hp[defs::character_class::fighter].size() == 3);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 7);
		REQUIRE(hp[defs::character_class::fighter][2] == 9);
		REQUIRE(hp[defs::character_class::mage].size() == 3);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::mage][2] == 2);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);

		hp.raise();
		REQUIRE(hp.current_hps() == 1);
		REQUIRE(hp.max() == 15);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.0666);
		REQUIRE(hp.limit() == 17);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 3);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 7);
		REQUIRE(hp[defs::character_class::fighter][2] == 9);
		REQUIRE(hp[defs::character_class::mage].size() == 3);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::mage][2] == 2);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);

		REQUIRE_THROWS_AS(hp.raise(), std::exception);
		REQUIRE(hp.current_hps() == 1);
		REQUIRE(hp.max() == 15);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.0666);
		REQUIRE(hp.limit() == 17);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 3);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 7);
		REQUIRE(hp[defs::character_class::fighter][2] == 9);
		REQUIRE(hp[defs::character_class::mage].size() == 3);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::mage][2] == 2);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);

		hp.drain(2);
		REQUIRE(hp.current_hps() == 1);
		REQUIRE(hp.max() == 11);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.0909);
		REQUIRE(hp.limit() == 13);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 2);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 7);
		REQUIRE(hp[defs::character_class::mage].size() == 2);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);

		hp.drain(5);
		REQUIRE(hp.current_hps() == 0);
		REQUIRE(hp.max() == 0);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.0);
		REQUIRE(hp.limit() == 0);
		REQUIRE(hp.alive() == false);
		REQUIRE(hp[defs::character_class::fighter].size() == 0);
		REQUIRE(hp[defs::character_class::mage].size() == 0);
		REQUIRE(hp[defs::character_class::cleric].size() == 0);

		REQUIRE_THROWS_AS(hp.raise_full(), std::exception);
		REQUIRE(hp.current_hps() == 0);
		REQUIRE(hp.max() == 0);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.0);
		REQUIRE(hp.limit() == 0);
		REQUIRE(hp.alive() == false);
		REQUIRE(hp[defs::character_class::fighter].size() == 0);
		REQUIRE(hp[defs::character_class::mage].size() == 0);
		REQUIRE(hp[defs::character_class::cleric].size() == 0);

		hp.add(cls, { 10, 4, 8 });
		REQUIRE(hp.current_hps() == 5);
		REQUIRE(hp.max() == 5);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
		REQUIRE(hp.limit() == 5);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 1);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::mage].size() == 1);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::cleric].size() == 1);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);

		hp.add(defs::character_class::fighter, 6);
		hp.add(defs::character_class::mage, 3);
		hp.add(defs::character_class::cleric, 6);
		REQUIRE(hp.current_hps() == 10);
		REQUIRE(hp.max() == 10);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
		REQUIRE(hp.limit() == 11);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 2);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 6);
		REQUIRE(hp[defs::character_class::mage].size() == 2);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::cleric].size() == 2);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);

		hp.add(defs::character_class::fighter, 7);
		hp.add(defs::character_class::mage, 2);
		hp.add(defs::character_class::cleric, 5);
		REQUIRE(hp.current_hps() == 14);
		REQUIRE(hp.max() == 14);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
		REQUIRE(hp.limit() == 17);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 3);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 6);
		REQUIRE(hp[defs::character_class::fighter][2] == 7);
		REQUIRE(hp[defs::character_class::mage].size() == 3);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::mage][2] == 2);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);

		hp.wound(100);
		REQUIRE(hp.current_hps() == 0);
		REQUIRE(hp.max() == 14);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 0.0);
		REQUIRE(hp.limit() == 17);
		REQUIRE(hp.alive() == false);
		REQUIRE(hp[defs::character_class::fighter].size() == 3);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 6);
		REQUIRE(hp[defs::character_class::fighter][2] == 7);
		REQUIRE(hp[defs::character_class::mage].size() == 3);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::mage][2] == 2);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);

		hp.raise_full();
		REQUIRE(hp.current_hps() == 14);
		REQUIRE(hp.max() == 14);
		REQUIRE(Approx(hp.health()).epsilon(0.01) == 1.0);
		REQUIRE(hp.limit() == 17);
		REQUIRE(hp.alive() == true);
		REQUIRE(hp[defs::character_class::fighter].size() == 3);
		REQUIRE(hp[defs::character_class::fighter][0] == 10);
		REQUIRE(hp[defs::character_class::fighter][1] == 6);
		REQUIRE(hp[defs::character_class::fighter][2] == 7);
		REQUIRE(hp[defs::character_class::mage].size() == 3);
		REQUIRE(hp[defs::character_class::mage][0] == 4);
		REQUIRE(hp[defs::character_class::mage][1] == 3);
		REQUIRE(hp[defs::character_class::mage][2] == 2);
		REQUIRE(hp[defs::character_class::cleric].size() == 3);
		REQUIRE(hp[defs::character_class::cleric][0] == 8);
		REQUIRE(hp[defs::character_class::cleric][1] == 6);
		REQUIRE(hp[defs::character_class::cleric][2] == 5);
	}
}

TEST_CASE("TC027 - Testing multi-store market and buy/sell features, random budget")
{
	character chr("Lancelot", defs::character_class::fighter, defs::character_race::human, defs::character_sex::male,
		defs::moral_alignment::lawful_neutral, true);

	for (auto cn : { defs::coin::copper, defs::coin::silver, defs::coin::electrum, defs::coin::gold, defs::coin::platinum })
		chr.money().get(cn) = 0;
	chr.money().get(defs::coin::gold) = 120;

	REQUIRE(chr.money().copper().value() == 0);
	REQUIRE(chr.money().silver().value() == 0);
	REQUIRE(chr.money().electrum().value() == 0);
	REQUIRE(chr.money().gold().value() == 120);
	REQUIRE(chr.money().platinum().value() == 0);
	
	market::market::init();

	SECTION("Winthrop's Store: random budget model and buy/sell spread")
	{
		uint32_t mktId = market::market::get_instance().create_store("Winthrop's Store", market::store::budget_model::random, true, true);

		for (auto w : { defs::equipment::item::battle_axe, defs::equipment::item::long_sword, defs::equipment::item::broad_sword,
				defs::equipment::item::composite_long_bow, defs::equipment::item::morning_star, defs::equipment::item::two_handed_sword })
		{
			market::market::get_instance()[mktId].add<equipment::weapon>(w);
		}
		for (auto a : { defs::equipment::item::full_plate, defs::equipment::item::hide, defs::equipment::item::plate_mail, defs::equipment::item::chain_mail })
		{
			market::market::get_instance()[mktId].add<equipment::armour>(a);
		}

		SECTION("Buying a broad sword")
		{
			defs::equipment::item itemID = defs::equipment::item::broad_sword;
			auto buyPrice = market::market::get_instance()[mktId].get_price(itemID, market::store::buy_sell_side::buy);
			auto sellPrice = market::market::get_instance()[mktId].get_price(itemID, market::store::buy_sell_side::sell);

			REQUIRE(buyPrice >= sellPrice);
			REQUIRE(market::market::get_instance()[mktId].in_stock(itemID) == true);
			REQUIRE(chr.money().check_availability(buyPrice) == true);

			auto it = market::market::get_instance()[mktId].buy(itemID);
			chr.money().subtract(buyPrice);
			chr.inventory_bag() += it;

			REQUIRE(chr.money().copper().value() == 0);
			REQUIRE(chr.money().silver().value() == 0);
			REQUIRE(chr.money().electrum().value() == 0);
			REQUIRE(chr.money().gold().value() == 120 - buyPrice);
			REQUIRE(chr.money().platinum().value() == 0);

			REQUIRE(market::market::get_instance()[mktId].in_stock(itemID) == false);
			REQUIRE(chr.money().check_availability(buyPrice) == true);

			REQUIRE_THROWS_AS(market::market::get_instance()[mktId].buy(itemID), std::exception);

			REQUIRE(chr.money().copper().value() == 0);
			REQUIRE(chr.money().silver().value() == 0);
			REQUIRE(chr.money().electrum().value() == 0);
			REQUIRE(chr.money().gold().value() == 120 - buyPrice);
			REQUIRE(chr.money().platinum().value() == 0);

			REQUIRE(market::market::get_instance()[mktId].count(itemID) == 0);
		}

		SECTION("Buying not affordable goods")
		{
			defs::equipment::item itemID = defs::equipment::item::full_plate;

			auto fullPlatePriceBuy = market::market::get_instance()[mktId].get_price(itemID, market::store::buy_sell_side::buy);
			auto fullPlatePriceSell = market::market::get_instance()[mktId].get_price(itemID, market::store::buy_sell_side::sell);

			REQUIRE(fullPlatePriceBuy >= fullPlatePriceSell);

			REQUIRE(market::market::get_instance()[mktId].count(itemID) == 1);
			auto prc = market::market::get_instance()[mktId].get_price(itemID, market::store::buy_sell_side::buy);
			REQUIRE(market::market::get_instance()[mktId].count(itemID) == 1);
			auto fp = market::market::get_instance()[mktId].buy(itemID);
			REQUIRE(market::market::get_instance()[mktId].count(itemID) == 0);
			REQUIRE(chr.money().check_availability(fullPlatePriceBuy) == false);
			REQUIRE_THROWS_AS(chr.money().subtract(fullPlatePriceBuy), std::exception);

			market::market::get_instance()[mktId].undo_buy(fp);
			REQUIRE(market::market::get_instance()[mktId].count(itemID) == 1);
		}

		SECTION("Buy out of stock goods")
		{
			defs::equipment::item itemID = defs::equipment::item::halberd;

			REQUIRE(market::market::get_instance()[mktId].count(itemID) == 0);
			REQUIRE(market::market::get_instance()[mktId].in_stock(itemID) == false);

			REQUIRE_THROWS_AS(market::market::get_instance()[mktId].get_price(itemID, market::store::buy_sell_side::buy), std::exception);
			auto sellPrice = market::market::get_instance()[mktId].get_price(itemID, market::store::buy_sell_side::sell);

			REQUIRE(market::market::get_instance()[mktId].count(itemID) == 0);
			REQUIRE(market::market::get_instance()[mktId].in_stock(itemID) == false);

			REQUIRE(sellPrice.value() > 0);
		}

		SECTION("Selling a two-handed sword")
		{
			defs::equipment::item itemID = defs::equipment::item::two_handed_sword;
			auto sword = equipment::items_pool::get_instance().create_as<equipment::weapon>(itemID);

			chr.inventory_bag() += sword;

			auto buyPrice = market::market::get_instance()[mktId].get_price(itemID, market::store::buy_sell_side::buy);
			auto sellPrice = market::market::get_instance()[mktId].get_price(itemID, market::store::buy_sell_side::sell);

			REQUIRE(buyPrice >= sellPrice);

			REQUIRE(chr.inventory_bag().find(itemID) != nullptr);
			auto prc = market::market::get_instance()[mktId].sell(sword);

			REQUIRE(prc == sellPrice);

			REQUIRE(chr.money().copper().value() == 0);
			REQUIRE(chr.money().silver().value() == 0);
			REQUIRE(chr.money().electrum().value() == 0);
			REQUIRE(chr.money().gold().value() == 120);
			REQUIRE(chr.money().platinum().value() == 0);

			chr.money().add(prc);

			REQUIRE(chr.money().copper().value() == 0);
			REQUIRE(chr.money().silver().value() == 0);
			REQUIRE(chr.money().electrum().value() == 0);
			REQUIRE(chr.money().gold().value() == 120 + prc);
			REQUIRE(chr.money().platinum().value() == 0);

			equipment::items_pool::get_instance().erase();
		}
	}
}

TEST_CASE("TC028 - Testing multi-store market and buy/sell features, unlimited budget")
{
	character chr("Gandalf", defs::character_class::mage, defs::character_race::elf, defs::character_sex::male,
		defs::moral_alignment::neutral_good, true);

	for (auto cn : { defs::coin::copper, defs::coin::silver, defs::coin::electrum, defs::coin::gold, defs::coin::platinum })
		chr.money().get(cn) = 0;
	chr.money().add(defs::coin::electrum, 10);

	REQUIRE(chr.money().copper().value() == 0);
	REQUIRE(chr.money().silver().value() == 0);
	REQUIRE(chr.money().electrum().value() == 10);
	REQUIRE(chr.money().gold().value() == 0);
	REQUIRE(chr.money().platinum().value() == 0);

	market::market::init();

	uint32_t mktId = market::market::get_instance().create_store("Friendly Arm's Store", market::store::budget_model::unlimited, true, true);

	for (auto i : { defs::equipment::item::quarterstaff, defs::equipment::item::knife, defs::equipment::item::full_plate,
				defs::equipment::item::hide, defs::equipment::item::plate_mail, defs::equipment::item::chain_mail,
				defs::equipment::item::robe_common, defs::equipment::item::tabard, defs::equipment::item::cloak_fine_fur })
	{
		market::market::get_instance()[mktId].add<equipment::item>(i);
	}

	SECTION("Gandalf buys a robe and get the change back")
	{
		defs::equipment::item itemID = defs::equipment::item::robe_common;
		auto price = market::market::get_instance()[mktId].get_price(itemID, market::store::buy_sell_side::buy);

		auto it = market::market::get_instance()[mktId].buy(itemID);
		chr.money().subtract(price);
		chr.inventory_bag() += it;

		REQUIRE(chr.money().copper().value() == 0);
		REQUIRE(chr.money().silver().value() == 50 - price.value());
		REQUIRE(chr.money().electrum().value() == 0);
		REQUIRE(chr.money().gold().value() == 0);
		REQUIRE(chr.money().platinum().value() == 0);

		REQUIRE(market::market::get_instance()[mktId].count(itemID) == 0);
		REQUIRE(market::market::get_instance()[mktId].in_stock(itemID) == false);
	}

	SECTION("Gandalf gets rich selling a fleet to the store")
	{
		uint32_t totalGold = chr.money().gold().value();
		defs::equipment::item itemId = defs::equipment::item::drakkar;

		uint32_t totalAmount = 0;
		for (int i = 0; i < 10; ++i)
		{
			equipment::item *it = equipment::items_pool::get_instance().create(itemId);

			auto prc = market::market::get_instance()[mktId].sell(it);
			chr.money().add(prc);
			totalAmount += prc.value();
		}

		REQUIRE(chr.money().copper().value() == 0);
		REQUIRE(chr.money().silver().value() == 0);
		REQUIRE(chr.money().electrum().value() == 10);
		REQUIRE(chr.money().gold().value() == totalGold + totalAmount);
		REQUIRE(chr.money().platinum().value() == 0);

		REQUIRE(market::market::get_instance()[mktId].count(itemId) == 10);

		equipment::items_pool::get_instance().erase();
	}
}

TEST_CASE("TC029 - Testing monsters creation and information retrieval")
{
	SECTION("A ghost can be turned by priests")
	{
		auto m = adnd::monster::monsters_den::get_instance().spawn(defs::monsters::monster::ghost);

		REQUIRE(m->turnable_as() == defs::turn_ability::turn_ghost);
	}

	SECTION("A banshee can be turned as special by priests")
	{
		auto m = adnd::monster::monsters_den::get_instance().spawn(defs::monsters::monster::banshee);

		REQUIRE(m->turnable_as() == defs::turn_ability::turn_special);
	}

	SECTION("A kobold cannot be turned by priests")
	{
		auto m = adnd::monster::monsters_den::get_instance().spawn(defs::monsters::monster::kobold);

		REQUIRE(m->turnable_as() == defs::turn_ability::turn_none);
	}
}