CHANGELOG
=====================

Foreword
=====================
Ddlib development is an ongoing process. There are some topics for which the library offers a shallow coverage.
They have been added to this release as personal commitment and included in the "Unreleased" section.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]
- Monsters:
	The complete list of AD&D monsters (TSR 2140 � Monstrous Manual (1993) � ISBN 1-56076-619-0) exists solely as definitions.
	At the moment it's not possible to instantiate monsters or retrieve their template.
- Spells:
	The complete list of spells is available. 
	There is no feature yet available to simulate the effect of a spell cast.
- Priests of specific mythos:
	The list of weapons allowed is not limited by the priest's creed. My plan is to allow for different weapons depending on the god worshipped.
- Character generator:
	This module should be able to make optimal decisions and generate a full character based on minimal input (skills, name and sex).
- Budget model in market::store definition:
	I may eventually decide to replace the budget model, specified when a store is created, with a "budget generator" class.
	This will enable the final user to define a custom budget model for a store, in addition to the standard models already available.

## [0.9.3b] - 2020-10-21
### Added
- New optional rule 'always_learn_new_spells' introduced.
	
## [0.9.2b] - 2020-10-20
### Changed
- The character constructor can no longer specify whether the skills are to be generated or not. They are always created with the standard method;
### Added
- The character class set_skills method can now be passed in a skill set other than the single skill values as before.

## [0.9.1b] - 2020-10-17
### Added
- This CHANGELOG file to hopefully keep track of the evolution of this project;
- A separate project for test cases;
- A separate project to demonstrate some of the most relevant features;
- Treasure generation and magical items;
- Spell book, and holy symbol likewise for priests, allow spell casters to learn/gain new spells and select them for usage;
- A mechanism to set/unset global configuration and enable/disable optional rules.

### Changed
- Class templates are now available for multiclasses;
- The data access layer has been reworked to be more flexible.