Wishlist & desiderata
=====================

This is a personal note, only in Italian (sorry) meant for internal use.
It describes what I would evenually like to see in the library.



Gestire incantesimi:
	bonus per sacerdoti di mito specifico/specialisti
Generazione di mostri:
	template
	classe monster_den come singleton per istanziare mostri
Introdurre la nozione di spazio per gestire i combattimenti ed il lancio di incantesimi con area d'effetto:
	classe play_ground come singleton cui aggiungere personaggi/mostri e le loro coordinate
		gestione combattimenti
			schedulazione eventi
			iniziativa
			tiri per colpire
			danni
			ferite
			morte
			controllo sul morale
			esperienza
			generazione report della batttaglia
		interazione tra personaggi
			scambio di oggetti

Memorandum
==========
	1)	Generatore di personaggio che sceglie classe ottimale in base allo skill set in input
	2)	Nel costruttore di market::store, sostituire budget_model con un budget generator, default ultimited (in overload del costruttore)