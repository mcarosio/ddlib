#pragma once

#include "defs.h"
#include "equipment.h"
#include "treasure.h"

#include <map>


namespace adnd
{
	namespace inventory
	{
		/**
		The projectile stock is an abstraction for a quiver. It may contain different kind of projectiles.
		It is aimed to contain those projectiles which must be made ready for use.
		Only available if a character owns a projectile containter (i.e. a quiver for arrows or quarrels),
		otherwise projectile are carried into the inventory and may not be directly accessible in combat.
		*/
		struct projectile_stock
		{
			projectile_stock() : m_count(0) {}

			unsigned short& operator=(const short& amount)
			{
				m_count = amount;
				return m_count;
			}
			projectile_stock& operator++()
			{
				++m_count;
				return (*this);
			}
			projectile_stock operator++(int)
			{
				projectile_stock stock;
				stock = m_count;
				++(*this);
				return stock;
			}
			projectile_stock operator--(int)
			{
				projectile_stock stock;
				stock = m_count;
				--(*this);
				return stock;
			}

			projectile_stock& operator--()
			{
				if (m_count == 0)
					throw std::exception("No projectile available for this weapon");
				m_count--;
				return (*this);
			}
			unsigned short& operator+=(const unsigned short& amount)
			{
				m_count += amount;
				return m_count;
			}
			unsigned short& operator-=(const unsigned short& amount)
			{
				if (m_count < amount)
					throw std::exception("No projectile available for this weapon");
				m_count -= amount;
				return m_count;
			}
			bool operator==(const short& amount)
			{
				return m_count == amount;
			}
			unsigned short count() { return m_count; }

		protected:
			unsigned short m_count;
		};

		class inventory_bag
		{
		public:
			inventory_bag();
			~inventory_bag();
		
			double weight();
			size_t count();
			inventory_bag& operator+=(equipment::item* it);
			equipment::wearable* wear(const defs::body_slot slot, equipment::wearable* it);

			//equipment::item* add(const defs::equipment::item& itemId);
			//void add(const defs::equipment::item& itemId, const short& count);
			void add(equipment::item* it);
			void add(const adnd::treasure::gem& g);
			void add(const adnd::treasure::object_of_art& o);
			equipment::item* remove(const uint32_t& itemUid);
			equipment::item* remove(const defs::equipment::item& itemId);
		
			const equipment::wearable* const head()		{ return m_wearableItems[defs::body_slot::head]; }
			const equipment::wearable* const body()		{ return m_wearableItems[defs::body_slot::body]; }
			const equipment::wearable* const feet()		{ return m_wearableItems[defs::body_slot::feet]; }
			const equipment::wearable* const neck()		{ return m_wearableItems[defs::body_slot::neck]; }
			const equipment::wearable* const arms()		{ return m_wearableItems[defs::body_slot::arms]; }
			const equipment::wearable* const mantle()	{ return m_wearableItems[defs::body_slot::mantle]; }
			const equipment::wearable* const belt()		{ return m_wearableItems[defs::body_slot::belt]; }

			//projectile_stock& operator[] (const defs::equipment::item& weaponId, const defs::equipment::item& projectileId)
			//{
			//	return get_projectile_stock(weaponId, projectileId);
			//}

			/**
			*	Does not account for weapons, only wearable items like gloves, gauntlets, etc...
			*/
			const equipment::wearable* const hands()	{ return m_wearableItems[defs::body_slot::hands]; }
		
			const equipment::wearable* const ring(const defs::hand_slot_type& handSlot)
			{
				return (handSlot == defs::hand_slot_type::primary) ? m_wearableItems[defs::body_slot::ring_right] : m_wearableItems[defs::body_slot::ring_left];
			}
		
			auto begin() { return m_collectedItems.begin(); }
			auto end() { return m_collectedItems.end(); }
			bool has(const defs::equipment::item& itemId);
			equipment::item* find(const uint32_t& itemId);
			equipment::item* find(const defs::equipment::item& itemId);

			template <class _ItemType>
			_ItemType* find_as(const uint32_t& itemId)
			{
				return static_cast<_ItemType*>(find(itemId));
			}

			template <class _ItemType>
			_ItemType* find_as(const defs::equipment::item& itemId)
			{
				return static_cast<_ItemType*>(find(itemId));
			}

		protected:
			std::map<uint32_t, equipment::item*>					m_collectedItems;
			std::map<defs::body_slot, equipment::wearable*>			m_wearableItems;
			//std::map<defs::equipment::item, uint16_t>				m_projectiles;
			std::map<defs::equipment::item, projectile_stock>			m_projectiles;
			std::map<uint32_t, adnd::treasure::gem>					m_collectedGems;
			std::map<uint32_t, adnd::treasure::object_of_art>		m_collectedObjectsOfArt;

			const defs::equipment::item get_projectile_container(const defs::equipment::item& projectileId);
			projectile_stock& get_projectile_stock(const defs::equipment::item& projectileId);
		};
	}
}