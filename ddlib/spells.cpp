#include "stdafx.h"
#include "spells.h"
#include "templates.h"

adnd::spells::spell_book::spell_book(const defs::character_class& cls)
	: m_classId(cls), m_intelligenceScore(defs::character_skill::intelligence, 9), m_casterLevel(1)
{
	std::string stmtId;
	auto classTempl = templates::metadata::get_instance().get_class_template(cls);

	if (classTempl.can_cast_wizard_spells())
	{
		m_spells.resize(defs::spell_book_max_level);

		// Initialised for a 1-st level spell caster
		for (int i = 0; i < defs::spell_book_max_level; ++i)
		{
			m_spells[i].level() = i+1;
			m_spells[i].accessible() = false;
		}
	}
}

/**
	Changes the spells availability according to the spell caster level and the intelligence score
	If the resize action causes the book to shrink:
	*	level(s) may become unavailable;
	*	random spells may be selected for deletion
	A record for deleted spells is kept, they don't get removed from the book, they simply become unavailable to the caster.
	
	In addition, whenever a specialist reaches a new spell level, he automatically gains one spell of his school to add to his spell books

*/
void adnd::spells::spell_book::resize(const adnd::skills::character_skill_value& intelligenceScore, const short& casterLevel)
{
	// Keep a record for the caster's intelligence and level
	m_intelligenceScore = intelligenceScore;
	m_casterLevel = casterLevel;

	auto classTempl = templates::metadata::get_instance().get_class_template(m_classId);

	// Trying to resize the book on a non spell caster class, raises an error
	if (!classTempl.can_cast_wizard_spells())
	{
		std::stringstream ss;
		ss << classTempl.get_long_name() << " cannot use a spell book";
		throw std::exception(ss.str().c_str());
	}

	auto sklTempl = templates::metadata::get_instance().get_skill_template(defs::character_skill::intelligence);
	auto intlStats = sklTempl.get_intelligence_stats(intelligenceScore);
	auto wsp = classTempl.get_wizard_spell_progression(casterLevel);

	// For each level in the book (index starting from 1)
	for (int i = 0; i < defs::spell_book_max_level; ++i)
	{
		bool nowAccessible = (i + 1 <= intlStats.spellLevel);
		if (wsp.spells[i] > 0 && nowAccessible && classTempl.is_specialist_wizard()
			&& !m_spells[i].has_specialist_spell())
		{
			/*
			A specialist gains one additional spell per spell level, provided the additional spell is
			taken in the specialist's school. Thus, a 1st-level illusionist could have two spells--one
			being any spell he knows and the other limited to spells of the illusion school.
			*/
			defs::spells::school schoolId = classTempl.specialist_school();
			m_spells[i].m_hasSpecialistSpell = true;
			auto resSpells = templates::metadata::get_instance().fetch<short>(templates::metadata::query::wizard_spells_per_school_and_level);
			auto spells = resSpells.filter<short>(static_cast<short>(schoolId)).filter<short>(i + 1).run();

			std::random_device rd;
			std::mt19937 generator(rd());
			std::uniform_int_distribution<int> distribution(0, spells.size()-1);
			auto offset = distribution(generator);

			auto it = spells.begin();
			std::advance(it, offset);
			auto spellId = static_cast<defs::spells::wizard_spell>(std::get<0>(*it));
			m_knownSpells[i+1].emplace(spellId);
			m_spells[i].add(spellId);
		}
		// i-th level is accessible if the caster's intelligence score allows for it
		m_spells[i].accessible() = nowAccessible;

		// The maximum number of spells for level is taken from the spell progression table
		//m_spells[i].limit() = wsp.spells[i];

		// This is the case when the spell book shrinks and spells must be selected for deletion
		while (m_spells[i].count() > wsp.spells[i])
		{
			m_spells[i].remove();
		}

		// If the optional rule is active, the total number of spells per level is checked against
		// the maximum allowed by the intelligence score
		if (configuration::get_instance().get(defs::config::optional::max_number_of_spells_per_level))
		{
			while (m_spells[i].count_distinct() > intlStats.maxNumberOfSpellsPerLevel)
				m_spells[i].remove();
		}
	}
}

/**
	Tries to add a spell to the book. The action may result in a failure if:
	(1)	the spell caster has no accesso to the school of magic connected to the spell;
	(2)	the spell level is not accessible to the spell caster (because of low intelligence score or level as a spell caster);
	(3)	there aren't free slots in the book level (i.e. the spell caster has already reached the maximum number of spell for the level, according the their intelligence score and level);
	(4)	the spell caster doesn't successfully pass the test to learn a new spell (the probability is given by the intelligence score);
	If the action fails because of (4), a record for the try is kept and the spell caster cannot retry to add this spell until
	they reach an higher level of experience.
*/
bool adnd::spells::spell_book::add(const defs::spells::wizard_spell& spellId)
{
	auto classTempl = templates::metadata::get_instance().get_class_template(m_classId);
	if (!classTempl.can_cast_wizard_spells())
	{
		std::stringstream ss;
		ss << classTempl.get_long_name() << " cannot use a spell book";
		throw std::exception(ss.str().c_str());
	}

	auto t = templates::metadata::get_instance().get_wizard_spell_template(spellId);
	auto sklTempl = templates::metadata::get_instance().get_skill_template(defs::character_skill::intelligence);
	auto intlStats = sklTempl.get_intelligence_stats(m_intelligenceScore);
	auto wsp = classTempl.get_wizard_spell_progression(m_casterLevel);

	if (classTempl.get_school_access(spellId) == defs::spells::school_access::opposite)
	{
		std::stringstream ss;
		ss << classTempl.get_long_name() << " cannot access " << t.name() << ". School of magic not allowed";
		throw std::exception(ss.str().c_str());
	}

	auto& lvl = m_spells[t.level() - 1];

	// Level not enabled, because of low intelligence score or low experience level
	if (!lvl.accessible() || capacity(lvl.level()) == 0)
	{
		std::stringstream ss;
		ss << "Unable to add " << t.name() << " to level " << lvl.level() << ". Level not accessible";
		throw std::exception(ss.str().c_str());
	}

	if (configuration::get_instance().get(defs::config::optional::max_number_of_spells_per_level))
	{
		if (m_knownSpells[t.level()].size() == intlStats.maxNumberOfSpellsPerLevel)
		{
			// No room for new spells...
			std::stringstream ss;
			ss << "Unable to learn " << t.name() << ". Maximum number of spells reached per level " << t.level();
			throw std::exception(ss.str().c_str());
		}
	}

	// Let's check if the wizard tried to learn this spell before
	if (m_spellLearningFailure.find(spellId) != m_spellLearningFailure.end()
		&& m_spellLearningFailure[spellId] >= m_casterLevel)
	{
		// He tried and failed
		std::stringstream ss;
		ss << "Level " << m_spellLearningFailure[spellId] + 1 << " is required to try again with " << t.name();
		throw std::exception(ss.str().c_str());
	}
	// The spell has never been encountered before or he has advanced in experience

	if (configuration::get_instance().get(defs::config::optional::always_learn_new_spells) == false)
	{
		//Let's check if the wizard can learn this spell...
		auto d = random::die(defs::die_faces::ten);
		auto prob = ((d.roll<short>() - 1) * 10 + (d.roll<short>() - 1)) + 1;

		/*
		Specialists receive a bonus of +15% when learning spells from their school and a
		penalty of -15% when learning spells from other schools. The bonus or penalty is applied
		to the percentile dice roll the player must make when the character tries to learn a new spell
		*/
		short bonus = 0;
		if (classTempl.is_specialist_wizard())
		{
			bonus = classTempl.get_school_access(spellId) == defs::spells::school_access::specialist ? 15 : -15;
		}

		if (prob > intlStats.chanceToLearnSpell + bonus)
		{
			/*
			If the wizard fails the roll, he cannot check that spell again until he advances to the next level
			(provided he still has access to the spell).
			In case of failure, the spell ID and the caster level are recorder for future use
			*/
			m_spellLearningFailure[spellId] = m_casterLevel;
			return false;
		}
	}

	if (m_knownSpells[t.level()].find(spellId) != m_knownSpells[t.level()].end())
	{
		// Already scribed in the book
		std::stringstream ss;
		ss << t.name() << " is already scribed in the spell book";
		throw std::exception(ss.str().c_str());
	}

	/*
		From now on, this spell is marked as learnt.
		Once a wizard has learned the maximum number of spells he is allowed in a given spell level,
		he cannot add any more spells of that level to his spell book (unless the optional spell research system is used).
		Once a spell is learned, it cannot be unlearned and replaced by a new spell.
	*/
	m_knownSpells[t.level()].emplace(spellId);

	lvl.add(spellId);
	return true;
}

/**
	Sets a spell as castable in the book. The action may result in a failure if:
	(1)	the spell level is not accessible to the spell caster (because of low intelligence score or level as a spell caster);
	(2)	there aren't free slots in the book level;
	(3)	the spell is not written in the book.
*/
void adnd::spells::spell_book::select(const defs::spells::wizard_spell& spellId)
{
	auto classTempl = templates::metadata::get_instance().get_class_template(m_classId);
	if (!classTempl.can_cast_wizard_spells())
	{
		std::stringstream ss;
		ss << classTempl.get_long_name() << " cannot use a spell book";
		throw std::exception(ss.str().c_str());
	}

	auto t = templates::metadata::get_instance().get_wizard_spell_template(spellId);
	auto wsp = classTempl.get_wizard_spell_progression(m_casterLevel);

	auto& lvl = m_spells[t.level() - 1];

	if (!lvl.accessible())
	{
		std::stringstream ss;
		ss << "Unable to select " << t.name() << " from level " << lvl.level() << ". Level not accessible";
		throw std::exception(ss.str().c_str());
	}

	if (lvl.count() == capacity(lvl.level()))
	{
		std::stringstream ss;
		ss << "Unable to select " << t.name() << " from level " << lvl.level() << ". Maximum capacity reached";
		throw std::exception(ss.str().c_str());
	}

	if (!lvl.find(spellId))
	{
		std::stringstream ss;
		ss << "Unable to select " << t.name() << ". The spell is not available";
		throw std::exception(ss.str().c_str());
	}

	lvl.select(spellId);
}

void adnd::spells::spell_book::remove(const adnd::defs::spells::wizard_spell& spellId)
{
	auto classTempl = templates::metadata::get_instance().get_class_template(m_classId);
	if (!classTempl.can_cast_wizard_spells())
	{
		std::stringstream ss;
		ss << classTempl.get_long_name() << " cannot use a spell book";
		throw std::exception(ss.str().c_str());
	}

	auto t = templates::metadata::get_instance().get_wizard_spell_template(spellId);
	auto& lvl = m_spells[t.level() - 1];

	if (!lvl.find(spellId))
	{
		std::stringstream ss;
		ss << "Unable to remove " << t.name() << " from level " << lvl.level() << ". Spell not found";
		throw std::exception(ss.str().c_str());
	}

	lvl.remove(spellId);
}

/**
	Returns the maximum level available to the spell caster
*/
short adnd::spells::spell_book::max_level()
{
	auto classTempl = templates::metadata::get_instance().get_class_template(m_classId);
	if (!classTempl.can_cast_wizard_spells())
	{
		return 0;
	}

	auto wsp = classTempl.get_wizard_spell_progression(m_casterLevel);
	for (int i = defs::spell_book_max_level; i>0; i--)
	{
		if (wsp.spells[i - 1] > 0)
		{
			auto st = templates::metadata::get_instance().get_skill_template(defs::character_skill::intelligence);
			auto stats = st.get_intelligence_stats(m_intelligenceScore);
			return std::min<short>(i, stats.spellLevel);
		}
	}

	return 0;
}

short adnd::spells::spell_book::capacity(const short& level)
{
	if (level <= 0 || level > defs::spell_book_max_level)
		throw std::exception("Invalid spell book level", level);

	if (level > max_level())
		return 0;

	auto classTempl = adnd::templates::metadata::get_instance().get_class_template(m_classId);
	auto wsp = classTempl.get_wizard_spell_progression(m_casterLevel);

	/*
		Whenever a specialist reaches a new spell level, he automatically gains one spell of his school
		to add to his spell books.
		One additional slot is made available to specialists. The check on the spell's school is performed in resize()
	*/
	short bonusForSpecialist = 0;
	if (wsp.spells[level-1] > 0 && classTempl.is_specialist_wizard())
	{
		bonusForSpecialist = 1;
	}

	return wsp.spells[level-1] + bonusForSpecialist;
}

short adnd::spells::spell_book::limit(const short& level) const
{
	if (configuration::get_instance().get(defs::config::optional::max_number_of_spells_per_level))
	{
		auto sklTempl = templates::metadata::get_instance().get_skill_template(defs::character_skill::intelligence);
		auto intlStats = sklTempl.get_intelligence_stats(m_intelligenceScore);
		return intlStats.maxNumberOfSpellsPerLevel;
	}
	return -1;
}

const adnd::spells::spell_level<adnd::defs::spells::wizard_spell>& adnd::spells::spell_book::operator[](const int& lvl)
{
	auto classTempl = adnd::templates::metadata::get_instance().get_class_template(m_classId);
	if (!classTempl.can_cast_wizard_spells())
	{
		std::stringstream ss;
		ss << classTempl.get_long_name() << " cannot use a spell book";
		throw std::exception(ss.str().c_str());
	}

	if (lvl <= 0 || lvl > max_level())
		throw std::exception("Index out of bound", lvl);

	return m_spells[lvl - 1];
}

const adnd::spells::spell_entry& adnd::spells::spell_book::lookup(const adnd::defs::spells::wizard_spell& spellId) const
{
	auto t = adnd::templates::metadata::get_instance().get_wizard_spell_template(spellId);
	bool found = m_spells[t.level() - 1].find(spellId);
	if (!found)
		throw std::exception("Spell not found", static_cast<int>(spellId));

	return m_spells[t.level() - 1].m_spells.at(spellId);
}

bool adnd::spells::spell_book::find(const adnd::defs::spells::wizard_spell& spellId) const
{
	auto t = adnd::templates::metadata::get_instance().get_wizard_spell_template(spellId);
	return m_spells[t.level() - 1].find(spellId);
}

void adnd::spells::spell_book::study(const short& hours)
{
	short spellsCount = std::div(hours, defs::rest_hours_per_spell).quot;

	short lvl = 0;
	while (lvl < max_level() && spellsCount > 0)
	{
		auto it = m_spells[lvl].cbegin();
		auto end = m_spells[lvl].cend();
		while (it != end && spellsCount > 0)
		{
			m_spells[lvl].memorise(it->first, spellsCount);
			++it;
		}
		lvl++;
	}
}

short adnd::spells::spell_book::study_all()
{
	short spellsCount = 0;

	for (short lvl = 0; lvl < max_level(); ++lvl)
	{
		auto it = m_spells[lvl].cbegin();
		auto end = m_spells[lvl].cend();
		while (it != end)
		{
			short count = it->second.count_total - it->second.count_castable;
			spellsCount += count;
			m_spells[lvl].memorise(it->first, count);
			++it;
		}
	}
	return spellsCount * defs::rest_hours_per_spell;
}

short adnd::spells::spell_book::study_required()
{
	short spellsCount = 0;

	for (short lvl = 0; lvl < max_level(); ++lvl)
	{
		auto it = m_spells[lvl].cbegin();
		auto end = m_spells[lvl].cend();
		while (it != end)
		{
			spellsCount += it->second.count_total - it->second.count_castable;
			++it;
		}
	}
	return spellsCount * defs::rest_hours_per_spell;
}

void adnd::spells::spell_book::cast(const defs::spells::wizard_spell& spellId)
{
	auto classTempl = templates::metadata::get_instance().get_class_template(m_classId);
	if (!classTempl.can_cast_priest_spells())
	{
		std::stringstream ss;
		ss << classTempl.get_long_name() << " cannot use a holy symbol";
		throw std::exception(ss.str().c_str());
	}

	auto t = templates::metadata::get_instance().get_wizard_spell_template(spellId);
	auto& lvl = m_spells[t.level() - 1];

	if (!lvl.find(spellId))
	{
		std::stringstream ss;
		ss << "Unable to cast " << t.name() << ". Unknown spell";
		throw std::exception(ss.str().c_str());
	}

	if (lvl.m_spells[spellId].count_castable == 0)
	{
		std::stringstream ss;
		ss << "Not enought power to cast " << t.name();
		throw std::exception(ss.str().c_str());
	}

	lvl.m_spells[spellId].count_castable--;
}

adnd::spells::holy_symbol::holy_symbol(const defs::character_class& cls, const defs::deities::deity& deityId)
	: m_classId(cls), m_deityId(deityId), m_useBonusForWisdom(false)
{
	m_wisdomScore = skills::character_skill_value(defs::character_skill::wisdom, 9);
	m_casterLevel = 1;

	std::string stmtId;

	auto classTempl = templates::metadata::get_instance().get_class_template(cls);
	auto cc = classTempl.get_class_composition();

	if (cc.is_type_of(defs::character_class_type::priest))
	{
		m_useBonusForWisdom = true;
	}

	if (classTempl.can_cast_priest_spells())
	{
		m_spells.resize(defs::holy_symbol_max_level);

		// Initialised for a 1-st level spell caster
		for (int i = 0; i < defs::holy_symbol_max_level; ++i)
		{
			m_spells[i].level() = i + 1;
			auto numSpells = capacity(i + 1);// psp.spells[i] + get_bonus_spells(i + 1);
			m_spells[i].accessible() = (numSpells > 0);

			//// Select spells for priests of specific mythos, according to the god worshipped
			//if (m_classId == defs::character_class::priest_of_specific_mythos)
			//{
			//	auto res = templates::metadata::get_instance().fetch<uint16_t>(templates::metadata::query::priest_spells_per_deity_and_level);
			//	auto rSpells = res.filter<uint16_t>(static_cast<uint16_t>(m_deityId)).filter<uint16_t>(static_cast<uint16_t>(i + 1)).run();
			//	
			//	for (auto s : rSpells)
			//	{
			//		auto spellId = static_cast<defs::spells::priest_spell>(std::get<0>(s));
			//		m_spells[i].add(spellId);
			//	}
			//}
			//else
			//{
			//	populate_level(cls, 1, i + 1);
			//}
		}
	}
}

void adnd::spells::holy_symbol::populate_level(const defs::character_class& cls, const short& casterLevel, const short& spellLevel)
{
	auto classTempl = templates::metadata::get_instance().get_class_template(cls);
	auto cc = classTempl.get_class_composition();

	defs::character_class casterClass = defs::character_class::none;
	if (cc.is(defs::character_class::druid))
	{
		casterClass = defs::character_class::druid;
	}
	else if (cc.is(defs::character_class::paladin))
	{
		casterClass = defs::character_class::paladin;
	}
	else if (cc.is(defs::character_class::ranger))
	{
		casterClass = defs::character_class::ranger;
	}
	else if (cc.is(defs::character_class::cleric))
	{
		casterClass = defs::character_class::cleric;
	}

	// Select priest spells for clerics, druids, paladins and rangers
	if (casterClass != defs::character_class::none)
	{
		auto p = templates::metadata::get_character_type_and_id(cls);
		auto res = templates::metadata::get_instance().fetch<short, short, short>(templates::metadata::query::priest_spells_per_level_and_class);
		auto rSpells = res.filter<uint16_t>(spellLevel).filter<uint32_t>(static_cast<uint32_t>(p.classId)).run();

		for (auto s : rSpells)
		{
			auto spellId = static_cast<defs::spells::priest_spell>(std::get<0>(s));
			auto accessLevel = static_cast<defs::spells::access_level>(std::get<1>(s));

			if (accessLevel == defs::spells::access_level::major
				|| (accessLevel == defs::spells::access_level::minor && spellLevel <= 4))
			{
				m_spells[spellLevel-1].add(spellId);
			}
		}
	}
}

void adnd::spells::holy_symbol::resize(const adnd::skills::character_skill_value& wisdomScore, const short& casterLevel)
{
	// Keep a record for the caster's wisdom and level
	m_wisdomScore = wisdomScore;
	m_casterLevel = casterLevel;

	auto classTempl = templates::metadata::get_instance().get_class_template(m_classId);
	if (!classTempl.can_cast_priest_spells())
	{
		std::stringstream ss;
		ss << classTempl.get_long_name() << " cannot use a holy symbol";
		throw std::exception(ss.str().c_str());
	}

	
	// For each level in the holy symbol (index starting from 1)
	for (int i = 0; i < defs::holy_symbol_max_level; ++i)
	{
		auto numSpells = capacity(i + 1);
		m_spells[i].accessible() = (numSpells > 0);

		// Select spells for priests of specific mythos, according to the god worshipped
		if (m_classId == defs::character_class::priest_of_specific_mythos)
		{
			auto res = templates::metadata::get_instance().fetch<uint16_t>(templates::metadata::query::priest_spells_per_deity_and_level);
			auto rSpells = res.filter<uint16_t>(static_cast<uint16_t>(m_deityId)).filter<uint16_t>(static_cast<uint16_t>(i + 1)).run();
				
			for (auto s : rSpells)
			{
				auto spellId = static_cast<defs::spells::priest_spell>(std::get<0>(s));
				m_spells[i].add(spellId);
			}
		}
		else
		{
			populate_level(m_classId, casterLevel, i + 1);
		}

		while (m_spells[i].count() > numSpells)
		{
			m_spells[i].remove();
		}
	}
}

void adnd::spells::holy_symbol::select(const defs::spells::priest_spell& spellId)
{
	auto classTempl = templates::metadata::get_instance().get_class_template(m_classId);
	if (!classTempl.can_cast_priest_spells())
	{
		std::stringstream ss;
		ss << classTempl.get_long_name() << " cannot use a holy symbol";
		throw std::exception(ss.str().c_str());
	}

	auto t = templates::metadata::get_instance().get_priest_spell_template(spellId);
	//auto sklTempl = templates::metadata::get_instance().get_skill_template(defs::character_skill::wisdom);
	//auto wisStats = sklTempl.get_wisdom_stats(m_wisdomScore);
	auto psp = classTempl.get_priest_spell_progression(m_casterLevel);

	auto& lvl = m_spells[t.level() - 1];

	if (!lvl.accessible())
	{
		std::stringstream ss;
		ss << "Unable to select " << t.name() << " from level " << lvl.level() << ". Level not accessible";
		throw std::exception(ss.str().c_str());
	}
	else if (lvl.count() == capacity(lvl.level()))
	{
		std::stringstream ss;
		ss << "Unable to select " << t.name() << " from level " << lvl.level() << ". Maximum capacity reached";
		throw std::exception(ss.str().c_str());
	}
	
	if (!lvl.find(spellId))
	{
		std::stringstream ss;
		ss << "Unable to select " << t.name() << ". The spell is not available";
		throw std::exception(ss.str().c_str());
	}

	lvl.select(spellId);
}

void adnd::spells::holy_symbol::remove(const adnd::defs::spells::priest_spell& spellId)
{
	auto classTempl = templates::metadata::get_instance().get_class_template(m_classId);
	if (!classTempl.can_cast_priest_spells())
	{
		std::stringstream ss;
		ss << classTempl.get_long_name() << " cannot use a holy symbol";
		throw std::exception(ss.str().c_str());
	}

	auto t = templates::metadata::get_instance().get_priest_spell_template(spellId);
	auto& lvl = m_spells[t.level() - 1];

	if (!lvl.find(spellId))
	{
		std::stringstream ss;
		ss << "Unable to remove " << t.name() << " from level " << lvl.level() << ". Spell not found";
		throw std::exception(ss.str().c_str());
	}

	lvl.remove(spellId);
}

/**
	Returns the maximum level available to the priest
*/
short adnd::spells::holy_symbol::max_level()
{
	auto classTempl = templates::metadata::get_instance().get_class_template(m_classId);
	if (!classTempl.can_cast_priest_spells())
	{
		return 0;
	}

	auto psp = classTempl.get_priest_spell_progression(m_casterLevel);
	for (int i = defs::holy_symbol_max_level; i > 0; i--)
	{
		if (psp.spells[i - 1] > 0)
		{
			return i;
		}
	}
	return 0;
}

short adnd::spells::holy_symbol::capacity(const short& level)
{
	if (level <= 0 || level > defs::holy_symbol_max_level)
		throw std::exception("Index out of bound", level);

	if (level > max_level())
		return 0;

	auto classTempl = adnd::templates::metadata::get_instance().get_class_template(m_classId);
	auto psp = classTempl.get_priest_spell_progression(m_casterLevel);

	return psp.spells[level-1] + get_bonus_spells(level);
}

const adnd::spells::spell_level<adnd::defs::spells::priest_spell>& adnd::spells::holy_symbol::operator[](const int& lvl)
{
	auto classTempl = adnd::templates::metadata::get_instance().get_class_template(m_classId);
	if (!classTempl.can_cast_priest_spells())
	{
		std::stringstream ss;
		ss << classTempl.get_long_name() << " cannot use a holy symbol";
		throw std::exception(ss.str().c_str());
	}

	if (lvl <= 0 || lvl > max_level())
		throw std::exception("Index out of bound", lvl);

	return m_spells[lvl - 1];
}

/**
	Returns the bonus spells for higher wisdom score.
	This applies only to priests and druids, rangers and paladins have no benefit from it
*/
short adnd::spells::holy_symbol::get_bonus_spells(const short & level)
{
	if (m_useBonusForWisdom)
	{
		auto sklTempl = templates::metadata::get_instance().get_skill_template(defs::character_skill::wisdom);
		auto wisStats = sklTempl.get_wisdom_stats(m_wisdomScore);
		return wisStats.bonusSpellLevel(level);
	}
	
	return 0;
}

void adnd::spells::holy_symbol::pray(const short& hours)
{
	short spellsCount = std::div(hours, defs::rest_hours_per_spell).quot;

	short lvl = 0;
	while (lvl < max_level() && spellsCount > 0)
	{
		auto it = m_spells[lvl].cbegin();
		auto end = m_spells[lvl].cend();
		while (it != end && spellsCount > 0)
		{
			m_spells[lvl].memorise(it->first, spellsCount);
			++it;
		}
		lvl++;
	}
}

short adnd::spells::holy_symbol::pray_all()
{
	short spellsCount = 0;

	for (short lvl = 0; lvl < max_level(); ++lvl)
	{
		auto it = m_spells[lvl].cbegin();
		auto end = m_spells[lvl].cend();
		while (it != end)
		{
			short count = it->second.count_total - it->second.count_castable;
			spellsCount += count;
			m_spells[lvl].memorise(it->first, count);
			++it;
		}
	}
	return spellsCount * defs::rest_hours_per_spell;
}

short adnd::spells::holy_symbol::pray_required()
{
	short spellsCount = 0;

	for (short lvl = 0; lvl < max_level(); ++lvl)
	{
		auto it = m_spells[lvl].cbegin();
		auto end = m_spells[lvl].cend();
		while (it != end)
		{
			spellsCount  += it->second.count_total - it->second.count_castable;
			++it;
		}
	}
	return spellsCount * defs::rest_hours_per_spell;
}

void adnd::spells::holy_symbol::cast(const defs::spells::priest_spell& spellId)
{
	auto classTempl = templates::metadata::get_instance().get_class_template(m_classId);
	if (!classTempl.can_cast_priest_spells())
	{
		std::stringstream ss;
		ss << classTempl.get_long_name() << " cannot use a holy symbol";
		throw std::exception(ss.str().c_str());
	}

	auto t = templates::metadata::get_instance().get_priest_spell_template(spellId);
	auto& lvl = m_spells[t.level() - 1];

	if (!lvl.find(spellId))
	{
		std::stringstream ss;
		ss << "Unable to cast " << t.name() << ". Unknown spell";
		throw std::exception(ss.str().c_str());
	}

	if (lvl.m_spells[spellId].count_castable == 0)
	{
		std::stringstream ss;
		ss << "Not enought power to cast " << t.name();
		throw std::exception(ss.str().c_str());
	}

	lvl.m_spells[spellId].count_castable--;
}