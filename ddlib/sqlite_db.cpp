//#include "sqlite3.h"
#include "sqlite_db.h"
#include "utils.h"

#include <fstream>

db::sqlite_db::sqlite_db(void)
	: m_status(status::unknown), m_dbConnection(nullptr), m_dbSource(db::sqlite_db::source::file), m_errorHandler(db::sqlite_db::error_handler_style::exception)
{
}

db::sqlite_db::~sqlite_db(void)
{
	close(false);
}

void db::sqlite_db::error(const char* text)
{
	if (m_errorHandler == db::sqlite_db::error_handler_style::exception)
	{
			throw std::exception(text);
	}
	else
	{
		m_lastError.assign(text);
	}
}

void db::sqlite_db::error(const std::string& text)
{
	error(text.c_str());
}

std::string db::sqlite_db::db_location()
{
	if (m_dbSource == db::sqlite_db::source::file)
	{
		return m_dbPath;
	}
	else if (m_dbSource == db::sqlite_db::source::memory)
	{
		return "file:" +m_dbPath+ "?mode=memory&cache=shared";
	}
	
	//throw std::exception();
	error("Invalid database source");
	return "";
}

bool db::sqlite_db::bind()
{	
	if (m_dbPath.empty())
	{
		error("Database file not specified");
		return false;
	}
	
	m_status = db::sqlite_db::status::bound;
	return true;
}

bool db::sqlite_db::open(const bool deleteLocal/*=false*/)
{
	if (m_status != db::sqlite_db::status::bound)
	{
		error("Database not bound");
		return false;
	}

	std::string path = db_location();

	//if (m_dbSource == source::file && deleteLocal && std::experimental::filesystem::exists(path))
	//{
	//	std::error_code errCode;
	//	if (!std::experimental::filesystem::remove(std::experimental::filesystem::path(path), errCode))
	//	{
	//		std::stringstream errMsg;
	//		errMsg << "Unable to delete local database: error " << errCode;
	//		error(errMsg.str());
	//		return false;
	//	}
	//}
	if (deleteLocal)
		drop_tables();

	//sqlite3_enable_shared_cache(0);
	int ret = sqlite3_open_v2(path.c_str(), &m_dbConnection, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_URI, NULL);
	if (ret != SQLITE_OK)
	{
		error(sqlite3_errstr(ret));
		return false;
	}
	m_status = db::sqlite_db::status::open;

	return true;
}

void db::sqlite_db::drop_tables()
{
	auto stmtSelectTables = prepare_statement("SELECT NAME FROM SQLITE_MASTER WHERE TYPE = 'table' AND NAME NOT LIKE 'sqlite_%'");
	db::sqlite_db::iterator it, end;
	std::list<std::string> dropList;
	execute(stmtSelectTables, it, end);
	while (it != end)
	{
		auto tabName = it.data<std::string>(1, "");
		std::stringstream ss;
		ss << "DROP TABLE " << tabName << ";";
		dropList.push_back(ss.str());
		++it;
	}
	for (auto dropStmt : dropList)
		execute(dropStmt);
}

bool db::sqlite_db::close(bool saveData)
{
	if (!is_open())
	{
		error("Database is not in open state");
		return false;
	}

	if (saveData && m_dbSource == db::sqlite_db::source::memory)
	{
		if (!save(m_dbPath))
		{
			error("Error saving data");
			return false;
		}
	}

	m_preparedStatements.erase(m_preparedStatements.begin(), m_preparedStatements.end());
	m_preparedStatementNames.erase(m_preparedStatementNames.begin(), m_preparedStatementNames.end());
	
	if (sqlite3_close_v2(m_dbConnection) != SQLITE_OK)
	{
		error("Error closing database");
		return false;
	}

	m_status = db::sqlite_db::status::close;
	return true;
}

void db::sqlite_db::clean_prepared_statements()
{
	 for(auto stmt : m_preparedStatements)
	 {
		 sqlite3_finalize(stmt.second);
	 }
	 m_preparedStatements.clear();
}

bool db::sqlite_db::clean_db(bool dropTables)
{
	std::string select_stmt = "SELECT name FROM sqlite_master WHERE type='table';";
	int ret = SQLITE_OK;

	db::sqlite_db::iterator it = begin(select_stmt);
	db::sqlite_db::iterator eit = end();
	std::string stmt;
	std::list<std::string> tables;
	while (it != eit)
	{
		std::string table = std::string(it.data<std::string>(1, ""));
		tables.push_back(table);
		++it;
	}

	for (auto t : tables)
	{
		stmt = (dropTables) ? "DROP TABLE IF EXISTS " + t + ";" : "DELETE FROM " + t + ";";
		if (!execute(stmt))
		{
			ret = SQLITE_ERROR;
		}
	}
	return ret == SQLITE_OK;
}

bool db::sqlite_db::execute(const char* sqlScript)
{
	if (!is_open())
	{
		error("Database is not in open state");
		return false;
	}

	char *zErrMsg = nullptr;
	int ret = sqlite3_exec(m_dbConnection, sqlScript, nullptr, 0, &zErrMsg);

	if (ret != SQLITE_OK)
	{
		error(zErrMsg);
		sqlite3_free(zErrMsg);
		return false;
	}
	return true;
}

sqlite3_stmt* db::sqlite_db::get_prepared_statement(const db::sqlite_db::statement_id& stmtId)
{
	prepared_statement_set::iterator it = m_preparedStatements.find(stmtId);
	if (it == m_preparedStatements.end())
	{
		error("Invalid prepared statement id");
		return false;
	}
	return m_preparedStatements[stmtId];
}

bool db::sqlite_db::execute(const db::sqlite_db::statement_id& stmtId)
{
	sqlite3_stmt *prepStmt = get_prepared_statement(stmtId);

	int ric = sqlite3_step(prepStmt);
	if (ric == SQLITE_DONE)
	{
		sqlite3_reset(prepStmt);
	}
	else if (ric != SQLITE_DONE && ric != SQLITE_ROW)
	{
		sqlite3_reset(prepStmt);
		error(sqlite3_errmsg(m_dbConnection));
		return false;
	}
	return true;
}

bool db::sqlite_db::execute(const std::experimental::filesystem::path& scriptPath)
{
	if (!std::experimental::filesystem::exists(scriptPath))
	{
		std::string err = "Unable to open file " + scriptPath.string();
		error(err.c_str());
		return false;
	}
	std::ifstream in(scriptPath.c_str(), std::ios::in);
	std::string sqlText;

	in.seekg(0, std::ios::end);   
	sqlText.reserve(static_cast<size_t>(in.tellg()));
	in.seekg(0, std::ios::beg);

	sqlText.assign((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());

	return execute(sqlText.c_str());
}

bool db::sqlite_db::execute(const std::string& sqlScript)
{
	return db::sqlite_db::execute(sqlScript.c_str());
}

bool db::sqlite_db::execute(const db::sqlite_db::statement_id& stmtId, db::sqlite_db::iterator& itBegin, db::sqlite_db::iterator& itEnd)
{
	sqlite3_stmt *prepStmt = get_prepared_statement(stmtId);

	auto b = begin(prepStmt);
	auto e = end();
	itBegin = b;
	itEnd = e;

	return true;
}

bool db::sqlite_db::execute(const db::sqlite_db::statement_id& stmtId, db::sqlite_db::record& firstRow)
{
	sqlite3_stmt *prepStmt = get_prepared_statement(stmtId);

	db::sqlite_db::iterator itBegin = begin(prepStmt);
	db::sqlite_db::iterator itEnd = end();

	if (itBegin != itEnd)
	{
		firstRow = *itBegin;
	}
	
	return sqlite3_reset(prepStmt) == SQLITE_OK;
}

bool db::sqlite_db::execute(const std::string &stmt, db::sqlite_db::record& firstRow)
{
	db::sqlite_db::iterator itBegin = begin(stmt);
	db::sqlite_db::iterator itEnd = end();

	if (itBegin != itEnd)
	{
		firstRow = *itBegin;
	}

	return true;
}

int exec_stmt_callback(void *data, int argc, char **argv, char **azColName)
{
	const db::sqlite_db::visit_callback *f = (db::sqlite_db::visit_callback*)data;
	db::sqlite_db::record r;
	r.reserve(argc);
	for (int i=0; i<argc; ++i)
	{
		if (argv[i] == nullptr)
			r.push_back("");
		else
			r.push_back(argv[i]);
	}
	(*f)(r);
	return 0;
}

bool db::sqlite_db::execute(const char* stmt, const db::sqlite_db::visit_callback &f)
{
	char *zErrMsg = NULL;
	
	m_lastError = "";
	int (*cbk)(void*, int, char**, char**) = exec_stmt_callback;
	int ret = sqlite3_exec(m_dbConnection, stmt, cbk, (void*)&f, &zErrMsg);

	if (ret != SQLITE_OK)
	{
		error(zErrMsg);
		sqlite3_free(zErrMsg);
		return false;
	}
	return true;
}

bool db::sqlite_db::execute(const std::string& stmt, const db::sqlite_db::visit_callback &f)
{
	return execute(stmt.c_str(), f);
}

bool db::sqlite_db::execute(const db::sqlite_db::statement_id& stmtId, const visit_callback& f)
{
	sqlite3_stmt *prepStmt = get_prepared_statement(stmtId);

	db::sqlite_db::iterator itBegin = begin(prepStmt);
	db::sqlite_db::iterator itEnd = end();

	while (itBegin != itEnd)
	{
		f(*itBegin);
		++itBegin;
	}
	
	return sqlite3_reset(prepStmt) == SQLITE_OK;
}

bool db::sqlite_db::explain_query_plan(const std::string &sql, std::vector<std::string> &output)
{
	int ric = 0;
	sqlite3_stmt *stmt = nullptr;
	std::string query = "EXPLAIN QUERY PLAN " + sql;

	ric = sqlite3_prepare_v2(m_dbConnection, sql.c_str(), static_cast<int>(sql.length()), &stmt, NULL);
	if (ric != SQLITE_OK) 
	{
		error(sqlite3_errmsg(m_dbConnection));
		return false;
	}

	db::sqlite_db::iterator itBegin = begin(stmt);
	db::sqlite_db::iterator itEnd = begin(stmt);
	std::stringstream ss;
	while (itBegin != itEnd)
	{
		int iSelectid = itBegin.data<int>(0, -1);
		int iOrder = itBegin.data<int>(1, -1);
		int iFrom = itBegin.data<int>(2, -1);
		std::string strDetail = itBegin.data<std::string>(3, "");

		ss << iSelectid << iOrder << iFrom << strDetail;
		output.push_back(ss.str());
	}

	sqlite3_finalize(stmt);

	return true;
}

db::sqlite_db::statement_id db::sqlite_db::prepare_statement(const std::string& statement, const std::string& symbolicName)
{
	db::sqlite_db::statement_id id(-1);
	sqlite3_stmt *prepStmt = NULL;
	int ric = sqlite3_prepare_v2(m_dbConnection, statement.c_str(), static_cast<int>(statement.length()), &prepStmt, NULL);
	if (ric != SQLITE_OK)
	{
		error(sqlite3_errmsg(m_dbConnection));
		return id;
	}
	
	id = m_preparedStatements.size();
	m_preparedStatements[id] = prepStmt;
	if (!symbolicName.empty())
	{
		m_preparedStatementNames[symbolicName] = id;
	}

	return id;
}

bool db::sqlite_db::create_function(const std::string& functionName, long numArgs, void (*funct)(sqlite3_context*, int, sqlite3_value**))
{
	int iRes = sqlite3_create_function_v2(m_dbConnection, functionName.c_str(), numArgs, SQLITE_UTF8, this, funct, NULL, NULL, NULL);
	if (iRes != SQLITE_OK)
	{
		error(sqlite3_errmsg(m_dbConnection));
		return false;
	}
	
	return true;
}

unsigned long db::sqlite_db::count_rows(const std::string &tableName)
{
	std::string select = "select count(*) from " + tableName + ";";
	db::sqlite_db::record r;
	execute(select, r);
	return adnd::utils::parse<unsigned long>(r[0].c_str(), r[0].length(), 0);
}

bool db::sqlite_db::save(const std::string& dbDest)
{
	bool bRestored = false;
	sqlite3_backup *backup = nullptr;
	sqlite3 *dest = nullptr;
	
	if (m_dbSource == db::sqlite_db::source::file)
		return true;

	int ret = sqlite3_open(dbDest.c_str(), &dest);
	if (ret != SQLITE_OK)
	{
		error(sqlite3_errstr(ret));
		return false;
	}
	
	backup = sqlite3_backup_init(dest, "main", m_dbConnection, "main");
	if (!backup)
	{
		error(std::string("Unable to open " + dbDest));
		return false;
	}
	sqlite3_backup_step(backup, -1);
	sqlite3_backup_finish(backup);

	ret = sqlite3_errcode(dest);
	if (ret != SQLITE_OK)
	{
		error(sqlite3_errstr(ret));
		return false;
	}

	ret = sqlite3_close(dest);
	if (ret != SQLITE_OK)
	{
		error(sqlite3_errstr(ret));
		return false;
	}
	return true;
}

bool db::sqlite_db::restore(const std::string& dbSrc)
{
	bool bRestored = false;
	sqlite3_backup *backup = nullptr;
	sqlite3 *src = nullptr;
	
	if (m_dbSource == db::sqlite_db::source::file)
		return true;

	int ret = sqlite3_open(dbSrc.c_str(), &src);
	if (ret != SQLITE_OK)
	{
		error(sqlite3_errstr(ret));
		return false;
	}
	
	backup = sqlite3_backup_init(m_dbConnection, "main", src, "main");
	if (!backup)
	{
		error(std::string("Unable to open " + dbSrc));
		return false;
	}

	sqlite3_backup_step(backup, -1);
	sqlite3_backup_finish(backup);

	ret = sqlite3_errcode(src);
	if (ret != SQLITE_OK)
	{
		error(sqlite3_errstr(ret));
		return false;
	}

	ret = sqlite3_close(src);
	if (ret != SQLITE_OK)
	{
		error(sqlite3_errstr(ret));
		return false;
	}
	return true;
}

bool db::sqlite_db::bind_parameter(const db::sqlite_db::statement_id& stmtId, int index, const std::time_t& value)
{			
	prepared_statement_set::iterator it = m_preparedStatements.find(stmtId);
	if (it == m_preparedStatements.end())
	{
		error("Invalid prepared statement id");
		return false;
	}

	sqlite3_stmt *prepStmt = m_preparedStatements[stmtId];
	if (sqlite3_bind_int64(prepStmt, index, value) != SQLITE_OK)
	{
		error(sqlite3_errmsg(m_dbConnection));
		return false;
	}
	return true;
}

bool db::sqlite_db::bind_parameter(const db::sqlite_db::statement_id& stmtId, int index, const std::string& value)
{				
	prepared_statement_set::iterator it = m_preparedStatements.find(stmtId);
	if (it == m_preparedStatements.end())
	{
		error("Invalid prepared statement id");
		return false;
	}

	sqlite3_stmt *prepStmt = m_preparedStatements[stmtId];
	if (sqlite3_bind_text(prepStmt, index, value.c_str(), static_cast<int>(value.length()), SQLITE_TRANSIENT) != SQLITE_OK)
	{
		error(sqlite3_errmsg(m_dbConnection));
		return false;
	}
	return true;
}

bool db::sqlite_db::bind_parameter(const db::sqlite_db::statement_id& stmtId, int index, const char* value)
{
	return bind_parameter(stmtId, index, std::string(value));
}

db::sqlite_db::iterator db::sqlite_db::begin(const std::string &stmt)
{
	return db::sqlite_db::iterator(this, stmt);
}

db::sqlite_db::iterator db::sqlite_db::begin(sqlite3_stmt *stmt)
{
	return db::sqlite_db::iterator(this, stmt);
}
db::sqlite_db::iterator db::sqlite_db::end()
{
	return db::sqlite_db::iterator();
}

bool db::sqlite_db::add_pragma(const std::string &key, const std::string &value)
{
	std::string pragma = "PRAGMA " +key+ "=" +value;
	if (!execute(pragma))
	{
		error(last_error().c_str());
		return false;
	}
	return true;
}

std::string db::sqlite_db::sql(const db::sqlite_db::statement_id& stmtId)
{
	prepared_statement_set::iterator it = m_preparedStatements.find(stmtId);
	if (it == m_preparedStatements.end())
	{
		m_lastError = "Invalid prepared statement id";
		return "";
	}
	sqlite3_stmt *prepStmt = m_preparedStatements[stmtId];
	return sqlite3_sql(prepStmt);
}

bool db::sqlite_db::register_function(const std::string& functionName, long numArgs, void (*func)(sqlite3_context*, int, sqlite3_value**), std::string &error)
{
	int res = sqlite3_create_function_v2(m_dbConnection, functionName.c_str(), numArgs, SQLITE_UTF8, this, func, NULL, NULL, NULL);
	if (res != SQLITE_OK)
	{
		error = sqlite3_errmsg(m_dbConnection);
		return false;
	}
	return true;
}

const char* db::sqlite_db::iterator::get_column_text(const int col) const
{
	const char *zValue = NULL;
	if (col <= 0 || col > m_numCols)
	{
		throw std::exception("Column index out of bound");
	}
						
	zValue = reinterpret_cast<const char*>(sqlite3_column_text(m_stmt, col-1));
	if (!zValue)
		throw std::exception("Error retrieving data");

	return zValue;
}

db::sqlite_db::iterator::iterator(db::sqlite_db* const db, const std::string &stmt)
	: m_result(SQLITE_OK), m_db(db), m_numCols(0), m_stmt(nullptr)
{
	int nByte = -1;
	const char *pzTail = NULL;

	m_result = sqlite3_prepare_v2(m_db->db_connection(), stmt.c_str(), nByte, &m_stmt, &pzTail);
	if (m_result != SQLITE_OK)
		throw std::exception(sqlite3_errmsg(m_db->db_connection()));

	m_numCols = sqlite3_column_count(m_stmt);
	m_result = sqlite3_step(m_stmt);
	if (m_result != SQLITE_ROW && m_result != SQLITE_DONE)
	{
		throw std::exception(sqlite3_errmsg(m_db->db_connection()));
	}
}

db::sqlite_db::iterator::iterator(db::sqlite_db* const db, sqlite3_stmt *stmt)
	: m_result(SQLITE_OK), m_db(db), m_numCols(0), m_stmt(stmt)
{
	m_numCols = sqlite3_column_count(m_stmt);
	m_result = sqlite3_step(m_stmt);
	if (m_result != SQLITE_ROW && m_result != SQLITE_DONE)
	{
		throw std::exception(sqlite3_errmsg(m_db->db_connection()));
	}
}

db::sqlite_db::iterator::iterator(db::sqlite_db* const db, const db::sqlite_db::statement_id& stmtId)
	: m_result(SQLITE_OK), m_db(db), m_numCols(0)//, m_stmt(stmt)
{
	m_stmt = db->m_preparedStatements[stmtId];
	m_numCols = sqlite3_column_count(m_stmt);
	m_result = sqlite3_step(m_stmt);
	if (m_result != SQLITE_ROW && m_result != SQLITE_DONE)
	{
		throw std::exception(sqlite3_errmsg(m_db->db_connection()));
	}
}

db::sqlite_db::iterator::iterator()
	: m_result(SQLITE_DONE), m_db(nullptr), m_numCols(0), m_stmt(nullptr)
{
}

db::sqlite_db::iterator::~iterator()
{
	if (m_stmt)
	{
		m_result = sqlite3_reset(m_stmt);
		m_stmt = nullptr;
	}
}
					
db::sqlite_db::iterator::operator bool() const
{
	return m_result == SQLITE_ROW;
}
					
bool db::sqlite_db::iterator::operator==(const db::sqlite_db::iterator& it) const
{
	return (m_result == it.m_result);
}

bool db::sqlite_db::iterator::operator!=(const db::sqlite_db::iterator& it) const
{
	return (m_result != it.m_result);
}

db::sqlite_db::iterator& db::sqlite_db::iterator::operator=(iterator& it)
{
	this->m_result = it.m_result;
	this->m_db = it.m_db;
	this->m_numCols = it.m_numCols;
	this->m_stmt = it.m_stmt;
	it.m_stmt = nullptr;	//takes ownership
	return (*this);
}
					
db::sqlite_db::iterator& db::sqlite_db::iterator::operator+=(const int& movement)
{
	for (int i=0; i<movement; ++i)
	{
		m_result = sqlite3_step(m_stmt);
	}
	return (*this);
}

db::sqlite_db::iterator& db::sqlite_db::iterator::operator++()
{
	m_result = sqlite3_step(m_stmt);
	return (*this);
}

db::sqlite_db::iterator db::sqlite_db::iterator::operator++(int)
{
	auto temp(*this);
	++(*this);
	return temp;
}

db::sqlite_db::record db::sqlite_db::iterator::operator*()
{
	db::sqlite_db::record row;
	const unsigned char *zValue = nullptr;
	for (long col=0; col<m_numCols; ++col)
	{
		zValue = sqlite3_column_text(m_stmt, col);
		if (zValue)
		{
			row.push_back(std::string(reinterpret_cast<const char*>(zValue)));
		}
		else
		{
			row.push_back("");
		}
	}
	return row;
}

const db::sqlite_db::record db::sqlite_db::iterator::operator*() const
{
	db::sqlite_db::record row;
	const unsigned char *zValue = NULL;
	for (long col=0; col<m_numCols; ++col)
	{
		zValue = sqlite3_column_text(m_stmt, col);
		if (zValue)
		{
			row.push_back(std::string(reinterpret_cast<const char*>(zValue)));
		}
	}
	return static_cast<const db::sqlite_db::record>(row);
}

int db::sqlite_db::iterator::get_result() const
{
	return m_result;
}

sqlite3_stmt const * db::sqlite_db::iterator::statement() const
{
	return m_stmt;
}

const int& db::sqlite_db::iterator::columns_count() const
{
	return m_numCols;
}

const db::sqlite_db::record db::sqlite_db::iterator::get_caption() const
{
	db::sqlite_db::record row;
	const char *zValue = NULL;
	for (long col=0; col<m_numCols; ++col)
	{
		zValue = sqlite3_column_name(m_stmt, col);
		if (zValue)
		{
			row.push_back(std::string(reinterpret_cast<const char*>(zValue)));
		}
	}
	return static_cast<const db::sqlite_db::record>(row);
}

const std::string db::sqlite_db::iterator::get_caption(int column) const
{
	const char *zCaption = NULL;
	if (column > m_numCols)
	{
		throw std::exception("Column index out of bound");
	}
	zCaption = sqlite3_column_name(m_stmt, column);
	if (!zCaption)
		throw std::exception("Error retrieving data");
	return std::string(zCaption);
}

std::string db::sqlite_db::iterator::sql() const
{
	return sqlite3_sql(m_stmt);
}