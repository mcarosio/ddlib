#pragma once

#include "defs.h"
#include "templates.h"
#include "tables.h"
//#include "treasure.h"

#include <map>
#include <string>
#include <iostream>
#include <cstdint>

namespace adnd
{
	namespace money
	{
		class amount
		{
		public:
			amount();
			amount(const defs::coin& c);
			amount(const defs::coin& c, const uint32_t& amount);
			~amount();

			amount& operator= (const uint32_t amount);
			amount& operator+= (const uint32_t amount);
			amount& operator-= (const uint32_t amount);
			const defs::coin& currency() const;

			bool operator== (const uint32_t& amount) const;
			bool operator< (const uint32_t& amount) const;
			bool operator<= (const uint32_t& amount) const;
			bool operator>= (const uint32_t& amount) const;
			bool operator> (const uint32_t& amount) const;
			bool operator!= (const uint32_t& amount) const;

			amount& operator= (const amount& amount);
			amount& operator+= (const amount& amount);
			amount& operator-= (const amount& amount);
			bool operator== (const amount& amount) const;
			bool operator< (const amount& amount) const;
			bool operator<= (const amount& amount) const;
			bool operator>= (const amount& amount) const;
			bool operator> (const amount& amount) const;
			bool operator!= (const amount& amount) const;

			operator uint32_t() const
			{
				return m_amount;
			}

			template <defs::coin currencyTo>
			uint32_t as() const
			{
				auto t = adnd::templates::metadata::get_instance().get_coin_template(m_currency);
				return static_cast<uint32_t>(std::trunc(t.to(currencyTo) * m_amount));
			}
			double as(const defs::coin& currencyTo) const;
			uint32_t value() const;

			friend std::ostream& operator<<(std::ostream& out, const amount& amt)
			{
				auto t = adnd::templates::metadata::get_instance().get_coin_template(amt.currency());
				out << static_cast<uint32_t>(amt) << " " << t.get_acronym();
				return out;
			}
			void replace(const money::amount& amt);
			void replace(const defs::coin& currency, const uint32_t& amount);

		protected:
			uint32_t	m_amount;
			defs::coin	m_currency;
		};

		adnd::money::amount normalise(const adnd::defs::coin& currency, const double& value);
		std::map<defs::coin, money::amount> split(const defs::coin& currency, const double& value);

		class money_bag
		{
		public:
			money_bag();
			~money_bag();

			void init();
			amount& get(const defs::coin& currency);
			//const double& rate(const defs::coin& from, const defs::coin& to);

			money_bag& operator+= (const amount& amt);
			money_bag& operator-= (const amount& amt);

			void add(const defs::coin& currency, uint32_t amt);
			void add(const money::amount& amt);
			//void add(const treasure::gem& g);
			//void add(const treasure::object_of_art& g);
			void subtract(const defs::coin& currency, uint32_t amt);
			void subtract(const amount& amt);
			//void remove(const uint32_t& gemUid);

			const amount& copper();
			const amount& silver();
			const amount& electrum();
			const amount& gold();
			const amount& platinum();

			bool check_availability(const amount& amt);
			void normalise();

		protected:
			std::map<defs::coin, amount>			m_money;
			//std::map<uint32_t, adnd::treasure::gem>	m_gems;

			std::list<money::amount> normalise(const defs::coin& currency, uint32_t amt);
		};
	}
}