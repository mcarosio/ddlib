#include "stdafx.h"
#include "skills.h"


adnd::skills::character_skill_value::character_skill_value()
	: m_skill(defs::character_skill::none), m_value(0), m_exceptional(0)
{
	if (m_skill != defs::character_skill::strength)
		m_exceptional = 0;
}

adnd::skills::character_skill_value::character_skill_value(defs::character_skill skl)
	: m_skill(skl), m_value(0), m_exceptional(0)
{
	if (m_skill != defs::character_skill::strength)
		m_exceptional = 0;
}

adnd::skills::character_skill_value::character_skill_value(defs::character_skill skl, const short& val)
	: m_skill(skl), m_value(val), m_exceptional(0)
{
	if (m_skill != defs::character_skill::strength)
		m_exceptional = 0;
}

adnd::skills::character_skill_value::character_skill_value(defs::character_skill skl, const short& val, const short& exceptional)
	: m_skill(skl), m_value(val), m_exceptional(exceptional)
{
	if (m_skill != defs::character_skill::strength)
		m_exceptional = 0;
}

adnd::skills::character_skill_value::character_skill_value(const character_skill_value& skl)
	: m_skill(skl.m_skill), m_value(skl.m_value), m_exceptional(skl.m_exceptional)
{
}

adnd::skills::character_skill_value& adnd::skills::character_skill_value::operator=(const short& val)
{
	if (val < 0 || val > 25)
		throw std::exception("Invalid skill value", val);

	m_value = val;
	m_exceptional = 0;

	return (*this);
}


adnd::skills::character_skill_value& adnd::skills::character_skill_value::operator=(const character_skill_value& val)
{
	m_skill = val.m_skill;
	m_value = val.m_value;
	m_exceptional = val.m_exceptional;

	return (*this);
}

adnd::skills::character_skill_value& adnd::skills::character_skill_value::operator+=(const short& val)
{
	short newValue = this->value() + val;
	if (newValue < 0 || newValue > 25)
		throw std::exception("Unable to modifiy skill value", newValue);

	m_value = newValue;
	m_exceptional = 0;

	return (*this);
}

adnd::skills::character_skill_value& adnd::skills::character_skill_value::operator+=(const character_skill_value& val)
{
	(*this) += val.value();
	return (*this);
}

adnd::skills::character_skill_value& adnd::skills::character_skill_value::operator-=(const short& val)
{
	(*this) += -val;
	return (*this);
}

adnd::skills::character_skill_value& adnd::skills::character_skill_value::operator-=(const character_skill_value& val)
{
	(*this) += -val.value();
	return (*this);
}

bool adnd::skills::character_skill_value::operator==(const character_skill_value& val) const
{
	if (m_skill != val.m_skill) // Comparison is impossible
		return false;
	if (m_skill != defs::character_skill::strength)
		return value() == val.value();
	if (value() == 18)
		return value() == val.value() && m_exceptional == val.m_exceptional;
	else
		return value() == val.value();
}

bool adnd::skills::character_skill_value::operator<(const character_skill_value& val) const
{
	if (m_skill != val.m_skill)
		throw std::exception("Unable to compare different skills");

	if (m_skill == defs::character_skill::strength && value() == 18)
		return value() == val.value() && m_exceptional < val.m_exceptional;
	else
		return value() < val.value();
}

void adnd::skills::character_skill_value::set_exceptional(const short& val)
{
	if (m_skill == defs::character_skill::strength)
	{
		if (m_value != 18)
			throw std::exception("Exceptional strength can only be set when the skill equals 18", m_value);
		if (val < 1 || val > 100)
			throw std::exception("Invalid skill value", val);
		m_exceptional = val;
	}
	else
		throw std::exception("Invalid skill value", val);
}



adnd::skills::character_skill_set::character_skill_set()
{
}

adnd::skills::character_skill_value& adnd::skills::character_skill_set::operator[](const defs::character_skill& skl)
{
	return m_skills[skl];
}

short adnd::skills::skill_generator::generate_exceptional_strength()
{
	random::die hundred(defs::die_faces ::ten);
	random::die units(defs::die_faces ::ten);
	short h = hundred.roll(1);
	short u = units.roll(1);
	return h * 10 + u;
}

short adnd::skills::skill_generator::generate(const short& minValue, const short& maxValue)
{
	short value = 0;
	do
	{
		value = generate();
	} while (!(minValue <= value && value <= maxValue));

	return value;
}

short adnd::skills::skill_generator::generate()
{
	random::die d6(defs::die_faces ::six);
	short value = 0;
	if (m_method == generation_method::standard)
	{
		value = d6.roll(3);
	}
	else if (m_method == generation_method::best_of_four)
	{
		short roll1 = d6.roll(1);
		short roll2 = d6.roll(1);
		short roll3 = d6.roll(1);
		short roll4 = d6.roll(1);
		short minRoll = skill_generator::min(roll1, roll2, roll3, roll4);
		value = roll1 + roll2 + roll3 + roll4 - minRoll;
	}
	else if (m_method == generation_method::best_of_each)
	{
		short roll1 = d6.roll(3);
		short roll2 = d6.roll(3);
		value = std::max<_int8>(roll1, roll2);
	}
	else
	{
		throw std::exception("Invalid generation method for a skill", static_cast<int>(m_method));
	}

	return value;
}

void adnd::skills::character_skill_set::set(const character_skill_value& strength, const character_skill_value& dexterity, const character_skill_value& constitution, const character_skill_value& intelligence, const character_skill_value& wisdom, const character_skill_value& charisma)
{
	m_skills[defs::character_skill::strength] = strength;
	m_skills[defs::character_skill::dexterity] = dexterity;
	m_skills[defs::character_skill::constitution] = constitution;
	m_skills[defs::character_skill::intelligence] = intelligence;
	m_skills[defs::character_skill::wisdom] = wisdom;
	m_skills[defs::character_skill::charisma] = charisma;
}

const adnd::skills::character_skill_value& adnd::skills::character_skill_set::strength() const
{
	return m_skills.at(defs::character_skill::strength);
}

const adnd::skills::character_skill_value& adnd::skills::character_skill_set::dexterity() const
{
	return m_skills.at(defs::character_skill::dexterity);
}

const adnd::skills::character_skill_value& adnd::skills::character_skill_set::constitution() const
{
	return m_skills.at(defs::character_skill::constitution);
}

const adnd::skills::character_skill_value& adnd::skills::character_skill_set::intelligence() const
{
	return m_skills.at(defs::character_skill::intelligence);
}

const adnd::skills::character_skill_value& adnd::skills::character_skill_set::wisdom() const
{
	return m_skills.at(defs::character_skill::wisdom);
}

const adnd::skills::character_skill_value& adnd::skills::character_skill_set::charisma() const
{
	return m_skills.at(defs::character_skill::charisma);
}

void adnd::skills::character_skill_set::set(const short& strength, const short& dexterity, const short& constitution, const short& intelligence, const short& wisdom, const short& charisma)
{
	set(strength, 0, dexterity, constitution, intelligence, wisdom, charisma);
}

void adnd::skills::character_skill_set::set(const short& strength, const short& exceptionalStrength, const short& dexterity, const short& constitution, const short& intelligence, const short& wisdom, const short& charisma)
{
	adnd::skills::character_skill_value str = adnd::skills::character_skill_value(adnd::defs::character_skill::strength, strength, exceptionalStrength);
	adnd::skills::character_skill_value dex = adnd::skills::character_skill_value(adnd::defs::character_skill::dexterity, dexterity);
	adnd::skills::character_skill_value con = adnd::skills::character_skill_value(adnd::defs::character_skill::constitution, constitution);
	adnd::skills::character_skill_value itl = adnd::skills::character_skill_value(adnd::defs::character_skill::intelligence, intelligence);
	adnd::skills::character_skill_value wis = adnd::skills::character_skill_value(adnd::defs::character_skill::wisdom, wisdom);
	adnd::skills::character_skill_value cha = adnd::skills::character_skill_value(adnd::defs::character_skill::charisma, charisma);

	set(str, dex, con, itl, wis, cha);
}

short adnd::skills::character_skill_set::skill_value(const adnd::defs::character_skill& skl) const
{
	return m_skills.find(skl)->second.value();
}






adnd::skills::skill_generator::skill_generator(const adnd::skills::generation_method& method)
	: m_method(method)
{
}

void adnd::skills::skill_generator::generate(character_skill_value& value, const short& minValue, const short& maxValue, bool generateExceptionalStrength)
{
	short val = generate(minValue, maxValue);
	value = val;

	if (generateExceptionalStrength && value.skill() == defs::character_skill::strength && val == 18)
	{
		auto exceptionalStrength = generate_exceptional_strength();
		value.set_exceptional(val);
	}
}

void adnd::skills::skill_generator::generate(character_skill_set& skills, bool generateExceptionalStrength)
{
	short strength = 0;
	short exceptionalStrength = 0;
	short dexterity = 0;
	short constitution = 0;
	short intelligence = 0;
	short wisdom = 0;
	short charisma = 0;

	generate(strength, exceptionalStrength, dexterity, constitution, intelligence, wisdom, charisma, generateExceptionalStrength);

	skills.set(strength, exceptionalStrength, dexterity, constitution, intelligence, wisdom, charisma);
}

void adnd::skills::skill_generator::generate(short& strength, short& exceptionalStrength, short& dexterity, short& constitution, short& intelligence, short& wisdom, short& charisma, bool generateExceptionalStrength)
{
	strength = generate();
	dexterity = generate();
	constitution = generate();
	intelligence = generate();
	wisdom = generate();
	charisma = generate();

	if (generateExceptionalStrength && strength == 18)
	{
		exceptionalStrength = generate_exceptional_strength();
	}
}

void adnd::skills::skill_generator::generate(character_skill_value& strength, character_skill_value& dexterity, character_skill_value& constitution,
											character_skill_value& intelligence, character_skill_value& wisdom, character_skill_value& charisma,
											bool generateExceptionalStrength)
{
	short str = 0;
	short excStr = 0;
	short dex = 0;
	short con = 0;
	short itl = 0;
	short wis = 0;
	short cha = 0;

	generate(str, excStr, dex, con, itl, wis, cha, generateExceptionalStrength);

	strength = adnd::skills::character_skill_value(adnd::defs::character_skill::strength, str, excStr);
	dexterity = adnd::skills::character_skill_value(adnd::defs::character_skill::dexterity, dex);
	constitution = adnd::skills::character_skill_value(adnd::defs::character_skill::constitution, con);
	intelligence = adnd::skills::character_skill_value(adnd::defs::character_skill::intelligence, itl);
	wisdom = adnd::skills::character_skill_value(adnd::defs::character_skill::wisdom, wis);
	charisma = adnd::skills::character_skill_value(adnd::defs::character_skill::charisma, cha);
}