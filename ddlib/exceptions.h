#pragma once

#include <exception>

#include "defs.h"

namespace adnd
{
	namespace exceptions
	{
		class class_not_allowed_for_race : std::exception
		{
		public:
			class_not_allowed_for_race(const defs::character_class& cls, const defs::character_race& race);
			~class_not_allowed_for_race();

			const defs::character_class&	get_class() { return m_class; };
			const defs::character_race&		get_race() { return m_race; };

			char const* what() const;

		protected:
			std::string				m_message;
			defs::character_class	m_class;
			defs::character_race	m_race;
		};

		class target_out_of_range : std::exception
		{
		public:
			target_out_of_range(const defs::equipment::item& it, const short& distance);
			~target_out_of_range();

			const defs::equipment::item&	get_item() { return m_itemId; };
			const short&					get_distance() { return m_distance; };

			char const* what() const;

		protected:
			std::string					m_message;
			defs::equipment::item		m_itemId;
			short						m_distance;
		};
	}
}