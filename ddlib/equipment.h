#pragma once

#include <map>

#include "defs.h"
#include "templates.h"


namespace adnd
{
	namespace equipment
	{
		class item
		{
		public:
			item(const uint32_t& uid, const defs::equipment::item& itemId, const std::string& name, const defs::item_class& itemClass, const defs::item_type& itemType, const double& weight,
				const defs::item_hand_required& handReq = defs::item_hand_required::none)
				: m_uniqueId(uid), m_itemId(itemId), m_name(name), m_handRequired(handReq), m_itemClass(itemClass), m_itemType(itemType), m_weight(weight)
			{
			}
			virtual ~item() {}

			const uint32_t& uid() const { return m_uniqueId; }
			const defs::equipment::item& id() const { return m_itemId; }
			const std::string& name() { return m_name; }
			const defs::item_hand_required& hand_required() { return m_handRequired; }
			const defs::item_class& item_class() { return m_itemClass; }
			const defs::item_type& item_type() { return m_itemType; }

			virtual double weight() { return m_weight; }
			virtual short initialive_modifier() { return 0; }

		protected:
			uint32_t					m_uniqueId;
			defs::equipment::item		m_itemId;
			std::string					m_name;
			defs::item_hand_required	m_handRequired;
			defs::item_class			m_itemClass;
			defs::item_type				m_itemType;
			double						m_weight;
		};

		class wearable : public item
		{
		public:
			wearable(const uint32_t& uid, const defs::equipment::item& id, const std::string& name, const defs::item_class& itemClass, const defs::item_type& itemType, const double& weight, const defs::body_slot& bodySlot)
				: item(uid, id, name, itemClass, itemType, weight, defs::item_hand_required::none), m_bodySlot(bodySlot)
			{
			}
			virtual ~wearable() {}

			const defs::body_slot& body_slot() const { return m_bodySlot; }
		
			//void use() {}
		
		protected:
			defs::body_slot		m_bodySlot;
		};


		class armour : public wearable
		{
		public:
			armour(const uint32_t& uid, const defs::equipment::item& id, const std::string& name, const defs::item_class& itemClass, const double& weight, const defs::body_slot& bodySlot,
					const uint16_t& acBonusMelee, const uint16_t& acBonusMissile, const uint16_t& numOfAttackProtection, const uint16_t& protections, const bool& allowsWeapons)
				: wearable(uid, id, name, itemClass, defs::item_type::armour, weight, bodySlot),
				m_acBonusMelee(acBonusMelee), m_acBonusMissile(acBonusMissile), m_numOfAttackProtection(numOfAttackProtection), m_protections(protections), m_allowsWeapons(allowsWeapons)
			{}
			~armour() {}

			const uint16_t& ac_bonus_melee() const { return m_acBonusMelee; }
			const uint16_t& ac_bonus_missile() const { return m_acBonusMissile; }
			const uint16_t& num_of_attack_protection() const { return m_numOfAttackProtection; }
			const bool protection(const defs::attack_side& side) const { return m_protections & static_cast<uint16_t>(side); }
			const bool& allows_weapons() const { return m_allowsWeapons; }

		protected:
			uint16_t		m_acBonusMelee;
			uint16_t		m_acBonusMissile;
			uint16_t		m_numOfAttackProtection;
			uint16_t		m_protections;
			bool			m_allowsWeapons;
		};

		class clothing : public wearable
		{
		public:
			clothing(const uint32_t& uid, const defs::equipment::item& id, const std::string& name, const defs::item_class& itemClass, const double& weight, const defs::body_slot& bodySlot)
				: wearable(uid, id, name, itemClass, defs::item_type::clothing, weight, bodySlot)
			{}
			~clothing() {}

		protected:
		};

		class daily_food_and_lodging : public item
		{
		public:
			daily_food_and_lodging(const uint32_t& uid, const defs::equipment::item& id, const std::string& name, const defs::item_class& itemClass, const double& weight)
				: item(uid, id, name, itemClass, defs::item_type::daily_food_and_lodging, weight)
			{}
			~daily_food_and_lodging() {}

		protected:
		};

		class household_provisioning : public item
		{
		public:
			household_provisioning(const uint32_t& uid, const defs::equipment::item& id, const std::string& name, const defs::item_class& itemClass, const double& weight)
				: item(uid, id, name, itemClass, defs::item_type::household_provisioning, weight)
			{}
			~household_provisioning() {}

		protected:
		};

		class service : public item
		{
		public:
			service(const uint32_t& uid, const defs::equipment::item& id, const std::string& name, const defs::item_class& itemClass, const double& weight)
				: item(uid, id, name, itemClass, defs::item_type::service, weight)
			{}
			~service() {}

		protected:
		};

		class transport : public item
		{
		public:
			transport(const uint32_t& uid, const defs::equipment::item& id, const std::string& name, const defs::item_class& itemClass, const double& weight)
				: item(uid, id, name, itemClass, defs::item_type::transport, weight)
			{}
			~transport() {}

		protected:
		};

		class animal : public item
		{
		public:
			animal(const uint32_t& uid, const defs::equipment::item& id, const std::string& name, const defs::item_class& itemClass, const double& weight)
				: item(uid, id, name, itemClass, defs::item_type::animal, weight)
			{}
			~animal() {}

		protected:
		};

		class tack_and_harness : public item
		{
		public:
			tack_and_harness(const uint32_t& uid, const defs::equipment::item& id, const std::string& name, const defs::item_class& itemClass, const double& weight)
				: item(uid, id, name, itemClass, defs::item_type::tack_and_harness, weight)
			{}
			~tack_and_harness() {}

		protected:
		};

		class miscellaneous_equipment : public item
		{
		public:
			miscellaneous_equipment(const uint32_t& uid, const defs::equipment::item& id, const std::string& name, const defs::item_class& itemClass, const double& weight)
				: item(uid, id, name, itemClass, defs::item_type::miscellaneous_equipment, weight)
			{}
			~miscellaneous_equipment() {}

		protected:
		};

		class weapon : public item
		{
		public:
			weapon(const uint32_t& uid, const defs::equipment::item& id, const std::string& name, const defs::item_class& itemClass, const double& weight, const defs::item_hand_required& handRequired,
				const defs::weapons::size& weaponSize, const short& speedFactor, const defs::weapons::type& weaponType, const bool& requiresProjectiles,
				const defs::die_faces & faces_SM, const short& numRolls_SM, const short& baseDamage_SM, const defs::die_faces & faces_L, const short& numRolls_L, const short& baseDamage_L);
			virtual ~weapon() {}

			short initialive_modifier() { return m_speedFactor; }

			void use() {}

			void select_projectile(const defs::equipment::item& projectileId);
			virtual short damage(const defs::weapons::target_size& targetSize);
			virtual short bonus_malus() { return 0; }
			const defs::equipment::item& projectile_in_use() { return m_projectileInUse; }

		protected:
			defs::weapons::size											m_weaponSize;
			short														m_speedFactor;
			defs::weapons::type											m_weaponType;
			bool														m_requiresProjectiles;

			std::map<defs::weapons::target_size, defs::die_faces >		m_faces;
			std::map<defs::weapons::target_size, short>					m_numRolls;
			std::map<defs::weapons::target_size, short>					m_baseDamage;

			//std::map<defs::equipment::item, projectile_stock>			m_projectiles;
			defs::equipment::item										m_projectileInUse;
		};
		
		class projectile : public item
		{
		public:
			projectile(const uint32_t& uid, const defs::equipment::item& id, const std::string& name, const defs::item_class& itemClass, const double& weight,
				const defs::weapons::size& weaponSize, const defs::weapons::type& weaponType,
				const defs::die_faces & faces_SM, const short& numRolls_SM, const short& baseDamage_SM, const defs::die_faces & faces_L, const short& numRolls_L, const short& baseDamage_L);
			virtual ~projectile() {}
		
			void use() {}
		
			//virtual short damage(const defs::weapons::target_size& targetSize);
			virtual short bonus_malus() { return 0; }
			virtual short damage(const defs::weapons::target_size& targetSize);
		
		protected:
			defs::weapons::size											m_weaponSize;
			defs::weapons::type											m_weaponType;

			std::map<defs::weapons::target_size, defs::die_faces >		m_faces;
			std::map<defs::weapons::target_size, short>					m_numRolls;
			std::map<defs::weapons::target_size, short>					m_baseDamage;
		};

		class magical_item : public item
		{
		public:
			magical_item(const uint32_t& uid, const defs::equipment::item& id, const std::string& name,
						const defs::item_class& itemClass, const defs::item_type& itemType, const double& weight,
						const defs::item_hand_required& handRequired, const std::string& unidentifiedName,
						const defs::magical::item& magicalId, const defs::weapons::attack_type& attackType,
						const defs::weapons::group& weaponGroup, const defs::body_slot& slot);
			~magical_item() {}

		protected:
			std::string					m_unidentifiedName;
			bool						m_identified;
			defs::magical::item			m_magicalId;
			defs::weapons::attack_type	m_attackType;
			defs::weapons::group		m_weaponGroup;
			defs::body_slot				m_bodySlot;
		};

		class items_pool
		{
		public:
			static items_pool& get_instance()
			{
				static items_pool _instance;
				//_instance.m_lastId = 0;
				return _instance;
			}

			template <class _ItemType>
			_ItemType* create_as(const defs::equipment::item& itemId, const std::string& itemName="", const defs::item_class& itemClass = defs::item_class::common)
			{
				item *it = create(itemId, itemName, itemClass);
				return static_cast<_ItemType*>(it);
			}
			//weapon* create(const std::string& name, const defs::equipment::item& itemId, const defs::item_class& itemClass = defs::item_class::common);
			item* create(const defs::equipment::item& itemId, const std::string& itemName ="", const defs::item_class& itemClass = defs::item_class::common);

			items_pool& operator-= (const uint32_t& uid)
			{
				if (m_items.find(uid) != m_items.end())
					m_items.erase(uid);
				return (*this);
			}
			items_pool& operator-= (item* it)
			{
				if (m_items.find(it->uid()) != m_items.end())
					m_items.erase(it->uid());
				return (*this);
			}

			auto begin() { return m_items.begin(); }
			auto end() { return m_items.end(); }
			auto cbegin() { return m_items.cbegin(); }
			auto cend() { return m_items.cend(); }
			size_t size() {return m_items.size(); }

			items_pool(items_pool const&) = delete;
			void operator=(items_pool const&) = delete;

			void erase();
			void detatch(const uint32_t& uid);

			uint32_t new_uid()
			{
				auto uid = m_lastId;
				m_lastId++;
				m_items[m_lastId] = nullptr;
				return uid;
			}

			~items_pool();

			/*template<typename _RetType>
			static const _RetType& as(item*& it)
			{
				return *(dynamic_cast<_RetType*>(it));
			}*/

		private:
			items_pool() : m_lastId(0) {}

			uint32_t						m_lastId;
			std::map<uint32_t, item*>		m_items;
		};
	};
};