#pragma once

#include "defs.h"
#include "random.h"
#include "exceptions.h"

#include <exception>
#include <functional>
#include <ostream>
#include <type_traits>
#include <map>


namespace adnd
{
	namespace skills
	{
		enum class generation_method
		{
			standard = 0,
			best_of_four,
			best_of_each
		};

		struct character_skill_value
		{
		public:
			//typedef std::function<int8_t(const character_skill_value& skillValue)> skill_modifier;

			//using modifier_id = short;

			character_skill_value();
			character_skill_value(defs::character_skill skl);
			character_skill_value(defs::character_skill skl, const short& val);
			character_skill_value(defs::character_skill skl, const short& val, const short& exceptional);
			character_skill_value(const character_skill_value& skl);

			character_skill_value& operator=(const short& val);
			character_skill_value& operator=(const character_skill_value& val);
			character_skill_value& operator+=(const short& val);
			character_skill_value& operator+=(const character_skill_value& val);
			character_skill_value& operator-=(const short& val);
			character_skill_value& operator-=(const character_skill_value& val);
			bool operator==(const character_skill_value& val) const;
			bool operator<(const character_skill_value& val) const;

			bool operator<=(const character_skill_value& val) const { return (*this) < val || (*this) == val; }
			bool operator>(const character_skill_value& val) const { return !((*this) <= val); }
			bool operator>=(const character_skill_value& val) const { return !((*this) < val); }
			bool operator!=(const character_skill_value& val) const { return !((*this) == val); }

			friend std::ostream& operator<< (std::ostream& out, const character_skill_value& skillValue)
			{
				auto val = skillValue.value();
				out << val;
				if (skillValue.m_skill == defs::character_skill::strength && val == 18)
				{
					auto exc = skillValue.exceptional();
					if (exc > 0)
					{
						out << "/";
						if (exc == 100)
							out << "00";
						else if (exc < 10)
							out << "0" << exc;
						else
							out << exc;
					}
				}
				return out;
			}

			short value() const { return m_value; }
			short exceptional() const
			{
				if (m_skill == defs::character_skill::strength && value() == 18)
					return m_exceptional;
				else
					return -1;
			}
			const defs::character_skill& skill() const { return m_skill; }
			void set_exceptional(const short& val);

		private:
			defs::character_skill		m_skill;
			short						m_value;
			short						m_exceptional;
		};

		struct character_skill_set
		{
		public:
			character_skill_set();

			void set(const short& strength, const short& dexterity, const short& constitution, const short& intelligence, const short& wisdom, const short& charisma);
			void set(const short& strength, const short& exceptionalStrength, const short& dexterity, const short& constitution, const short& intelligence, const short& wisdom, const short& charisma);
			void set(const character_skill_value& strength, const character_skill_value& dexterity, const character_skill_value& constitution, const character_skill_value& intelligence, const character_skill_value& wisdom, const character_skill_value& charisma);
			
			character_skill_value& operator[](const defs::character_skill& skl);
			const character_skill_value& strength() const;
			const character_skill_value& dexterity() const;
			const character_skill_value& constitution() const;
			const character_skill_value& intelligence() const;
			const character_skill_value& wisdom() const;
			const character_skill_value& charisma() const;

			short skill_value(const defs::character_skill& skl) const;

			std::map<defs::character_skill, character_skill_value>::iterator begin() { return m_skills.begin(); }
			std::map<defs::character_skill, character_skill_value>::iterator end() { return m_skills.end(); }
			std::map<defs::character_skill, character_skill_value>::const_iterator begin() const { return m_skills.begin(); }
			std::map<defs::character_skill, character_skill_value>::const_iterator end() const { return m_skills.end(); }

		private:
			std::map<defs::character_skill, character_skill_value>	m_skills;
		};

		struct skill_generator
		{
			skill_generator(const generation_method& method);

			void generate(character_skill_value& value, const short& minValue, const short& maxValue, bool generateExceptionalStrength = false);
			void generate(character_skill_set& skills, bool generateExceptionalStrength = false);
			short generate(const short& minValue, const short& maxValue);
			void generate(short& strength, short& exceptionalStrength, short& dexterity, short& constitution, short& intelligence, short& wisdom, short& charisma, bool generateExceptionalStrength = false);
			void generate(character_skill_value& strength, character_skill_value& dexterity, character_skill_value& constitution,
						character_skill_value& intelligence, character_skill_value& wisdom, character_skill_value& charisma, bool generateExceptionalStrength = false);
			
			short generate();

		private:
			generation_method	m_method;

			template<typename _Last>
			_Last min(_Last&& t) { return std::forward<_Last>(t); }

			template<typename _First, typename _Second, typename... _Remainings>
			typename std::common_type<_First, _Second, _Remainings... >::type min(_First&& a, _Second&& b, _Remainings&&... vs)
			{
				if (b < a) return min(b, std::forward<_Remainings>(vs)...);
				else return min(a, std::forward<_Remainings>(vs)...);
			}

			short generate_exceptional_strength();
		};
	}
}