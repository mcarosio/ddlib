#include "stdafx.h"
#include "random.h"


adnd::random::die::die()
	: m_numFaces(defs::die_faces::six)
{
}

adnd::random::die::die(const adnd::defs::die_faces& numFaces)
	: m_numFaces(numFaces)
{
}

adnd::random::die_roll::die_roll()
	: m_die(defs::die_faces::six), m_rolls(1)
{
}

adnd::random::die_roll::die_roll(const defs::die_faces& numFaces, const short& numRolls)
	: m_die(numFaces), m_rolls(numRolls)
{
}

adnd::defs::die_faces adnd::random::die_roll::faces() const
{
	return m_die.faces();
}

short adnd::random::die_roll::rolls() const
{
	return m_rolls;
}

adnd::random::dice_set::dice_set()
{
}

adnd::random::dice_set::dice_set(const size_t size)
{
	m_dice.resize(size);
}

adnd::random::dice_set& adnd::random::dice_set::operator+=(const adnd::random::die& d)
{
	m_dice.push_back(d);
	return (*this);
}

const adnd::random::die& adnd::random::dice_set::operator[] (const size_t& position) const
{
	return m_dice[position];
}

size_t adnd::random::dice_set::size() const
{
	return m_dice.size();
}

adnd::defs::die_faces adnd::random::dice_set::faces(const size_t& position) const
{
	return m_dice[position].faces();
}

void adnd::random::dice_set::reset()
{
	m_dice.clear();
	m_dice.resize(0);
}