#pragma once

#include <map>
#include <vector>
#include <set>

#include "defs.h"
#include "skills.h"
#include "tables.h"

namespace adnd
{
	namespace spells
	{
		/*
			When the caster adds a spell, it initially has count = 0
			Once selected, the count_total is incremented (i.e. the caster selects the spell as active)
			The spell becomes castable after the required hurs of rest/study, then becomes castable (i.e. count_usable is incremented)
		*/
		struct spell_entry
		{
			unsigned short count_castable;	// Number of spells the caster can actually cast
			unsigned short count_total;		// Number of spells the caster gets ready to cast
		};

		template<typename _spell_type>
		class spell_level
		{
		public:

			spell_level()
				: m_level(0), m_hasSpecialistSpell(false)
			{
			}
			spell_level(const short& level)
				: m_level(level), m_hasSpecialistSpell(false)
			{
			}
			~spell_level() {}
			
			/*
				Returns the level number (the index is 1 based)
			*/
			short& level() { return m_level; }

			/*
				Returns the current number of spells the caster can cast
			*/
			short count_castable() const
			{
				short numSpells = 0;
				for (auto s : m_spells)
					numSpells += s.second.count_castable;
				return numSpells;
			}

			/*
				Returns the actual number of spells the caster has selected as in use
			*/
			short count() const
			{
				short numSpells = 0;
				for (auto s : m_spells)
					numSpells += s.second.count_total;
				return numSpells;
			}

			/*
				Returns the number of distinct usable spells (i.e. those the caster has written in the book and are ready to be used)
			*/
			short count_distinct() const { return m_spells.size(); }

			/*
				Adds the spell to the level
			*/
			void add(const _spell_type& spellId)
			{
				if (m_spells.find(spellId) == m_spells.end())
				{
					m_spells[spellId].count_castable = 0;	// The spell is only written, not selected for usage
					m_spells[spellId].count_total = 0;
				}
			}

			/*
				Selects the spell as usable
			*/
			void select(const _spell_type& spellId, const short& number = 1)
			{
				if (m_spells.find(spellId) != m_spells.end())
					m_spells[spellId].count_total += number;
			}

			/*
				Unselect the specified spell, freeing slots for other spells
			*/
			void unselect(const _spell_type& spellId, const unsigned short& number = 1)
			{
				if (m_spells.find(spellId) != m_spells.end())
				{
					m_spells[spellId].count_total = std::max(m_spells[spellId].count_total - number, 0);
					m_spells[spellId].count_usable = std::min(m_spells[spellId].count_usable, m_spells[spellId].count_total);
				}
			}

			/*
				Removes the specified spell from the level (i.e. the caster deliberately frees all the slots assigned to the spell).
				This does not affect the set of available spells, or remove it from the list of known spell.
			*/
			void remove(const _spell_type& spellId)
			{
				m_spells[spellId].count_total = 0;
				m_spells[spellId].count_castable = 0;
			}

			/*
				Removes a spell from the level (i.e. the caster deliberately frees all the slots assigned to the spell).
				This does not affect the set of available spells, or remove it from the list of known spell.
				There is no control over the choice of the spell to remove
			*/
			void remove()
			{
				_spell_type spellId = m_spells.rbegin()->first;
				remove(spellId);
			}

			/*
				Returns true if the spell is found at the level, false otherwise
			*/
			bool find(const _spell_type& spellId) const
			{
				return m_spells.find(spellId) != m_spells.end();
			}

			//const spell_entry& at(const _spell_type& spellId) const
			//{
			//	auto it = m_spells.find(spellId);
			//	return *it;
			//}

			auto cbegin() const { return m_spells.cbegin(); }
			auto cend() const { return m_spells.cend(); }

			/*const spell_entry& operator[] (const _spell_type& spellId) const
			{
				return m_spells[spellId];
			}*/
			const bool& has_specialist_spell() const { return m_hasSpecialistSpell; }

			void memorise(const _spell_type& spellId, short& spellsCount)
			{
				short diff = m_spells[spellId].count_total - m_spells[spellId].count_castable;
				short add = std::min<short>(spellsCount, diff);
				m_spells[spellId].count_castable += add;
				spellsCount -= add;
			}

		protected:
			short										m_level;
			std::map<_spell_type, spell_entry>			m_spells;
			bool										m_accessible;
			bool										m_hasSpecialistSpell;

			/*
				Returns true if the level is accessible to the spell caster, false otherwise (i.e. they require more experience or because of other limitations)
			*/
			bool& accessible() { return m_accessible; }

			friend class spell_book;
			friend class holy_symbol;
		};

		class spell_book
		{
		public:
			spell_book(const defs::character_class& cls);
			~spell_book() {}

			bool& enabled() { return m_enabled; }

			void resize(const skills::character_skill_value& intelligenceScore, const short& casterLevel);
			bool add(const defs::spells::wizard_spell& spellId);
			void select(const defs::spells::wizard_spell& spellId);
			void remove(const defs::spells::wizard_spell& spellId);

			/**
				Maximum level available to the caster
			*/
			short max_level();
			/**
				Maximum number of spells the level can contain according to the wizard spell progression table
			*/
			short capacity(const short& level);
			/**
				Maximum number of spells the caster can write in the level.
				If the optional rule "Maximum number of spells per level" is enabled,
				it returns the parameter value according to the intelligence statistics table, -1 otherwise
			*/
			short limit(const short& level) const;

			const spell_level<defs::spells::wizard_spell>& operator[](const int& lvl);

			const spell_entry& lookup(const defs::spells::wizard_spell& spellId) const;
			bool find(const defs::spells::wizard_spell& spellId) const;

			void study(const short& hours);
			short study_all();
			short study_required();

			void cast(const defs::spells::wizard_spell& spellId);

		protected:
			bool					m_enabled;
			defs::character_class	m_classId;
			std::vector<spell_level<defs::spells::wizard_spell>>
									m_spells;

			std::map<short, tables::wizard_spell_progression>
									m_progression;
			std::map<short, std::set<defs::spells::wizard_spell>>
									m_knownSpells;

			std::map<defs::spells::wizard_spell, short>
									m_spellLearningFailure;
			adnd::skills::character_skill_value
									m_intelligenceScore;
			short					m_casterLevel;
		};

		class holy_symbol
		{
		public:
			holy_symbol(const defs::character_class& cls, const defs::deities::deity& deityId);
			~holy_symbol() {}

			bool& enabled() { return m_enabled; }

			void resize(const skills::character_skill_value& wisdomScore, const short& casterLevel);

			void select(const defs::spells::priest_spell& spellId);
			void remove(const defs::spells::priest_spell& spellId);

			short max_level();
			short capacity(const short& level);

			const spell_level<defs::spells::priest_spell>& operator[](const int& lvl);

			void pray(const short& hours);
			short pray_all();
			short pray_required();

			void cast(const defs::spells::priest_spell& spellId);

		protected:
			bool					m_enabled;
			defs::character_class	m_classId;
			defs::deities::deity	m_deityId;
			std::vector<spell_level<defs::spells::priest_spell>>
									m_spells;

			std::map<short, tables::priest_spell_progression>
									m_progression;
			adnd::skills::character_skill_value
									m_wisdomScore;
			short					m_casterLevel;
			bool					m_useBonusForWisdom;

			short	get_bonus_spells(const short& level);
			void	populate_level(const defs::character_class& cls, const short& casterLevel, const short& spellLevel);
		};
	}
}