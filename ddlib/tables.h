#pragma once

#include "defs.h"
#include "random.h"

#include <string>
#include <tuple>


namespace adnd
{
	namespace tables
	{
		////////////////////////////////////////////////////////////////////////////////
		// Classes, types and descriptive data
		////////////////////////////////////////////////////////////////////////////////
		struct class_id
		{
			uint16_t	classType;
			uint32_t	classId;
			bool		multiclass;
		};

		struct class_info
		{
			defs::character_class	id;
			uint16_t				classType;
			uint16_t				classId;
			char					longName[80];
			char					shortName[32];
			char					acronym[5];
			bool					multiclass;

			std::string get_long_name() const;
			std::string get_short_name() const;
			std::string get_acronym() const;
		};

		struct class_type_info
		{
			class_type_info() : id(defs::character_class_type::none), hpAfterTitle(0), hitDice(), titleLevel(0)
			{
				memset(description, 0, sizeof(description));
				memset(title, 0, sizeof(title));
			}

			defs::character_class_type	id;
			char						description[80];
			short						titleLevel;
			char						title[32];
			random::die					hitDice;
			short						hpAfterTitle;

			std::string get_description() const;
			const random::die hit_dice() const;
		};

		struct moral_alignment_info
		{
			defs::moral_alignment		id;
			char						name[30];
			char						acronym[2];
			unsigned short				moralOrderId;
			char						moralOrderName[30];
			char						moralOrderAcronym[2];
			unsigned short				moralViewId;
			char						moralViewName[30];
			char						moralViewAcronym[2];
		};

		struct coin_info
		{
			defs::coin	id;
			char		acronym[2];
			char		description[16];

			std::string get_acronym() const;
			std::string get_description() const;
		};

		struct race_info
		{
			char		name[50];
			char		acronym[8];
			short		baseMovementRate;

			std::string get_name() const;
			std::string get_acronym() const;
		};

		////////////////////////////////////////////////////////////////////////////////
		// Class skills
		////////////////////////////////////////////////////////////////////////////////
		struct saving_throws
		{
		public:
			saving_throws() {}
			saving_throws(const short& par, const short& rod, const short& petr, const short& bre, const short& spl)
			{
				m_savings = std::tuple<short, short, short, short, short>(par, rod, petr, bre, spl);
			}

			short& paralysation() { return save<defs::saving_throws::paralysation>(); }
			short& poison() { return save<defs::saving_throws::poison>(); }
			short& death_magic() { return save<defs::saving_throws::death_magic>(); }
			short& rod() { return save<defs::saving_throws::rod>(); }
			short& staff() { return save<defs::saving_throws::staff>(); }
			short& wand() { return save<defs::saving_throws::wand>(); }
			short& petrification() { return save<defs::saving_throws::petrification>(); }
			short& polymorph() { return save<defs::saving_throws::polymorph>(); }
			short& breath_weapon() { return save<defs::saving_throws::breath_weapon>(); }
			short& spell() { return save<defs::saving_throws::spell>(); }

			template <defs::saving_throws s>
			short& save() { return std::get<static_cast<int>(s)>(m_savings); }

			const short& save(const adnd::defs::saving_throws& type) const;

		protected:
			std::tuple<short, short, short, short, short> m_savings;
		};

		struct thief_ability
		{
		public:
			thief_ability() {}
			thief_ability(const short& pick, const short& open, const short& traps, const short& move, const short& hide, const short& hear, const short& climb, const short& read)
			{
				m_abilities = std::tuple<short, short, short, short, short, short, short, short>(pick, open, traps, move, hide, hear, climb, read);
			}

			short pick_pockets() { return ability<defs::thief_ability::pick_pockets>(); }
			short open_locks() { return ability<defs::thief_ability::open_locks>(); }
			short find_remove_traps() { return ability<defs::thief_ability::find_remove_traps>(); }
			short move_silently() { return ability<defs::thief_ability::move_silently>(); }
			short hide_in_shadows() { return ability<defs::thief_ability::hide_in_shadows>(); }
			short hear_noise() { return ability<defs::thief_ability::hear_noise>(); }
			short climb_walls() { return ability<defs::thief_ability::climb_walls>(); }
			short read_languages() { return ability<defs::thief_ability::read_languages>(); }

			template <defs::thief_ability t>
			short ability() const { return std::get<static_cast<int>(t)>(m_abilities); }

			short ability(const defs::thief_ability& type) const;

		protected:
			std::tuple<short, short, short, short, short, short, short, short> m_abilities;
		};

		struct turn_undead
		{
			turn_undead() : m_res(defs::turn_effect::turn), m_turnChance(0) {}
			turn_undead(const defs::turn_effect& res, const short chance)
				: m_res(res), m_turnChance(chance)
			{}

			const defs::turn_effect& type() const { return m_res; }
			const short& chance() const { return m_turnChance; }

			friend std::ostream& operator<< (std::ostream &out, const turn_undead &t)
			{
				if (t.type() == defs::turn_effect::percentage)
					out << t.chance();
				else if (t.type() == defs::turn_effect::turn)
					out << "T";
				else if (t.type() == defs::turn_effect::destroy)
					out << "D";
				else if (t.type() == defs::turn_effect::destroy_more)
					out << "D*";
				else if (t.type() == defs::turn_effect::no_effect)
					out << "-";
				return out;
			}

		protected:
			defs::turn_effect		m_res;
			short					m_turnChance;
		};

		struct turn_ability
		{
		public:
			turn_ability() {}
			turn_ability(tables::turn_undead skel_hd1, tables::turn_undead zombie, tables::turn_undead ghoul_hd2, tables::turn_undead shadow_hd3_4, tables::turn_undead wight_hd5, tables::turn_undead ghast, tables::turn_undead wraith_hd6,
				tables::turn_undead mummy_hd7, tables::turn_undead spectre_hd8, tables::turn_undead vamp_hd9, tables::turn_undead ghost_hd10, tables::turn_undead lich_hd11_more, tables::turn_undead special)
			{
				m_score = std::tuple<tables::turn_undead, tables::turn_undead, tables::turn_undead, tables::turn_undead, tables::turn_undead, tables::turn_undead, tables::turn_undead, tables::turn_undead, tables::turn_undead, tables::turn_undead, tables::turn_undead, tables::turn_undead, tables::turn_undead>
					(skel_hd1, zombie, ghoul_hd2, shadow_hd3_4, wight_hd5, ghast, wraith_hd6,
						mummy_hd7, spectre_hd8, vamp_hd9, ghost_hd10, lich_hd11_more, special);
			}

			template <defs::turn_ability type>
			const tables::turn_undead& turn() const { return std::get<static_cast<int>(type)>(m_score); }

			const tables::turn_undead& turn(const defs::turn_ability& type) const;

		protected:
			std::tuple<tables::turn_undead, tables::turn_undead, tables::turn_undead, tables::turn_undead, tables::turn_undead, tables::turn_undead, tables::turn_undead, tables::turn_undead,
				tables::turn_undead, tables::turn_undead, tables::turn_undead, tables::turn_undead, tables::turn_undead>
				m_score;
		};

		////////////////////////////////////////////////////////////////////////////////
		// Statistics and static data
		////////////////////////////////////////////////////////////////////////////////
		struct strength_stats
		{
			short	hitModifier;
			short	damageAdjustment;
			double	weightAllowance;
			double	maximumPress;
			short	openDoors;
			short	openDoorsSpecial;
			short	bendBarsLiftGates;
		};
		
		struct dexterity_stats
		{
			short	reactionAdjustment;
			short	missileAttackAdjustment;
			short	defensiveAdjustment;
		};
		
		struct constitution_stats
		{
			short	hitPointAdjustment;
			short	hitPointAdjustmentWarriors;
			short	rollsUpgrade;
			short	systemShock;
			short	resurrectionSurvival;
			short	poisonSave;
			short	regenerationPoints;
			short	regenerationTurns;
		};
		
		struct intelligence_stats
		{
			short	numberOfLanguages;
			short	spellLevel;
			short	chanceToLearnSpell;
			short	maxNumberOfSpellsPerLevel;
			short	illusionImmunity;
		};
		
		struct wisdom_stats
		{
			short	magicalDefenceAdjustment;
			short	bonusSpellLevel1;
			short	bonusSpellLevel2;
			short	bonusSpellLevel3;
			short	bonusSpellLevel4;
			short	bonusSpellLevel5;
			short	bonusSpellLevel6;
			short	bonusSpellLevel7;
			short	chanceOfSpellFailure;
			short	spellImmunity;

			short bonusSpellLevel(const short& level)
			{
				if (level == 1)
					return bonusSpellLevel1;
				else if (level == 2)
					return bonusSpellLevel2;
				else if (level == 3)
					return bonusSpellLevel3;
				else if (level == 4)
					return bonusSpellLevel4;
				else if (level == 5)
					return bonusSpellLevel5;
				else if (level == 6)
					return bonusSpellLevel6;
				else if (level == 7)
					return bonusSpellLevel7;
				else
					return 0;
			}
		};
		
		struct charisma_stats
		{
			short	maximumNumberOfHenchmen;
			short	loyaltyBase;
			short	reactionAdjustment;
		};

		/*struct racial_stats
		{
			short				heightBaseMale;
			short				heightBaseFemale;
			short				heightDiceNumber;
			defs::die_faces		heightDiceFaces;
			short				heightDiceNumber2;
			defs::die_faces		heightDiceFaces2;
			short				weightBaseMale;
			short				weightBaseFemale;
			short				weightDiceNumber;
			defs::die_faces		weightDiceFaces;
			short				weightDiceNumber2;
			defs::die_faces		weightDiceFaces2;
			short				ageStarting;
			short				ageDiceNumber;
			defs::die_faces		ageDiceFaces;
			short				ageMaximum;
			short				ageMaximumDiceNumber;
			defs::die_faces		ageMaximumDiceFaces;
			short				middleAge;
			short				oldAge;
			short				venerableAge;
			short				limitAge;
		};*/

		struct thaco_info
		{
			short	score;
			short	factor;
		};

		//struct skill_limit
		//{
		//	short	minValue;
		//	short	maxValue;
		//};

		//struct starting_money_info
		//{
		//	defs::character_class_type		classType;
		//	random::die_roll				die;
		//	short							rollBase;
		//	short							multiplier;
		//};

		struct deity_info
		{
			defs::deities::deity		deityId;
			char						name[50];
			defs::deities::rank			deityRank;
			defs::moral_alignment		deityAlignment;
			defs::planes::plane			deityPlane;
			defs::planes::realm			deityRealm;
			defs::moral_alignment		worshipperAlignment;
		};

		struct skill_info
		{
			defs::character_skill		skillId;
			char						name[20];
		};

		struct proficiency_slot_info
		{
			short			weaponInitialScore;
			short			weaponNumLevels;
			short			hitPenalty;
			short			nonWeaponInitialScore;
			short			nonWeaponNumLevels;
		};

		////////////////////////////////////////////////////////////////////////////////
		// Modifiers
		////////////////////////////////////////////////////////////////////////////////
		//struct ageing_modifiers
		//{
		//	defs::character_age	id;
		//	uint8_t				strengthModifier;
		//	uint8_t				dexterityModifier;
		//	uint8_t				constitutionModifier;
		//	uint8_t				intelligenceModifier;
		//	uint8_t				wisdomModifier;
		//	uint8_t				charismaModifier;
		//
		//	uint8_t value(const defs::character_skill& skill)
		//	{
		//		if (skill == defs::character_skill::strength)
		//			return strengthModifier;
		//		else if (skill == defs::character_skill::dexterity)
		//			return dexterityModifier;
		//		else if (skill == defs::character_skill::constitution)
		//			return constitutionModifier;
		//		else if (skill == defs::character_skill::intelligence)
		//			return intelligenceModifier;
		//		else if (skill == defs::character_skill::wisdom)
		//			return wisdomModifier;
		//		else //if (skill == defs::character_skill::charisma)
		//			return charismaModifier;
		//	}
		//};

		struct prime_requisite_bonuses
		{
			uint8_t				abilityScore;
			short				additionalLevels;
		};

		struct modified_movement_rates
		{
			//skills::character_skill_value	strengthFrom;
			//skills::character_skill_value	strengthTo;
			double							load;
			short							movementRate1;
			short							movementRate2;
		};

		////////////////////////////////////////////////////////////////////////////////
		// Spells
		////////////////////////////////////////////////////////////////////////////////
		struct priest_spell_progression
		{
			short	level;
			short	castingSpell;
			short	spells[defs::holy_symbol_max_level];
		};

		struct wizard_spell_progression
		{
			short	level;
			short	spells[defs::spell_book_max_level];
		};

		////////////////////////////////////////////////////////////////////////////////
		// Combat
		////////////////////////////////////////////////////////////////////////////////
		struct attack_rate
		{
			defs::weapons::attack_type		type;
			short							level;
			short							attacks;
			short							rounds;
		};

		struct rate_of_fire
		{
			short	attacks;
			short	rounds;
		
		};

		struct missile_weapon_range
		{
			defs::equipment::item		weaponId;
			defs::equipment::item		projectileId;
			//rate_of_fire				rof;
			short						rofAttacks;
			short						rofRounds;
			short						shortDistanceFrom;
			short						shortDistanceTo;
			short						mediumDistanceFrom;
			short						mediumDistanceTo;
			short						longDistanceFrom;
			short						longDistanceTo;
			short						shortDistanceModifier;
			short						mediumDistanceModifier;
			short						longDistanceModifier;
		};

		struct specialist_attacks_per_round
		{
			short			level;
			rate_of_fire	meleeWeapon;
			rate_of_fire	lightBow;
			rate_of_fire	heavyBow;
			rate_of_fire	thrownDagger;
			rate_of_fire	thrownDart;
			rate_of_fire	otherMissile;
		};

		////////////////////////////////////////////////////////////////////////////////
		// Inventory, items and equpment
		////////////////////////////////////////////////////////////////////////////////
		struct item_info
		{
			defs::equipment::item		id;
			char						name[50];
			defs::item_type				type;
			defs::item_hand_required	handRequired;
			defs::coin					currency;
			double						minCost;
			double						maxCost;
			double						weight;
		};

		struct clothing_info : public item_info
		{
			defs::body_slot		bodySlot;
		};

		struct armour_info : public item_info
		{
			defs::body_slot		bodySlot;
			uint16_t			acBonusMelee;
			uint16_t			acBonusMissile;
			uint16_t			numOfAttackProtection;
			uint16_t			protections;
			bool				allowsWeapons;
		};

		struct projectile_info : public item_info
		{
			defs::weapons::size				weaponSize;
			defs::weapons::type				weaponType;
			uint16_t						speedFactor;
			defs::die_faces					damageFaces_SM;
			short							damageNumRolls_SM;
			short							damageBase_SM;
			defs::die_faces					damageFaces_L;
			short							damageNumRolls_L;
			short							damageBase_L;
		};

		struct weapon_info : public item_info
		{
			defs::weapons::size				weaponSize;
			defs::weapons::type				weaponType;
			uint16_t						speedFactor;
			defs::die_faces					damageFaces_SM;
			short							damageNumRolls_SM;
			short							damageBase_SM;
			defs::die_faces					damageFaces_L;
			short							damageNumRolls_L;
			short							damageBase_L;
			defs::weapons::attack_type		attackType;
			defs::weapons::group			weaponGroup;
		};

		struct weapon_projectile_info
		{
			defs::equipment::item		weapon;
			defs::equipment::item		projectile;
		};

		struct treasure_class_info
		{
			defs::treasure::treasure_class		id;
			char								acronym;
			defs::treasure::location			location;
		};

		struct treasure_component_info
		{
			defs::treasure::component			id;
			char								name[50];
		};

		struct treasure_location_info
		{
			defs::treasure::location			id;
			char								name[50];
		};

		struct treasure_nature_info
		{
			defs::treasure::nature				id;
			char								name[50];
		};

		struct gem_info
		{
			defs::treasure::gems				id;
			char								name[50];
			defs::treasure::gem_type			type;
			char								type_name[50];
			defs::coin							value_currency;
			int									value_from;
			int									value_to;
		};
		
		struct gem_type_info
		{
			defs::treasure::gem_type			id;
			char								name[50];
			defs::coin							base_currency;
			int									base_value;
			int									probability_from;
			int									probability_to;
		};

		//struct object_of_art_info
		//{
		//	short			probability_from;
		//	short			probability_to;
		//	defs::coin		currency;
		//	uint32_t		value_from;
		//	uint32_t		value_to;
		//};

		struct magical_item_info
		{
			defs::magical::item			id;
			char						name[50];
			defs::item_type				type;
			defs::item_hand_required	handRequired;
			defs::weapons::attack_type	attackType;
			defs::weapons::group		weaponGroup;
			char						unidentifiedName[50];
			defs::body_slot				bodySlot;
			defs::equipment::item		equipmentId;
			defs::item_type				itemType;
		};

		struct treasure_composition_info
		{
			defs::treasure::treasure_class		treasureClass;
			defs::treasure::component			component;
			uint32_t							countFrom;
			uint32_t							countTo;
			short								probability;
			short								nature;
			short								additionalNature;
			uint32_t							additionalCount;
		};
	}
}