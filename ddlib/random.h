#pragma once

#include "defs.h"

#include <random>


namespace adnd
{
	namespace random
	{
		struct die
		{
			die();
			die(const defs::die_faces& numFaces);

			template <typename _RetType = uint8_t>
			_RetType roll(short numRolls = 1) const
			{
				_RetType result = _RetType();
				
				std::random_device rd;
				std::mt19937 generator(rd());	//mersenne_twister_engine
				std::uniform_int_distribution<int> distribution(1, static_cast<int>(m_numFaces));

				for (int r = 0; r < numRolls; ++r)
				{
					int roll = distribution(generator);
					result += static_cast<_RetType>(roll);
				}
				return result;
			}

			friend std::ostream& operator<< (std::ostream& out, const die& d)
			{
				out << "d" << static_cast<short>(d.faces());
				return out;
			}
			operator short()
			{
				return static_cast<short>(faces());
			}

			defs::die_faces faces() const { return m_numFaces; }

		private:
			defs::die_faces		m_numFaces;
		};

		struct die_roll
		{
			die_roll();
			die_roll(const defs::die_faces& numFaces, const short& numRolls = 1);

			defs::die_faces faces() const;
			short rolls() const;

			template <typename _RetType = uint8_t>
			_RetType roll() const { return m_die.roll<_RetType>(m_rolls); }
			operator short() { return roll<short>(); }

			friend std::ostream& operator<< (std::ostream& out, const die_roll& d)
			{
				out << d.roll<short>();
				return out;
			}

		private:
			random::die		m_die;
			short			m_rolls;
		};

		struct dice_set
		{
			dice_set();
			dice_set(const size_t size);

			dice_set& operator+=(const random::die& d);
			const random::die& operator[] (const size_t& position) const;
			
			template <typename _RetType = uint8_t>
			_RetType roll(short numRolls = 1) const
			{
				_RetType ret = _RetType();
				for (auto d : m_dice)
					ret += d.roll<_RetType>(numRolls);
				return ret;
			}
			size_t size() const;
			defs::die_faces faces(const size_t& position) const;
			void reset();

			friend std::ostream& operator<<(std::ostream& out, dice_set& set)
			{
				for (size_t s=0; s<set.size(); ++s)
				{
					if (s != 0)
						out << "/";
					out << static_cast<short>(set.faces(s));
				}
				return out;
			}

		protected:
			std::vector<random::die>	m_dice;
		};
	}
}