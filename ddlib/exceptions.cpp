#include "stdafx.h"
#include "exceptions.h"

#include <sstream>
#include "templates.h"

adnd::exceptions::class_not_allowed_for_race::class_not_allowed_for_race(const adnd::defs::character_class& cls, const adnd::defs::character_race& race)
	: std::exception(), m_class(cls), m_race(race)
{
	std::stringstream ss;

	auto p = templates::metadata::get_character_type_and_id(cls);
	auto resCls = templates::metadata::get_instance().fetch<uint16_t, uint16_t, uint32_t, std::string, std::string, std::string, char>(templates::metadata::query::class_info);
	auto rc = resCls.filter<uint32_t>(p.classId).run().first();

	auto resRace = templates::metadata::get_instance().fetch<std::string, std::string, short>(templates::metadata::query::race_info);
	auto rr = resRace.filter<uint16_t>(static_cast<uint16_t>(race)).run().first();

	ss << "Character class " << std::get<3>(rc) << " not allowed for the race " << std::get<0>(rr);
	m_message = ss.str();
}

adnd::exceptions::class_not_allowed_for_race::~class_not_allowed_for_race()
{
}

char const* adnd::exceptions::class_not_allowed_for_race::what() const
{
	return m_message.c_str();
}


adnd::exceptions::target_out_of_range::target_out_of_range(const defs::equipment::item& it, const short& distance)
	: std::exception(), m_itemId(it), m_distance(distance)
{
	std::stringstream ss;
	//auto info = adnd::templates::metadata::get_instance().get_item_info(it);
	ss << "Target out of range";// << clsInfo.get_long_name() << " not allowed for the race " << raceInfo.get_name();
	m_message = ss.str();
}

adnd::exceptions::target_out_of_range::~target_out_of_range()
{
}

char const* adnd::exceptions::target_out_of_range::what() const
{
	return m_message.c_str();
}