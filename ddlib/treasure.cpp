//#include "stdafx.h"
#include "treasure.h"

#include <random>
#include <iomanip>

bool adnd::treasure::treasure_pool::m_initialised = false;

adnd::treasure::treasure_pool& adnd::treasure::treasure_pool::get_instance()
{
	static treasure_pool _instance;

	if (!m_initialised)
	{
		_instance.m_lastItemId = 0;

		auto types = templates::metadata::get_instance().fetch<short, std::string, short, int, short, short>(templates::metadata::query::gem_types).run();
		for (auto t : types)
		{
			_instance.m_gemTypeProbability[std::get<5>(t)] = static_cast<defs::treasure::gem_type>(std::get<0>(t));
		}

		auto probabilities = templates::metadata::get_instance().fetch<short, short, short, uint32_t, uint32_t>(templates::metadata::query::objects_of_art).run();
		for (auto prob : probabilities)
		{
			auto info = std::make_tuple(std::get<0>(prob), std::get<1>(prob), static_cast<defs::coin>(std::get<2>(prob)), std::get<3>(prob), std::get<4>(prob));
			_instance.m_objectsOfArtProbability[std::get<1>(prob)] = info;
		}

		m_initialised = true;
	}
	return _instance;
}

const adnd::treasure::treasure& adnd::treasure::treasure_pool::create(const defs::treasure::treasure_class& cls)
{
	uint32_t treasureUid = static_cast<uint32_t>(m_treasures.size());
	m_treasures[treasureUid] = treasure(treasureUid, cls);
	treasure& t = m_treasures[treasureUid];

	auto templ = templates::metadata::get_instance().get_treasure_template(cls);
	for (auto comp : templ.components())
	{
		std::random_device rd;
		std::mt19937 generator(rd());
		std::uniform_real_distribution<double> distribution(0.0, 100.0);

		if (distribution(generator) <= static_cast<double>(templ[comp].probability))
		{
			if (comp == defs::treasure::component::copper)
			{
				t += create_amount(defs::coin::copper, templ[comp].countFrom, templ[comp].countTo);
			}
			else if (comp == defs::treasure::component::silver)
			{
				t += create_amount(defs::coin::silver, templ[comp].countFrom, templ[comp].countTo);
			}
			else if (comp == defs::treasure::component::gold)
			{
				t += create_amount(defs::coin::gold, templ[comp].countFrom, templ[comp].countTo);
			}
			else if (comp == defs::treasure::component::platinum_or_electrum)
			{
				std::random_device rd;
				std::mt19937 generator(rd());
				std::uniform_int_distribution<int> distribution(1, 100);

				if (distribution(generator) % 2 == 0)
					t += create_amount(defs::coin::platinum, templ[comp].countFrom, templ[comp].countTo);
				else
					t += create_amount(defs::coin::electrum, templ[comp].countFrom, templ[comp].countTo);
			}
			else if (comp == defs::treasure::component::gems)
			{
				std::random_device rd;
				std::mt19937 generator(rd());
				std::uniform_int_distribution<uint32_t> distribution(templ[comp].countFrom, templ[comp].countTo);
				uint32_t count = distribution(generator);

				for (uint32_t cnt = 0; cnt < count; ++cnt)
				{
					auto gemID = pick_gem();
					auto gt = templates::metadata::get_instance().get_gem_template(gemID);

					money::amount gemValue = create_amount(gt.get_value_currency(), gt.get_value_from(), gt.get_value_to());
					gem g(m_lastItemId++, gemID, gemValue);

					t += g;
				}
			}
			else if (comp == defs::treasure::component::art_objects)
			{
				std::random_device rd;
				std::mt19937 generator(rd());
				std::uniform_int_distribution<uint32_t> distribution(templ[comp].countFrom, templ[comp].countTo);
				uint32_t count = distribution(generator);

				for (uint32_t cnt = 0; cnt < count; ++cnt)
				{
					auto d = random::die(defs::die_faces::ten);
					auto prob = (d.roll<short>() - 1) * 10 + (d.roll<short>() - 1);

					auto objInfo = m_objectsOfArtProbability.lower_bound(prob)->second;

					money::amount objValue = create_amount(std::get<2>(objInfo), std::get<3>(objInfo), std::get<4>(objInfo));
					object_of_art o(m_lastItemId++, objValue);
				
					t += o;
				}
			}
			else if (comp == defs::treasure::component::magical_item
					|| comp == defs::treasure::component::additional_magical_item)
			{
				std::random_device rd;
				std::mt19937 generator(rd());
				std::uniform_int_distribution<uint32_t> distribution(templ[comp].countFrom, templ[comp].countTo);
				uint32_t count = distribution(generator);

				for (uint32_t cnt = 0; cnt < count; ++cnt)
				{
					auto magicalID = pick_magical();
					auto mt = templates::metadata::get_instance().get_magical_item_template(magicalID);

					uint32_t uid = equipment::items_pool::get_instance().new_uid();

					equipment::magical_item magItem(uid, mt.id(), mt.description(), mt.item_class(), mt.item_type(), mt.weight(),
												mt.hand_required(), mt.unidentified_name(), mt.magical_id(), mt.attack_type(),
												mt.weapon_group(), mt.body_slot());
					t += magItem;
				}
			}
		}
	}

	return t;
}

void adnd::treasure::treasure_pool::detatch(const uint32_t& uid)
{
	m_treasures.erase(uid);
}

adnd::money::amount adnd::treasure::treasure_pool::create_amount(const defs::coin& coin, const uint32_t& from, const uint32_t& to)
{
	uint32_t amount = 0;

	if (from == to)
	{
		amount = money::amount(coin, from);
	}
	else
	{
		std::random_device rd;
		std::mt19937 generator(rd());
		std::uniform_int_distribution<uint32_t> distribution(from, to);

		amount = distribution(generator);
	}

	return money::amount(coin, amount);
}

adnd::defs::treasure::gems adnd::treasure::treasure_pool::pick_gem()
{
	auto d = random::die(defs::die_faces::ten);
	auto prob = (d.roll<short>()-1) * 10 + (d.roll<short>()-1);

	auto typeID = m_gemTypeProbability.lower_bound(prob)->second;

	auto r = templates::metadata::get_instance().fetch<short>(templates::metadata::query::gems_by_type);
	auto gems = r.filter<short>(static_cast<short>(typeID)).run();

	std::random_device rd;
	std::mt19937 generator(rd());
	std::uniform_int_distribution<short> distribution(0, static_cast<short>(gems.size()) - 1);
	
	auto index = distribution(generator);
	auto g = std::get<0>(gems.at(index));
	
	return static_cast<defs::treasure::gems>(g);
}

adnd::defs::magical::item adnd::treasure::treasure_pool::pick_magical()
{
	auto d = random::die(defs::die_faces::ten);
	auto prob = (d.roll<short>() - 1) * 10 + (d.roll<short>() - 1) + 1;
	
	defs::magical::item it;
	if (prob >= 1 && prob <= 20)
	{
		it = pick_potion();
	}
	else if (prob >= 21 && prob <= 35)
	{
		it = pick_scroll();
	}
	else if (prob >= 36 && prob <= 40)
	{
		it = pick_ring();
	}
	else if (prob == 41)
	{
		it = pick_rod();
	}
	else if (prob == 42)
	{
		it = pick_stave();
	}
	else if (prob >= 43 && prob <= 45)
	{
		it = pick_wand();
	}
	else if (prob == 46)
	{
		it = pick_book();
	}
	else if (prob >= 47 && prob <= 48)
	{
		it = pick_jewel();
	}
	else if (prob >= 49 && prob <= 50)
	{
		it = pick_cloak();
	}
	else if (prob >= 51 && prob <= 52)
	{
		it = pick_boot();
	}
	else if (prob == 53)
	{
		it = pick_girdle();
	}
	else if (prob >= 54 && prob <= 55)
	{
		it = pick_bag();
	}
	else if (prob == 56)
	{
		it = pick_dust();
	}
	else if (prob == 57)
	{
		it = pick_household();
	}
	else if (prob == 58)
	{
		it = pick_musical();
	}
	else if (prob >= 59 && prob <= 60)
	{
		it = pick_wierd();
	}
	else if (prob >= 61 && prob <= 75)
	{
		it = pick_armour();
	}
	else if (prob >= 76 && prob <= 100)
	{
		it = pick_weapon();
	}
	
	return it;
}


adnd::defs::magical::item adnd::treasure::treasure_pool::pick_potion()
{
	auto d = random::die(defs::die_faces::six);
	auto subtable = d.roll<short>();

	auto p = random::die(defs::die_faces::twenty);
	auto prob = d.roll<short>();

	defs::magical::item it = defs::magical::item::none;

	if (subtable == 1 || subtable == 2)
	{
		//switch (prob)
		//{
		//case 1:	it = defs::magical::item::po
		//}
	}
	else if (subtable == 3 || subtable == 4)
	{

	}
	else if (subtable == 5 || subtable == 6)
	{

	}

	return it;
}

adnd::defs::magical::item adnd::treasure::treasure_pool::pick_scroll()
{
	defs::magical::item it = defs::magical::item::none;
	return it;
}

adnd::defs::magical::item adnd::treasure::treasure_pool::pick_ring()
{
	defs::magical::item it = defs::magical::item::none;
	return it;
}

adnd::defs::magical::item adnd::treasure::treasure_pool::pick_rod()
{
	defs::magical::item it = defs::magical::item::none;
	return it;
}

adnd::defs::magical::item adnd::treasure::treasure_pool::pick_stave()
{
	defs::magical::item it = defs::magical::item::none;
	return it;
}

adnd::defs::magical::item adnd::treasure::treasure_pool::pick_wand()
{
	defs::magical::item it = defs::magical::item::none;
	return it;
}

adnd::defs::magical::item adnd::treasure::treasure_pool::pick_book()
{
	defs::magical::item it = defs::magical::item::none;
	return it;
}

adnd::defs::magical::item adnd::treasure::treasure_pool::pick_jewel()
{
	defs::magical::item it = defs::magical::item::none;
	return it;
}

adnd::defs::magical::item adnd::treasure::treasure_pool::pick_cloak()
{
	defs::magical::item it = defs::magical::item::none;
	return it;
}

adnd::defs::magical::item adnd::treasure::treasure_pool::pick_boot()
{
	defs::magical::item it = defs::magical::item::none;
	return it;
}

adnd::defs::magical::item adnd::treasure::treasure_pool::pick_girdle()
{
	defs::magical::item it = defs::magical::item::none;
	return it;
}

adnd::defs::magical::item adnd::treasure::treasure_pool::pick_bag()
{
	defs::magical::item it = defs::magical::item::none;
	return it;
}

adnd::defs::magical::item adnd::treasure::treasure_pool::pick_dust()
{
	defs::magical::item it = defs::magical::item::none;
	return it;
}

adnd::defs::magical::item adnd::treasure::treasure_pool::pick_household()
{
	defs::magical::item it = defs::magical::item::none;
	return it;
}

adnd::defs::magical::item adnd::treasure::treasure_pool::pick_musical()
{
	defs::magical::item it = defs::magical::item::none;
	return it;
}

adnd::defs::magical::item adnd::treasure::treasure_pool::pick_wierd()
{
	defs::magical::item it = defs::magical::item::none;
	return it;
}

adnd::defs::magical::item adnd::treasure::treasure_pool::pick_armour()
{
	defs::magical::item it = defs::magical::item::none;
	return it;
}

adnd::defs::magical::item adnd::treasure::treasure_pool::pick_weapon()
{
	defs::magical::item it = defs::magical::item::none;
	return it;
}


adnd::treasure::treasure::treasure(const uint32_t uid, const defs::treasure::treasure_class& cls)
	: m_uid(uid), m_class(cls)
{
	m_coins.init();
}

adnd::treasure::treasure& adnd::treasure::treasure::operator+=(const money::amount& amt)
{
	m_coins.add(amt);
	return (*this);
}

adnd::treasure::treasure& adnd::treasure::treasure::operator+=(const gem& g)
{
	m_gems.push_back(g);
	return (*this);
}

adnd::treasure::treasure& adnd::treasure::treasure::operator+=(const object_of_art& o)
{
	m_objectsOfArt.push_back(o);
	return (*this);
}

adnd::treasure::treasure& adnd::treasure::treasure::operator+=(const equipment::magical_item& m)
{
	m_magicalItems.push_back(m);
	return (*this);
}

std::set<adnd::defs::coin> adnd::treasure::treasure::coins()
{
	std::set<defs::coin> result;

	for (auto c : { defs::coin::copper, defs::coin::silver, defs::coin::electrum, defs::coin::gold, defs::coin::platinum })
	{
		if (m_coins.get(c).value() > 0)
			result.emplace(c);
	}
	return result;
}

const adnd::money::amount& adnd::treasure::treasure::coins(const defs::coin& currency)
{
	return m_coins.get(currency);
}

const std::list<adnd::treasure::gem>& adnd::treasure::treasure::gems()
{
	return m_gems;
}

const std::list<adnd::treasure::object_of_art>& adnd::treasure::treasure::objects_of_art()
{
	return m_objectsOfArt;
}

double adnd::treasure::treasure::total_value(const adnd::defs::coin& currency)
{
	double treasureValue = 0.0;
	auto ct = templates::metadata::get_instance().get_coin_template(currency);

	for (auto c : { defs::coin::copper, defs::coin::silver, defs::coin::electrum, defs::coin::gold, defs::coin::platinum })
	{
		auto val = m_coins.get(c).value();
		if (val > 0)
		{
			double fx = ct.from(c);
			treasureValue += val * fx;
		}
	}
	for (auto g : m_gems)
	{
		money::amount amt = g.value();
		double fx = ct.from(amt.currency());
		treasureValue += amt.value() * fx;
	}
	for (auto o : m_objectsOfArt)
	{
		money::amount amt = o.value();
		double fx = ct.from(amt.currency());
		treasureValue += amt.value() * fx;
	}
	return treasureValue;
}

adnd::treasure::gem::gem(const uint32_t& uid, const adnd::defs::treasure::gems& gemId, const money::amount& value)
	: m_uid(uid), m_gemId(gemId), m_value(value)
{
}

adnd::treasure::object_of_art::object_of_art(const uint32_t& uid, const money::amount& value, const char *name)
	: m_uid(uid), m_value(value)
{
	if (name != nullptr)
		m_name.assign(name);
	else
	{
		std::stringstream ss;
		ss << "Object of art " << std::setfill('0') << std::setw(3) << uid;
		m_name = ss.str().c_str();
	}
}
