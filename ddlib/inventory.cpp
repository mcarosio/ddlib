#include "stdafx.h"
#include "inventory.h"
#include "exceptions.h"


adnd::inventory::inventory_bag::inventory_bag()
{
	for (auto slot : { defs::body_slot::head, defs::body_slot::neck, defs::body_slot::ring_right, defs::body_slot::ring_left,
					defs::body_slot::arms, defs::body_slot::body, defs::body_slot::feet, defs::body_slot::mantle, defs::body_slot::belt })
	{
		m_wearableItems[slot] = nullptr;
	}
}

adnd::inventory::inventory_bag::~inventory_bag()
{
}

double adnd::inventory::inventory_bag::weight()
{
	double totalWeight = 0.0;
	for (auto i : m_collectedItems)
		totalWeight += i.second->weight();
	for (auto i : m_wearableItems)
		totalWeight += (i.second) ? i.second->weight() : 0.0;

	for (auto p : m_projectiles)
	{
		auto t = templates::metadata::get_instance().get_projectile_template(p.first);
		totalWeight += p.second.count() * t->weight();
	}
	return totalWeight;
}

/**
Does not account for projectiles
*/
size_t adnd::inventory::inventory_bag::count()
{
	size_t wearablesCount = 0;
	for (auto i : m_wearableItems)
		wearablesCount += (i.second) ? 1 : 0;
	return m_collectedItems.size() + wearablesCount;
}

/**
Put items into the inventory.
If the item is a projectile, the projectile stock is updated as well if and only if the projectile_stock is enabled (i.e. the character owns the proper container)
*/
adnd::inventory::inventory_bag& adnd::inventory::inventory_bag::operator+=(equipment::item* it)
{
	if (m_collectedItems.find(it->uid()) != m_collectedItems.end())
		throw std::exception("Item already added", static_cast<int>(it->uid()));

	m_collectedItems.emplace(it->uid(), it);

	if (it->item_type() == defs::item_type::projectile)
	{
		auto container = get_projectile_container(it->id());
		if (has(container))
			m_projectiles[it->id()]++;
	}

	return (*this);
}

//adnd::equipment::item* adnd::inventory::inventory_bag::add(const defs::equipment::item& itemId)
//{
//	auto t = templates::metadata::get_instance().get_item_template(itemId);
//	adnd::equipment::item* it = adnd::equipment::items_pool::get_instance().create(itemId, t->description());
//	(*this) += it;
//	return it;
//}
//
//void adnd::inventory::inventory_bag::add(const defs::equipment::item& itemId, const short& count)
//{
//	for (int i = 0; i < count; ++i)
//	{
//		add(itemId);
//	}
//}

void adnd::inventory::inventory_bag::add(equipment::item* it)
{
	(*this) += it;
}


void adnd::inventory::inventory_bag::add(const adnd::treasure::gem& g)
{
	if (m_collectedGems.find(g.uid()) != m_collectedGems.end())
		throw std::exception("Gem already added", static_cast<int>(g.uid()));

	m_collectedGems.emplace(g.uid(), g);
}

void adnd::inventory::inventory_bag::add(const adnd::treasure::object_of_art& o)
{
	if (m_collectedObjectsOfArt.find(o.uid()) != m_collectedObjectsOfArt.end())
		throw std::exception("Object of art already added", static_cast<int>(o.uid()));

	m_collectedObjectsOfArt.emplace(o.uid(), o);
}

/**
The item identified by the unique ID is removed from the inventory.
If the item is a projectile, the projectile stock is updated as well if and only if the projectile_stock is enabled (i.e. the character owns the proper container).
*/
adnd::equipment::item* adnd::inventory::inventory_bag::remove(const uint32_t& itemUid)
{
	adnd::equipment::item* removed = nullptr;

	if (m_collectedItems.find(itemUid) != m_collectedItems.end())
	{
		removed = m_collectedItems[itemUid];

		if (removed->item_type() == defs::item_type::projectile)
		{
			auto container = get_projectile_container(removed->id());
			if (has(container))
				m_projectiles[removed->id()]--;
		}

		m_collectedItems.erase(itemUid);
		return removed;
	}
	else if (!m_wearableItems[defs::body_slot::head] && m_wearableItems[defs::body_slot::head]->uid() == itemUid)
	{
		removed = m_wearableItems[defs::body_slot::head];
		m_wearableItems[defs::body_slot::head] = nullptr;
		return removed;
	}
	else if (!m_wearableItems[defs::body_slot::body] && m_wearableItems[defs::body_slot::body]->uid() == itemUid)
	{
		removed = m_wearableItems[defs::body_slot::body];
		m_wearableItems[defs::body_slot::body] = nullptr;
		return removed;
	}
	else if (!m_wearableItems[defs::body_slot::feet] && m_wearableItems[defs::body_slot::feet]->uid() == itemUid)
	{
		removed = m_wearableItems[defs::body_slot::feet];
		m_wearableItems[defs::body_slot::feet] = nullptr;
		return removed;
	}

	throw std::exception("Unable to find item", static_cast<int>(itemUid));
}

/**
The first instance of item of this type is removed from the inventory.
If the item is a projectile, the projectile stock is updated as well if and only if the projectile_stock is enabled (i.e. the character owns the proper container).
*/
adnd::equipment::item* adnd::inventory::inventory_bag::remove(const defs::equipment::item& itemId)
{
	auto it = find(itemId);
	return remove(it->uid());
}

const adnd::defs::equipment::item adnd::inventory::inventory_bag::get_projectile_container(const defs::equipment::item& projectileId)
{
	adnd::defs::equipment::item container = defs::equipment::item::none;
	if (projectileId == defs::equipment::item::flight_arrow || projectileId == defs::equipment::item::sheaf_arrow)
	{
		container = defs::equipment::item::quiver;
	}
	else if (projectileId == defs::equipment::item::barbed_dart || projectileId == defs::equipment::item::needle)
	{
		container = defs::equipment::item::bolt_case;
	}
	else if (projectileId == defs::equipment::item::hand_quarrel || projectileId == defs::equipment::item::heavy_quarrel || projectileId == defs::equipment::item::light_quarrel)
	{
		container = defs::equipment::item::quiver;
	}
	//else if (projectileId == defs::equipment::item::sling_bullet || projectileId == defs::equipment::item::sling_stone)
	//{
	//	container = defs::equipment::item::
	//}
	//else if (projectileId == defs::equipment::item::gunpowder)
	//{
	//	//container = ???
	//}
	return container;
}

adnd::inventory::projectile_stock& adnd::inventory::inventory_bag::get_projectile_stock(const defs::equipment::item& projectileId)
{
	defs::equipment::item container = get_projectile_container(projectileId);

	if (container != defs::equipment::item::none && has(container))
	{
		auto tp = templates::metadata::get_instance().get_item_template(container);
		std::stringstream ss;
		ss << tp->description() << " required";
		throw std::exception(ss.str().c_str());
	}

	if (m_projectiles.find(projectileId) == m_projectiles.end())
	{
		m_projectiles[projectileId] = 0;
	}
	return m_projectiles[projectileId];
}

bool adnd::inventory::inventory_bag::has(const defs::equipment::item& itemId)
{
	for (auto c : m_collectedItems)
		if (c.second->id() == itemId)
			return true;
	for (auto w : m_wearableItems)
		if (w.second && w.second->id() == itemId)
			return true;

	return false;
}

adnd::equipment::item* adnd::inventory::inventory_bag::find(const uint32_t& itemId)
{
	if (m_collectedItems.find(itemId) != m_collectedItems.end())
		return m_collectedItems[itemId];
	else if (!m_wearableItems[defs::body_slot::head] && m_wearableItems[defs::body_slot::head]->uid() == itemId)
		return m_wearableItems[defs::body_slot::head];
	else if (!m_wearableItems[defs::body_slot::body] && m_wearableItems[defs::body_slot::body]->uid() == itemId)
		return m_wearableItems[defs::body_slot::body];
	else if (!m_wearableItems[defs::body_slot::feet] && m_wearableItems[defs::body_slot::feet]->uid() == itemId)
		return m_wearableItems[defs::body_slot::feet];

	throw std::exception("Item not found", itemId);
}

adnd::equipment::item* adnd::inventory::inventory_bag::find(const defs::equipment::item& itemId)
{
	for (auto c : m_collectedItems)
		if (c.second->id() == itemId)
			return c.second;
	for (auto w : m_wearableItems)
		if (w.second && w.second->id() == itemId)
			return w.second;
	
	throw std::exception("Item not found", static_cast<int>(itemId));
}

adnd::equipment::wearable* adnd::inventory::inventory_bag::wear(const defs::body_slot slot, equipment::wearable* it)
{
	if (m_wearableItems[slot] == nullptr)
	{
		m_wearableItems[slot] = it;
		return nullptr;
	}
	
	auto previousItem = m_wearableItems[slot];
	m_wearableItems[slot] = it;
	return previousItem;
}