#pragma once

#include <map>

#include "defs.h"

namespace adnd
{
	namespace monster
	{
		struct type
		{
			static const uint32_t undead = 0x00000001;
			static const uint32_t elemental = 0x00000002;
			static const uint32_t dragon = 0x00000004;
		};

		class monster
		{
		public:
			monster(const uint32_t& uid, const defs::monsters::monster& id, const std::string& name,
					uint32_t monsterType, const short& morale, const std::string& uniqueName = "");
			~monster() {}

			const uint32_t& uid() { return m_uid; }
			const defs::monsters::monster& id() { return m_id; }
			const std::string& name() { return m_name; }
			const short& morale() { return m_morale; }

			defs::turn_ability turnable_as();

		protected:

			uint32_t					m_uid;
			defs::monsters::monster		m_id;
			std::string					m_name;
			std::string					m_uniqueName;
			uint32_t					m_type;

			short						m_morale;
		};

		class monsters_den
		{
		public:
			static monsters_den& get_instance()
			{
				static monsters_den _instance;
				//_instance.m_lastId = 0;
				return _instance;
			}

			monster* spawn(const defs::monsters::monster& id, const std::string& uniqueName = "");

			monsters_den(monsters_den const&) = delete;
			void operator=(monsters_den const&) = delete;

			~monsters_den();

		private:
			monsters_den() : m_lastId(0) {}

			uint32_t						m_lastId;
			std::map<uint32_t, monster*>	m_monsters;
		};
	}
}