#pragma once

#include "defs.h"
#include "money.h"
#include "equipment.h"

namespace adnd
{
	namespace treasure
	{
		class gem
		{
		public:
			gem() {}
			gem(const uint32_t& uid, const defs::treasure::gems& gemId, const money::amount& value);
			~gem() {}

			const uint32_t& uid() const { return m_uid; }
			const defs::treasure::gems& id() const { return m_gemId; }
			const money::amount& value() const { return m_value; }


		protected:
			uint32_t						m_uid;
			defs::treasure::gems			m_gemId;
			adnd::money::amount				m_value;
		};

		class object_of_art
		{
		public:
			object_of_art() {}
			object_of_art(const uint32_t& uid, const money::amount& value, const char *name = nullptr);
			~object_of_art() {}

			const uint32_t& uid() const { return m_uid; }
			const money::amount& value() const { return m_value; }
			const std::string& name() const { return m_name; }


		protected:
			uint32_t						m_uid;
			adnd::money::amount				m_value;
			std::string						m_name;
		};

		class treasure
		{
		public:
			treasure() {}
			treasure(const uint32_t uid, const defs::treasure::treasure_class& cls);
			~treasure() {}

			const uint32_t& uid() { return m_uid; }

			treasure& operator+= (const adnd::money::amount& amt);
			treasure& operator+= (const gem& g);
			treasure& operator+= (const object_of_art& o);
			treasure& operator+= (const equipment::magical_item& m);

			std::set<defs::coin>			coins();
			const adnd::money::amount&		coins(const defs::coin& currency);
			const std::list<gem>&			gems();
			const std::list<object_of_art>&	objects_of_art();

			double							total_value(const defs::coin& currency);

		protected:
			uint32_t								m_uid;
			defs::treasure::treasure_class			m_class;
			adnd::money::money_bag					m_coins;
			std::list<gem>							m_gems;
			std::list<object_of_art>				m_objectsOfArt;
			std::list<equipment::magical_item>		m_magicalItems;
		};

		class treasure_pool
		{
		public:

			static treasure_pool& get_instance();
			static void init()
			{
				//Ensure first initialisation
				get_instance();
			}
			treasure_pool(treasure_pool const&) = delete;
			void operator=(treasure_pool const&) = delete;

			const treasure& create(const defs::treasure::treasure_class& cls);
			void detatch(const uint32_t& uid);

		private:
			treasure_pool() {}

			static bool						m_initialised;
			std::map<uint32_t, treasure>	m_treasures;
			uint32_t						m_lastItemId;
			std::map<short, defs::treasure::gem_type>
											m_gemTypeProbability;
			std::map<short, std::tuple<short, short, defs::coin, uint32_t, uint32_t>>
											m_objectsOfArtProbability;


			money::amount create_amount(const defs::coin& coin, const uint32_t& from, const uint32_t& to);
			defs::treasure::gems pick_gem();
			defs::magical::item pick_magical();

			defs::magical::item pick_potion();
			defs::magical::item pick_scroll();
			defs::magical::item pick_ring();
			defs::magical::item pick_rod();
			defs::magical::item pick_stave();
			defs::magical::item pick_wand();
			defs::magical::item pick_book();
			defs::magical::item pick_jewel();
			defs::magical::item pick_cloak();
			defs::magical::item pick_boot();
			defs::magical::item pick_girdle();
			defs::magical::item pick_bag();
			defs::magical::item pick_dust();
			defs::magical::item pick_household();
			defs::magical::item pick_musical();
			defs::magical::item pick_wierd();
			defs::magical::item pick_armour();
			defs::magical::item pick_weapon();
		};
	}
}