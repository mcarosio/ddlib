#pragma once

#include <map>
#include <functional>
#include <filesystem>
#include <sstream>

#include "utils.h"
#include "sqlite3.h"


//extern struct sqlite3;
//extern struct sqlite3_stmt;
//extern struct sqlite3_context;
//extern struct sqlite3_value;


namespace db
{
	class sqlite_db
	{
		public:

			using statement_id = size_t;
			using prepared_statement_set = std::map<statement_id, sqlite3_stmt*>;
			using statement_name_set = std::map<std::string, statement_id>;

			enum class source
			{
				file,
				memory
			};

			enum class status
			{
				unknown,
				bound,
				open,
				close
			};

			enum class error_handler_style
			{
				internal,
				exception
			};

			class record : public std::vector<std::string>
			{
			public:
				friend std::ostream& operator<< (std::ostream &out, const db::sqlite_db::record &r)
				{
					std::stringstream ss;
					size_t c = 0;
					for (db::sqlite_db::record::const_iterator it = r.cbegin(); it!=r.cend(); ++it)
					{
						ss << *it;
						if (c < r.size())
						{
							ss << ", ";
						}
						++c;
					}
					out << ss.str();
					return out;
				}

				template <typename _FieldType>
				_FieldType field(const size_t& column, const _FieldType& defaultValue) const
				{
					return adnd::utils::parse<_FieldType>(at(column).c_str(), at(column).length(), defaultValue).value();
				}

				template <>
				std::string field(const size_t& column, const std::string& defaultValue) const
				{
					return at(column);
				}
			};

			using visit_callback = std::function<void (const db::sqlite_db::record&)>;

			class iterator
			{
				friend class sqlite_db;
				public:

					iterator(sqlite_db* const db, const std::string &stmt);
					iterator(sqlite_db* const db, sqlite3_stmt *stmt);
					iterator(sqlite_db* const db, const statement_id& stmtId);
					iterator();
					~iterator();
					
					operator bool()const;
					bool operator==(const iterator& it) const;
					bool operator!=(const iterator& it) const;
					iterator& operator=(iterator& it);
					iterator& operator+=(const int& movement);
					iterator& operator++();
					iterator operator++(int);
					db::sqlite_db::record operator*();
					const db::sqlite_db::record operator*() const;
					int get_result() const;
					sqlite3_stmt const * statement() const;
					const int& columns_count() const;
					const db::sqlite_db::record get_caption() const;
					const std::string get_caption(int column) const;
					std::string sql() const;

					template <typename _Type>
					_Type data(const int col, const _Type& defaultValue) const
					{
						const char *zValue = get_column_text(col);
						return adnd::utils::parse<_Type>(zValue, std::strlen(zValue), defaultValue);
					}

				protected:
					const char * get_column_text(const int col) const;

					int							m_result;
					sqlite_db					*m_db;
					int							m_numCols;
					sqlite3_stmt				*m_stmt;
			};

			sqlite_db(void);
			virtual ~sqlite_db(void);
				
			void error(const char* text);
			void error(const std::string& text);
				
			bool bind();
			bool open(const bool deleteLocal=false);
			bool close(bool saveData = false);
			void drop_tables();
			
			bool clean_db(bool dropTables = false);
			
			bool execute(const std::experimental::filesystem::path& scriptPath);
			bool execute(const std::string& sqlScript);
			bool execute(const char* sqlScript);
			bool execute(const statement_id& stmtId);
			bool execute(const statement_id& stmtId, iterator& itBegin, iterator& itEnd);
			//bool execute(const char* sqlScript, iterator& itBegin, iterator& itEnd);
			//bool execute(const std::string& sqlScript, iterator& itBegin, iterator& itEnd);
			bool execute(const std::string &stmt, db::sqlite_db::record& firstRow);
			bool execute(const statement_id& stmtId, record& firstRow);
			bool execute(const char* stmt, const visit_callback& f);
			bool execute(const std::string& stmt, const visit_callback& f);
			bool execute(const statement_id& stmtId, const visit_callback& f);

			template <typename T>
			bool execute(const std::string &stmt, T& value, const T& defaultValue)
			{
				db::sqlite_db::record r;
				bool ok = execute(stmt, r);
				value = adnd::utils::parse<T>(r[0].c_str(), r[0].length(), defaultValue);
				return ok;
			}

			template <typename T>
			bool execute(const statement_id& stmtId, T& value, const T& defaultValue)
			{
				db::sqlite_db::record r;
				bool ok = execute(stmtId, r);
				value = adnd::utils::parse<T>(r[0].c_str(), r[0].length(), defaultValue);
				return ok;
			}

			bool create_function(const std::string& functionName, long numArgs, void (*funct)(sqlite3_context*, int, sqlite3_value**));

			template <typename T>
			bool bind_parameter(const statement_id& stmtId, const int index, const T& value)
			{
				prepared_statement_set::iterator it = m_preparedStatements.find(stmtId);
				if (it == m_preparedStatements.end())
				{
					error("Invalid prepared statement id");
					return false;
				}

				//sqlite3_value = value;
				std::stringstream ss;
				ss << value;
				return bind_parameter(stmtId, index, ss.str());
			}
			//bool bind_parameter(const statement_id& stmtId, int index, const long& value);
			//bool bind_parameter(const statement_id& stmtId, int index, const double& value);
			bool bind_parameter(const statement_id& stmtId, int index, const std::time_t& value);
			bool bind_parameter(const statement_id& stmtId, int index, const std::string& value);
			bool bind_parameter(const statement_id& stmtId, int index, const char* value);
			void clean_prepared_statements();

			iterator begin(const std::string &stmt);
			iterator begin(sqlite3_stmt *stmt);
			iterator end();
			
			bool begin_transaction() { return execute("BEGIN TRANSACTION"); }
			bool commit_transaction() { return execute("COMMIT TRANSACTION"); }
			bool rollback_transaction() { return execute("ROLLBACK TRANSACTION"); }

			bool explain_query_plan(const std::string &sql, std::vector<std::string> &output);
			unsigned long count_rows(const std::string &tableName);
			bool save(const std::string& dbPathDest);
			bool restore(const std::string& dbSrc);

			std::string sql(const statement_id& stmtId);
			bool register_function(const std::string& functionName, long numArgs, void (*func)(sqlite3_context*, int, sqlite3_value**), std::string &error);

			statement_id prepare_statement(const std::string& statement, const std::string& symbolicName = "");

			sqlite3* const db_connection() const { return m_dbConnection; };
			std::string& db_path() { return m_dbPath; }
			db::sqlite_db::source& db_source() { return m_dbSource; }
			bool is_open() { return m_status == status::open; }
			std::string db_location();
			const std::string& last_error() { return m_lastError; }
			const statement_id& resolve_by_name(const std::string& statementName) { return m_preparedStatementNames[statementName]; }
			error_handler_style& error_handler() { return m_errorHandler; }

			bool add_pragma(const std::string &key, const std::string &value);

		protected:
			status							m_status;
			std::string						m_dbPath;
			db::sqlite_db::source			m_dbSource;
			sqlite3							*m_dbConnection;
			prepared_statement_set			m_preparedStatements;
			statement_name_set				m_preparedStatementNames;
			error_handler_style				m_errorHandler;
			std::string						m_lastError;

			sqlite3_stmt* get_prepared_statement(const db::sqlite_db::statement_id& stmtId);
			//bool bind_parameter(const statement_id& stmtId, const int index, const sqlite3_value& value);
	};
}