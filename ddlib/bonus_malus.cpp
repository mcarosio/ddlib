#include "stdafx.h"
#include "bonus_malus.h"


adnd::bonus_malus::bonus_malus(const short& value, const bonus_malus_type& type)
	: m_value(value), m_type(type)
{
}

adnd::bonus_malus::~bonus_malus()
{
}

adnd::bonus_malus::operator short()
{
	return m_value;
}

adnd::bonus_malus_type adnd::bonus_malus::type()
{
	return m_type;
}

adnd::bonus_sequence::bonus_sequence()
{
}

adnd::bonus_sequence::~bonus_sequence()
{
}

adnd::bonus_sequence& adnd::bonus_sequence::operator+= (const short& value)
{
	if (value > 0)
		m_seq.push_back(bonus_malus(value, bonus_malus_type::bonus));
	else if (value < 0)
		m_seq.push_back(bonus_malus(-value, bonus_malus_type::malus));

	return (*this);
}

adnd::bonus_sequence::operator short()
{
	short bm = 0;
	for (auto& b : m_seq)
	{
		if (b.type() == bonus_malus_type::bonus)
			bm += b;
		else if (b.type() == bonus_malus_type::malus)
			bm -= b;
	}
	return bm;
}

size_t adnd::bonus_sequence::size()
{
	return m_seq.size();
}

void adnd::bonus_sequence::reset()
{
	m_seq.clear();
}