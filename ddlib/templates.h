#pragma once

#include "defs.h"
#include "skills.h"
#include "random.h"
#include "tables.h"
#include "sqlite3.h"
#include "utils.h"
#include "spells.h"

#include <map>
#include <set>
#include <algorithm>
#include <sstream>
#include <list>
#include <type_traits>
#include <ctime>


namespace adnd
{
	class configuration
	{
	public:
	
		enum class error_handler_style
		{
			internal,
			exception
		};
		
		enum class database_source
		{
			file,
			memory
		};

		static configuration& get_instance();
		static void init()
		{
			//Ensure first initialisation
			get_instance();
		}
		configuration(configuration const&) = delete;
		void operator=(configuration const&) = delete;

		bool get(const defs::config::optional& opt);
		void set(const defs::config::optional& opt, bool value);
		void reset();

		std::string&					get_database_path() { return m_dbDir; };
		std::string&					get_definition_script_path() { return m_dbDefScriptDir; };
		std::string&					get_init_script_path() { return m_dbInitScriptDir; };
		error_handler_style&			get_error_handler_style() { return m_errorHandlerStyle; };
		database_source&				get_database_source() { return m_dbSource; };

	private:
		configuration() {}

		static bool				m_initialised;
		std::map<defs::config::optional, bool>
			m_settings;
		std::map<defs::config::optional, bool>
			m_default;

		std::string				m_dbDir;
		std::string				m_dbDefScriptDir;
		std::string				m_dbInitScriptDir;
		error_handler_style		m_errorHandlerStyle;
		database_source			m_dbSource;
	};

	namespace templates
	{
		class character_class_id
		{
		public:

			using character_classes = std::vector<defs::character_class>;
			using character_class_types = std::vector<defs::character_class_type>;

			character_class_id() : m_class(defs::character_class::none) {}
			character_class_id(const defs::character_class& cls);

			const defs::character_class& id() { return m_class; }
			bool is_multiclass();

			defs::character_class select_class_by_type(const defs::character_class_type& clsType);

			bool is_type_of(const defs::character_class_type& classType);
			bool is(const defs::character_class& cls);

			template <defs::character_class clsDef>
			bool class_match()
			{
				auto multiclassMask = ~(static_cast<uint32_t>(defs::character_class_type::multiclass) << 24);
				auto classMask = static_cast<uint32_t>(m_class) & multiclassMask;
				bool b = (classMask & static_cast<uint32_t>(clsDef)) == static_cast<uint32_t>(clsDef);
				return b;
			}

			character_classes classes() { return m_baseClasses; }
			character_class_types types() { return m_baseClassTypes; }

		protected:
			defs::character_class						m_class;
			std::vector<defs::character_class>			m_baseClasses;
			std::vector<defs::character_class_type>		m_baseClassTypes;
		};

		class character_class
		{
		public:
			character_class() : m_class(defs::character_class::none), m_startingMoneyDie(), m_startingMoneyBase(0), m_startingMoneyMultiplier(0)
			{
			}
			character_class(const defs::character_class& cls);
			~character_class(void) {}

			void init();

			////////////////////////////////////////////////////////////////////////////////
			// Properties
			////////////////////////////////////////////////////////////////////////////////
			std::string get_long_name() { return m_longName; }
			std::string get_short_name() { return m_shortName; }
			short title_level();
			short hp_after_title();
			defs::character_class get_class() { return m_class.id(); }
			const character_class_id& get_class_composition() { return m_class; }
			auto get_moral_alignments() { return m_moralAlignments; }

			////////////////////////////////////////////////////////////////////////////////
			// Skills
			////////////////////////////////////////////////////////////////////////////////
			void get_skill_boundaries(const defs::character_skill& skill, short& minValue, short& maxValue);
			//std::list<adnd::defs::character_skill> get_prime_requisites();

			////////////////////////////////////////////////////////////////////////////////
			// Turn undead skills
			////////////////////////////////////////////////////////////////////////////////
			bool can_turn_undead()
			{
				return m_class.is(defs::character_class::paladin)
					|| m_class.is(defs::character_class::cleric);
			}
			const tables::turn_undead& turn(const short& level, const defs::turn_ability& type);

			////////////////////////////////////////////////////////////////////////////////
			// Class type handling
			////////////////////////////////////////////////////////////////////////////////

			////////////////////////////////////////////////////////////////////////////////
			// Checkings and capabilities
			////////////////////////////////////////////////////////////////////////////////
			bool check_skill(const defs::character_skill& skl, const skills::character_skill_value& sklValue);
			bool check_skill(const defs::character_skill& skl, const short& sklValue);
			bool check_moral_alignment(const defs::moral_alignment& align);
			bool can_use(const defs::equipment::item& itemId);

			////////////////////////////////////////////////////////////////////////////////
			// Class-specific information
			////////////////////////////////////////////////////////////////////////////////
			std::map<defs::character_class, random::die> hit_dice();
			short get_thaco(const short& level);
			const tables::attack_rate& get_attack_rate(const defs::weapons::attack_type& attackType, const short& level);
			const tables::saving_throws& saving_throws(const short& level);
			double experience_bonus(const skills::character_skill_set& skills);

			////////////////////////////////////////////////////////////////////////////////
			// Computed values/tabular data
			////////////////////////////////////////////////////////////////////////////////
			std::map<defs::character_class, short> get_levels(const long& xp);
			long get_xp(const short& lvl, const defs::character_class& cls);
			short get_level(const long& xp, const defs::character_class& cls = defs::character_class::none);

			////////////////////////////////////////////////////////////////////////////////
			// Thief skills
			////////////////////////////////////////////////////////////////////////////////
			bool has_thief_abilities()
			{
				return m_class.is(defs::character_class::thief)
					|| m_class.is(defs::character_class::bard);
			}
			const adnd::tables::thief_ability& thievery(const short& level);

			////////////////////////////////////////////////////////////////////////////////
			// Spells casting
			////////////////////////////////////////////////////////////////////////////////
			bool can_cast_priest_spells()
			{
				return m_class.is(defs::character_class::paladin)
					|| m_class.is(defs::character_class::cleric)
					|| m_class.is(defs::character_class::ranger)
					|| m_class.is(defs::character_class::druid);
			}
			bool can_cast_wizard_spells()
			{
				return m_class.is(defs::character_class::mage)
					|| m_class.is(defs::character_class::bard)
					|| is_specialist_wizard();
			}
			bool is_specialist_wizard()
			{
				//return m_class.is(defs::character_class::abjurer)
				//	|| m_class.is(defs::character_class::conjurer)
				//	|| m_class.is(defs::character_class::diviner)
				//	|| m_class.is(defs::character_class::enchanter)
				//	|| m_class.is(defs::character_class::illusionist)
				//	|| m_class.is(defs::character_class::invoker)
				//	|| m_class.is(defs::character_class::necromancer)
				//	|| m_class.is(defs::character_class::transmuter);
				return m_specialistSchool != defs::spells::school::none;
			}

			//const std::set<defs::spells::school> schools_of_magic() { return m_schoolsOfMagic; }
			adnd::defs::spells::school_access get_school_access(const defs::spells::school& id);
			defs::spells::school_access get_school_access(const defs::spells::wizard_spell& id);
			const defs::spells::school& specialist_school() { return m_specialistSchool; }

			tables::wizard_spell_progression get_wizard_spell_progression(const short& casterLevel);
			tables::priest_spell_progression get_priest_spell_progression(const short& casterLevel);

			////////////////////////////////////////////////////////////////////////////////
			// Starting money
			////////////////////////////////////////////////////////////////////////////////
			const random::die_roll& starting_money() { return m_startingMoneyDie; }
			const short& starting_money_base() { return m_startingMoneyBase; }
			const short& starting_money_multiplier() { return m_startingMoneyMultiplier; }

		protected:
			std::map<defs::character_skill, std::pair<short, short>>
										m_skillRequisites;
			std::set<defs::character_skill>
										m_primarySkills;
			std::set<defs::moral_alignment>
										m_moralAlignments;
			std::map<defs::character_class, long>
										m_advancementFactor;
			std::map<defs::character_class, std::map<short, long>>
										m_levelAdvancements;
			std::map<adnd::defs::character_class_type, std::tuple<std::string, short, std::string, random::die, short>>
										m_classTypeInfo;
			std::string					m_shortName;
			std::string					m_longName;
			character_class_id			m_class;
			std::map<short, tables::saving_throws>
										m_savings;
			std::map<short, tables::thief_ability>
										m_thiefAbilities;
			std::map<short, tables::turn_ability>
										m_turnAbilities;
			std::map<short, tables::priest_spell_progression>
										m_priestSpellProgression;
			std::map<short, tables::wizard_spell_progression>
										m_wizardSpellProgression;
			std::map<defs::character_class, random::die>
										m_hitDice;

			std::map<adnd::defs::weapons::attack_type, std::map<short, adnd::tables::attack_rate>>
										m_attackRates;
			std::set<defs::equipment::item>
										m_itemsAllowed;

			random::die_roll			m_startingMoneyDie;
			short						m_startingMoneyBase;
			short						m_startingMoneyMultiplier;

			std::map<defs::spells::school, defs::spells::school_access>		m_schoolsOfMagic;
			defs::spells::school		m_specialistSchool;

			////////////////////////////////////////////////////////////////////////////////
			// Skills
			////////////////////////////////////////////////////////////////////////////////
			bool is_primary(const defs::character_skill& skill) { return m_primarySkills.find(skill) != m_primarySkills.end(); }
			void set_skill_requisites(const defs::character_skill& skill, const short minValue, const short maxValue);


			std::set<adnd::defs::equipment::item>	select_allowed_items();
			adnd::tables::turn_undead				parse_turn_ability(const std::string& score);
		};

		class character_race
		{
		public:
			character_race();
			character_race(const defs::character_race& race);
			~character_race(void) {}

			////////////////////////////////////////////////////////////////////////////////
			// Properties
			////////////////////////////////////////////////////////////////////////////////
			const std::string& race_name() { return m_raceName; }
			const std::string& race_acronym() { return m_raceAcronym; }
			const short& base_movement_rate() { return m_baseMovementRate; }

			////////////////////////////////////////////////////////////////////////////////
			// Race type handling
			////////////////////////////////////////////////////////////////////////////////
			bool is_demihuman() { return m_race != defs::character_race::human; }

			////////////////////////////////////////////////////////////////////////////////
			// Capabilities check and skills
			////////////////////////////////////////////////////////////////////////////////
			bool check_skill(const defs::character_skill& skl, const skills::character_skill_value& sklValue);
			bool check_skill(const defs::character_skill& skl, const short& sklValue);
			void get_skill_boundaries(const defs::character_skill& skill, short& minValue, short& maxValue);
			const short& get_skill_modifier(const defs::character_skill& skill);
			bool check_class(const defs::character_class& cls);
			bool check_class(const defs::character_class& cls, short& levelLimit);

			////////////////////////////////////////////////////////////////////////////////
			// Computed values/tabular data
			////////////////////////////////////////////////////////////////////////////////
			std::map<adnd::defs::character_class, short> get_level_limit(const defs::character_class& cls, const skills::character_skill_set& values);
			std::list<adnd::defs::character_class>		get_available_classes(skills::character_skill_set& skills);
			std::list<adnd::defs::character_class>		get_available_classes(const skills::character_skill_value& strength, const skills::character_skill_value& dexterity, const skills::character_skill_value& constitution, const skills::character_skill_value& intelligence, const skills::character_skill_value& wisdom, const skills::character_skill_value& charisma);

			////////////////////////////////////////////////////////////////////////////////
			// Computed values/tabular data
			////////////////////////////////////////////////////////////////////////////////
			const short& height_base(const defs::character_sex& sex);
			const short& weight_base(const defs::character_sex& sex);
			const short& middle_age()				{ return m_middleAge; }
			const short& old_age()					{ return m_oldAge; }
			const short& venerable_age()			{ return m_venerableAge; }
			const short& limit_age()				{ return m_limitAge; }
			const short& age_starting()				{ return m_ageStarting; }
			const short& age_maximum()				{ return m_ageMaximum; }

			const random::die_roll& height_dice1()		{ return m_heightDice1; }
			const random::die_roll& height_dice2()		{ return m_heightDice2; }
			const random::die_roll& weight_dice1()		{ return m_weightDice1; }
			const random::die_roll& weight_dice2()		{ return m_weightDice2; }
			const random::die_roll& age_dice()			{ return m_ageDice; }
			const random::die_roll& age_maximum_dice()	{ return m_ageMaximumDice; }

			const short& get_thieving_modifiers_per_race(const adnd::defs::thief_ability& abil);

			short get_movement_rate(const skills::character_skill_value& strengthValue, const double& load);

		protected:
			defs::character_race				m_race;
			std::set<defs::character_class>		m_availableClasses;
			std::map<defs::character_skill, std::pair<short, short>>
												m_skillRequisites;
			std::map<defs::character_skill, short>
												m_skillModifiers;
			std::map<defs::character_class, short>
												m_levelLimits;
			std::string							m_raceName;
			std::string							m_raceAcronym;
			short								m_baseMovementRate;

			// Racial statistics
			short								m_heightBaseMale;
			short								m_heightBaseFemale;
			random::die_roll					m_heightDice1;
			random::die_roll					m_heightDice2;
			short								m_weightBaseMale;
			short								m_weightBaseFemale;
			random::die_roll					m_weightDice1;
			random::die_roll					m_weightDice2;
			short								m_ageStarting;
			random::die_roll					m_ageDice;
			short								m_ageMaximum;
			random::die_roll					m_ageMaximumDice;
			short								m_middleAge;
			short								m_oldAge;
			short								m_venerableAge;
			short								m_limitAge;

			std::map<defs::thief_ability, short>	m_thievingModifiers;

			void add_available_class(const defs::character_class& cls, const short limit = 0);
			void set_skill_requisites(const defs::character_skill& skill, const short minValue, const short maxValue);
			void set_skill_modifier(const defs::character_skill& skill, const short modifier);
		};
		
		class item
		{
		public:
			item();
			item(const defs::equipment::item& itemId);
			~item();
		
			const defs::equipment::item& id() const { return m_id; };
			const std::string& description() const { return m_description; };
			const defs::item_type& item_type() const { return m_itemType; };
			const defs::item_hand_required&	hand_required() const { return m_handRequired; };
			const defs::item_class& item_class() const { return m_itemClass; };
			
			//const money::amount& min_cost() { return m_minCost; };
			//const money::amount& max_cost() { return m_maxCost; };
			const double& min_cost_amount() const { return m_minCostAmount; };
			const double& max_cost_amount() const { return m_maxCostAmount; };
			const defs::coin& cost_currency() const { return m_costCurrency; };
		
			const double& weight() const { return m_weight; };
		
			bool usable_by(const defs::character_class& cls);
		
		protected:
			defs::equipment::item		m_id;
			std::string					m_description;
			defs::item_type				m_itemType;
			defs::item_hand_required	m_handRequired;
			defs::item_class			m_itemClass;
			double						m_minCostAmount;
			double						m_maxCostAmount;
			defs::coin					m_costCurrency;
			double						m_weight;
			std::set<defs::character_class>
										m_allowedToClasses;
		};
		
		class clothing : public item
		{
		public:
			clothing();
			clothing(const defs::equipment::item& itemId);
			~clothing() {};

			const defs::body_slot& body_slot() const { return m_bodySlot; }

		protected:
			defs::body_slot		m_bodySlot;
		};

		class daily_food_and_lodging : public item
		{
		public:
			daily_food_and_lodging() {}
			daily_food_and_lodging(const defs::equipment::item& itemId) : item(itemId) {}
			~daily_food_and_lodging() {};
		};

		class household_provisioning : public item
		{
		public:
			household_provisioning() {}
			household_provisioning(const defs::equipment::item& itemId) : item(itemId) {}
			~household_provisioning() {};
		};

		class service : public item
		{
		public:
			service() {}
			service(const defs::equipment::item& itemId) : item(itemId) {}
			~service() {};
		};

		class transport : public item
		{
		public:
			transport() {}
			transport(const defs::equipment::item& itemId) : item(itemId) {}
			~transport() {};
		};

		class animal : public item
		{
		public:
			animal() {}
			animal(const defs::equipment::item& itemId) : item(itemId) {}
			~animal() {};
		};

		class tack_and_harness : public item
		{
		public:
			tack_and_harness() : item() {}
			tack_and_harness(const defs::equipment::item& itemId) : item(itemId) {}
			~tack_and_harness() {};
		};

		class miscellaneous_equipment : public item
		{
		public:
			miscellaneous_equipment() : item() {}
			miscellaneous_equipment(const defs::equipment::item& itemId) : item(itemId) {}
			~miscellaneous_equipment() {};
		};

		class armour : public item
		{
		public:
			armour();
			armour(const defs::equipment::item& itemId);
			~armour();

			const defs::body_slot& body_slot() const { return m_bodySlot; }
			const uint16_t& ac_bonus_melee() const { return m_acBonusMelee; }
			const uint16_t& ac_bonus_missile() const { return m_acBonusMissile; }
			const uint16_t& num_of_attack_protection() const { return m_numOfAttackProtection; }
			const uint16_t protection() const { return m_protections; }
			const bool protection(const defs::attack_side& side) const { return m_protections & static_cast<uint16_t>(side); }
			const bool& allows_weapons() const { return m_allowsWeapons; }

		protected:
			defs::body_slot		m_bodySlot;
			uint16_t			m_acBonusMelee;
			uint16_t			m_acBonusMissile;
			uint16_t			m_numOfAttackProtection;
			uint16_t			m_protections;
			bool				m_allowsWeapons;
		};

		class weapon : public item
		{
		public:
			weapon();
			weapon(const defs::equipment::item& itemId);

			const defs::weapons::size& size() const { return m_weaponSize; }
			const short& speed() const { return m_speedFactor; }
			const defs::weapons::type& weapon_type() const { return m_weaponType; }

			const defs::weapons::attack_type& attack_type() const { return m_attackType; }
			const defs::weapons::group& weapon_group() const { return m_weaponGroup; }

			const defs::die_faces & faces(const defs::weapons::target_size& tgtSize) const { return m_faces.at(tgtSize); }
			const short& rolls(const defs::weapons::target_size& tgtSize) const { return m_numRolls.at(tgtSize); }
			const short& base_damage(const defs::weapons::target_size& tgtSize) const { return m_baseDamage.at(tgtSize); }

			bool requires_projectiles() const { return m_projectilesAllowed.size() > 0; }
			bool allows_projectile(const defs::equipment::item& projectileType) const;
			std::pair<short, short> rate_of_fire() const;

			short distance_modifier(const short& distance, const defs::proficiency::level& profLevel) const;
			short distance_modifier(const adnd::defs::equipment::item& projectileId, const short& distance, const defs::proficiency::level& profLevel) const;

		protected:
			defs::weapons::size											m_weaponSize;
			short														m_speedFactor;
			defs::weapons::type											m_weaponType;

			std::map<defs::weapons::target_size, defs::die_faces >		m_faces;
			std::map<defs::weapons::target_size, short>					m_numRolls;
			std::map<defs::weapons::target_size, short>					m_baseDamage;
			std::map<defs::equipment::item, tables::missile_weapon_range>
																		m_projectilesAllowed;
			defs::weapons::attack_type									m_attackType;
			defs::weapons::group										m_weaponGroup;
		};

		class projectile : public item
		{
		public:
			projectile();
			projectile(const defs::equipment::item& itemId);

			const defs::weapons::size& size() const { return m_weaponSize; }
			const defs::weapons::type& weapon_type() const { return m_weaponType; }

			const defs::die_faces & faces(const defs::weapons::target_size& tgtSize) const { return m_faces.at(tgtSize); }
			const short& rolls(const defs::weapons::target_size& tgtSize) const { return m_numRolls.at(tgtSize); }
			const short& base_damage(const defs::weapons::target_size& tgtSize) const { return m_baseDamage.at(tgtSize); }
		
		protected:
			defs::weapons::size											m_weaponSize;
			defs::weapons::type											m_weaponType;
			std::map<defs::weapons::target_size, defs::die_faces >		m_faces;
			std::map<defs::weapons::target_size, short>					m_numRolls;
			std::map<defs::weapons::target_size, short>					m_baseDamage;
		};

		class coin
		{
		public:
			coin();
			coin(const defs::coin& coinId);
			~coin() {}

			const defs::coin& get_id() { return m_id; }
			const std::string& get_acronym() { return m_acronym; }
			const std::string& get_description() { return m_description; }

			double from(const defs::coin& currency);
			double to(const defs::coin& currency);

		protected:
			defs::coin					m_id;
			std::string					m_acronym;
			std::string					m_description;
			std::map<defs::coin, std::map<defs::coin, double>>
										m_exchangeRates;
		};

		class deity
		{
		public:
			deity();
			deity(const defs::deities::deity& deityId);
			~deity() {}

			const defs::deities::deity& get_id() { return m_id; }
			const std::string& get_name() { return m_name; }
			const defs::deities::rank& get_rank() { return m_rank; }
			const defs::moral_alignment& get_moral_alignment() { return m_moralAlignment; }
			const defs::planes::plane& get_plane() { return m_planeOfExistence; }
			
			bool is_worshipped_by(const defs::moral_alignment& align);
			bool allows(const defs::spells::priest_spell& spellId);

		protected:
			defs::deities::deity		m_id;
			std::string					m_name;
			defs::deities::rank			m_rank;
			defs::moral_alignment		m_moralAlignment;
			defs::planes::plane			m_planeOfExistence;
			defs::planes::realm			m_realm;
			std::set<defs::moral_alignment>
										m_worshippersAlignment;
			std::map<defs::spells::sphere, std::pair<defs::spells::access_level, defs::spells::cast_mode>>
										m_spheres;
		};

		class skill
		{
		public:
			skill();
			skill(const defs::character_skill& skillId);
			~skill() {}

			const defs::character_skill& get_id() { return m_id; }
			const std::string& get_name() { return m_name; }
			bool allows_exceptional() { return m_id == defs::character_skill::strength; }

			const tables::strength_stats& get_strength_stats(const skills::character_skill_value& value);
			const tables::dexterity_stats& get_dexterity_stats(const skills::character_skill_value& value);
			const tables::constitution_stats& get_constitution_stats(const skills::character_skill_value& value);
			const tables::intelligence_stats& get_intelligence_stats(const skills::character_skill_value& value);
			const tables::wisdom_stats& get_wisdom_stats(const skills::character_skill_value& value);
			const tables::charisma_stats& get_charisma_stats(const skills::character_skill_value& value);

			short get_thieving_modifiers_per_dexterity(const short& dexterityScore, const adnd::defs::thief_ability& ability);
			const tables::modified_movement_rates get_modified_movement_rates(const adnd::skills::character_skill_value& value, const double& load);

		protected:
			defs::character_skill		m_id;
			std::string					m_name;

			std::map<skills::character_skill_value, tables::strength_stats>		m_strengthStats;
			std::map<skills::character_skill_value, tables::dexterity_stats>	m_dexterityStats;
			std::map<skills::character_skill_value, tables::constitution_stats>	m_constitutionStats;
			std::map<skills::character_skill_value, tables::intelligence_stats>	m_intelligenceStats;
			std::map<skills::character_skill_value, tables::wisdom_stats>		m_wisdomStats;
			std::map<skills::character_skill_value, tables::charisma_stats>		m_charismaStats;

			std::map<short, tables::thief_ability>								m_thievingModifiersPerDexterity;
			std::map<skills::character_skill_value, std::map<double, tables::modified_movement_rates>>
																				m_modifiedMovementRates;
		};

		class treasure
		{
		public:
			treasure();
			treasure(const defs::treasure::treasure_class& treasureClass, const char& acronym, const defs::treasure::location& loc);
			~treasure() {}

			const char& acronym() { return m_acronym; }
			const defs::treasure::location& location() { return m_location; }
			const std::string& location_name() { return m_locationName; }

			std::set<defs::treasure::component> components();
			const adnd::tables::treasure_composition_info& operator[] (const defs::treasure::component& comp)
			{
				return m_items[comp];
			}
			
			const std::set<defs::treasure::nature>& magical_items()
			{
				return m_magicalItemTypes;
			}
			const std::set<defs::treasure::nature>& additional_magical_items()
			{
				return m_additionalMagicalItemTypes;
			}

		protected:
			std::map<defs::treasure::component, tables::treasure_composition_info>
														m_items;
			std::set<defs::treasure::nature>			m_magicalItemTypes;
			std::set<defs::treasure::nature>			m_additionalMagicalItemTypes;
			defs::treasure::location					m_location;
			std::string									m_locationName;
			char										m_acronym;
		};

		class moral_alignment
		{
		public:
			moral_alignment();
			moral_alignment(const defs::moral_alignment& alignId);
			~moral_alignment() {}

			const defs::moral_alignment&		get_id() { return m_id; }
			const std::string&					get_name() { return m_name; }
			const std::string&					get_acronym() { return m_acronym; }
			const unsigned short&				get_moral_order_id() { return m_moralOrderId; }
			const std::string&					get_moral_order_name() { return m_moralOrderName; }
			const std::string&					get_moral_order_acronym() { return m_moralOrderAcronym; }
			const unsigned short&				get_moral_view_id() { return m_moralViewId; }
			const std::string&					get_moral_view_name() { return m_moralViewName; }
			const std::string&					get_moral_view_acronym() { return m_moralViewAcronym; }

		protected:
			defs::moral_alignment		m_id;
			std::string					m_name;
			std::string					m_acronym;
			unsigned short				m_moralOrderId;
			std::string					m_moralOrderName;
			std::string					m_moralOrderAcronym;
			unsigned short				m_moralViewId;
			std::string					m_moralViewName;
			std::string					m_moralViewAcronym;
		};

		class gem
		{
		public:
			gem();
			gem(const defs::treasure::gems& gemId);
			~gem() {}

			const defs::treasure::gems&			get_id() { return m_id; }
			const std::string&					get_name() { return m_name; }
			const defs::treasure::gem_type&		get_type() { return m_type; }
			const std::string&					get_type_name() { return m_typeName; }
			const defs::coin&					get_value_currency() { return m_currency; }
			const uint32_t&						get_value_from() { return m_valueFrom; }
			const uint32_t&						get_value_to() { return m_valueTo; }

		protected:
			defs::treasure::gems		m_id;
			std::string					m_name;
			defs::treasure::gem_type	m_type;
			std::string					m_typeName;
			defs::coin					m_currency;
			uint32_t					m_valueFrom;
			uint32_t					m_valueTo;
		};

		class magical_item : public item
		{
		public:
			magical_item();
			magical_item(const defs::magical::item& magicalId);
			~magical_item() {}

			const std::string&					unidentified_name() { return m_unidentifiedName; }
			const defs::magical::item&			magical_id() { return m_magicalId; }
			const defs::weapons::attack_type&	attack_type() { return m_attackType; }
			const defs::weapons::group&			weapon_group() { return m_weaponGroup; }
			const defs::body_slot&				body_slot() { return m_bodySlot; }

		protected:
			std::string					m_unidentifiedName;
			defs::magical::item			m_magicalId;
			defs::weapons::attack_type	m_attackType;
			defs::weapons::group		m_weaponGroup;
			defs::body_slot				m_bodySlot;
		};

		class school_of_magic
		{
		public:
			school_of_magic();
			school_of_magic(const defs::spells::school& id);
			~school_of_magic() {}
		
			const defs::spells::school&	id() { return m_id; }
			const std::string&			name() { return m_name; }
		
			bool is_included(const defs::spells::wizard_spell& id);
			bool is_opposite(const defs::spells::wizard_spell& id);
			bool is_neutral(const defs::spells::wizard_spell& id);
		
		protected:
			defs::spells::school			m_id;
			std::string						m_name;
			std::string						m_alias;
			defs::character_class			m_wizardClassId;
			skills::character_skill_value	m_requisite;
		
			std::set<defs::spells::school>	m_opposites;
		};
		
		class sphere_of_influence
		{
		public:
			sphere_of_influence();
			sphere_of_influence(const defs::spells::sphere& id);
			~sphere_of_influence() {}
		
			const defs::spells::sphere&	id() { return m_id; }
			const std::string&			name() { return m_name; }
		
		protected:
			defs::spells::sphere			m_id;
			std::string						m_name;
		};

		class wizard_spell
		{
		public:
			wizard_spell();
			wizard_spell(const defs::spells::wizard_spell& spellId);
			~wizard_spell() {}
		
			const defs::spells::wizard_spell&	id() { return m_id; }
			const std::string&					name() { return m_name; }
			const short&						level() { return m_level; }
		
			const std::set<defs::spells::school>& schools() { return m_schools; }
		
		protected:
			defs::spells::wizard_spell		m_id;
			std::string						m_name;
			short							m_level;
		
			std::set<defs::spells::school>	m_schools;
		};
		
		class priest_spell
		{
		public:
			priest_spell();
			priest_spell(const defs::spells::priest_spell& spellId);
			~priest_spell() {}
		
			const defs::spells::priest_spell&	id() { return m_id; }
			const std::string&					name() { return m_name; }
			const short&						level() { return m_level; }
			
			const std::set<defs::spells::sphere>& spheres() { return m_spheres; }
		
		protected:
			defs::spells::priest_spell		m_id;
			std::string						m_name;
			short							m_level;
		
			std::set<defs::spells::sphere>	m_spheres;
		};

		class monster
		{
		public:
			monster();
			monster(const defs::monsters::monster& id);
			~monster() {}

			const defs::monsters::monster&		id() { return m_id; }
			const std::string&					name() { return m_name; }
			const bool&							is_undead() { return m_isUndead; }
			const bool&							is_elemental() { return m_isElemental; }
			const bool&							is_dragon() { return m_isDragon; }
			const defs::turn_ability&			turnable_as() { return m_turnableAs; };
			const short&						element() { return m_element; };
			const short&						dragon_type() { return m_dragonType; };

			const short&							climate() { return m_climate; };
			const short&							terrain() { return m_terrain; };
			const defs::monsters::frequency&		frequency() { return m_frequency; };
			const short&							organisation() { return m_organisation; };
			const short&							activity_cycle() { return m_activityCycle; };
			const short&							diet() { return m_diet; };
			const defs::monsters::intelligence&		intelligence() { return m_intelligence; };
			const defs::moral_alignment&			alignment() { return m_alignment; };
			const defs::monsters::morale&			morale() { return m_morale; };
			const short&							morale_from() { return m_moraleFrom; };
			const short&							morale_to() { return m_moraleTo; };

		protected:
			defs::monsters::monster			m_id;
			std::string						m_name;
			bool							m_isUndead;
			bool							m_isElemental;
			bool							m_isDragon;
			defs::turn_ability				m_turnableAs;
			short							m_element;
			short							m_dragonType;

			short							m_climate;
			short							m_terrain;
			defs::monsters::frequency		m_frequency;
			short							m_organisation;
			short							m_activityCycle;
			short							m_diet;
			defs::monsters::intelligence	m_intelligence;
			defs::moral_alignment			m_alignment;
			defs::monsters::morale			m_morale;
			short							m_moraleFrom;
			short							m_moraleTo;
		};

		class metadata
		{
		public:

			static metadata& get_instance();
			static void init()
			{
				//Ensure first initialisation
				get_instance();
			}
			metadata(metadata const&) = delete;
			void operator=(metadata const&) = delete;

			//////////////////////////////////////////////////////////////////////////////////
			//// Templates retrieval
			//////////////////////////////////////////////////////////////////////////////////
			const character_class&	get_class_template(const defs::character_class& cls);
			const character_race&	get_race_template(const defs::character_race& race);
			const item*				get_item_template(const defs::equipment::item& itemId);
			const clothing*			get_clothing_template(const defs::equipment::item& itemId);
			const armour*			get_armour_template(const defs::equipment::item& itemId);
			const weapon*			get_weapon_template(const defs::equipment::item& itemId);
			const projectile*		get_projectile_template(const defs::equipment::item& itemId);
			const coin&				get_coin_template(const defs::coin& coinId);
			const deity&			get_deity_template(const defs::deities::deity& deityId);
			const skill&			get_skill_template(const defs::character_skill& skillId);
			const treasure&			get_treasure_template(const defs::treasure::treasure_class& treasureClass);
			const moral_alignment&	get_moral_alignment_template(const defs::moral_alignment& alignmentId);
			const gem&				get_gem_template(const defs::treasure::gems& gemId);
			const magical_item&		get_magical_item_template(const defs::magical::item& magicalId);
			const wizard_spell&		get_wizard_spell_template(const defs::spells::wizard_spell& spellId);
			const priest_spell&		get_priest_spell_template(const defs::spells::priest_spell& spellId);
			const school_of_magic&	get_school_of_magic_template(const defs::spells::school& schoolId);
			const sphere_of_influence&	get_sphere_of_influence_template(const defs::spells::sphere& sphereId);
			const monster&			get_monster_template(const defs::monsters::monster& monsterId);

			//////////////////////////////////////////////////////////////////////////////////
			//// Class type and id handling
			//////////////////////////////////////////////////////////////////////////////////
			static adnd::tables::class_id				get_character_type_and_id(const defs::character_class& cls);
			static adnd::defs::character_class_type		get_type_of(const defs::character_class& cls);
			static adnd::defs::character_class			get_class_id_of(const defs::character_class& cls);

			//////////////////////////////////////////////////////////////////////////////////
			//// Capabilities retrieval
			//////////////////////////////////////////////////////////////////////////////////
			std::list<adnd::defs::character_race> get_available_races(skills::character_skill_set& skills);
			std::list<adnd::defs::character_race> get_available_races(const skills::character_skill_value& strength, const skills::character_skill_value& dexterity,
				const skills::character_skill_value& constitution, const skills::character_skill_value& intelligence,
				const skills::character_skill_value& wisdom, const skills::character_skill_value& charisma);
			std::list<adnd::defs::character_race> get_available_races(const short& strength, const short& dexterity, const short& constitution, const short& intelligence, const short& wisdom, const short& charisma);

			//////////////////////////////////////////////////////////////////////////////////
			//// Tabular data access (structured data)
			//////////////////////////////////////////////////////////////////////////////////
			std::tuple<uint16_t, uint16_t, uint16_t, uint16_t, uint16_t>
														get_starting_money_info(const defs::character_class& cls);
			const std::tuple<short, short>				get_skill_limits(const defs::character_class& cls, const defs::character_skill& skill);
			const std::tuple<short, short>				get_skill_limits(const defs::character_race& race, const defs::character_skill& skill);

			struct query
			{
				static const char *skill_info;
				static const char *classes;
				static const char *saving_throws;
				static const char *turn_undead;
				static const char *class_info;
				static const char *class_type_info;
				static const char *skill_limits_per_race;
				static const char *skill_limits_per_class;
				static const char *classes_per_race;
				static const char *prime_requisite_per_class;
				static const char *hit_dice_info;
				static const char *level_advancement;
				static const char *level_advancement_factor;
				static const char *alignment_per_class;
				static const char *moral_alignment;
				static const char *moral_alignments;
				static const char *skill_modifier_per_race;
				static const char *thief_abilities;
				static const char *ranger_abilities;
				static const char *bard_abilities;
				static const char *thaco_info;
				static const char *strength_stats;
				static const char *dexterity_stats;
				static const char *constitution_stats;
				static const char *intelligence_stats;
				static const char *wisdom_stats;
				static const char *charisma_stats;
				static const char *racial_stats;
				static const char *thieving_modifiers_per_dexterity;
				static const char *thieving_modifiers_per_race;
				static const char *wizard_spell_progression;
				static const char *bard_spell_progression;
				static const char *priest_spell_progression;
				static const char *paladin_spell_progression;
				static const char *ranger_spell_progression;
				static const char *race_info;
				static const char *exchange_rates;
				static const char *coin_info;
				static const char *starting_money_info;
				static const char *melee_attacks;
				static const char *ageing_modifiers;
				static const char *prime_requisite_bonuses;
				static const char *items_info;
				static const char *item_info;
				static const char *clothing_info;
				static const char *armour_info;
				static const char *projectile_info;
				static const char *weapon_info;
				static const char *weapon_projectile_info;
				static const char *items_allowed;
				static const char *item_capabilities_per_character_class;
				static const char *modified_movement_rates;
				static const char *missile_range_modifiers;
				static const char *deities;
				static const char *deity_info;
				static const char *deity_worshippers_info;
				static const char *spheres_per_deity;
				static const char *proficiency_slots;
				static const char *specialist_attacks_per_round;
				static const char *treasure_composition;
				static const char *treasure_classes;
				static const char *treasure_component;
				static const char *treasure_location;
				static const char *treasure_nature;
				static const char *gems;
				static const char *gems_by_type;
				static const char *gem_info;
				static const char *gem_types;
				static const char *objects_of_art;
				static const char *magical_items;
				static const char *magical_item_info;
				static const char* wizard_spells;
				static const char* priest_spells;
				static const char* wizard_spell_info;
				static const char* priest_spell_info;
				static const char* wizard_spell_schools;
				static const char* priest_spell_spheres;
				static const char* schools_of_magic;
				static const char* school_of_magic_info;
				static const char* spheres_of_influence;
				static const char* sphere_of_influence_info;
				static const char* school_of_magic_opposites;
				static const char* schools_per_class;
				static const char* opposite_schools_per_class;
				static const char* priest_spells_per_deity_and_level;
				static const char* priest_spells_per_level_and_class;
				static const char* wizard_spells_per_school_and_level;
				static const char* monsters;
				static const char* monster_info;
			};

		protected:

			class query_manager
			{
			public:

				using query_id = size_t;
				using prepared_statement_set = std::map<query_id, sqlite3_stmt*>;
				using statement_name_set = std::map<std::string, query_id>;

				enum class source
				{
					file,
					memory
				};

				//using visit_callback = std::function<void(const std::vector<std::string>&)>;

				//template <typename... Types>
				//struct parameters_pack
				//{
				//};

				class iterator
				{
					friend class query_manager;
				public:

					iterator(query_manager* const db, const std::string &stmt);
					iterator(query_manager* const db, sqlite3_stmt *stmt);
					//iterator(query_manager* const db, const query_id& stmtId);
					iterator();
					~iterator();

					operator bool()const;
					bool operator==(const iterator& it) const;
					bool operator!=(const iterator& it) const;
					iterator& operator=(iterator& it);
					iterator& operator+=(const int& movement);
					iterator& operator++();
					iterator operator++(int);
					//std::vector<std::string> operator*();
					const std::vector<std::string> operator*() const;
					int get_result() const;
					//sqlite3_stmt const * statement() const;
					const int& columns_count() const;
					const std::vector<std::string> get_caption() const;
					const std::string get_caption(int column) const;
					//std::string sql() const;

					template <typename _Type>
					_Type data(const int col, const _Type& defaultValue) const
					{
						const char *zValue = get_column_text(col);
						return adnd::utils::parse<_Type>(zValue, std::strlen(zValue), defaultValue);
					}

				protected:
					const char * get_column_text(const int col) const;

					int							m_result;
					int							m_numCols;
					sqlite3_stmt				*m_stmt;
					query_manager				*m_db;
				};

				template <typename... _OutputTypes>
				class result
				{
				public:
					using record_t = std::tuple<_OutputTypes...>;

					result(const query_manager& qm, const query_id& queryId)
						: m_qm(qm), m_queryId(queryId), m_filterIndex(0)
					{}

					template <typename _FilterType>
					result<_OutputTypes...>& filter(const _FilterType& val)
					{
						m_qm.bind_parameter<_FilterType>(m_queryId, ++m_filterIndex, val);
						return (*this);
					}

					result<_OutputTypes...>& run()
					{
						metadata::query_manager::iterator it, end;
						if (!m_qm.execute(m_queryId, it, end))
							throw std::exception("Unable to fetch data");

						while (it != end)
						{
							record_t r;
							fill(it, r);
							m_values.push_back(r);
							++it;
						}
						return (*this);
					}

					auto begin() { return m_values.begin(); }
					auto end() { return m_values.end(); }
					const record_t& first() { return m_values.front(); }
					size_t size() { return m_values.size(); }
					bool has_values() { return m_values.size() > 0; }
					const record_t& at(const size_t& position)
					{
						if (position < 0 || position >= m_values.size())
							throw std::exception("Index out of bound", static_cast<int>(position));

						typename std::list<record_t>::iterator it = m_values.begin();
						std::advance(it, position);
						return *it;
					}

				protected:
					std::list<record_t>		m_values;
					query_manager			m_qm;
					query_id				m_queryId;
					int						m_filterIndex;

					template<size_t I = 0, typename... _Types>
					void fill(query_manager::iterator& it, std::tuple<_Types...>& r)
					{
						std::string val = it.data<std::string>(I + 1, "");
						std::get<I>(r) = utils::parse<decltype(std::get<I>(r))>(val.c_str(), val.length(), std::get<I>(r));
						if constexpr (I + 1 != sizeof...(_Types))
							fill<I + 1>(it, r);
					}
				};

				query_manager(void);
				virtual ~query_manager(void);

				sqlite3* db_connection() { return m_dbConnection; };
				std::string& db_path()
				{
					return adnd::configuration::get_instance().get_database_path();
				}
				configuration::error_handler_style& error_handler()
				{
					return adnd::configuration::get_instance().get_error_handler_style();
				}
				configuration::database_source& db_source()
				{
					return adnd::configuration::get_instance().get_database_source();
				}

				void error(const char* text);

				bool open();
				//bool close(bool saveData = false);
				//void drop_tables();

				bool clean_db(bool dropTables = false);

				bool execute_script(const std::experimental::filesystem::path& scriptPath);
				bool execute(const query_id& stmtId);
				bool execute(const query_id& stmtId, iterator& itBegin, iterator& itEnd);
				bool execute(const char* sqlScript);
				//bool execute(const char* sqlScript, iterator& itBegin, iterator& itEnd);
				//bool execute(const std::string& sqlScript, iterator& itBegin, iterator& itEnd);
				//bool execute(const std::string &stmt, std::vector<std::string>& firstRow);
				//bool execute(const query_id& stmtId, std::vector<std::string>& firstRow);
				//bool execute(const char* stmt, const visit_callback& f);
				//bool execute(const std::string& stmt, const visit_callback& f);
				//bool execute(const query_id& stmtId, const visit_callback& f);

				template <typename T>
				bool execute(const std::string &stmt, T& value, const T& defaultValue)
				{
					std::vector<std::string> r;
					bool ok = execute(stmt, r);
					value = adnd::utils::parse<T>(r[0].c_str(), r[0].length(), defaultValue);
					return ok;
				}

				/*template <typename T>
				bool execute(const query_id& stmtId, T& value, const T& defaultValue)
				{
					std::vector<std::string> r;
					bool ok = execute(stmtId, r);
					value = adnd::utils::parse<T>(r[0].c_str(), r[0].length(), defaultValue);
					return ok;
				}*/

				//bool create_function(const std::string& functionName, long numArgs, void(*funct)(sqlite3_context*, int, sqlite3_value**));

				template <typename T>
				bool bind_parameter(const query_id& stmtId, const int index, const T& value)
				{
					prepared_statement_set::iterator it = m_preparedStatements.find(stmtId);
					if (it == m_preparedStatements.end())
					{
						error("Invalid prepared statement id");
						return false;
					}

					std::stringstream ss;
					ss << value;
					return bind_parameter(stmtId, index, ss.str());
				}

				query_id prepare_statement(const std::string& statement, const std::string& symbolicName = "");

				bool bind_parameter(const query_id& stmtId, int index, const std::time_t& value);
				bool bind_parameter(const query_id& stmtId, int index, const std::string& value);
				bool bind_parameter(const query_id& stmtId, int index, const char* value);
				//void clean_prepared_statements();

				//bool begin_transaction() { return execute("BEGIN TRANSACTION"); }
				//bool commit_transaction() { return execute("COMMIT TRANSACTION"); }
				//bool rollback_transaction() { return execute("ROLLBACK TRANSACTION"); }

				//unsigned long count_rows(const std::string &tableName);
				//bool save(const std::string& dbPathDest);
				//bool restore(const std::string& dbSrc);

				//std::string sql(const query_id& stmtId);
				//bool register_function(const std::string& functionName, long numArgs, void(*func)(sqlite3_context*, int, sqlite3_value**), std::string &error);

				const std::string& last_error() { return m_lastError; }

				bool add_pragma(const std::string &key, const std::string &value);
				const query_id& resolve_by_name(const std::string& statementName) { return m_preparedStatementNames[statementName]; }

			protected:
				//std::string						m_dbPath;
				//configuration::database_source	m_dbSource;
				sqlite3							*m_dbConnection;
				prepared_statement_set			m_preparedStatements;
				statement_name_set				m_preparedStatementNames;
				std::string						m_lastError;

				std::string db_location();
				sqlite3_stmt* get_prepared_statement(const query_manager::query_id& stmtId);

				iterator begin(sqlite3_stmt *stmt);
				iterator begin(const std::string &stmt);
				iterator end();
			};

			//////////////////////////////////////////////////////////////////////////////////
			//// Load tabular static data
			//////////////////////////////////////////////////////////////////////////////////
			//void load_prime_requisite_bonuses();
			void load_equipment_templates();
			void load_deity_templates();
			void load_coin_templates();
			void load_race_templates();
			//void load_modified_movement_rates();

			void create();

			////////////////////////////////////////////////////////////////////////////////
			// Capability settings
			////////////////////////////////////////////////////////////////////////////////
			//void add_weapon_template(const defs::weapons::weapon& weaponId, const defs::die_faces & faces, const short& numRolls, const short& baseDamage,
			//	const short& speedFactor, const defs::weapons::size& weaponSize, const defs::weapons::type& weaponType, const defs::item_hand_required& handRequired);
			//void add_weapon_template(const defs::weapons::weapon& weaponId, const defs::die_faces & faces_SM, const short& numRolls_SM, const short& baseDamage_SM,
			//	const defs::die_faces & faces_L, const short& numRolls_L, const short& baseDamage_L,
			//	const short& speedFactor, const defs::weapons::size& weaponSize, const defs::weapons::type& weaponType, const defs::item_hand_required& handRequired);
			//void add_weapon_template(const defs::weapons::weapon& weaponId, const short& speedFactor, const defs::weapons::size& weaponSize,
			//	const defs::weapons::type& weaponType, const defs::item_hand_required& handRequired);
			//void add_projectile_template(const defs::weapons::projectile& projectileType, const defs::die_faces & faces, const short& numRolls, const short& baseDamage);
			//void add_projectile_template(const defs::weapons::projectile& projectileType, const defs::die_faces & faces_SM, const short& numRolls_SM, const short& modifier_SM, const defs::die_faces & faces_L, const short& numRolls_L, const short& modifier_L);
			//void add_allowed_projectile(const defs::weapons::weapon& weaponType, const defs::weapons::projectile& projectile);

		private:
			metadata() {}

			std::map<defs::character_class, templates::character_class>
									m_classTemplates;
			std::map<defs::character_race, templates::character_race>
									m_raceTemplates;
			std::map<defs::equipment::item, templates::item*>
									m_itemTemplates;
			std::map<defs::coin, templates::coin>
									m_coinTemplates;
			std::map<defs::deities::deity, templates::deity>
									m_deityTemplates;
			std::map<defs::character_skill, templates::skill>
									m_skillTemplates;
			std::map<defs::treasure::treasure_class, templates::treasure>
									m_treasureTemplates;
			std::map<defs::moral_alignment, templates::moral_alignment>
									m_moralAlignmentTemplates;
			std::map<defs::treasure::gems, templates::gem>
									m_gemTemplates;
			std::map<defs::magical::item, templates::magical_item>
									m_magicalTemplates;
			std::map<defs::spells::wizard_spell, templates::wizard_spell>
									m_wizardSpellTemplates;
			std::map<defs::spells::priest_spell, templates::priest_spell>
									m_priestSpellTemplates;
			std::map<defs::spells::school, templates::school_of_magic>
									m_schoolOfMagicTemplates;
			std::map<defs::spells::sphere, templates::sphere_of_influence>
									m_spheresOfInfluenceTemplates;
			std::map<defs::monsters::monster, templates::monster>
									m_monsterTemplates;
			
			static bool				m_initialised;
			query_manager			m_db;

		public:
			query_manager& get_query_manager() { return m_db; }

			//template<size_t I = 0, typename... _Types>
			//void fill(query_manager::iterator& it, std::tuple<_Types...>& r)
			//{
			//	std::string val = it.data<std::string>(I+1, "");
			//	std::get<I>(r) = utils::parse<decltype(std::get<I>(r))>(val.c_str(), val.length(), std::get<I>(r));
			//	if constexpr (I + 1 != sizeof...(_Types))
			//		fill<I + 1>(it, r);
			//}

			//template<size_t I = 0, typename... _FTypes>
			//void apply_filters(query_manager::query_id& queryId, std::tuple<_FTypes...>& f)
			//{
			//	m_db.bind_parameter<decltype(std::get<I>(f))>(queryId, I + 1, std::get<I>(f));
			//	if constexpr (I + 1 != sizeof...(_FTypes))
			//		apply_filters<I + 1>(queryId, f);
			//}

			//template<typename... _OutputTypes>
			//std::list<std::tuple<_OutputTypes...>> fetch(const char *stmtName)
			//{
			//	auto stmt = m_db.resolve_by_name(stmtName);

			//	//if (filters)
			//	//apply_filters(stmt, filters)

			//	metadata::query_manager::iterator it, end;
			//	std::list<std::tuple<_OutputTypes...>> result;
			//	if (!m_db.execute(stmt, it, end))
			//		throw std::exception("Unable to fetch data");
			//	
			//	while (it != end)
			//	{
			//		std::tuple<_OutputTypes...> record;
			//	
			//		fill(it, record);
			//		result.push_back(record);
			//		++it;
			//	}
			//	return result;
			//}

			template<typename... _OutputTypes>
			query_manager::result<_OutputTypes...> fetch(const char *stmtName)
			{
				auto stmtId = m_db.resolve_by_name(stmtName);
				query_manager::result<_OutputTypes...> res(m_db, stmtId);
				return res;
			}
		};

	}
}