#include "stdafx.h"
#include "market.h"

#include <random>
#include <sstream>


bool adnd::market::market::m_initialised = false;


const double adnd::market::store::m_buySpreadForPrice = 0.92;
const double adnd::market::store::m_sellSpreadForPrice = 1.08;
const double adnd::market::store::m_buySpreadForPriceRange = 0.85;
const double adnd::market::store::m_sellSpreadForPriceRange = 1.15;


adnd::market::store::store() : m_uid(0), m_budget(budget_model::unlimited), m_buySpread(false), m_sellSpread(false)//, m_lastTransactionUid(0)
{
}

adnd::market::store::store(const uint32_t& uid, const std::string& name, const budget_model& bud, bool buySpread, bool sellSpread)
	: m_uid(uid), m_name(name), m_budget(bud), m_buySpread(buySpread), m_sellSpread(sellSpread)//, m_lastTransactionUid(0)
{
	if (m_budget == budget_model::random)
	{
		std::random_device rd;
		std::mt19937 generator(rd());
		std::uniform_int_distribution<unsigned long> cpGenerator(1500, 15000);
		std::uniform_int_distribution<unsigned long> spGenerator(8000, 40000);
		std::uniform_int_distribution<unsigned long> epGenerator(3000, 35000);
		std::uniform_int_distribution<unsigned long> gpGenerator(1000, 10000);
		std::uniform_int_distribution<unsigned long> ppGenerator(30, 300);

		m_availability.add(defs::coin::copper, static_cast<uint32_t>(cpGenerator(generator)));
		m_availability.add(defs::coin::silver, static_cast<uint32_t>(spGenerator(generator)));
		m_availability.add(defs::coin::electrum, static_cast<uint32_t>(epGenerator(generator)));
		m_availability.add(defs::coin::gold, static_cast<uint32_t>(gpGenerator(generator)));
		m_availability.add(defs::coin::platinum, static_cast<uint32_t>(ppGenerator(generator)));
	}
}

adnd::market::store::~store()
{
}

void adnd::market::store::set_availability(const defs::coin& currency, const uint32_t& amount)
{
	if (m_budget == budget_model::assigned)
	{
		m_availability.add(currency, amount);
	}
}

void adnd::market::store::set_availability(const money::amount& amount)
{
	set_availability(amount.currency(), amount.value());
}

/**
	Returns a price for an item not in stock
*/
adnd::money::amount adnd::market::store::get_price(const uint32_t& itemUid, const buy_sell_side& side)
{
	return make_price_for(m_uidToId[itemUid], side);
}

adnd::money::amount adnd::market::store::make_price_for(const double& minPrice, const double& maxPrice, const defs::coin& currency, const buy_sell_side& side)
{
	double price = maxPrice;

	if (minPrice != maxPrice)
	{
		if (side == buy_sell_side::buy)
		{
			std::random_device rd;
			std::mt19937 generator(rd());
			std::uniform_real_distribution<double> distribution(std::max(minPrice, maxPrice *m_buySpreadForPrice), maxPrice);
			price = distribution(generator);
		}
		else if (side == buy_sell_side::sell)
		{
			std::random_device rd;
			std::mt19937 generator(rd());
			std::uniform_real_distribution<double> distribution(minPrice, std::min(minPrice * m_sellSpreadForPrice, maxPrice));
			price = distribution(generator);
		}
	}
	else
	{
		if (side == buy_sell_side::buy && m_buySpread)
		{
			double spread = 0.0;
			std::random_device rd;
			std::mt19937 generator(rd());
			std::normal_distribution<double> distribution(price, std::max(0.85, price * 0.15));

			double d = distribution(generator);
			spread = std::round(std::abs(price - d));
			price += spread;
		}
		else if (side == buy_sell_side::sell && m_sellSpread)
		{
			double spread = 0.0;
			std::random_device rd;
			std::mt19937 generator(rd());
			std::normal_distribution<double> distribution(price, std::max(0.85, price * 0.15));

			double d = distribution(generator);
			spread = std::round(std::abs(price - d));
			price = std::max(price - spread, 0.0);
		}

	}

	return money::amount(currency, static_cast<uint32_t>(price));
}

/**
	Returns a price for an item type not in stock
*/
adnd::money::amount adnd::market::store::make_price_for(const defs::equipment::item& itemId, const buy_sell_side& side)
{
	auto t = adnd::templates::metadata::get_instance().get_item_template(itemId);
	return make_price_for(t->min_cost_amount(), t->max_cost_amount(), t->cost_currency(), side);
}

/**
	Returns a price for a gem not in stock
*/
adnd::money::amount adnd::market::store::make_price_for(const defs::treasure::gems& gemId, const buy_sell_side& side)
{
	auto t = adnd::templates::metadata::get_instance().get_gem_template(gemId);
	return make_price_for(t.get_value_from(), t.get_value_to(), t.get_value_currency(), side);
}

/**
	Prepares an entry for an item type in the store.
	Makes the price for it, both buy and sell side but does not add the item to the store
*/
void adnd::market::store::add_to_store(const defs::equipment::item& itemId)
{
	if (m_shelf.find(itemId) == m_shelf.end())
	{
		money::amount buyPrice = make_price_for(itemId, buy_sell_side::buy);
		money::amount sellPrice = make_price_for(itemId, buy_sell_side::sell);

		std::get<1>(m_shelf[itemId]).replace(buyPrice);
		std::get<2>(m_shelf[itemId]).replace(sellPrice);
	}
}

void adnd::market::store::add_to_store(const defs::treasure::gems& gemId)
{
	if (m_gemStock.find(gemId) == m_gemStock.end())
	{
		money::amount buyPrice = make_price_for(gemId, buy_sell_side::buy);
		money::amount sellPrice = make_price_for(gemId, buy_sell_side::sell);

		std::get<1>(m_gemStock[gemId]).replace(buyPrice);
		std::get<2>(m_gemStock[gemId]).replace(sellPrice);
	}
}

void adnd::market::store::add_to_store(const adnd::treasure::object_of_art& o)
{
	if (m_artStock.find(o.uid()) == m_artStock.end())
	{
		money::amount buyPrice = make_price_for(o.value().value(), o.value().value(), o.value().currency(), buy_sell_side::buy);
		money::amount sellPrice = make_price_for(o.value().value(), o.value().value(), o.value().currency(), buy_sell_side::sell);

		std::get<1>(m_artStock[o.uid()]).replace(buyPrice);
		std::get<2>(m_artStock[o.uid()]).replace(sellPrice);
	}
}

/**
	Adds an item instance to the stock and makes the price for it, both buy and sell side
*/
void adnd::market::store::add(adnd::equipment::item* it)
{
	m_uidToId[it->uid()] = it->id();
	add_to_store(it->id());

	std::get<0>(m_shelf[it->id()])[it->uid()] = it;
}

/**
	Adds a gem instance to the stock and makes the price for it, both buy and sell side
*/
void adnd::market::store::add(const adnd::treasure::gem& g)
{
	//m_uidToId[g.uid()] = g.id();
	add_to_store(g.id());

	std::get<0>(m_gemStock[g.id()])[g.uid()] = g;
}

/**
	Adds an object of art instance to the stock and makes the price for it, both buy and sell side
*/
void adnd::market::store::add(const adnd::treasure::object_of_art& o)
{
	//m_uidToId[g.uid()] = g.id();
	add_to_store(o);

	std::get<0>(m_artStock[o.uid()]) = o;
}

/**
	Returns true if the store has enought money to cover the amount in input
*/
bool adnd::market::store::check_availability(const adnd::money::amount& amt)
{
	if (m_budget == budget_model::unlimited)
		return true;

	return m_availability.check_availability(amt);
}

/**
	Returns a price for an item type.
	If the request side is buy, then
*/
adnd::money::amount adnd::market::store::get_price(const defs::equipment::item& item, const store::buy_sell_side& side)
{
	if (side == store::buy_sell_side::buy)
	{
		if (!in_stock(item))
		{
			throw std::exception("Item not in stock", static_cast<int>(item));
		}
		return std::get<1>(m_shelf[item]);
	}
	else if (side == store::buy_sell_side::sell)
	{
		money::amount prc;
		if (!in_stock(item))
		{
			prc = make_price_for(item, side);
		}
		else
			prc = std::get<2>(m_shelf[item]);

		if (!check_availability(prc))
			throw std::exception("Not enought money", static_cast<int>(prc.value()));

		return prc;
	}

	throw std::exception("Unkonwn item ID", static_cast<int>(item));
}

/**
	Performs a buy transaction from the store side (i.e. the customer buys from the store).
	If the item type is out of stock, then the method fails throwing an exception.
	Otherwise, the store availability is increased by the buy price of the item,
	an item of the specifide type is removed from the store and returned to the caller.
*/
adnd::equipment::item* adnd::market::store::buy(const defs::equipment::item& item)
{
	if (!in_stock(item))
		throw std::exception("Item out ouf stock", static_cast<int>(item));

	m_availability += std::get<1>(m_shelf[item]);
	adnd::equipment::item* it = std::get<0>(m_shelf[item]).begin()->second;
	std::get<0>(m_shelf[item]).erase(it->uid());
	m_uidToId.erase(it->uid());

	return it;
}

/**
	Undo the buy transaction.
	The buy price is returned to the buyer and the item to the store
*/
void adnd::market::store::undo_buy(adnd::equipment::item* it)
{
	if (m_budget != budget_model::unlimited)
		m_availability.subtract(std::get<1>(m_shelf[it->id()]));
	add(it);
}

/**
	Performs a sell transaction from the store side (i.e. the customer sells to the store).
	If the item type is in stock, then the price is taken from the store price list, otherwise a price is assigned (both buy and sell side).
	If the store has enought money to buy the goods, then the transaction continues, otherwise an exception is thrown
*/
adnd::money::amount adnd::market::store::sell(adnd::equipment::item* it)
{
	money::amount price;
	if (!in_stock(it->id()))
		add_to_store(it->id());

	price = std::get<2>(m_shelf[it->id()]);

	if (!check_availability(price))
		throw std::exception("Not enough money", price);

	if (m_budget != budget_model::unlimited)
		m_availability.subtract(price);
	add(it);

	return price;
}

adnd::treasure::gem adnd::market::store::buy(const defs::treasure::gems& gemId)
{
	if (!in_stock(gemId))
		throw std::exception("Gem out ouf stock", static_cast<int>(gemId));

	m_availability += std::get<1>(m_gemStock[gemId]);
	adnd::treasure::gem g = std::get<0>(m_gemStock[gemId]).begin()->second;
	std::get<0>(m_gemStock[gemId]).erase(g.uid());
	m_uidToId.erase(g.uid());

	return g;
}

void adnd::market::store::undo_buy(const adnd::treasure::gem& g)
{
	if (m_budget != budget_model::unlimited)
		m_availability.subtract(std::get<1>(m_gemStock[g.id()]));
	add(g);
}

/**
	Performs a sell transaction from the store side (i.e. the customer sells to the store).
	If the gem type is in stock, then the price is taken from the store price list, otherwise a price is assigned (both buy and sell side).
	If the store has enought money to buy the goods, then the transaction continues, otherwise an exception is thrown
*/
adnd::money::amount adnd::market::store::sell(const adnd::treasure::gem& g)
{
	money::amount price;
	if (!in_stock(g.id()))
		add_to_store(g.id());
	
	price = std::get<2>(m_gemStock[g.id()]);
	
	if (!check_availability(price))
		throw std::exception("Not enough money", price);
	
	if (m_budget != budget_model::unlimited)
		m_availability.subtract(price);
	add(g);
	
	return price;
}

adnd::money::amount adnd::market::store::sell(const adnd::treasure::object_of_art& o)
{
	money::amount price;
	if (!in_stock(o.uid()))
		add_to_store(o);

	price = std::get<2>(m_artStock[o.uid()]);

	if (!check_availability(price))
		throw std::exception("Not enough money", price);

	if (m_budget != budget_model::unlimited)
		m_availability.subtract(price);
	add(o);

	return price;
}


uint32_t adnd::market::market::create_store(const std::string& name, const store::budget_model& storeBudget, bool buySpread, bool sellSpread)
{
	uint32_t nextStoreUid = static_cast<uint32_t>(m_stores.size());
	std::string storeName = name;
	if (storeName.empty())
	{
		std::stringstream ss;
		ss << "Store " << nextStoreUid;
		storeName = ss.str();
	}
	m_stores[nextStoreUid] = store(nextStoreUid, storeName, storeBudget, buySpread, sellSpread);
	m_nameToUid[storeName] = nextStoreUid;

	return m_nameToUid[storeName];
}

adnd::market::market& adnd::market::market::get_instance()
{
	static market _instance;

	if (!m_initialised)
	{
		m_initialised = true;
	}
	return _instance;
}
