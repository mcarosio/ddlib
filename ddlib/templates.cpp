#include "stdafx.h"
#include "templates.h"
#include "utils.h"
#include "sqlite_db.h"

#include <tuple>
#include <string>
#include <experimental/filesystem>
#include <fstream>

bool adnd::templates::metadata::m_initialised = false;
bool adnd::configuration::m_initialised = false;

const char* adnd::templates::metadata::query::skill_info = "skill_info";
const char* adnd::templates::metadata::query::classes = "classes";
const char* adnd::templates::metadata::query::saving_throws = "saving_throws";
const char* adnd::templates::metadata::query::turn_undead = "turn_undead";
const char* adnd::templates::metadata::query::class_info = "class_info";
const char* adnd::templates::metadata::query::class_type_info = "class_type_info";
const char* adnd::templates::metadata::query::skill_limits_per_race = "skill_limits_per_race";
const char* adnd::templates::metadata::query::skill_limits_per_class = "skill_limits_per_class";
const char* adnd::templates::metadata::query::classes_per_race = "classes_per_race";
const char* adnd::templates::metadata::query::prime_requisite_per_class = "prime_requisite_per_class";
const char* adnd::templates::metadata::query::hit_dice_info = "hit_dice_info";
const char* adnd::templates::metadata::query::level_advancement = "level_advancement";
const char* adnd::templates::metadata::query::level_advancement_factor = "level_advancement_factor";
const char* adnd::templates::metadata::query::alignment_per_class = "alignment_per_class";
const char* adnd::templates::metadata::query::moral_alignment = "moral_alignment";
const char* adnd::templates::metadata::query::moral_alignments = "moral_alignments";
const char* adnd::templates::metadata::query::skill_modifier_per_race = "skill_modifier_per_race";
const char* adnd::templates::metadata::query::thief_abilities = "thief_abilities";
const char* adnd::templates::metadata::query::ranger_abilities = "ranger_abilities";
const char* adnd::templates::metadata::query::bard_abilities = "bard_abilities";
const char* adnd::templates::metadata::query::thaco_info = "thaco_info";
const char* adnd::templates::metadata::query::strength_stats = "strength_stats";
const char* adnd::templates::metadata::query::dexterity_stats = "dexterity_stats";
const char* adnd::templates::metadata::query::constitution_stats = "constitution_stats";
const char* adnd::templates::metadata::query::intelligence_stats = "intelligence_stats";
const char* adnd::templates::metadata::query::wisdom_stats = "wisdom_stats";
const char* adnd::templates::metadata::query::charisma_stats = "charisma_stats";
const char* adnd::templates::metadata::query::racial_stats = "racial_stats";
const char* adnd::templates::metadata::query::thieving_modifiers_per_dexterity = "thieving_modifiers_per_dexterity";
const char* adnd::templates::metadata::query::thieving_modifiers_per_race = "thieving_modifiers_per_race";
const char* adnd::templates::metadata::query::wizard_spell_progression = "wizard_spell_progression";
const char* adnd::templates::metadata::query::bard_spell_progression = "bard_spell_progression";
const char* adnd::templates::metadata::query::priest_spell_progression = "priest_spell_progression";
const char* adnd::templates::metadata::query::paladin_spell_progression = "paladin_spell_progression";
const char* adnd::templates::metadata::query::ranger_spell_progression = "ranger_spell_progression";
const char* adnd::templates::metadata::query::race_info = "race_info";
const char* adnd::templates::metadata::query::exchange_rates = "exchange_rates";
const char* adnd::templates::metadata::query::coin_info = "coin_info";
const char* adnd::templates::metadata::query::starting_money_info = "starting_money_info";
const char* adnd::templates::metadata::query::melee_attacks = "melee_attacks";
const char* adnd::templates::metadata::query::ageing_modifiers = "ageing_modifiers";
const char* adnd::templates::metadata::query::prime_requisite_bonuses = "prime_requisite_bonuses";
const char* adnd::templates::metadata::query::items_info = "items_info";
const char* adnd::templates::metadata::query::item_info = "item_info";
const char* adnd::templates::metadata::query::clothing_info = "clothing_info";
const char* adnd::templates::metadata::query::armour_info = "armour_info";
const char* adnd::templates::metadata::query::projectile_info = "projectile_info";
const char* adnd::templates::metadata::query::weapon_info = "weapon_info";
const char* adnd::templates::metadata::query::weapon_projectile_info = "weapon_projectile_info";
const char* adnd::templates::metadata::query::items_allowed = "items_allowed";
const char* adnd::templates::metadata::query::item_capabilities_per_character_class = "item_capabilities_per_character_class";
const char* adnd::templates::metadata::query::modified_movement_rates = "modified_movement_rates";
const char* adnd::templates::metadata::query::missile_range_modifiers = "missile_range_modifiers";
const char* adnd::templates::metadata::query::deities = "deities";
const char* adnd::templates::metadata::query::deity_info = "deity_info";
const char* adnd::templates::metadata::query::deity_worshippers_info = "deity_worshippers_info";
const char* adnd::templates::metadata::query::spheres_per_deity = "spheres_per_deity";
const char* adnd::templates::metadata::query::proficiency_slots = "proficiency_slots";
const char* adnd::templates::metadata::query::specialist_attacks_per_round = "specialist_attacks_per_round";
const char* adnd::templates::metadata::query::treasure_composition = "treasure_composition";
const char* adnd::templates::metadata::query::treasure_classes = "treasure_classes";
const char* adnd::templates::metadata::query::treasure_component = "treasure_components";
const char* adnd::templates::metadata::query::treasure_location = "treasure_locations";
const char* adnd::templates::metadata::query::treasure_nature = "treasure_natures";
const char* adnd::templates::metadata::query::gems = "gems";
const char* adnd::templates::metadata::query::gems_by_type = "gems_by_type";
const char* adnd::templates::metadata::query::gem_info = "gem_info";
const char* adnd::templates::metadata::query::gem_types = "gem_types";
const char* adnd::templates::metadata::query::objects_of_art = "objects_of_art";
const char* adnd::templates::metadata::query::magical_items = "magical_items";
const char* adnd::templates::metadata::query::magical_item_info = "magical_item_info";
const char* adnd::templates::metadata::query::wizard_spells = "wizard_spells";
const char* adnd::templates::metadata::query::priest_spells = "priest_spells";
const char* adnd::templates::metadata::query::wizard_spell_info = "wizard_spell_info";
const char* adnd::templates::metadata::query::priest_spell_info = "priest_spell_info";
const char* adnd::templates::metadata::query::wizard_spell_schools = "wizard_spell_schools";
const char* adnd::templates::metadata::query::priest_spell_spheres = "priest_spell_spheres";
const char* adnd::templates::metadata::query::schools_of_magic = "schools_of_magic";
const char* adnd::templates::metadata::query::school_of_magic_info = "school_of_magic_info";
const char* adnd::templates::metadata::query::spheres_of_influence = "spheres_of_influence";
const char* adnd::templates::metadata::query::sphere_of_influence_info = "sphere_of_influence_info";
const char* adnd::templates::metadata::query::school_of_magic_opposites = "school_of_magic_opposites";
const char* adnd::templates::metadata::query::schools_per_class = "schools_per_class";
const char* adnd::templates::metadata::query::opposite_schools_per_class = "opposite_schools_per_class";
const char* adnd::templates::metadata::query::priest_spells_per_deity_and_level = "priest_spells_per_deity_and_level";
const char* adnd::templates::metadata::query::priest_spells_per_level_and_class = "priest_spells_per_level_and_class";
const char* adnd::templates::metadata::query::wizard_spells_per_school_and_level = "wizard_spells_per_school_and_level";
const char* adnd::templates::metadata::query::monsters = "monsters";
const char* adnd::templates::metadata::query::monster_info = "monster_info";



adnd::templates::character_class::character_class(const defs::character_class& cls)
		: m_class(cls), m_classTypeInfo(), m_startingMoneyDie(), m_startingMoneyBase(0), m_startingMoneyMultiplier(0)
{
}

void adnd::templates::character_class::init()
{
	// Load skill requisites
	for (auto s : { defs::character_skill::strength, defs::character_skill::dexterity, defs::character_skill::constitution,
				defs::character_skill::intelligence, defs::character_skill::wisdom, defs::character_skill::charisma })
	{
		auto limits = adnd::templates::metadata::get_instance().get_skill_limits(m_class.id(), s);
		set_skill_requisites(s, std::get<0>(limits), std::get<1>(limits));
	}

	// Load prime requisites
	for (auto c : m_class.classes())
	{
		auto p = metadata::get_character_type_and_id(c);
		auto res = templates::metadata::get_instance().fetch<short>(templates::metadata::query::prime_requisite_per_class);
		auto rSkl = res.filter<uint16_t>(p.classType).filter<uint32_t>(p.classId).run();

		for (auto s : rSkl)
		{
			auto skl = static_cast<defs::character_skill>(std::get<0>(s));
			m_primarySkills.emplace(skl);
		}
	}

	// Load saving throws table
	for (auto c : m_class.classes())
	{
		auto type = static_cast<uint16_t>(metadata::get_type_of(c));
		auto res = templates::metadata::get_instance().fetch<short, short, short, short, short, short>(templates::metadata::query::saving_throws);
		auto rSave = res.filter<uint16_t>(type).run();

		short scores[5];
		for (auto s : rSave)
		{
			short level = std::get<0>(s);
			scores[0] = std::get<1>(s);
			scores[1] = std::get<2>(s);
			scores[2] = std::get<3>(s);
			scores[3] = std::get<4>(s);
			scores[4] = std::get<5>(s);

			if (m_savings.find(level) != m_savings.end())
			{
				auto row = tables::saving_throws(
					std::min<short>(m_savings[level].paralysation(), scores[0]),
					std::min<short>(m_savings[level].rod(), scores[1]),
					std::min<short>(m_savings[level].petrification(), scores[2]),
					std::min<short>(m_savings[level].breath_weapon(), scores[3]),
					std::min<short>(m_savings[level].spell(), scores[4]));
				m_savings.emplace(level, row);
			}
			else
			{
				auto row = tables::saving_throws(scores[0], scores[1], scores[2], scores[3], scores[4]);
				m_savings.emplace(level, row);
			}
		}
	}

	// Load class info
	{
		auto p = metadata::get_character_type_and_id(m_class.id());
		auto res = templates::metadata::get_instance().fetch<uint16_t, uint16_t, uint32_t, std::string, std::string, std::string, char, short>(templates::metadata::query::class_info);
		auto r = res.filter<uint32_t>(p.classId).run().first();
		
		m_longName = std::get<3>(r);
		m_shortName = std::get<4>(r);

		bool multiclass = std::get<6>(r) == 'Y';
		if (multiclass && m_class.is_type_of(defs::character_class_type::wizard))
		{
			auto clsType = m_class.select_class_by_type(defs::character_class_type::wizard);
			auto pMulti = metadata::get_character_type_and_id(clsType);
			auto resMulticlass = templates::metadata::get_instance().fetch<uint16_t, uint16_t, uint32_t, std::string, std::string, std::string, char, short>(templates::metadata::query::class_info);
			auto rMulti = resMulticlass.filter<uint32_t>(pMulti.classId).run().first();

			m_specialistSchool = static_cast<defs::spells::school>(std::get<7>(rMulti));
		}
		else
		{
			m_specialistSchool = static_cast<defs::spells::school>(std::get<7>(r));
		}
	}

	// Load class type info
	for (auto c : m_class.classes())
	{
		auto p = metadata::get_character_type_and_id(c);
		auto clsType = static_cast<defs::character_class_type>(p.classType);

		auto res = metadata::get_instance().fetch<std::string, short, std::string, short, short>(metadata::query::class_type_info);
		auto r = res.filter<short>(static_cast<uint16_t>(clsType)).run().first();

		std::tuple<std::string, short, std::string, random::die, short> info = std::make_tuple(
			std::get<0>(r), std::get<1>(r), std::get<2>(r),
			random::die(static_cast<defs::die_faces>(std::get<3>(r))),
			std::get<4>(r)
		);
		m_classTypeInfo[clsType] = info;
	}

	for (auto clsInfo : m_classTypeInfo)
	{
		//defs::die_faces faces = std::max<defs::die_faces>(faces, clsInfo.second.hitDice.faces());
		auto cls = m_class.select_class_by_type(clsInfo.first);
		m_hitDice[cls] = random::die(std::get<3>(clsInfo.second).faces());
	}

	// Load level advancement table
	for (auto c : m_class.classes())
	{
		auto p = metadata::get_character_type_and_id(c);

		auto lvlAdv = metadata::get_instance().fetch<short, long>(metadata::query::level_advancement).filter<uint16_t>(p.classType).filter<uint32_t>(p.classId).run();
		for (auto l : lvlAdv)
		{
			m_levelAdvancements[c][std::get<0>(l)] = std::get<1>(l);
		}

		auto f = metadata::get_instance().fetch<long>(metadata::query::level_advancement_factor).filter<uint16_t>(p.classType).filter<uint32_t>(p.classId).run();
		m_advancementFactor[c] = std::get<0>(f.first());

	}

	// Load moral alignments
	{
		std::map<defs::moral_alignment, short> moralAligns;
		for (auto c : m_class.classes())
		{
			auto p = metadata::get_character_type_and_id(c);

			auto rAligns = metadata::get_instance().fetch<short>(metadata::query::alignment_per_class);
			auto aligns = rAligns.filter<uint16_t>(p.classType).filter<uint32_t>(p.classId).run();
			for (auto a : aligns)
			{
				auto align = static_cast<defs::moral_alignment>(std::get<0>(a));
				//m_moralAlignments.emplace(align);
				moralAligns[align]++;
			}
		}
		// Merge the alignments, sets only those allowed to every class
		for (auto a : moralAligns)
		{
			if (a.second == m_class.classes().size())
				m_moralAlignments.emplace(a.first);
		}
	}

	if (can_turn_undead())
	{
		auto turnTable = templates::metadata::get_instance().fetch<short, std::string, std::string, std::string, std::string, std::string, std::string, std::string, std::string, std::string, std::string, std::string, std::string, std::string>(metadata::query::turn_undead).run();
		
		for (auto t : turnTable)
		{
			std::string scores[13];
			short level = std::get<0>(t);
			scores[0] = std::get<1>(t);
			scores[1] = std::get<2>(t);
			scores[2] = std::get<3>(t);
			scores[3] = std::get<4>(t);
			scores[4] = std::get<5>(t);
			scores[5] = std::get<6>(t);
			scores[6] = std::get<7>(t);
			scores[7] = std::get<8>(t);
			scores[8] = std::get<9>(t);
			scores[9] = std::get<10>(t);
			scores[10] = std::get<11>(t);
			scores[11] = std::get<12>(t);
			scores[12] = std::get<13>(t);
		
			tables::turn_ability t = tables::turn_ability(
										parse_turn_ability(scores[0]), parse_turn_ability(scores[1]), parse_turn_ability(scores[2]), parse_turn_ability(scores[3]),
										parse_turn_ability(scores[4]), parse_turn_ability(scores[5]), parse_turn_ability(scores[6]), parse_turn_ability(scores[7]),
										parse_turn_ability(scores[8]), parse_turn_ability(scores[9]), parse_turn_ability(scores[10]), parse_turn_ability(scores[11]),
										parse_turn_ability(scores[12]));
		
			m_turnAbilities.emplace(level, t);
		}
	}

	if (has_thief_abilities())
	{
		std::string stmtId;
		
		if (m_class.is(defs::character_class::thief))
			stmtId = std::string(metadata::query::thief_abilities);
		if (m_class.is(defs::character_class::ranger))
			stmtId = std::string(metadata::query::ranger_abilities);
		else if (m_class.is(defs::character_class::bard))
			stmtId = std::string(metadata::query::bard_abilities);
		
		if (!stmtId.empty())
		{
			auto thiefSkl = templates::metadata::get_instance().fetch<short, short, short, short, short, short, short, short, short>(stmtId.c_str()).run();
			
			short scores[8];
			for (auto ts : thiefSkl)
			{
				short level = std::get<0>(ts);
				scores[0] = std::get<1>(ts);
				scores[1] = std::get<2>(ts);
				scores[2] = std::get<3>(ts);
				scores[3] = std::get<4>(ts);
				scores[4] = std::get<5>(ts);
				scores[5] = std::get<6>(ts);
				scores[6] = std::get<7>(ts);
				scores[7] = std::get<8>(ts);
		
				auto row = adnd::tables::thief_ability(scores[0], scores[1], scores[2], scores[3], scores[4], scores[5], scores[6], scores[7]);
				m_thiefAbilities.emplace(level, row);
			}
		}
	}
	
	if (can_cast_priest_spells())
	{
		std::string stmtId;
		
		if (m_class.is(defs::character_class::cleric) || m_class.is(defs::character_class::druid) || m_class.is(defs::character_class::priest_of_specific_mythos))
			stmtId = std::string(metadata::query::priest_spell_progression);
		else if (m_class.id() == defs::character_class::paladin)
			stmtId = std::string(metadata::query::paladin_spell_progression);
		else if (m_class.is(defs::character_class::ranger))
			stmtId = std::string(metadata::query::ranger_spell_progression);
				
		if (!stmtId.empty())
		{
			auto spellsProg = templates::metadata::get_instance().fetch<short, short, short, short, short, short, short, short, short>(stmtId.c_str()).run();

			for (auto s : spellsProg)
			{
				adnd::tables::priest_spell_progression sp;
				sp.level = std::get<0>(s);
				sp.castingSpell = std::get<1>(s);
				sp.spells[0] = std::get<2>(s);
				sp.spells[1] = std::get<3>(s);
				sp.spells[2] = std::get<4>(s);
				sp.spells[3] = std::get<5>(s);
				sp.spells[4] = std::get<6>(s);
				sp.spells[5] = std::get<7>(s);
				sp.spells[6] = std::get<8>(s);

				m_priestSpellProgression.emplace(sp.level, sp);
			}
		}
	}

	if (can_cast_wizard_spells())
	{
		std::string stmtId;
		
		if (m_class.is_type_of(defs::character_class_type::wizard))
			stmtId = std::string(metadata::query::wizard_spell_progression);
		else if (m_class.is(defs::character_class::bard))
			stmtId = std::string(metadata::query::bard_spell_progression);
		
		if (!stmtId.empty())
		{
			auto spellsProg = templates::metadata::get_instance().fetch<short, short, short, short, short, short, short, short, short, short>(stmtId.c_str()).run();
		
			for (auto s : spellsProg)
			{
				adnd::tables::wizard_spell_progression sp;
				sp.level = std::get<0>(s);
				sp.spells[0] = std::get<1>(s);
				sp.spells[1] = std::get<2>(s);
				sp.spells[2] = std::get<3>(s);
				sp.spells[3] = std::get<4>(s);
				sp.spells[4] = std::get<5>(s);
				sp.spells[5] = std::get<6>(s);
				sp.spells[6] = std::get<7>(s);
				sp.spells[7] = std::get<8>(s);
				sp.spells[8] = std::get<9>(s);
				
				m_wizardSpellProgression.emplace(sp.level, sp);
			}
		}
	}

	auto moneyInfo = adnd::templates::metadata::get_instance().get_starting_money_info(m_class.id());
	m_startingMoneyDie = random::die_roll(static_cast<defs::die_faces>(std::get<2>(moneyInfo)), std::get<1>(moneyInfo));
	m_startingMoneyBase = std::get<3>(moneyInfo);
	m_startingMoneyMultiplier = std::get<4>(moneyInfo);
	
	std::map<short, adnd::tables::attack_rate> meleeRates;
	
	if (!m_class.is_type_of(adnd::defs::character_class_type::warrior))
	{
		adnd::tables::attack_rate rate;
		rate.type = defs::weapons::attack_type::melee;
		rate.level = 1;
		rate.attacks = 1;
		rate.rounds = 1;
		meleeRates.emplace(rate.level, rate);
	}
	else
	{
		auto rates = templates::metadata::get_instance().fetch<short, short, short>(metadata::query::melee_attacks);
		for (auto r : rates)
		{
			adnd::tables::attack_rate rate;
			rate.type = defs::weapons::attack_type::melee;
			rate.level = std::get<0>(r);
			rate.attacks = std::get<1>(r);
			rate.rounds = std::get<2>(r);

			meleeRates.emplace(rate.level, rate);
		}
	}

	m_attackRates[defs::weapons::attack_type::melee] = meleeRates;
	
	// Selecting allowed items for the class
	std::map<adnd::defs::equipment::item, short> itemsAllowed;
	for (auto c : m_class.classes())
	{
		auto p = templates::metadata::get_character_type_and_id(c);

		auto res = adnd::templates::metadata::get_instance().fetch<short>(templates::metadata::query::items_allowed);
		auto items = res.filter<uint16_t>(p.classType).filter<uint32_t>(p.classId).run();

		for (auto i : items)
		{
			auto item = static_cast<defs::equipment::item>(std::get<0>(i));
			itemsAllowed[item]++;
		}
	}

	for (auto i : itemsAllowed)
	{
		if (i.second == m_class.classes().size())
			m_itemsAllowed.emplace(i.first);
	}

	// Selecting the schools of magic (if any)
	if (m_class.is_type_of(defs::character_class_type::wizard))
	{
		auto cls = m_class.select_class_by_type(defs::character_class_type::wizard);
		auto p = templates::metadata::get_character_type_and_id(cls);

		auto allSchools = metadata::get_instance().fetch<short>(metadata::query::schools_of_magic).run();

		for (auto s : allSchools)
		{
			auto schoolId = static_cast<defs::spells::school>(std::get<0>(s));
			m_schoolsOfMagic[schoolId] = defs::spells::school_access::neutral;
		}

		if (is_specialist_wizard())
		{
			auto resOpposites = metadata::get_instance().fetch<short>(metadata::query::opposite_schools_per_class);
			auto oppositeSchools = resOpposites.filter<uint16_t>(p.classType).filter<uint32_t>(p.classId).run();
			for (auto s : oppositeSchools)
			{
				auto schoolId = static_cast<defs::spells::school>(std::get<0>(s));
				m_schoolsOfMagic[schoolId] = defs::spells::school_access::opposite;
			}

			auto resSpecialist = metadata::get_instance().fetch<short>(metadata::query::schools_per_class);
			auto specialistSchools = resSpecialist.filter<uint16_t>(p.classType).filter<uint32_t>(p.classId).run();
			for (auto s : specialistSchools)
			{
				auto schoolId = static_cast<defs::spells::school>(std::get<0>(s));
				m_schoolsOfMagic[schoolId] = defs::spells::school_access::specialist;
			}
		}
	}
}


adnd::tables::turn_undead adnd::templates::character_class::parse_turn_ability(const std::string& score)
{
	defs::turn_effect eff;
	short chance = 0;

	if (utils::compare_no_case(score, "-") == 0)
		eff = defs::turn_effect::no_effect;
	else if (utils::compare_no_case(score, "T") == 0)
		eff = defs::turn_effect::turn;
	else if (utils::compare_no_case(score, "D") == 0)
		eff = defs::turn_effect::destroy;
	else if (utils::compare_no_case(score, "D*") == 0)
		eff = defs::turn_effect::destroy_more;
	else
	{
		eff = defs::turn_effect::percentage;
		chance = utils::parse<short>(score.c_str(), score.length(), 20);
	}
	return tables::turn_undead(eff, chance);
}


std::set<adnd::defs::equipment::item> adnd::templates::character_class::select_allowed_items()
{
	std::map<adnd::defs::equipment::item, short> itemsAllowed;
	for (auto c : m_class.classes())
	{
		auto p = templates::metadata::get_character_type_and_id(c);

		auto res = adnd::templates::metadata::get_instance().fetch<short>(templates::metadata::query::items_allowed);
		auto items = res.filter<uint16_t>(p.classType).filter<uint32_t>(p.classId).run();

		for (auto i : items)
		{
			auto item = static_cast<defs::equipment::item>(std::get<0>(i));
			itemsAllowed[item]++;
		}
	}

	std::set<adnd::defs::equipment::item> result;
	for (auto i : itemsAllowed)
	{
		if (i.second == m_class.classes().size())
			result.emplace(i.first);
	}
	return result;
}

short adnd::templates::character_class::title_level()
{
	short titleLevel = 20;
	for (auto clsInfo : m_classTypeInfo)
		titleLevel = std::min<short>(titleLevel, std::get<1>(clsInfo.second));
	return titleLevel;
}

short adnd::templates::character_class::hp_after_title()
{
	short hpAfterTitle = 1;
	for (auto clsInfo : m_classTypeInfo)
		hpAfterTitle = std::max<short>(hpAfterTitle, std::get<4>(clsInfo.second));
	return hpAfterTitle;
}

bool adnd::templates::character_class::check_skill(const defs::character_skill& skl, const skills::character_skill_value& sklValue)
{
	return check_skill(skl, sklValue.value());
}

bool adnd::templates::character_class::check_skill(const defs::character_skill& skl, const short& sklValue)
{
	return m_skillRequisites[skl].first <= sklValue && sklValue <= m_skillRequisites[skl].second;
}

bool adnd::templates::character_class::check_moral_alignment(const defs::moral_alignment& align)
{
	return m_moralAlignments.find(align) != m_moralAlignments.end();
}

void adnd::templates::character_class::get_skill_boundaries(const defs::character_skill& skill, short& minValue, short& maxValue)
{
	minValue = m_skillRequisites[skill].first;
	maxValue = m_skillRequisites[skill].second;
}

std::map<adnd::defs::character_class, adnd::random::die> adnd::templates::character_class::hit_dice()
{
	return m_hitDice;
}

const adnd::tables::saving_throws& adnd::templates::character_class::saving_throws(const short& level)
{
	return m_savings[level];
}

double adnd::templates::character_class::experience_bonus(const skills::character_skill_set & skills)
{
	bool experienceBonusAvailable = true;
	for (auto skl : m_primarySkills)
	{
		if (skills.skill_value(skl) < 16)
			experienceBonusAvailable = false;
	}

	return (experienceBonusAvailable) ? 0.1 : 0.0;
}

const adnd::tables::turn_undead& adnd::templates::character_class::turn(const short& level, const defs::turn_ability& type)
{
	return m_turnAbilities.lower_bound(level)->second.turn(type);
}

const adnd::tables::thief_ability& adnd::templates::character_class::thievery(const short& level)
{
	return m_thiefAbilities.lower_bound(level)->second;
}

adnd::defs::spells::school_access adnd::templates::character_class::get_school_access(const defs::spells::school& id)
{
	auto s = m_schoolsOfMagic.find(id);
	if (s == m_schoolsOfMagic.end())
		throw std::exception("Unable to find school of magic");

	return s->second;
}

adnd::defs::spells::school_access adnd::templates::character_class::get_school_access(const defs::spells::wizard_spell& id)
{
	auto ct = metadata::get_instance().get_class_template(m_class.id());
	auto st = metadata::get_instance().get_wizard_spell_template(id);
	
	defs::spells::school_access access = defs::spells::school_access::opposite;
	for (auto s : st.schools())
	{
		access = static_cast<defs::spells::school_access>(std::max<short>(static_cast<short>(ct.get_school_access(s)), static_cast<short>(access)));
	}
	return access;
}

adnd::tables::wizard_spell_progression adnd::templates::character_class::get_wizard_spell_progression(const short& casterLevel)
{
	if (m_class.is_multiclass())
	{
		auto cls = m_class.select_class_by_type(defs::character_class_type::wizard);
		auto t = metadata::get_instance().get_class_template(cls);
		return t.get_wizard_spell_progression(casterLevel);
	}
	else
		return m_wizardSpellProgression.lower_bound(casterLevel)->second;
}

adnd::tables::priest_spell_progression adnd::templates::character_class::get_priest_spell_progression(const short& casterLevel)
{
	if (m_class.is_multiclass())
	{
		auto cls = m_class.select_class_by_type(defs::character_class_type::priest);
		auto t = metadata::get_instance().get_class_template(cls);
		return t.get_priest_spell_progression(casterLevel);
	}
	else
		return m_priestSpellProgression.lower_bound(casterLevel)->second;
}

bool adnd::templates::character_class::can_use(const defs::equipment::item& itemId)
{
	return m_itemsAllowed.find(itemId) != m_itemsAllowed.end();
}

std::map<adnd::defs::character_class, short> adnd::templates::character_class::get_levels(const long& xp)
{
	bool continueSearch = true;
	short level = 1;
	long ptExp = 0;
	std::map<defs::character_class, short> result;

	long ripartedXP = xp / static_cast<long>(m_class.classes().size());

	for (auto c : m_class.classes())
	{
		for (auto it = m_levelAdvancements[c].begin(); it != m_levelAdvancements[c].end() && continueSearch; ++it)
		{
			if (ripartedXP < it->second)
				continueSearch = false;
			else
			{
				level = it->first;
				ptExp = it->second;
			}
		}
		if (continueSearch && level == m_levelAdvancements[c].size() + 1)
		{
			level += m_levelAdvancements[c][ptExp] + static_cast<short>(((ripartedXP - ptExp) / m_advancementFactor[c]));
		}
		result[c] = level;
		continueSearch = true;
	}
	return result;
}

short adnd::templates::character_class::get_level(const long& xp, const adnd::defs::character_class& cls)
{
	auto levels = get_levels(xp);
	if (cls == defs::character_class::none)
		return levels.begin()->second;
	else
		return levels[cls];
}

long adnd::templates::character_class::get_xp(const short& lvl, const defs::character_class& cls)
{
	bool continueSearch = true;
	short level = 1;
	long ptExp = 0;

	if (lvl == 1) return 0;

	for (auto it = m_levelAdvancements[cls].begin(); it != m_levelAdvancements[cls].end() && continueSearch; ++it)
	{
		if (lvl <= it->first)
			continueSearch = false;
		level = it->first;
		ptExp = it->second;
	}
	if (continueSearch && level == m_levelAdvancements[cls].size() + 1)
	{
		ptExp = m_levelAdvancements[cls].rbegin()->second + ((lvl - m_levelAdvancements[cls].rbegin()->first) * m_advancementFactor[cls]);
	}
	return ptExp;
}

void adnd::templates::character_class::set_skill_requisites(const defs::character_skill& skill, const short minValue, const short maxValue)
{
	auto req = std::pair<short, short>(minValue, maxValue);
	m_skillRequisites[skill] = req;
}

short adnd::templates::character_class::get_thaco(const short& level)
{
	short bestThaco = 20;

	for (auto c : m_class.classes())
	{
		auto type = static_cast<uint16_t>(metadata::get_type_of(c));
		auto t = templates::metadata::get_instance().fetch<short, short>(metadata::query::thaco_info).filter<uint16_t>(type).run().first();
		auto thaco = 20 - (level - 1 / std::get<0>(t)) * std::get<1>(t);
		bestThaco = std::min<short>(bestThaco, thaco);
	}
	return bestThaco;
}

const adnd::tables::attack_rate& adnd::templates::character_class::get_attack_rate(const adnd::defs::weapons::attack_type& attackType, const short& level)
{
	return m_attackRates[attackType].lower_bound(level)->second;
}

//std::list<adnd::defs::character_skill> adnd::templates::character_class::get_prime_requisites()
//{
//	std::list<defs::character_skill> reqs;
//	for (auto skl : m_primarySkills)
//		reqs.push_back(skl);
//	return reqs;
//}

adnd::templates::character_race::character_race()
	: m_heightBaseMale(0), m_heightBaseFemale(0), m_heightDice1(defs::die_faces ::one), m_heightDice2(defs::die_faces ::one),
	m_weightBaseMale(0), m_weightBaseFemale(0), m_weightDice1(defs::die_faces ::one), m_weightDice2(defs::die_faces ::one),
	m_ageStarting(0), m_ageDice(defs::die_faces ::one), m_ageMaximum(0), m_ageMaximumDice(defs::die_faces ::one),
	m_middleAge(0), m_oldAge(0), m_venerableAge(0), m_limitAge(0)
{
}

adnd::templates::character_race::character_race(const defs::character_race& race)
	: m_race(race),
	m_heightBaseMale(0), m_heightBaseFemale(0), m_heightDice1(defs::die_faces ::one), m_heightDice2(defs::die_faces ::one),
	m_weightBaseMale(0), m_weightBaseFemale(0), m_weightDice1(defs::die_faces ::one), m_weightDice2(defs::die_faces ::one),
	m_ageStarting(0), m_ageDice(defs::die_faces ::one), m_ageMaximum(0), m_ageMaximumDice(defs::die_faces ::one),
	m_middleAge(0), m_oldAge(0), m_venerableAge(0), m_limitAge(0)
{
	for (auto s : { defs::character_skill::strength, defs::character_skill::dexterity, defs::character_skill::constitution,
					defs::character_skill::intelligence, defs::character_skill::wisdom, defs::character_skill::charisma })
	{
		auto limit = adnd::templates::metadata::get_instance().get_skill_limits(m_race, s);
		set_skill_requisites(s, std::get<0>(limit), std::get<1>(limit));

		set_skill_modifier(s, 0);
	}

	auto classes = metadata::get_instance().fetch<uint32_t, short>(metadata::query::classes_per_race).filter<int>(static_cast<int>(race)).run();
	for (auto& c : classes)
	{
		add_available_class(static_cast<defs::character_class>(std::get<0>(c)), std::get<1>(c));
	}

	{
		auto r = templates::metadata::get_instance().fetch<short, short>(metadata::query::skill_modifier_per_race);
		auto sklModifiers = r.filter<unsigned short>(static_cast<unsigned short>(m_race)).run();
		for (auto m : sklModifiers)
		{
			auto skill = static_cast<defs::character_skill>(std::get<0>(m));
			auto modifier = std::get<1>(m);
			m_skillModifiers[skill] = modifier;
		}
	}
	
	{
		auto r = templates::metadata::get_instance().fetch<short, short, short, short, short, short, short, short, short, short, short, short, short, short, short, short, short, short, short, short, short, short>(metadata::query::racial_stats);
		auto racialStats = r.filter<short>(static_cast<short>(m_race)).run().first();
		m_heightBaseMale = std::get<0>(racialStats);
		m_heightBaseFemale = std::get<1>(racialStats);
		m_heightDice1 = random::die_roll(static_cast<defs::die_faces>(std::get<3>(racialStats)), std::get<2>(racialStats));
		m_heightDice2 = random::die_roll(static_cast<defs::die_faces>(std::get<5>(racialStats)), std::get<4>(racialStats));

		m_weightBaseMale = std::get<6>(racialStats);
		m_weightBaseFemale = std::get<7>(racialStats);
		m_weightDice1 = random::die_roll(static_cast<defs::die_faces>(std::get<9>(racialStats)), std::get<8>(racialStats));
		m_weightDice2 = random::die_roll(static_cast<defs::die_faces>(std::get<11>(racialStats)), std::get<10>(racialStats));

		m_ageStarting = std::get<12>(racialStats);
		m_ageDice = random::die_roll(static_cast<defs::die_faces>(std::get<14>(racialStats)), std::get<13>(racialStats));

		m_ageMaximum = std::get<15>(racialStats);
		m_ageMaximumDice = random::die_roll(static_cast<defs::die_faces>(std::get<17>(racialStats)), std::get<16>(racialStats));

		m_middleAge = std::get<18>(racialStats);
		m_oldAge = std::get<19>(racialStats);
		m_venerableAge = std::get<20>(racialStats);
		m_limitAge = std::get<21>(racialStats);
	}
	
	{
		auto r = templates::metadata::get_instance().fetch<std::string, std::string, short>(metadata::query::race_info);
		auto info = r.filter<uint16_t>(static_cast<uint16_t>(race)).run().first();

		m_raceName = std::get<0>(info);
		m_raceAcronym = std::get<1>(info);
		m_baseMovementRate = std::get<2>(info);
	}
	
	auto res = templates::metadata::get_instance().fetch<short, short, short, short, short, short, short, short>(metadata::query::thieving_modifiers_per_race);
	auto rModif = res.filter<uint32_t>(static_cast<uint32_t>(m_race)).run();

	if (rModif.has_values())
	{
		auto thievingModifiersPerRace = rModif.first();

		m_thievingModifiers[defs::thief_ability::pick_pockets] = std::get<0>(thievingModifiersPerRace);
		m_thievingModifiers[defs::thief_ability::open_locks] = std::get<1>(thievingModifiersPerRace);
		m_thievingModifiers[defs::thief_ability::find_remove_traps] = std::get<2>(thievingModifiersPerRace);
		m_thievingModifiers[defs::thief_ability::move_silently] = std::get<3>(thievingModifiersPerRace);
		m_thievingModifiers[defs::thief_ability::hide_in_shadows] = std::get<4>(thievingModifiersPerRace);
		m_thievingModifiers[defs::thief_ability::hear_noise] = std::get<5>(thievingModifiersPerRace);
		m_thievingModifiers[defs::thief_ability::climb_walls] = std::get<6>(thievingModifiersPerRace);
		m_thievingModifiers[defs::thief_ability::read_languages] = std::get<7>(thievingModifiersPerRace);
	}
}

bool adnd::templates::character_race::check_class(const defs::character_class& cls)
{
	short levelLimit = 0;
	return check_class(cls, levelLimit);
}
bool adnd::templates::character_race::check_class(const defs::character_class& cls, short& levelLimit)
{
	if (m_levelLimits.find(cls) != m_levelLimits.end())
		levelLimit = m_levelLimits[cls];
	return m_availableClasses.find(cls) != m_availableClasses.end();
}
bool adnd::templates::character_race::check_skill(const defs::character_skill& skl, const skills::character_skill_value& sklValue)
{
	return check_skill(skl, sklValue.value());
}
bool adnd::templates::character_race::check_skill(const defs::character_skill& skl, const short& sklValue)
{
	return m_skillRequisites[skl].first <= sklValue && sklValue <= m_skillRequisites[skl].second;
}
void adnd::templates::character_race::get_skill_boundaries(const defs::character_skill& skill, short& minValue, short& maxValue)
{
	minValue = m_skillRequisites[skill].first;
	maxValue = m_skillRequisites[skill].second;
}

const short& adnd::templates::character_race::get_skill_modifier(const defs::character_skill& skill)
{
	return m_skillModifiers[skill];
}

void adnd::templates::character_race::add_available_class(const defs::character_class& cls, const short limit)
{
	m_availableClasses.emplace(cls);
	if (limit > 0)
		m_levelLimits[cls] = limit;
}

void adnd::templates::character_race::set_skill_requisites(const defs::character_skill& skill, const short minValue, const short maxValue)
{
	auto req = std::pair<short, short>(minValue, maxValue);
	m_skillRequisites[skill] = req;
}

void adnd::templates::character_race::set_skill_modifier(const defs::character_skill& skill, const short modifier)
{
	m_skillModifiers[skill] = modifier;
}

std::map<adnd::defs::character_class, short> adnd::templates::character_race::get_level_limit(const defs::character_class& cls, const skills::character_skill_set& values)
{
	std::map<adnd::defs::character_class, short> bonuses;

	/*
		Table of additional bonus levels by primary skill value
		| SCORE		| LEVEL	|
		|-----------|-------|
		| 1  - 13	| 0		|
		| 14 - 15	| 1		|
		| 16 - 17	| 2		|
		| 18		| 3		|
		| 19+		| 4		|
	*/
	std::map<short, short> addLevels;
	if (configuration::get_instance().get(defs::config::optional::exceeding_maximum_level_limit_for_demihumans))
	{
		auto rBonus = metadata::get_instance().fetch<short, short>(metadata::query::prime_requisite_bonuses).run();
		for (auto b : rBonus)
			addLevels[std::get<0>(b)] = std::get<1>(b);
	}

	auto clsTempl = metadata::get_instance().get_class_template(cls);
	auto cc = clsTempl.get_class_composition();
	for (auto c : cc.classes())
	{
		auto p = metadata::get_character_type_and_id(c);
		auto res = templates::metadata::get_instance().fetch<short>(templates::metadata::query::prime_requisite_per_class);
		auto primeRequisites = res.filter<uint16_t>(p.classType).filter<uint32_t>(p.classId).run();

		short maxBonus = 0;
		for (auto req : primeRequisites)
		{
			auto sklVal = values.skill_value(static_cast<defs::character_skill>(std::get<0>(req)));
			short bonus = 0;
			if (configuration::get_instance().get(defs::config::optional::exceeding_maximum_level_limit_for_demihumans))
			{
				bonus = addLevels[sklVal];
			}
			maxBonus = std::max<short>(bonus, maxBonus);
		}

		bonuses[c] = m_levelLimits[c] + maxBonus;
	}

	return bonuses;
}

std::list<adnd::defs::character_class> adnd::templates::character_race::get_available_classes(skills::character_skill_set& skills)
{
	return get_available_classes(skills.strength(), skills.dexterity(), skills.constitution(), skills.intelligence(), skills.wisdom(), skills.charisma());
}

std::list<adnd::defs::character_class> adnd::templates::character_race::get_available_classes(const skills::character_skill_value& strength, const skills::character_skill_value& dexterity, const skills::character_skill_value& constitution, const skills::character_skill_value& intelligence, const skills::character_skill_value& wisdom, const skills::character_skill_value& charisma)
{
	std::list<adnd::defs::character_class> result;
	std::set<defs::character_class> classChecked;

	auto availableClasses = templates::metadata::get_instance().fetch<uint32_t, short>(metadata::query::classes_per_race).filter<uint16_t>(static_cast<uint16_t>(m_race)).run();
	
	for (auto& cls : availableClasses)
	{
		defs::character_class clsId = static_cast<defs::character_class>(std::get<0>(cls));

		auto t = metadata::get_instance().get_class_template(clsId);
		auto cc = t.get_class_composition();
	
		if (!cc.is_multiclass())
		{
			auto templ = metadata::get_instance().get_class_template(clsId);
	
			if (templ.check_skill(defs::character_skill::strength, strength) && templ.check_skill(defs::character_skill::dexterity, dexterity)
				&& templ.check_skill(defs::character_skill::constitution, constitution) && templ.check_skill(defs::character_skill::intelligence, intelligence)
				&& templ.check_skill(defs::character_skill::wisdom, wisdom) && templ.check_skill(defs::character_skill::charisma, charisma))
			{
				classChecked.emplace(clsId);
				result.push_back(clsId);
			}
		}
		else
		{
			size_t count = 0;
			for (auto c : cc.classes())
			{
				if (classChecked.find(c) == classChecked.end())
					break;
				++count;
			}
			if (count == cc.classes().size())
				// Allows for multiclasses iif all the basic classes are allowed
				result.push_back(clsId);
		}
	}

	return result;
}

const short& adnd::templates::character_race::height_base(const defs::character_sex& sex)
{
	return (sex == defs::character_sex::male) ? m_heightBaseMale : m_heightBaseFemale;
}

const short& adnd::templates::character_race::weight_base(const defs::character_sex& sex)
{
	return (sex == defs::character_sex::male) ? m_weightBaseMale : m_weightBaseFemale;
}

const short& adnd::templates::character_race::get_thieving_modifiers_per_race(const adnd::defs::thief_ability& abil)
{
	return m_thievingModifiers[abil];
}

short adnd::templates::character_race::get_movement_rate(const skills::character_skill_value & strengthValue, const double & load)
{
	auto t = adnd::templates::metadata::get_instance().get_skill_template(defs::character_skill::strength);
	auto modif = t.get_modified_movement_rates(strengthValue, load);
	
	return (m_baseMovementRate == 6) ? modif.movementRate1 : modif.movementRate2;
}

adnd::templates::metadata& adnd::templates::metadata::get_instance()
{
	static metadata _instance;

	if (!m_initialised)
	{
		_instance.create();
		m_initialised = true;

		auto classes = metadata::get_instance().fetch<uint32_t>(metadata::query::classes).run();
		for (auto c : classes)
		{
			auto clsId = static_cast<defs::character_class>(std::get<0>(c));
			_instance.m_classTemplates[clsId] = templates::character_class(clsId);
			_instance.m_classTemplates[clsId].init();
		}
		
		for (auto skl : { defs::character_skill::strength, defs::character_skill::dexterity,
			defs::character_skill::constitution, defs::character_skill::intelligence,
			defs::character_skill::wisdom, defs::character_skill::charisma })
		{
			_instance.m_skillTemplates[skl] = templates::skill(skl);
		}

		auto treasureClasses = _instance.fetch<short, char, short>(query::treasure_classes).run();
		for (auto tc : treasureClasses)
		{
			auto cls = static_cast<defs::treasure::treasure_class>(std::get<0>(tc));
			auto loc = static_cast<defs::treasure::location>(std::get<2>(tc));
			_instance.m_treasureTemplates[cls] = templates::treasure(cls, std::get<1>(tc), loc);
		}

		auto aligns = _instance.fetch<short>(query::moral_alignments).run();
		for (auto alg : aligns)
		{
			auto a = static_cast<defs::moral_alignment>(std::get<0>(alg));
			_instance.m_moralAlignmentTemplates[a] = templates::moral_alignment(a);
		}

		auto gems = _instance.fetch<short>(query::gems).run();
		for (auto g : gems)
		{
			defs::treasure::gems gemId = static_cast<defs::treasure::gems>(std::get<0>(g));
			_instance.m_gemTemplates[gemId] = templates::gem(gemId);
		}

		auto magicals = _instance.fetch<short>(query::magical_items).run();
		for (auto m : magicals)
		{
			auto magicalId = static_cast<defs::magical::item>(std::get<0>(m));
			_instance.m_magicalTemplates[magicalId] = templates::magical_item(magicalId);
		}

		_instance.load_race_templates();
		_instance.load_coin_templates();

		_instance.load_equipment_templates();
		//_instance.load_modified_movement_rates();
		_instance.load_deity_templates();

		auto wizardSpells = _instance.fetch<short, std::string, short>(query::wizard_spells).run();
		for (auto ws : wizardSpells)
		{
			auto spellId = static_cast<defs::spells::wizard_spell>(std::get<0>(ws));
			_instance.m_wizardSpellTemplates[spellId] = templates::wizard_spell(spellId);
		}
		
		auto priestSpells = _instance.fetch<short, std::string, short>(query::priest_spells).run();
		for (auto ps : priestSpells)
		{
			auto spellId = static_cast<defs::spells::priest_spell>(std::get<0>(ps));
			_instance.m_priestSpellTemplates[spellId] = templates::priest_spell(spellId);
		}

		auto schoolsOfMagic = _instance.fetch<short>(query::schools_of_magic).run();
		for (auto s : schoolsOfMagic)
		{
			auto id = static_cast<defs::spells::school>(std::get<0>(s));
			_instance.m_schoolOfMagicTemplates[id] = templates::school_of_magic(id);
		}
		
		auto spheresOfInfluence = _instance.fetch<short>(query::spheres_of_influence).run();
		for (auto s : spheresOfInfluence)
		{
			auto id = static_cast<defs::spells::sphere>(std::get<0>(s));
			_instance.m_spheresOfInfluenceTemplates[id] = templates::sphere_of_influence(id);
		}

		auto monsters = _instance.fetch<short>(query::monsters).run();
		for (auto m : monsters)
		{
			auto id = static_cast<defs::monsters::monster>(std::get<0>(m));
			_instance.m_monsterTemplates[id] = templates::monster(id);
		}
	}
	return _instance;
}

void adnd::templates::metadata::create()
{
	std::string path = adnd::configuration::get_instance().get_database_path();

	m_db.db_path() = path;
	m_db.db_source() = adnd::configuration::get_instance().get_database_source();

	m_db.open();
	auto stmtSelectTables = m_db.prepare_statement("SELECT [NAME], [TYPE] FROM SQLITE_MASTER WHERE TYPE IN ('table', 'view') AND NAME NOT LIKE 'sqlite_%'");
	adnd::templates::metadata::query_manager::iterator it, end;
	std::list<std::string> dropList;
	m_db.execute(stmtSelectTables, it, end);
	while (it != end)
	{
		auto objectName = it.data<std::string>(1, "");
		auto objectType = it.data<std::string>(2, "");
		std::stringstream ss;
		ss << "drop " << objectType << " " << objectName << ";";
		dropList.push_back(ss.str());
		++it;
	}
	for (auto dropStmt : dropList)
		m_db.execute(dropStmt.c_str());

	m_db.add_pragma("synchronous", "NORMAL");
	m_db.add_pragma("journal_mode", "WAL");
	m_db.add_pragma("temp_store", "MEMORY");
	m_db.add_pragma("page_size", "65536");
	m_db.add_pragma("cache_size", "10000");

	m_db.execute_script(std::experimental::filesystem::path(adnd::configuration::get_instance().get_definition_script_path()));
	m_db.execute_script(std::experimental::filesystem::path(adnd::configuration::get_instance().get_init_script_path()));

	auto stmtSkillInfo = m_db.prepare_statement("SELECT ID, NAME FROM SKILL WHERE ID = ?;", query::skill_info);
	auto stmtClasses = m_db.prepare_statement("SELECT CLASS_ID FROM V_CLASSES;", query::classes);

	auto stmtSavingsGet = m_db.prepare_statement("SELECT LEVEL, SCORE_0, SCORE_1, SCORE_2, SCORE_3, SCORE_4 FROM V_SAVING_ROLLS WHERE CLASS_TYPE_ID = ?;", query::saving_throws);

	auto stmtTurnUndead = m_db.prepare_statement("SELECT LEVEL, SKELETON_HD1, ZOMBIE, GHOUL_HD2, SHADOW_HD3_4, WIGHT_HD5, GHAST, WRAITH_HD6, MUMMY_HD7, SPECTRE_HD8, VAMPIRE_HD9, GHOST_HD10, LICH_HD11, SPECIAL FROM V_TURN_UNDEAD_TABLE;", query::turn_undead);

	auto stmtClassInfo = m_db.prepare_statement("SELECT C.ID, C.CLASS_TYPE_ID, C.CLASS_ID, C.LONG_NAME, C.SHORT_NAME, C.ACRONYM, CASE WHEN C.CLASS_TYPE_ID - 128 > 0 THEN 'Y' ELSE 'N' END AS MULTICLASS, IFNULL(S.ID, -1) AS SCHOOL "
					"FROM CHARACTER_CLASS C LEFT JOIN SCHOOL_OF_MAGIC S ON S.CLASS_ID = C.ID WHERE C.CLASS_ID = ?;", query::class_info);

	auto stmtClassTypeInfo = m_db.prepare_statement("SELECT DESCRIPTION, TITLE_LEVEL, TITLE, HIT_DICE, HP_AFTER_TITLE FROM CHARACTER_CLASS_TYPE WHERE ID = ?;", query::class_type_info);

	auto stmtSkillLimitsPerRace = m_db.prepare_statement("SELECT MAX(S.MIN_VALUE) AS MIN_VALUE, MIN(S.MAX_VALUE) AS MAX_VALUE FROM ( "
					"SELECT SR.SKILL_ID, SR.[MIN] AS MIN_VALUE, SR.[MAX] AS MAX_VALUE FROM RACE R "
					"INNER JOIN SKILL_REQUISITE_VALUES SR ON SR.OBJECT = R.ID AND SR.REQUISITE_ID = 3 "
					"WHERE R.ID = ? "
					"UNION ALL SELECT SKILL_ID, [MIN], [MAX] FROM SKILL_REQUISITE_VALUES WHERE REQUISITE_ID = 0 "
					") AS S "
					"WHERE S.SKILL_ID = ?;", query::skill_limits_per_race);

	auto stmtSkillLimitsPerClass = m_db.prepare_statement("SELECT MIN_VALUE, MAX_VALUE FROM V_SKILL_LIMITS_PER_CLASS WHERE CLASS_TYPE_ID = ? AND CLASS_ID = ? AND SKILL_ID = ?", query::skill_limits_per_class);
	

	auto stmtClassesPerRace = m_db.prepare_statement("SELECT CLASS_ID, LEVEL_LIMIT FROM V_CLASSES_PER_RACE WHERE ID = ?;", query::classes_per_race);

	auto stmtPrimeRequisitePerClass = m_db.prepare_statement("SELECT P.SKILL_ID FROM PRIMARY_SKILL P INNER JOIN CHARACTER_CLASS C ON C.ID = P.CLASS_ID AND C.CLASS_TYPE_ID = ? AND C.CLASS_ID = ?;", query::prime_requisite_per_class);

	auto stmtHitDiceInfo = m_db.prepare_statement("SELECT HIT_DICE FROM CHARACTER_CLASS_TYPE WHERE ID = ?;", query::hit_dice_info);

	auto stmtLevelAdvancement = m_db.prepare_statement("SELECT L.LEVEL, L.SCORE FROM CHARACTER_CLASS C INNER JOIN LEVEL_ADVANCEMENT L ON L.CLASS_ID = C.ID WHERE C.CLASS_TYPE_ID = ? AND C.CLASS_ID = ?;",
					query::level_advancement);

	auto stmtLevelAdvancementFactor = m_db.prepare_statement("SELECT F.SCORE FROM CHARACTER_CLASS C INNER JOIN LEVEL_ADVANCEMENT_FACTOR F ON F.CLASS_ID = C.ID WHERE C.CLASS_TYPE_ID = ? AND C.CLASS_ID = ?;",
					query::level_advancement_factor);

	auto stmtAlignmentPerClass = m_db.prepare_statement("SELECT M.ALIGNMENT_ID FROM CHARACTER_CLASS C INNER JOIN MORAL_ALIGNMENT_PER_CLASS M ON M.CLASS_ID = C.ID WHERE C.CLASS_TYPE_ID = ? AND C.CLASS_ID = ?;",
					query::alignment_per_class);

	auto stmtMoralAlignment = m_db.prepare_statement("SELECT ID, NAME, ACRONYM, ORDER_ID, ORDER_NAME, ORDER_ACRONYM, VIEW_ID, VIEW_NAME, VIEW_ACRONYM FROM V_MORAL_ALIGNMENTS WHERE ID = ?;", query::moral_alignment);
	auto stmtMoralAlignments = m_db.prepare_statement("SELECT ID FROM MORAL_ALIGNMENT;", query::moral_alignments);

	auto stmtSkillModifPerRace = m_db.prepare_statement("SELECT SKILL_ID, VALUE FROM SKILL_MODIFIER WHERE RACE_ID = ?;", query::skill_modifier_per_race);

	auto stmtThiefAbilities = m_db.prepare_statement("SELECT LEVEL, PICK_POCKETS, OPEN_LOCKS, FIND_REMOVE_TRAPS, MOVE_SILENTLY, HIDE_IN_SHADOWS, HEAR_NOISE, CLIMB_WALLS, READ_LANGUAGES FROM V_THIEF_ABILITIES;", query::thief_abilities);
	auto stmtRangerAbilities = m_db.prepare_statement("SELECT LEVEL, 0 AS PICK_POCKETS, 0 AS OPEN_LOCKS, 0 AS FIND_REMOVE_TRAPS, MOVE_SILENTLY, HIDE_IN_SHADOWS, 0 AS HEAR_NOISE, 0 AS CLIMB_WALLS, 0 AS READ_LANGUAGES FROM RANGER_ABILITIES;", query::ranger_abilities);
	auto stmtBardAbilities = m_db.prepare_statement("SELECT LEVEL, PICK_POCKETS, OPEN_LOCKS, FIND_REMOVE_TRAPS, MOVE_SILENTLY, HIDE_IN_SHADOWS, HEAR_NOISE, CLIMB_WALLS, READ_LANGUAGES FROM V_BARD_ABILITIES;", query::bard_abilities);

	auto stmtThacoInfo = m_db.prepare_statement("SELECT SCORE, FACTOR FROM THACO WHERE CLASS_TYPE_ID = ?", query::thaco_info);

	auto stmtStrengthStats = m_db.prepare_statement("SELECT ABILITY_SCORE_FROM, IFNULL(EXCP_FROM, 0) AS EXCP_FROM, ABILITY_SCORE_TO, IFNULL(EXCP_TO, 0) AS EXCP_TO, HIT_MODIFIER, DAMAGE_ADJUSTMENT, "
					"WEIGHT_ALLOWANCE, MAXIMUM_PRESS, OPEN_DOORS, IFNULL(OPEN_DOORS_SPECIAL, 0) AS OPEN_DOORS_SPECIAL, BEND_BARS_LIFT_GATES FROM STRENGTH_STATS;", query::strength_stats);
	auto stmtDexterityStats = m_db.prepare_statement("SELECT ABILITY_SCORE, REACTION_ADJUSTMENT, MISSILE_ATTACK_ADJUSTMENT, DEFENSIVE_ADJUSTMENT FROM DEXTERITY_STATS;", query::dexterity_stats);
	auto stmtConstitutionStats = m_db.prepare_statement("SELECT ABILITY_SCORE, HIT_POINT_ADJUSTMENT, IFNULL(HIT_POINT_ADJUSTMENT_WARRIORS, HIT_POINT_ADJUSTMENT) AS HIT_POINT_ADJUSTMENT_WARRIORS, IFNULL(ROLLS_UPGRADE, 0) AS ROLLS_UPGRADE, SYSTEM_SHOCK, RESURRECTION_SURVIVAL, POISON_SAVE, IFNULL(REGENERATION_POINTS, 0) AS REGENERATION_POINTS, IFNULL(REGENERATION_TURNS, 0) AS REGENERATION_TURNS FROM CONSTITUTION_STATS;", query::constitution_stats);
	auto stmtIntelligenceStats = m_db.prepare_statement("SELECT ABILITY_SCORE, NUMBER_OF_LANGUAGES, IFNULL(SPELL_LEVEL, 0) AS SPELL_LEVEL, IFNULL(CHANCE_TO_LEARN_SPELL, 0) AS CHANCE_TO_LEARN_SPELL, IFNULL(MAX_NUMBER_OF_SPELLS_PER_LEVEL, 999) AS MAX_NUMBER_OF_SPELLS_PER_LEVEL, IFNULL(ILLUSION_IMMUNITY, 0) AS ILLUSION_IMMUNITY FROM INTELLIGENCE_STATS;", query::intelligence_stats);
	auto stmtWisdomStats = m_db.prepare_statement("SELECT ABILITY_SCORE, MAGICAL_DEFENCE_ADJUSTMENT, BONUS_SPELL_LEVEL_1, BONUS_SPELL_LEVEL_2, BONUS_SPELL_LEVEL_3, "
					"BONUS_SPELL_LEVEL_4, BONUS_SPELL_LEVEL_5, BONUS_SPELL_LEVEL_6, BONUS_SPELL_LEVEL_7, CHANCE_OF_SPELL_FAILURE, SPELL_IMMUNITY FROM V_WISDOM_STATS;", query::wisdom_stats);
	auto stmtCharismaStats = m_db.prepare_statement("SELECT ABILITY_SCORE, MAXIMUM_NUMBER_OF_HENCHMEN, LOYALTY_BASE, REACTION_ADJUSTMENT FROM CHARISMA_STATS;", query::charisma_stats);

	auto stmtRacialStats = m_db.prepare_statement("SELECT HEIGHT_BASE_MALE, HEIGHT_BASE_FEMALE, HEIGHT_DICE_NUMBER, HEIGHT_DICE_FACES, HEIGHT_DICE_NUMBER_2, "
					"HEIGHT_DICE_FACES_2, WEIGHT_BASE_MALE, WEIGHT_BASE_FEMALE, WEIGHT_DICE_NUMBER, WEIGHT_DICE_FACES, WEIGHT_DICE_NUMBER_2, WEIGHT_DICE_FACES_2, AGE_STARTING, AGE_DICE_NUMBER, AGE_DICE_FACES, "
					"AGE_MAXIMUM, AGE_MAXIMUM_DICE_NUMBER, AGE_MAXIMUM_DICE_FACES, MIDDLE_AGE, OLD_AGE, VENERABLE_AGE, LIMIT_AGE FROM V_RACIAL_STATS WHERE RACE_ID = ?;", query::racial_stats);

	auto stmtThievingModifiersPerDexterity = m_db.prepare_statement("SELECT DEXTERITY, SCORE_0, SCORE_1, SCORE_2, SCORE_3, SCORE_4, SCORE_5, SCORE_6, SCORE_7 FROM V_THIEVING_MODIFIERS_PER_DEXTERITY;", query::thieving_modifiers_per_dexterity);
	auto stmtThievingModifiersPerRace = m_db.prepare_statement("SELECT SCORE_0, SCORE_1, SCORE_2, SCORE_3, SCORE_4, SCORE_5, SCORE_6, SCORE_7 FROM V_THIEVING_MODIFIERS_PER_RACE WHERE RACE_ID = ?;", query::thieving_modifiers_per_race);

	auto stmtPriestSpellProgression = m_db.prepare_statement("SELECT LEVEL, CASTING_LEVEL, SPELL_LEVEL_1, SPELL_LEVEL_2, SPELL_LEVEL_3, SPELL_LEVEL_4, SPELL_LEVEL_5, SPELL_LEVEL_6, SPELL_LEVEL_7 FROM V_PRIEST_SPELL_PROGRESSION;", query::priest_spell_progression);
	auto stmtPaladinSpellProgression = m_db.prepare_statement("SELECT LEVEL, CASTING_LEVEL, SPELL_LEVEL_1, SPELL_LEVEL_2, SPELL_LEVEL_3, SPELL_LEVEL_4, SPELL_LEVEL_5, SPELL_LEVEL_6, SPELL_LEVEL_7 FROM V_PALADIN_SPELL_PROGRESSION;", query::paladin_spell_progression);
	auto stmtRangerSpellProgression = m_db.prepare_statement("SELECT LEVEL,CASTING_LEVEL, SPELL_LEVEL_1, SPELL_LEVEL_2, SPELL_LEVEL_3, SPELL_LEVEL_4, SPELL_LEVEL_5, SPELL_LEVEL_6, SPELL_LEVEL_7 FROM V_RANGER_SPELL_PROGRESSION;", query::ranger_spell_progression);

	auto stmtWizardSpellProgression = m_db.prepare_statement("SELECT LEVEL, SPELL_LEVEL_1, SPELL_LEVEL_2, SPELL_LEVEL_3, SPELL_LEVEL_4, SPELL_LEVEL_5, SPELL_LEVEL_6, SPELL_LEVEL_7, SPELL_LEVEL_8, SPELL_LEVEL_9 FROM V_WIZARD_SPELL_PROGRESSION;", query::wizard_spell_progression);
	auto stmtBardSpellProgression = m_db.prepare_statement("SELECT LEVEL, SPELL_LEVEL_1, SPELL_LEVEL_2, SPELL_LEVEL_3, SPELL_LEVEL_4, SPELL_LEVEL_5, SPELL_LEVEL_6, SPELL_LEVEL_7, SPELL_LEVEL_8, SPELL_LEVEL_9 FROM V_BARD_SPELL_PROGRESSION;", query::bard_spell_progression);

	auto stmtRaceInfo = m_db.prepare_statement("SELECT NAME, ACRONYM, BASE_MOVEMENT_RATE FROM RACE WHERE ID = ?;", query::race_info);
	auto stmtExchangeRates = m_db.prepare_statement("SELECT COIN_FROM, COIN_TO, EXCHANGE_VALUE FROM COIN_EXCHANGE_VALUES;", query::exchange_rates);
	//auto stmtExchangeRate = m_db.prepare_statement("SELECT EXCHANGE_VALUE FROM COIN_EXCHANGE_VALUES WHERE COIN_FROM = ? AND COIN_TO = ?;", _stmt::exchange_rate);
	auto stmtCoinInfo = m_db.prepare_statement("SELECT ID, ACRONYM, DESCRIPTION FROM COIN WHERE ID = ?;", query::coin_info);
	auto stmtStartingMoneyInfo = m_db.prepare_statement("SELECT CLASS_TYPE_ID, DIE_NUMBER, DIE_FACES, DIE_BASE, MULTIPLIER FROM STARTING_MONEY WHERE CLASS_TYPE_ID = ?;", query::starting_money_info);
	auto stmtAttacksRate = m_db.prepare_statement("SELECT LEVEL_FROM, ATTACKS, ROUNDS FROM WARRIOR_MELEE_ATTACKS;", query::melee_attacks);
	auto stmtAgeingModifiers = m_db.prepare_statement("SELECT A.ID AS ID, SUM(B.STRENGTH_MODIFIER) AS STRENGTH_MODIFIER, SUM(B.DEXTERITY_MODIFIER) AS DEXTERITY_MODIFIER, "
					"SUM(B.CONSTITUTION_MODIFIER) AS CONSTITUTION_MODIFIER, SUM(B.INTELLIGENCE_MODIFIER) AS INTELLIGENCE_MODIFIER, SUM(B.WISDOM_MODIFIER) AS WISDOM_MODIFIER, SUM(B.CHARISMA_MODIFIER) AS CHARISMA_MODIFIER "
					"FROM AGEING_EFFECTS A INNER JOIN AGEING_EFFECTS B WHERE B.ID <= A.ID GROUP BY A.ID HAVING A.ID = ?", query::ageing_modifiers);
	
	auto stmtPrimeRequisitesBonuses = m_db.prepare_statement("SELECT ABILITY_SCORE, ADDITIONAL_LEVELS FROM PRIME_REQUISITE_BONUSES;", query::prime_requisite_bonuses);

	auto stmtItemsInfo = m_db.prepare_statement("SELECT ID, TYPE FROM V_EQUIPMENT;", query::items_info);
	auto stmtItemInfo = m_db.prepare_statement("SELECT ID, NAME, TYPE, HAND_REQUIRED, COST_COIN, COST_MIN, COST_MAX, WEIGHT FROM V_EQUIPMENT WHERE ID = ?;", query::item_info);

	auto stmtClothingInfo = m_db.prepare_statement("SELECT E.ID, E.NAME, E.TYPE, E.HAND_REQUIRED, E.COST_COIN, E.COST_MIN, E.COST_MAX, E.WEIGHT, C.BODY_SLOT "
		"FROM V_EQUIPMENT E INNER JOIN CLOTHING C ON C.EQUIPMENT_ID = E.ID WHERE E.ID = ?;", query::clothing_info);
	auto stmtArmourInfo = m_db.prepare_statement("SELECT E.ID, E.NAME, E.TYPE, E.HAND_REQUIRED, E.COST_COIN, E.COST_MIN, E.COST_MAX, E.WEIGHT, "
					"A.BODY_SLOT, A.AC_BONUS_MELEE, A.AC_BONUS_MISSILE, IFNULL(A.NUMBER_OF_ATTACKS_PROTECTION, 0) AS NUMBER_OF_ATTACKS_PROTECTION, A.PROTECTION_SIDE, A.ALLOWS_WEAPON "
					"FROM V_EQUIPMENT E INNER JOIN ARMOUR A ON A.EQUIPMENT_ID = E.ID WHERE E.ID = ?;", query::armour_info);
	auto stmtProjectileInfo = m_db.prepare_statement("SELECT E.ID, E.NAME, E.TYPE, E.HAND_REQUIRED, E.COST_COIN, E.COST_MIN, E.COST_MAX, E.WEIGHT, "
					"W.WEAPON_SIZE, W.WEAPON_TYPE, W.TARGET_SIZE, IFNULL(W.SPEED_FACTOR, 0), IFNULL(W.DAMAGE_DICE_NUMBER, 0), IFNULL(W.DAMAGE_DIE_FACES, 1), IFNULL(W.DAMAGE_MODIFIER, 0) "
					"FROM V_EQUIPMENT E INNER JOIN WEAPON W ON W.EQUIPMENT_ID = E.ID WHERE E.TYPE = 10 AND E.ID = ?;", query::projectile_info);
	auto stmtWeaponInfo = m_db.prepare_statement("SELECT E.ID, E.NAME, E.TYPE, E.HAND_REQUIRED, E.COST_COIN, E.COST_MIN, E.COST_MAX, E.WEIGHT, "
					"W.WEAPON_SIZE, W.WEAPON_TYPE, W.TARGET_SIZE, W.SPEED_FACTOR, IFNULL(W.DAMAGE_DICE_NUMBER, 0), IFNULL(W.DAMAGE_DIE_FACES, 1), IFNULL(W.DAMAGE_MODIFIER, 0), "
					"E.ATTACK_TYPE, E.WEAPON_GROUP "
					"FROM V_EQUIPMENT E INNER JOIN WEAPON W ON W.EQUIPMENT_ID = E.ID WHERE E.TYPE = 9 AND E.ID = ?;", query::weapon_info);
	auto stmtWeaponProjectileInfo = m_db.prepare_statement("SELECT WP.WEAPON_ID, WP.PROJECTILE_ID FROM WEAPON_PROJECTILE WP WHERE WP.WEAPON_ID = ?;", query::weapon_projectile_info);

	auto stmtItemsAllowed = m_db.prepare_statement("SELECT EQUIPMENT_ID FROM ITEM_CAPABILITIES_PER_CHARACTER_CLASS WHERE CLASS_TYPE_ID = ? OR CLASS_ID = ? ORDER BY EQUIPMENT_ID;", query::items_allowed);
	auto stmtItemCapabilitiesPerCharacterClass = m_db.prepare_statement("SELECT CLASS_ID FROM V_ITEM_CAPABILITIES_PER_CHARACTER_CLASS WHERE EQUIPMENT_ID = ?;", query::item_capabilities_per_character_class);

	auto stmtModifiedMovementRates = m_db.prepare_statement("SELECT STRENGTH_FROM, IFNULL(EXC_STRENGTH_FROM, 0) AS EXC_STRENGTH_FROM, STRENGTH_TO, IFNULL(EXC_STRENGTH_TO, 0) AS EXC_STRENGTH_TO, "
					"LOAD, MOVEMENT_RATE_1, MOVEMENT_RATE_2 FROM MODIFIED_MOVEMENT_RATES;", query::modified_movement_rates);

	auto stmtMissileRangeModifier = m_db.prepare_statement("SELECT IFNULL(PROJECTILE_ID, -1) AS PROJECTILE_ID, RATE_OF_FIRE_NUM_ATTACKS, RATE_OF_FIRE_NUM_ROUND, "
					"IFNULL(SHORT_DISTANCE_FROM, 0) AS SHORT_DISTANCE_FROM, SHORT_DISTANCE_TO, IFNULL(MEDIUM_DISTANCE_FROM, SHORT_DISTANCE_TO) AS MEDIUM_DISTANCE_FROM, "
					"MEDIUM_DISTANCE_TO, IFNULL(LONG_DISTANCE_FROM, MEDIUM_DISTANCE_TO) AS LONG_DISTANCE_FROM, LONG_DISTANCE_TO, SHORT_DISTANCE_MODIFIER, MEDIUM_DISTANCE_MODIFIER, LONG_DISTANCE_MODIFIER "
					"FROM MISSILE_WEAPON_RANGE WHERE WEAPON_ID = ?;", query::missile_range_modifiers);

	auto stmtDeities = m_db.prepare_statement("SELECT ID FROM DEITY;", query::deities);
	auto stmtDeityInfo = m_db.prepare_statement("SELECT D.ID, D.NAME, D.RANK, D.MORAL_ALIGNMENT, R.PLANE_ID AS PLANE_OF_EXISTENCE, R.ID AS REALM FROM DEITY D "
					"INNER JOIN REALM R ON R.ID = D.REALM WHERE D.ID = ?;", query::deity_info);
	auto stmtDeityWorshippers = m_db.prepare_statement("SELECT WA.MORAL_ALIGNMENT AS WORSHIPPER_ALIGNMENT FROM DEITY D INNER JOIN WORSHIPPER_ALIGNMENT WA ON WA.DEITY_ID = D.ID WHERE D.ID = ?;", query::deity_worshippers_info);
	auto stmtSpheresPerDeity = m_db.prepare_statement("SELECT SPHERE_ID, IFNULL(ACCESS_LEVEL, 0) AS ACCESS_LEVEL, IFNULL(MODE, 0) AS MODE FROM DEITY_SPHERE_OF_INFLUENCE WHERE DEITY_ID = ?;", query::spheres_per_deity);

	auto stmtProficiencySlots = m_db.prepare_statement("SELECT WEAPON_INITIAL_SCORE, WEAPON_NUM_LEVELS, PENALTY, NON_WEAPON_INITIAL_SCORE, NON_WEAPON_NUM_LEVELS FROM PROFICIENCY_SLOTS WHERE CLASS_TYPE_ID = ?;", query::proficiency_slots);
	auto stmtSpecialistAttacksPerRound = m_db.prepare_statement("SELECT LEVEL, MELEE_WEAPON_ATTACKS, MELEE_WEAPON_ROUND, LIGHT_X_BOW_ATTACKS, LIGHT_X_BOW_ROUND, HEAVY_X_BOW_ATTACKS, HEAVY_X_BOW_ROUND, THROWN_DAGGER_ATTACKS, THROWN_DAGGER_ROUND, THROWN_DART_ATTACKS, THROWN_DART_ROUND, OTHER_MISSILE_ATTACKS, OTHER_MISSILE_ROUND FROM SPECIALIST_ATTACKS_PER_ROUND ORDER BY LEVEL;", query::specialist_attacks_per_round);

	auto stmtTreasureComposition = m_db.prepare_statement("SELECT CLASS, COMPONENT, IFNULL(COUNT_FROM, 0) AS COUNT_FROM, IFNULL(COUNT_TO, 0) AS COUNT_TO, IFNULL(PROBABILITY, 0) AS PROBABILITY, IFNULL(NATURE, 0) AS NATURE, IFNULL(ADDITIONAL_COMPONENT, -1) AS ADDITIONAL_COMPONENT, IFNULL(ADDITIONAL_COUNT, 0) AS ADDITIONAL_COUNT FROM TREASURE_COMPOSITION WHERE CLASS = ?;", query::treasure_composition);
	auto stmtTreasureClasses = m_db.prepare_statement("SELECT ID, ACRONYM, LOCATION FROM TREASURE_CLASS;", query::treasure_classes);
	auto stmtTreasureComponentInfo = m_db.prepare_statement("SELECT ID, NAME FROM TREASURE_COMPONENT WHERE ID = ?;", query::treasure_component);
	auto stmtTreasureLocationInfo = m_db.prepare_statement("SELECT ID, NAME FROM TREASURE_LOCATION WHERE ID = ?;", query::treasure_location);
	auto stmtTreasureNatureInfo = m_db.prepare_statement("SELECT ID, NAME FROM TREASURE_NATURE WHERE ID = ?;", query::treasure_nature);

	auto stmtGems = m_db.prepare_statement("SELECT ID FROM GEMS;", query::gems);
	auto stmtGemsByType = m_db.prepare_statement("SELECT ID FROM GEMS WHERE TYPE = ?;", query::gems_by_type);
	auto stmtGemInfo = m_db.prepare_statement("SELECT ID, NAME, TYPE, TYPE_NAME, COIN_ID, VALUE_FROM, VALUE_TO FROM V_GEMS_INFO WHERE ID = ?;", query::gem_info);
	auto stmtGemTypes = m_db.prepare_statement("SELECT ID, NAME, BASE_COIN, BASE_VALUE, PROBABILITY_FROM, PROBABILITY_TO FROM GEM_TYPE;", query::gem_types);
	auto stmtObjectsOfArt = m_db.prepare_statement("SELECT PROBABILITY_FROM, PROBABILITY_TO, VALUE_COIN, VALUE_FROM, VALUE_TO FROM OBJECT_OF_ART;", query::objects_of_art);

	auto stmtMagicalItems = m_db.prepare_statement("SELECT ID FROM MAGICAL_ITEM;", query::magical_items);
	auto stmtMagicalItemInfo = m_db.prepare_statement("SELECT ID, NAME, TYPE, HAND_REQUIRED, ATTACK_TYPE, WEAPON_GROUP, UNIDENTIFIED_NAME, BODY_SLOT, EQUIPMENT_ID, ITEM_TYPE FROM V_MAGICAL_ITEM WHERE ID = ?;", query::magical_item_info);

	auto stmtWizardSpells = m_db.prepare_statement("SELECT ID, NAME, LEVEL FROM WIZARD_SPELL;", query::wizard_spells);
	auto stmtPriestSpells = m_db.prepare_statement("SELECT ID, NAME, LEVEL FROM PRIEST_SPELL;", query::priest_spells);
	auto stmtWizardSpellInfo = m_db.prepare_statement("SELECT ID, NAME, LEVEL FROM WIZARD_SPELL WHERE ID = ?;", query::wizard_spell_info);
	auto stmtPriestSpellInfo = m_db.prepare_statement("SELECT ID, NAME, LEVEL FROM PRIEST_SPELL WHERE ID = ?;", query::priest_spell_info);

	auto stmtWizardSpellSchools = m_db.prepare_statement("SELECT SCHOOL_ID FROM WIZARD_SPELL_SCHOOL WHERE SPELL_ID = ?;", query::wizard_spell_schools);
	auto stmtPriestSpellSpheres = m_db.prepare_statement("SELECT SPHERE_ID FROM PRIEST_SPELL_SPHERE WHERE SPELL_ID = ?;", query::priest_spell_spheres);

	auto stmtSchools = m_db.prepare_statement("SELECT ID FROM SCHOOL_OF_MAGIC;", query::schools_of_magic);
	auto stmtSchoolInfo = m_db.prepare_statement("SELECT ID, NAME, IFNULL(ALIAS, '') AS ALIAS, CLASS_ID, SKILL_ID, SKILL_VALUE_REQUIRED FROM SCHOOL_OF_MAGIC WHERE ID = ?;", query::school_of_magic_info);
	auto stmtSpheres = m_db.prepare_statement("SELECT ID FROM SPHERES_OF_INFLUENCE;", query::spheres_of_influence);
	auto stmtSphereInfo = m_db.prepare_statement("SELECT ID, NAME FROM SPHERES_OF_INFLUENCE WHERE ID = ?;", query::sphere_of_influence_info);
	auto stmtSchoolOpposites = m_db.prepare_statement("SELECT OPPOSITE_ID FROM SCHOOL_OF_MAGIC_OPPOSITION WHERE SCHOOL_ID = ?;", query::school_of_magic_opposites);

	auto stmtSchoolsPerClass = m_db.prepare_statement("SELECT A.SCHOOL_ID AS ID FROM CHARACTER_CLASS C INNER JOIN SCHOOL_OF_MAGIC_ACCESS A ON A.CLASS_ID = C.ID WHERE C.CLASS_TYPE_ID = ? AND C.CLASS_ID = ?;", query::schools_per_class);
	auto stmtOppositeSchoolsPerClass = m_db.prepare_statement("SELECT O.OPPOSITE_ID AS ID FROM CHARACTER_CLASS C INNER JOIN SCHOOL_OF_MAGIC_ACCESS A ON A.CLASS_ID = C.ID INNER JOIN SCHOOL_OF_MAGIC_OPPOSITION O ON O.SCHOOL_ID = A.SCHOOL_ID WHERE C.CLASS_TYPE_ID = ? AND C.CLASS_ID = ?;", query::opposite_schools_per_class);

	auto stmtPriestSpellPerDeityAndLevel = m_db.prepare_statement("SELECT PS.ID FROM DEITY D "
							"INNER JOIN DEITY_SPHERE_OF_INFLUENCE S ON S.DEITY_ID = D.ID "
							"INNER JOIN PRIEST_SPELL_SPHERE PSS ON PSS.SPHERE_ID = S.SPHERE_ID "
							"INNER JOIN PRIEST_SPELL PS ON PS.ID = PSS.SPELL_ID "
							"WHERE D.ID = ? AND PS.LEVEL = ? "
							"GROUP BY PS.ID;", query::priest_spells_per_deity_and_level);
	auto stmtPriestSpellPerLevel = m_db.prepare_statement("SELECT SPELL_ID, ACCESS_LEVEL, ACCESS_MODE FROM SPHERE_ACCESS_PER_CLASS A "
							"INNER JOIN PRIEST_SPELL_SPHERE S ON S.SPHERE_ID = A.SPHERE_ID "
							"INNER JOIN PRIEST_SPELL P ON P.ID = S.SPELL_ID AND P.LEVEL = ? "
							"WHERE A.CLASS_ID = ? "
							"GROUP BY SPELL_ID, ACCESS_LEVEL, ACCESS_MODE;", query::priest_spells_per_level_and_class);
	auto stmtWizardSpellPerLevel = m_db.prepare_statement("SELECT WS.ID AS ID FROM WIZARD_SPELL WS INNER JOIN WIZARD_SPELL_SCHOOL WSS ON WSS.SPELL_ID = WS.ID AND WSS.SCHOOL_ID = ? WHERE WS.LEVEL = ?;", query::wizard_spells_per_school_and_level);

	auto stmtMonsters = m_db.prepare_statement("SELECT ID FROM MONSTER;", query::monsters);
	auto stmtMonsterInfo = m_db.prepare_statement("SELECT ID, NAME, UNDEAD, ELEMENTAL, DRAGON, TURNED_AS, ELEMENT_ID, DRAGON_TYPE, "
							"CLIMATE, TERRAIN, FREQUENCY, ORGANISATION, ACTIVITY_CYCLE, DIET, INTELLIGENCE, ALIGNMENT, MORALE, MORALE_FROM, MORALE_TO "
							"FROM V_MONSTER WHERE ID = ?;", query::monster_info);
}

const adnd::templates::character_class& adnd::templates::metadata::get_class_template(const defs::character_class& cls)
{
	if (m_classTemplates.find(cls) == m_classTemplates.end())
		throw std::exception("Invalid character class specified");
	return m_classTemplates[cls];
}

const adnd::templates::character_race& adnd::templates::metadata::get_race_template(const defs::character_race& race)
{
	if (m_raceTemplates.find(race) == m_raceTemplates.end())
		throw std::exception("Invalid character race specified");
	return m_raceTemplates[race];
}

const adnd::templates::item* adnd::templates::metadata::get_item_template(const defs::equipment::item& itemId)
{
	return m_itemTemplates[itemId];
}

const adnd::templates::clothing* adnd::templates::metadata::get_clothing_template(const defs::equipment::item& itemId)
{
	if (m_itemTemplates[itemId]->item_type() != defs::item_type::clothing)
		throw std::exception("Invalid clothing identifier", static_cast<int>(itemId));

	return static_cast<adnd::templates::clothing*>(m_itemTemplates[itemId]);
}

const adnd::templates::armour* adnd::templates::metadata::get_armour_template(const defs::equipment::item& itemId)
{
	if (m_itemTemplates[itemId]->item_type() != defs::item_type::armour)
		throw std::exception("Invalid armour identifier", static_cast<int>(itemId));

	return static_cast<adnd::templates::armour*>(m_itemTemplates[itemId]);
}

const adnd::templates::weapon* adnd::templates::metadata::get_weapon_template(const defs::equipment::item& itemId)
{
	if (m_itemTemplates[itemId]->item_type() != defs::item_type::weapon)
		throw std::exception("Invalid weapon identifier", static_cast<int>(itemId));

	return static_cast<adnd::templates::weapon*>(m_itemTemplates[itemId]);
}

const adnd::templates::projectile* adnd::templates::metadata::get_projectile_template(const defs::equipment::item& itemId)
{
	if (m_itemTemplates[itemId]->item_type() != defs::item_type::projectile)
		throw std::exception("Invalid projectile identifier", static_cast<int>(itemId));

	return static_cast<adnd::templates::projectile*>(m_itemTemplates[itemId]);
}

const adnd::templates::coin& adnd::templates::metadata::get_coin_template(const defs::coin& coinId)
{
	return m_coinTemplates[coinId];
}

const adnd::templates::deity& adnd::templates::metadata::get_deity_template(const defs::deities::deity & deityId)
{
	return m_deityTemplates[deityId];
}

const adnd::templates::skill& adnd::templates::metadata::get_skill_template(const defs::character_skill& skillId)
{
	return m_skillTemplates[skillId];
}

const adnd::templates::treasure& adnd::templates::metadata::get_treasure_template(const defs::treasure::treasure_class& treasureClass)
{
	return m_treasureTemplates[treasureClass];
}

const adnd::templates::moral_alignment& adnd::templates::metadata::get_moral_alignment_template(const defs::moral_alignment& alignmentId)
{
	return m_moralAlignmentTemplates[alignmentId];
}

const adnd::templates::gem& adnd::templates::metadata::get_gem_template(const defs::treasure::gems& gemId)
{
	return m_gemTemplates[gemId];
}

const adnd::templates::magical_item& adnd::templates::metadata::get_magical_item_template(const defs::magical::item& magicalId)
{
	return m_magicalTemplates[magicalId];
}

const adnd::templates::wizard_spell& adnd::templates::metadata::get_wizard_spell_template(const defs::spells::wizard_spell& spellId)
{
	return m_wizardSpellTemplates[spellId];
}

const adnd::templates::priest_spell& adnd::templates::metadata::get_priest_spell_template(const defs::spells::priest_spell& spellId)
{
	return m_priestSpellTemplates[spellId];
}

const adnd::templates::school_of_magic& adnd::templates::metadata::get_school_of_magic_template(const defs::spells::school& schoolId)
{
	return m_schoolOfMagicTemplates[schoolId];
}

const adnd::templates::sphere_of_influence& adnd::templates::metadata::get_sphere_of_influence_template(const defs::spells::sphere& sphereId)
{
	return m_spheresOfInfluenceTemplates[sphereId];
}

const adnd::templates::monster& adnd::templates::metadata::get_monster_template(const defs::monsters::monster& monsterId)
{
	return m_monsterTemplates[monsterId];
}


/**
Splits a character class identifier into its three components:
	- class type (warrior, rogue, ...);
	- class id (fighter, paladin, ...)
	- flag for multiclass
*/
adnd::tables::class_id adnd::templates::metadata::get_character_type_and_id(const defs::character_class& cls)
{
	tables::class_id clsId;
	uint16_t type = static_cast<uint16_t>(metadata::get_type_of(cls));
	uint32_t id = static_cast<uint32_t>(metadata::get_class_id_of(cls));
	bool isMulticlass = static_cast<uint32_t>(cls) & (static_cast<uint32_t>(defs::character_class_type::multiclass) << 24);
	if (isMulticlass)
		type += static_cast<uint16_t>(defs::character_class_type::multiclass);

	clsId.classType = type;
	clsId.classId = id;
	clsId.multiclass = isMulticlass;

	return clsId;
}

adnd::defs::character_class_type adnd::templates::metadata::get_type_of(const defs::character_class& cls)
{
	uint8_t type = static_cast<uint32_t>(cls) >> 24;
	uint8_t mask = static_cast<uint8_t>(defs::character_class_type::multiclass);
	return static_cast<defs::character_class_type>(type & ~mask);
}

adnd::defs::character_class adnd::templates::metadata::get_class_id_of(const defs::character_class& cls)
{
	uint32_t clsValue = static_cast<uint32_t>(cls) & 0x00FFFFFF;
	return static_cast<defs::character_class>(clsValue);
}

const std::tuple<short, short> adnd::templates::metadata::get_skill_limits(const defs::character_class& cls, const defs::character_skill& skill)
{
	auto cc = m_classTemplates[cls].get_class_composition();

	std::tuple<short, short> limit;
	std::get<0>(limit) = 3;
	std::get<1>(limit) = 18;

	for (auto c : cc.classes())
	{
		auto p = metadata::get_character_type_and_id(c);
		auto r = fetch<short, short>(query::skill_limits_per_class);
		auto l = r.filter<uint16_t>(p.classType).filter<uint32_t>(p.classId).filter<short>(static_cast<short>(skill)).run();
		if (l.has_values())
		{
			std::get<0>(limit) = std::max<short>(std::get<0>(limit), std::get<0>(l.first()));
			std::get<1>(limit) = std::min<short>(std::get<1>(limit), std::get<1>(l.first()));
		}
	}

	return limit;
}

const std::tuple<short, short> adnd::templates::metadata::get_skill_limits(const defs::character_race& race, const defs::character_skill& skill)
{
	auto r = fetch<short, short>(query::skill_limits_per_race);
	std::tuple<short, short> limit = std::make_tuple<short, short>(3, 18);
		
	auto l = r.filter<unsigned short>(static_cast<unsigned short>(race)).filter<uint32_t>(static_cast<uint32_t>(skill)).run();

	if (l.has_values())
	{
		//std::get<0>(limit) = std::get<0>(r.first());
		//std::get<1>(limit) = std::get<1>(r.first());
		limit = l.first();
	}

	return limit;
}

std::tuple<uint16_t, uint16_t, uint16_t, uint16_t, uint16_t> adnd::templates::metadata::get_starting_money_info(const defs::character_class& cls)
{
	auto cc = m_classTemplates[cls].get_class_composition();
	auto info = std::make_tuple<uint16_t, uint16_t, uint16_t, uint16_t, uint16_t>(1, 1, 1, 0, 1);

	for (auto c : cc.classes())
	{
		auto p = metadata::get_character_type_and_id(c);

		auto r = fetch<uint16_t, uint16_t, uint16_t, uint16_t, uint16_t>(query::starting_money_info);
		auto i = r.filter<uint16_t>(p.classType).run().first();

		defs::character_class_type classType = static_cast<defs::character_class_type>(std::get<0>(i));
		uint16_t rolls = std::get<1>(i);
		uint16_t faces = std::get<2>(i);
		uint16_t rollBase = std::get<3>(i);
		uint16_t multiplier = std::get<4>(i);
		
		short current = (std::get<1>(info) * std::get<2>(info) + std::get<3>(info)) * std::get<4>(info);
		if ((faces * rolls + rollBase) * multiplier > current)
		{
			info = i;
		}
	}

	return info;
}

std::list<adnd::defs::character_race> adnd::templates::metadata::get_available_races(skills::character_skill_set& skills)
{
	return get_available_races(skills.strength(), skills.dexterity(), skills.constitution(), skills.intelligence(), skills.wisdom(), skills.charisma());
}

std::list<adnd::defs::character_race> adnd::templates::metadata::get_available_races(const skills::character_skill_value& strength, const skills::character_skill_value& dexterity, const skills::character_skill_value& constitution, const skills::character_skill_value& intelligence, const skills::character_skill_value& wisdom, const skills::character_skill_value& charisma)
{
	return get_available_races(strength.value(), dexterity.value(), constitution.value(), intelligence.value(), wisdom.value(), charisma.value());
}

std::list<adnd::defs::character_race> adnd::templates::metadata::get_available_races(const short& strength, const short& dexterity, const short& constitution, const short& intelligence, const short& wisdom, const short& charisma)
{
	std::list<adnd::defs::character_race> result;
	for (auto r : { defs::character_race::human, defs::character_race::elf, defs::character_race::dwarf, defs::character_race::gnome, defs::character_race::half_elf, defs::character_race::halfling })
	{
		auto templ = get_race_template(r);
		if (templ.check_skill(defs::character_skill::strength, strength)
			&& templ.check_skill(defs::character_skill::dexterity, dexterity)
			&& templ.check_skill(defs::character_skill::constitution, constitution)
			&& templ.check_skill(defs::character_skill::intelligence, intelligence)
			&& templ.check_skill(defs::character_skill::wisdom, wisdom)
			&& templ.check_skill(defs::character_skill::charisma, charisma))
		{
			result.push_back(r);
		}
	}
	return result;
}

void adnd::templates::metadata::load_equipment_templates()
{
	auto items = fetch<short, short>(query::items_info).run();
	for (auto i : items)
	{
		auto infoId = static_cast<defs::equipment::item>(std::get<0>(i));
		auto itemType = static_cast<defs::item_type>(std::get<1>(i));

		switch (itemType)
		{
			case defs::item_type::clothing:
				m_itemTemplates[infoId] = new templates::clothing(infoId);
				break;
			case defs::item_type::daily_food_and_lodging:
				m_itemTemplates[infoId] = new templates::daily_food_and_lodging(infoId);
				break;
			case defs::item_type::household_provisioning:
				m_itemTemplates[infoId] = new templates::household_provisioning(infoId);
				break;
			case defs::item_type::service:
				m_itemTemplates[infoId] = new templates::service(infoId);
				break;
			case defs::item_type::transport:
				m_itemTemplates[infoId] = new templates::transport(infoId);
				break;
			case defs::item_type::animal:
				m_itemTemplates[infoId] = new templates::animal(infoId);
				break;
			case defs::item_type::tack_and_harness:
				m_itemTemplates[infoId] = new templates::tack_and_harness(infoId);
				break;
			case defs::item_type::miscellaneous_equipment:
				m_itemTemplates[infoId] = new templates::miscellaneous_equipment(infoId);
				break;
			case defs::item_type::armour:
				m_itemTemplates[infoId] = new templates::armour(infoId);
				break;
			case defs::item_type::weapon:
				m_itemTemplates[infoId] = new templates::weapon(infoId);
				break;
			case defs::item_type::projectile:
				m_itemTemplates[infoId] = new templates::projectile(infoId);
				break;
			default:
				m_itemTemplates[infoId] = new templates::item(infoId);
		}
	}
}

void adnd::templates::metadata::load_deity_templates()
{
	auto deities = metadata::get_instance().fetch<uint32_t>(metadata::query::deities).run();
	for (auto d : deities)
	{
		auto deityId = static_cast<defs::deities::deity>(std::get<0>(d));
		m_deityTemplates[deityId] = templates::deity(deityId);
	}
}

void adnd::templates::metadata::load_coin_templates()
{
	for (auto c : { defs::coin::copper, defs::coin::silver, defs::coin::electrum, defs::coin::gold, defs::coin::platinum })
	{
		m_coinTemplates[c] = templates::coin(c);
	}
}

void adnd::templates::metadata::load_race_templates()
{
	for (auto r : {
		defs::character_race::human,
		defs::character_race::dwarf,
		defs::character_race::elf,
		defs::character_race::half_elf,
		defs::character_race::halfling,
		defs::character_race::gnome })
	{
		m_raceTemplates[r] = templates::character_race(r);
	}
}

//const adnd::tables::racial_stats adnd::templates::metadata::get_racial_stats(const defs::character_race& race)
//{
//	auto stmt = m_db.resolve_by_name(metadata::_stmt::racial_stats);
//	adnd::templates::metadata::query_manager::record r;
//
//	m_db.bind_parameter<uint16_t>(stmt, 1, static_cast<uint16_t>(race));
//	m_db.execute(stmt, r);
//
//	short scores[22];
//	scores[0] =		utils::parse<short>(r[0]);
//	scores[1] =		utils::parse<short>(r[1]);
//	scores[2] =		utils::parse<short>(r[2]);
//	scores[3] =		utils::parse<short>(r[3]);
//	scores[4] =		utils::parse<short>(r[4]);
//	scores[5] =		utils::parse<short>(r[5]);
//	scores[6] =		utils::parse<short>(r[6]);
//	scores[7] =		utils::parse<short>(r[7]);
//	scores[8] =		utils::parse<short>(r[8]);
//	scores[9] =		utils::parse<short>(r[9]);
//	scores[10] =	utils::parse<short>(r[10]);
//	scores[11] =	utils::parse<short>(r[11]);
//	scores[12] =	utils::parse<short>(r[12]);
//	scores[13] =	utils::parse<short>(r[13]);
//	scores[14] =	utils::parse<short>(r[14]);
//	scores[15] =	utils::parse<short>(r[15]);
//	scores[16] =	utils::parse<short>(r[16]);
//	scores[17] =	utils::parse<short>(r[17]);
//	scores[18] =	utils::parse<short>(r[18]);
//	scores[19] =	utils::parse<short>(r[19]);
//	scores[20] =	utils::parse<short>(r[20]);
//	scores[21] =	utils::parse<short>(r[21]);
//
//	tables::racial_stats row = { scores[0], scores[1], scores[2], static_cast<defs::die_faces >(scores[3]), scores[4], static_cast<defs::die_faces >(scores[5]), scores[6],
//								scores[7], scores[8], static_cast<defs::die_faces >(scores[9]), scores[10], static_cast<defs::die_faces >(scores[11]), scores[12], scores[13],
//								static_cast<defs::die_faces >(scores[14]), scores[15], scores[16], static_cast<defs::die_faces >(scores[17]), scores[18], scores[19], scores[20],
//								scores[21] };
//	return row;
//}
//
//const adnd::tables::race_info adnd::templates::metadata::get_race_info(const adnd::defs::character_race& race)
//{
//	auto stmt = m_db.resolve_by_name(metadata::_stmt::race_info);
//	adnd::templates::metadata::query_manager::record r;
//
//	m_db.bind_parameter<uint16_t>(stmt, 1, static_cast<uint16_t>(race));
//	if (!m_db.execute(stmt, r))
//		throw std::exception("Unable to retrieve race info", static_cast<int>(race));
//
//	adnd::tables::race_info info;
//	adnd::utils::write<std::string>(info.name, sizeof(info.name), r[0], ' ', adnd::utils::justify::left);
//	adnd::utils::write<std::string>(info.acronym, sizeof(info.acronym), r[1], ' ', adnd::utils::justify::left);
//	info.baseMovementRate = utils::parse<short>(r[2]);
//
//	return info;
//}

adnd::templates::item::item()
	: m_id(defs::equipment::item::none), m_itemType(defs::item_type::none), m_handRequired(adnd::defs::item_hand_required::none), m_itemClass(defs::item_class::common),
	m_costCurrency(adnd::defs::coin::copper), m_minCostAmount(0.0), m_maxCostAmount(0.0), m_weight(0.0)
{
}

adnd::templates::item::item(const defs::equipment::item& itemId)
{
	auto resItems = metadata::get_instance().fetch<short, std::string, short, short, short, double, double, double>(metadata::query::item_info);
	auto r = resItems.filter<uint32_t>(static_cast<uint32_t>(itemId)).run().first();

	m_id = static_cast<defs::equipment::item>(std::get<0>(r));
	m_description = std::get<1>(r);
	m_itemType = static_cast<defs::item_type>(std::get<2>(r));
	m_handRequired = static_cast<defs::item_hand_required>(std::get<3>(r));
	m_costCurrency = static_cast<defs::coin>(std::get<4>(r));
	m_minCostAmount = std::get<5>(r);
	m_maxCostAmount = std::get<6>(r);
	m_weight = std::get<7>(r);

	auto resCapab = metadata::get_instance().fetch<uint32_t>(metadata::query::item_capabilities_per_character_class);
	auto classes = resCapab.filter<uint32_t>(static_cast<uint32_t>(itemId));
	for (auto c : classes)
	{
		auto cls = static_cast<defs::character_class>(std::get<0>(c));
		m_allowedToClasses.emplace(cls);
	}
}

adnd::templates::item::~item()
{
}

bool adnd::templates::item::usable_by(const defs::character_class& cls)
{
	return m_allowedToClasses.find(cls) != m_allowedToClasses.end();
}


adnd::templates::clothing::clothing() : item()
{
	m_bodySlot = defs::body_slot::body;
}

adnd::templates::clothing::clothing(const defs::equipment::item& itemId) : item(itemId)
{
	auto res = templates::metadata::get_instance().fetch<uint32_t, std::string, short, short, short, double, double, short, short>(metadata::query::clothing_info);
	auto r = res.filter<uint32_t>(static_cast<uint32_t>(itemId)).run().first();

	m_bodySlot = static_cast<defs::body_slot>(std::get<8>(r));
}


adnd::templates::armour::armour() : item()
{
	m_bodySlot = defs::body_slot::body;
	m_acBonusMelee = defs::base_armour_class;
	m_acBonusMissile = defs::base_armour_class;
	m_numOfAttackProtection = 0;
	m_protections = static_cast<uint16_t>(defs::attack_side::none);
	m_allowsWeapons = true;
}

adnd::templates::armour::armour(const defs::equipment::item& itemId) : item(itemId)
{
	auto res = templates::metadata::get_instance().fetch<uint32_t, std::string, short, short, short, double, double, double, uint16_t, uint16_t, uint16_t, uint16_t, uint16_t, char>(metadata::query::armour_info);
	auto r = res.filter<uint32_t>(static_cast<uint32_t>(itemId)).run().first();

	m_bodySlot = defs::body_slot::body;
	m_acBonusMelee = std::get<9>(r);
	m_acBonusMissile = std::get<10>(r);
	m_numOfAttackProtection = std::get<11>(r);
	m_protections = std::get<12>(r);
	m_allowsWeapons = std::get<13>(r);
}

adnd::templates::armour::~armour()
{
}

adnd::templates::projectile::projectile() : item()
{
	m_faces[defs::weapons::target_size::small_medium] = defs::die_faces ::one;
	m_numRolls[defs::weapons::target_size::small_medium] = 0;
	m_baseDamage[defs::weapons::target_size::small_medium] = 0;

	m_faces[defs::weapons::target_size::large] = defs::die_faces ::one;
	m_numRolls[defs::weapons::target_size::large] = 0;
	m_baseDamage[defs::weapons::target_size::large] = 0;
}

adnd::templates::projectile::projectile(const defs::equipment::item& itemId) : item(itemId)
{
	auto res = templates::metadata::get_instance().fetch<uint32_t, std::string, short, short, short, double, double, double, short, short, short, short, short, short, short>(metadata::query::projectile_info);
	auto rProjectiles = res.filter<uint32_t>(static_cast<uint32_t>(itemId)).run();

	for (auto p : rProjectiles)
	{
		auto targetSize = static_cast<defs::weapons::target_size>(std::get<10>(p));

		if (targetSize == defs::weapons::target_size::small_medium || targetSize == defs::weapons::target_size::all)
		{
			m_numRolls[defs::weapons::target_size::small_medium] = std::get<12>(p);
			m_faces[defs::weapons::target_size::small_medium] = static_cast<defs::die_faces>(std::get<13>(p));
			m_baseDamage[defs::weapons::target_size::small_medium] = std::get<14>(p);
		}
		if (targetSize == defs::weapons::target_size::large || targetSize == defs::weapons::target_size::all)
		{
			m_numRolls[defs::weapons::target_size::large] = std::get<12>(p);
			m_faces[defs::weapons::target_size::large] = static_cast<defs::die_faces>(std::get<13>(p));
			m_baseDamage[defs::weapons::target_size::large] = std::get<14>(p);
		}
	}
}


adnd::templates::weapon::weapon()
	: item(), m_weaponSize(defs::weapons::size::small), m_speedFactor(0), m_weaponType(defs::weapons::type::none)
{
	m_faces[defs::weapons::target_size::small_medium] = defs::die_faces ::one;
	m_numRolls[defs::weapons::target_size::small_medium] = 0;
	m_baseDamage[defs::weapons::target_size::small_medium] = 0;

	m_faces[defs::weapons::target_size::large] = defs::die_faces ::one;
	m_numRolls[defs::weapons::target_size::large] = 0;
	m_baseDamage[defs::weapons::target_size::large] = 0;
}

adnd::templates::weapon::weapon(const defs::equipment::item& itemId) : item(itemId)
{

	auto res = templates::metadata::get_instance().fetch<uint32_t, std::string, short, short, short, double, double, double, short, short, short, short, short, short, short, short, short>(templates::metadata::query::weapon_info);
	auto rWeapons = res.filter<uint32_t>(static_cast<uint32_t>(itemId)).run();

	for (auto w : rWeapons)
	{
		m_weaponSize = static_cast<defs::weapons::size>(std::get<8>(w));
		m_weaponType = static_cast<defs::weapons::type>(std::get<9>(w));
		defs::weapons::target_size targetSize = static_cast<defs::weapons::target_size>(std::get<10>(w));
		m_speedFactor = std::get<11>(w);

		if (targetSize == defs::weapons::target_size::small_medium || targetSize == defs::weapons::target_size::all)
		{
			m_faces[defs::weapons::target_size::small_medium] = static_cast<defs::die_faces>(std::get<13>(w));
			m_numRolls[defs::weapons::target_size::small_medium] = std::get<12>(w);
			m_baseDamage[defs::weapons::target_size::small_medium] = std::get<14>(w);
		}
		if (targetSize == defs::weapons::target_size::large || targetSize == defs::weapons::target_size::all)
		{
			m_faces[defs::weapons::target_size::large] = static_cast<defs::die_faces>(std::get<13>(w));
			m_numRolls[defs::weapons::target_size::large] = std::get<12>(w);
			m_baseDamage[defs::weapons::target_size::large] = std::get<14>(w);
		}

		m_attackType = static_cast<defs::weapons::attack_type>(std::get<15>(w));
		m_weaponGroup = static_cast<defs::weapons::group>(std::get<16>(w));
	}

	auto resProj = templates::metadata::get_instance().fetch<uint32_t, uint32_t>(templates::metadata::query::weapon_projectile_info);
	auto rProj = resProj.filter<uint32_t>(static_cast<uint32_t>(itemId)).run();

	std::map<defs::equipment::item, std::tuple<defs::equipment::item, defs::equipment::item>> projectileInfo;
	for (auto p : rProj)
	{
		auto weaponId = static_cast<defs::equipment::item>(std::get<0>(p));
		auto projectileId = static_cast<defs::equipment::item>(std::get<1>(p));

		projectileInfo[weaponId] = std::make_tuple(weaponId, projectileId);
	}

	auto resMissileRangeModif = templates::metadata::get_instance().fetch<int, short, short, short, short, short, short, short, short, short, short, short>(templates::metadata::query::missile_range_modifiers);
	auto modifs = resMissileRangeModif.filter<uint32_t>(static_cast<uint32_t>(itemId)).run();

	std::map<adnd::defs::equipment::item, adnd::tables::missile_weapon_range> missileModifiers;
	for (auto m : modifs)
	{
		adnd::tables::missile_weapon_range modif;
		
		modif.weaponId = itemId;
		modif.projectileId = static_cast<defs::equipment::item>(std::get<0>(m));
		
		modif.rofAttacks = std::get<1>(m);
		modif.rofRounds = std::get<2>(m);
		modif.shortDistanceFrom = std::get<3>(m);
		modif.shortDistanceTo = std::get<4>(m);
		modif.mediumDistanceFrom = std::get<5>(m);
		modif.mediumDistanceTo = std::get<6>(m);
		modif.longDistanceFrom = std::get<7>(m);
		modif.longDistanceTo = std::get<8>(m);
		modif.shortDistanceModifier = std::get<9>(m);
		modif.mediumDistanceModifier = std::get<10>(m);
		modif.longDistanceModifier = std::get<11>(m);
		
		missileModifiers[modif.projectileId] = modif;
	}

	for (auto wp : projectileInfo)
	{
		m_projectilesAllowed[std::get<1>(wp.second)] = missileModifiers[std::get<1>(wp.second)];
	}
}

bool adnd::templates::weapon::allows_projectile(const adnd::defs::equipment::item& projectileType) const
{
	return m_projectilesAllowed.find(projectileType) != m_projectilesAllowed.end();
}

std::pair<short, short> adnd::templates::weapon::rate_of_fire() const
{
	return std::make_pair(m_projectilesAllowed.begin()->second.rofAttacks, m_projectilesAllowed.begin()->second.rofRounds);
}

short adnd::templates::weapon::distance_modifier(const short& distance, const defs::proficiency::level& profLevel) const
{
	if (m_projectilesAllowed.size() > 1)
		throw std::exception("Projectile not specified");

	if (m_projectilesAllowed.size() == 0)
		// Melee weapon
		if (distance <= 3)
			return 0;
		else
			throw std::exception("Target unreachable", static_cast<int>(distance)); 

	// Use the only projectile available
	return distance_modifier(m_projectilesAllowed.begin()->first, distance, profLevel);
}

short adnd::templates::weapon::distance_modifier(const adnd::defs::equipment::item& projectileId, const short& distance, const defs::proficiency::level& profLevel) const
{
	if (m_projectilesAllowed.find(projectileId) == m_projectilesAllowed.end())
		throw std::exception("Invalid projectile specified", static_cast<int>(projectileId));

	/*
	Bow and crossbow specialists gain an additional range category: point blank.
	Point - blank range for bows is from six feet to 30 feet.
	Point - blank range for crossbows is from six feet to 60 feet.At point - blank range, the character gains a + 2 modifier on attack rolls.
	*/

	short modif = 0;
	if (m_weaponGroup == defs::weapons::group::bows
		&& 2 < distance && distance <= m_projectilesAllowed.at(projectileId).shortDistanceFrom
		&& profLevel == defs::proficiency::level::specialised)
	{
		modif = 2;
	}
	else if (m_weaponGroup == defs::weapons::group::crossbows
		&& 2 < distance && distance <= m_projectilesAllowed.at(projectileId).shortDistanceFrom
		&& profLevel == defs::proficiency::level::specialised)
	{
		modif = 2;
	}
	else if (m_projectilesAllowed.at(projectileId).shortDistanceFrom < distance && distance <= m_projectilesAllowed.at(projectileId).shortDistanceTo)
	{
		modif = m_projectilesAllowed.at(projectileId).shortDistanceModifier;
	}
	else if (m_projectilesAllowed.at(projectileId).mediumDistanceFrom < distance && distance <= m_projectilesAllowed.at(projectileId).mediumDistanceTo)
	{
		modif = m_projectilesAllowed.at(projectileId).mediumDistanceModifier;
	}
	else if (m_projectilesAllowed.at(projectileId).longDistanceFrom < distance && distance <= m_projectilesAllowed.at(projectileId).longDistanceTo)
	{
		modif = m_projectilesAllowed.at(projectileId).longDistanceModifier;
	}
	else //if (distance > m_projectilesAllowed.at(projectileId).longDistanceTo)
	{
		throw std::exception("Target unreachable", static_cast<int>(distance));
	}

	return modif;
}

adnd::templates::coin::coin()
	: m_id(defs::coin::copper)
{
}

adnd::templates::coin::coin(const defs::coin& coinId)
	: m_id(coinId)
{
	auto info = templates::metadata::get_instance().fetch<short, std::string, std::string>(templates::metadata::query::coin_info).filter<unsigned short>(static_cast<unsigned short>(coinId)).run().first();
	m_acronym = std::get<1>(info);
	m_description = std::get<2>(info);
	
	auto rates = templates::metadata::get_instance().fetch<short, short, double>(templates::metadata::query::exchange_rates).run();
	
	for (auto& r : rates)
	{
		auto currencyFrom = static_cast<defs::coin>(std::get<0>(r));
		auto currencyTo = static_cast<defs::coin>(std::get<1>(r));
		auto rate = std::get<2>(r);
		
		if (currencyFrom == m_id || currencyTo == m_id)
		{
			m_exchangeRates[currencyFrom][currencyTo] = rate;
		}
	}
}

double adnd::templates::coin::from(const defs::coin & currency)
{
	return m_exchangeRates[currency][m_id];
}

double adnd::templates::coin::to(const defs::coin & currency)
{
	return m_exchangeRates[m_id][currency];
}



adnd::templates::deity::deity()
	: m_id(defs::deities::deity::none)
{
}

adnd::templates::deity::deity(const defs::deities::deity& deityId)
	: m_id(deityId)
{
	auto res = templates::metadata::get_instance().fetch<uint32_t, std::string, short, short, short, short>(templates::metadata::query::deity_info);
	auto r = res.filter<uint32_t>(static_cast<uint32_t>(deityId)).run().first();

	m_name = std::get<1>(r);
	m_rank = static_cast<defs::deities::rank>(std::get<2>(r));
	m_moralAlignment = static_cast<defs::moral_alignment>(std::get<3>(r));
	m_planeOfExistence = static_cast<defs::planes::plane>(std::get<4>(r));
	m_realm = static_cast<defs::planes::realm>(std::get<5>(r));

	auto aligns = templates::metadata::get_instance().fetch<short>(metadata::query::deity_worshippers_info).filter<uint32_t>(static_cast<uint32_t>(deityId)).run();
	for (auto a : aligns)
	{
		m_worshippersAlignment.emplace(static_cast<defs::moral_alignment>(std::get<0>(a)));
	}

	auto shperes = templates::metadata::get_instance().fetch<short, short, short>(metadata::query::spheres_per_deity).filter<uint32_t>(static_cast<uint32_t>(deityId)).run();
	for (auto s : shperes)
	{
		auto sphere = static_cast<defs::spells::sphere>(std::get<0>(s));
		auto access = static_cast<defs::spells::access_level>(std::get<0>(s));
		auto mode = static_cast<defs::spells::cast_mode>(std::get<0>(s));
		
		m_spheres[sphere] = std::make_pair(access, mode);
	}
}

bool adnd::templates::deity::is_worshipped_by(const defs::moral_alignment& align)
{
	return m_worshippersAlignment.find(align) != m_worshippersAlignment.end();
}

bool adnd::templates::deity::allows(const defs::spells::priest_spell & spellId)
{
	auto st = metadata::get_instance().get_priest_spell_template(spellId);
	for (auto s : st.spheres())
	{
		if (m_spheres.find(s) != m_spheres.end())
			return true;
	}
	return false;
}

adnd::templates::skill::skill()
	: m_id(defs::character_skill::none)
{
}

adnd::templates::skill::skill(const defs::character_skill& skillId)
{
	auto info = templates::metadata::get_instance().fetch<short, std::string>(metadata::query::skill_info).filter<short>(static_cast<short>(skillId)).run().first();

	m_id = static_cast<defs::character_skill>(std::get<0>(info));
	m_name = std::get<1>(info);

	if (m_id == defs::character_skill::strength)
	{
		auto statsResult = metadata::get_instance().fetch<short, short, short, short, short, short, double, double, short, short, short>(metadata::query::strength_stats);

		for (auto s : statsResult)
		{
			tables::strength_stats stats;

			auto abilityScoreFrom = std::get<0>(s);
			auto excStrengthFrom = std::get<1>(s);
			auto abilityScoreTo = std::get<2>(s);
			auto excStrengthTo = std::get<3>(s);
			stats.hitModifier = std::get<4>(s);
			stats.damageAdjustment = std::get<5>(s);
			stats.weightAllowance = std::get<6>(s);
			stats.maximumPress = std::get<7>(s);
			stats.openDoors = std::get<8>(s);
			stats.openDoorsSpecial = std::get<9>(s);
			stats.bendBarsLiftGates = std::get<10>(s);

			skills::character_skill_value sklValue;
			if (abilityScoreFrom == 18 && excStrengthFrom != 0)
			{
				sklValue = skills::character_skill_value(defs::character_skill::strength, abilityScoreFrom, excStrengthFrom);
			}
			else
			{
				skills::character_skill_value(defs::character_skill::strength, abilityScoreFrom);
			}
			m_strengthStats[sklValue] = stats;
		}

		auto movResult = metadata::get_instance().fetch<short, short, short, short, double, short, short>(metadata::query::modified_movement_rates).run();
		for (auto m : movResult)
		{
			adnd::tables::modified_movement_rates modifiers;

			auto skillTo = std::get<2>(m);
			auto excTo = std::get<3>(m);
			adnd::skills::character_skill_value skl(defs::character_skill::strength, skillTo, excTo);
			modifiers.load = std::get<4>(m);
			modifiers.movementRate1 = std::get<5>(m);
			modifiers.movementRate2 = std::get<6>(m);

			m_modifiedMovementRates[skl][modifiers.load] = modifiers;
		}
	}
	else if (m_id == defs::character_skill::dexterity)
	{
		auto res = metadata::get_instance().fetch<short, short, short, short>(metadata::query::dexterity_stats).run();
		for (auto r : res)
		{
			tables::dexterity_stats stats;

			auto abilityScore = std::get<0>(r);
			stats.reactionAdjustment = std::get<1>(r);
			stats.missileAttackAdjustment = std::get<2>(r);
			stats.defensiveAdjustment = std::get<3>(r);

			skills::character_skill_value sklValue = skills::character_skill_value(defs::character_skill::dexterity, abilityScore);
			m_dexterityStats[sklValue] = stats;
		}

		auto thiefRes = metadata::get_instance().fetch<short, short, short, short, short, short, short, short, short>(metadata::query::thieving_modifiers_per_dexterity).run();
		for (auto r : thiefRes)
		{
			short scores[8];
			short dexScore = std::get<0>(r);
			scores[0] = std::get<1>(r);
			scores[1] = std::get<2>(r);
			scores[2] = std::get<3>(r);
			scores[3] = std::get<4>(r);
			scores[4] = std::get<5>(r);
			scores[5] = std::get<6>(r);
			scores[6] = std::get<7>(r);
			scores[7] = std::get<8>(r);

			auto row = adnd::tables::thief_ability(scores[0], scores[1], scores[2], scores[3], scores[4], scores[5], scores[6], scores[7]);
			m_thievingModifiersPerDexterity.emplace(dexScore, row);
		}
	}
	else if (m_id == defs::character_skill::constitution)
	{
		auto res = metadata::get_instance().fetch<short, short, short, short, short, short, short, short, short>(metadata::query::constitution_stats).run();
		for (auto r : res)
		{
			tables::constitution_stats stats;
			
			auto abilityScore = std::get<0>(r);
			stats.hitPointAdjustment = std::get<1>(r);
			stats.hitPointAdjustmentWarriors = std::get<2>(r);
			stats.rollsUpgrade = std::get<3>(r);
			stats.systemShock = std::get<4>(r);
			stats.resurrectionSurvival = std::get<5>(r);
			stats.poisonSave = std::get<6>(r);
			stats.regenerationPoints = std::get<7>(r);
			stats.regenerationTurns = std::get<8>(r);
			
			skills::character_skill_value sklValue = skills::character_skill_value(defs::character_skill::constitution, abilityScore);
			m_constitutionStats[sklValue] = stats;
		}
	}
	else if (m_id == defs::character_skill::intelligence)
	{
		auto res = metadata::get_instance().fetch<short, short, short, short, short, short>(metadata::query::intelligence_stats).run();
		for (auto r : res)
		{
			tables::intelligence_stats stats;
			
			auto abilityScore = std::get<0>(r);
			stats.numberOfLanguages = std::get<1>(r);
			stats.spellLevel = std::get<2>(r);
			stats.chanceToLearnSpell = std::get<3>(r);
			stats.maxNumberOfSpellsPerLevel = std::get<4>(r);
			stats.illusionImmunity = std::get<5>(r);

			skills::character_skill_value sklValue = skills::character_skill_value(defs::character_skill::intelligence, abilityScore);
			m_intelligenceStats[sklValue] = stats;
		}
	}
	else if (m_id == defs::character_skill::wisdom)
	{
		auto res = metadata::get_instance().fetch<short, short, short, short, short, short, short, short, short, short, short>(metadata::query::wisdom_stats).run();
		for (auto r : res)
		{
			tables::wisdom_stats stats;
			
			auto abilityScore = std::get<0>(r);
			stats.magicalDefenceAdjustment = std::get<1>(r);
			stats.bonusSpellLevel1 = std::get<2>(r);
			stats.bonusSpellLevel2 = std::get<3>(r);
			stats.bonusSpellLevel3 = std::get<4>(r);
			stats.bonusSpellLevel4 = std::get<5>(r);
			stats.bonusSpellLevel5 = std::get<6>(r);
			stats.bonusSpellLevel6 = std::get<7>(r);
			stats.bonusSpellLevel7 = std::get<8>(r);
			stats.chanceOfSpellFailure = std::get<9>(r);
			stats.spellImmunity = std::get<10>(r);
			
			skills::character_skill_value sklValue = skills::character_skill_value(defs::character_skill::wisdom, abilityScore);
			m_wisdomStats[sklValue] = stats;
		}
		
	}
	else if (m_id == defs::character_skill::charisma)
	{
		auto res = metadata::get_instance().fetch<short, short, short, short>(metadata::query::charisma_stats).run();
		for (auto r : res)
		{
			tables::charisma_stats stats;
			
			auto abilityScore = std::get<0>(r);
			stats.maximumNumberOfHenchmen = std::get<1>(r);
			stats.loyaltyBase = std::get<2>(r);
			stats.reactionAdjustment = std::get<3>(r);
			
			skills::character_skill_value sklValue = skills::character_skill_value(defs::character_skill::charisma, abilityScore);
			m_charismaStats[sklValue] = stats;;
		}
	}
}

const adnd::tables::strength_stats& adnd::templates::skill::get_strength_stats(const adnd::skills::character_skill_value& value)
{
	if (m_id != defs::character_skill::strength)
		throw std::exception("Statistics not available");

	return m_strengthStats.lower_bound(value)->second;
}

const adnd::tables::dexterity_stats& adnd::templates::skill::get_dexterity_stats(const adnd::skills::character_skill_value& value)
{
	if (m_id != defs::character_skill::dexterity)
		throw std::exception("Statistics not available");

	return m_dexterityStats[value];
}

const adnd::tables::constitution_stats& adnd::templates::skill::get_constitution_stats(const adnd::skills::character_skill_value& value)
{
	if (m_id != defs::character_skill::constitution)
		throw std::exception("Statistics not available");

	return m_constitutionStats[value];
}

const adnd::tables::intelligence_stats& adnd::templates::skill::get_intelligence_stats(const adnd::skills::character_skill_value& value)
{
	if (m_id != defs::character_skill::intelligence)
		throw std::exception("Statistics not available");

	return m_intelligenceStats[value];
}

const adnd::tables::wisdom_stats& adnd::templates::skill::get_wisdom_stats(const adnd::skills::character_skill_value& value)
{
	if (m_id != defs::character_skill::wisdom)
		throw std::exception("Statistics not available");

	return m_wisdomStats[value];
}

const adnd::tables::charisma_stats& adnd::templates::skill::get_charisma_stats(const adnd::skills::character_skill_value& value)
{
	if (m_id != defs::character_skill::charisma)
		throw std::exception("Statistics not available");

	return m_charismaStats[value];
}

short adnd::templates::skill::get_thieving_modifiers_per_dexterity(const short& dexterityScore, const adnd::defs::thief_ability& ability)
{
	if (m_id != defs::character_skill::dexterity)
		throw std::exception("Statistics not available");

	return m_thievingModifiersPerDexterity[dexterityScore].ability(ability);
}

const adnd::tables::modified_movement_rates adnd::templates::skill::get_modified_movement_rates(const adnd::skills::character_skill_value& value, const double& load)
{
	adnd::tables::modified_movement_rates movRates{ 0.0, 0, 0 };

	if (m_modifiedMovementRates.lower_bound(value) != m_modifiedMovementRates.end())
	{
		if (m_modifiedMovementRates.lower_bound(value)->second.lower_bound(load) != m_modifiedMovementRates.lower_bound(value)->second.end())
		{
			movRates = m_modifiedMovementRates.lower_bound(value)->second.lower_bound(load)->second;
		}
	}

	return movRates;
}

adnd::templates::character_class_id::character_class_id(const defs::character_class & cls)
	: m_class(cls)
{
	if (!is_multiclass())
		m_baseClasses.push_back(m_class);
	else
	{
		if (class_match<defs::character_class::fighter>())
			m_baseClasses.push_back(defs::character_class::fighter);
		if (class_match<defs::character_class::ranger>())
			m_baseClasses.push_back(defs::character_class::ranger);
		if (class_match<defs::character_class::mage>())
			m_baseClasses.push_back(defs::character_class::mage);
		if (class_match<defs::character_class::illusionist>())
			m_baseClasses.push_back(defs::character_class::illusionist);
		if (class_match<defs::character_class::cleric>())
			m_baseClasses.push_back(defs::character_class::cleric);
		if (class_match<defs::character_class::druid>())
			m_baseClasses.push_back(defs::character_class::druid);
		if (class_match<defs::character_class::thief>())
			m_baseClasses.push_back(defs::character_class::thief);

		if (class_match<defs::character_class::paladin>()
			|| class_match<defs::character_class::abjurer>()
			|| class_match<defs::character_class::conjurer>()
			|| class_match<defs::character_class::diviner>()
			|| class_match<defs::character_class::enchanter>()
			|| class_match<defs::character_class::invoker>()
			|| class_match<defs::character_class::necromancer>()
			|| class_match<defs::character_class::transmuter>()
			|| class_match<defs::character_class::priest_of_specific_mythos>()
			|| class_match<defs::character_class::bard>())
		{
			throw std::exception("Class not allowed in multiclass combination", static_cast<int>(m_class));
		}
	}

	if (is_type_of(defs::character_class_type::warrior))
		m_baseClassTypes.push_back(defs::character_class_type::warrior);
	if (is_type_of(defs::character_class_type::wizard))
		m_baseClassTypes.push_back(defs::character_class_type::wizard);
	if (is_type_of(defs::character_class_type::priest))
		m_baseClassTypes.push_back(defs::character_class_type::priest);
	if (is_type_of(defs::character_class_type::rogue))
		m_baseClassTypes.push_back(defs::character_class_type::rogue);
}

bool adnd::templates::character_class_id::is_multiclass()
{
	return ((static_cast<uint32_t>(m_class) >> 24) & (static_cast<uint32_t>(defs::character_class_type::multiclass))) != 0;
}

adnd::defs::character_class adnd::templates::character_class_id::select_class_by_type(const adnd::defs::character_class_type& clsType)
{
	for (auto c : m_baseClasses)
	{
		if (adnd::templates::metadata::get_type_of(c) == clsType)
			return c;
	}
	return adnd::defs::character_class::none;
}

bool adnd::templates::character_class_id::is_type_of(const adnd::defs::character_class_type& classType)
{
	return !(((static_cast<uint32_t>(m_class) >> 24) & static_cast<uint32_t>(classType)) ^ static_cast<uint32_t>(classType));
}

bool adnd::templates::character_class_id::is(const adnd::defs::character_class& cls)
{
	for (auto c : m_baseClasses)
		if (c == cls) return true;
	return false;
}

adnd::templates::treasure::treasure()
{
}

adnd::templates::treasure::treasure(const defs::treasure::treasure_class& treasureClass, const char& acronym, const defs::treasure::location& loc)
	: m_acronym(acronym), m_location(loc)
{
	auto res = metadata::get_instance().fetch<short, short, uint32_t, uint32_t, short, short, short, uint32_t>(metadata::query::treasure_composition);
	auto comp = res.filter<short>(static_cast<short>(treasureClass)).run();

	for (auto c : comp)
	{
		tables::treasure_composition_info info;
		
		info.treasureClass = static_cast<defs::treasure::treasure_class>(std::get<0>(c));
		info.component = static_cast<defs::treasure::component>(std::get<1>(c));
		info.countFrom = std::get<2>(c);
		info.countTo = std::get<3>(c);
		info.probability = std::get<4>(c);
		info.nature = std::get<5>(c);
		info.additionalNature = std::get<6>(c);
		info.additionalCount = std::get<7>(c);
		
		m_items[info.component] = info;
	}
	

	auto locInfo = metadata::get_instance().fetch<short, std::string>(metadata::query::treasure_location).filter<short>(static_cast<short>(loc)).run().first();
	m_locationName = std::get<1>(locInfo);
	
	if (m_items.find(defs::treasure::component::magical_item) != m_items.end())
	{
		for (auto n : { defs::treasure::nature::magical_weapon, defs::treasure::nature::magical_armour,
					defs::treasure::nature::potions_and_oils, defs::treasure::nature::scrolls,
					defs::treasure::nature::rings, defs::treasure::nature::wands_staves_and_rods,
					defs::treasure::nature::miscellaneous, defs::treasure::nature::artifacts_and_relics })
		{
			if (static_cast<short>(n) & m_items[defs::treasure::component::magical_item].nature)
				m_magicalItemTypes.emplace(n);
		}
	}
	else
		m_magicalItemTypes.emplace(defs::treasure::nature::none);
	
	if (m_items.find(defs::treasure::component::additional_magical_item) != m_items.end())
	{
		for (auto n : { defs::treasure::nature::magical_weapon, defs::treasure::nature::magical_armour,
					defs::treasure::nature::potions_and_oils, defs::treasure::nature::scrolls,
					defs::treasure::nature::rings, defs::treasure::nature::wands_staves_and_rods,
					defs::treasure::nature::miscellaneous, defs::treasure::nature::artifacts_and_relics })
		{
			if (static_cast<short>(n) & m_items[defs::treasure::component::additional_magical_item].nature)
				m_additionalMagicalItemTypes.emplace(n);
		}
	}
	else
		m_additionalMagicalItemTypes.emplace(defs::treasure::nature::none);
}

std::set<adnd::defs::treasure::component> adnd::templates::treasure::components()
{
	decltype(m_items)::const_iterator it;
	std::set<defs::treasure::component> k;
	for (it = m_items.begin(); it != m_items.end(); ++it)
		k.insert(it->first);
	return k;
}

adnd::templates::moral_alignment::moral_alignment()
{
}

adnd::templates::moral_alignment::moral_alignment(const adnd::defs::moral_alignment& alignId)
{
	auto res = metadata::get_instance().fetch<short, std::string, std::string, short, std::string, char, short, std::string, char>(metadata::query::moral_alignment);
	auto r = res.filter<uint16_t>(static_cast<uint16_t>(alignId)).run().first();

	m_id = static_cast<defs::moral_alignment>(std::get<0>(r));
	m_name = std::get<1>(r);
	m_acronym = std::get<2>(r);
	
	m_moralOrderId = std::get<3>(r);
	m_moralOrderName = std::get<4>(r);
	m_moralOrderAcronym = std::get<5>(r);
	
	m_moralViewId = std::get<6>(r);
	m_moralViewName = std::get<7>(r);
	m_moralViewAcronym = std::get<8>(r);
}

adnd::templates::gem::gem()
	: m_id(defs::treasure::gems::none), m_type(defs::treasure::gem_type::none),
	m_currency(defs::coin::copper), m_valueFrom(0), m_valueTo(0)
{
}

adnd::templates::gem::gem(const defs::treasure::gems& gemId)
{
	auto res = metadata::get_instance().fetch<short, std::string, short, std::string, short, uint32_t, uint32_t>(metadata::query::gem_info);
	auto r = res.filter<short>(static_cast<short>(gemId)).run().first();

	m_id = static_cast<defs::treasure::gems>(std::get<0>(r));
	m_name = std::get<1>(r);
	m_type = static_cast<defs::treasure::gem_type>(std::get<2>(r));
	m_typeName = std::get<3>(r);
	m_currency = static_cast<defs::coin>(std::get<4>(r));
	m_valueFrom = std::get<5>(r);
	m_valueTo = std::get<6>(r);
}

adnd::templates::magical_item::magical_item()
{
}

adnd::templates::magical_item::magical_item(const defs::magical::item& magicalId)
{
	auto res = metadata::get_instance().fetch<short, std::string, short, short, short, short, std::string, short, short, short>(metadata::query::magical_item_info);
	auto r = res.filter<uint32_t>(static_cast<uint32_t>(magicalId)).run().first();

	m_id = static_cast<defs::equipment::item>(std::get<8>(r));
	m_magicalId = static_cast<defs::magical::item>(std::get<0>(r));
	m_description = std::get<1>(r);
	m_itemType = static_cast<defs::item_type>(std::get<2>(r));
	m_handRequired = static_cast<defs::item_hand_required>(std::get<3>(r));
	m_itemClass = defs::item_class::magical;
	m_minCostAmount = 0;
	m_maxCostAmount = 0;
	m_costCurrency = defs::coin::gold;
	m_weight = 0;
	m_unidentifiedName = std::get<6>(r);
	m_attackType = static_cast<defs::weapons::attack_type>(std::get<4>(r));
	m_weaponGroup = static_cast<defs::weapons::group>(std::get<5>(r));
	m_bodySlot = static_cast<defs::body_slot>(std::get<7>(r));
}

adnd::configuration& adnd::configuration::get_instance()
{
	static configuration _instance;

	if (!m_initialised)
	{
		// Default values for optional rules
		_instance.m_default[defs::config::optional::encumbrance] = true;
		_instance.m_default[defs::config::optional::weapon_proficiency] = true;
		_instance.m_default[defs::config::optional::exception_on_turn_undead] = false;
		_instance.m_default[defs::config::optional::max_number_of_spells_per_level] = true;
		_instance.m_default[defs::config::optional::exceeding_maximum_level_limit_for_demihumans] = true;
		_instance.m_default[defs::config::optional::always_learn_new_spells] = false;
		
		// Applies default setting to current configuration
		_instance.reset();

		_instance.m_dbDefScriptDir = "";
		_instance.m_dbInitScriptDir = "";
		_instance.m_errorHandlerStyle = adnd::configuration::error_handler_style::exception;
		_instance.m_dbSource = adnd::configuration::database_source::file;

		m_initialised = true;
	}
	return _instance;
}

bool adnd::configuration::get(const defs::config::optional& opt)
{
	if (m_settings.find(opt) == m_settings.end())
		return false;

	return m_settings[opt];
}

void adnd::configuration::set(const adnd::defs::config::optional& opt, bool value)
{
	m_settings[opt] = value;
}

void adnd::configuration::reset()
{
	m_settings = m_default;
}

adnd::templates::metadata::query_manager::query_manager(void)
	: m_dbConnection(nullptr)//, m_dbSource(adnd::configuration::database_source::file)
{
}

adnd::templates::metadata::query_manager::~query_manager(void)
{
	//close(false);
}

void adnd::templates::metadata::query_manager::error(const char* text)
{
	if (adnd::configuration::get_instance().get_error_handler_style() == adnd::configuration::error_handler_style::internal)
		m_lastError.assign(text);
	else
	{
		throw std::exception(text);
	}
}


bool adnd::templates::metadata::query_manager::open()
{
	std::string path = db_location();

	int ret = sqlite3_open_v2(path.c_str(), &m_dbConnection, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_URI, NULL);
	if (ret != SQLITE_OK)
	{
		error(sqlite3_errstr(ret));
		return false;
	}

	return true;
}

std::string adnd::templates::metadata::query_manager::db_location()
{
	std::string path = adnd::configuration::get_instance().get_database_path();
	configuration::database_source src = adnd::configuration::get_instance().get_database_source();
	if (src == adnd::configuration::database_source::file)
	{
		return path;
	}
	else if (src == adnd::configuration::database_source::memory)
	{
		return "file:" + path + "?mode=memory&cache=shared";
	}

	error("Invalid database source");
	return "";
}

//void adnd::templates::metadata::query_manager::drop_tables()
//{
//	auto stmtSelectTables = prepare_statement("SELECT NAME FROM SQLITE_MASTER WHERE TYPE = 'table' AND NAME NOT LIKE 'sqlite_%'");
//	adnd::templates::metadata::query_manager::iterator it, end;
//	std::list<std::string> dropList;
//	execute(stmtSelectTables, it, end);
//	while (it != end)
//	{
//		auto tabName = it.data<std::string>(1, "");
//		std::stringstream ss;
//		ss << "DROP TABLE " << tabName << ";";
//		dropList.push_back(ss.str());
//		++it;
//	}
//	for (auto dropStmt : dropList)
//		execute(dropStmt);
//}

//bool adnd::templates::metadata::query_manager::close(bool saveData)
//{
//	if (!is_open())
//	{
//		error("Database is not in open state");
//		return false;
//	}
//
//	if (saveData && m_dbSource == adnd::templates::metadata::query_manager::source::memory)
//	{
//		if (!save(m_dbPath))
//		{
//			error("Error saving data");
//			return false;
//		}
//	}
//
//	m_preparedStatements.erase(m_preparedStatements.begin(), m_preparedStatements.end());
//	m_preparedStatementNames.erase(m_preparedStatementNames.begin(), m_preparedStatementNames.end());
//
//	if (sqlite3_close_v2(m_dbConnection) != SQLITE_OK)
//	{
//		error("Error closing database");
//		return false;
//	}
//
//	m_status = adnd::templates::metadata::query_manager::status::close;
//	return true;
//}

//void adnd::templates::metadata::query_manager::clean_prepared_statements()
//{
//	for (auto stmt : m_preparedStatements)
//	{
//		sqlite3_finalize(stmt.second);
//	}
//	m_preparedStatements.clear();
//}

bool adnd::templates::metadata::query_manager::clean_db(bool dropTables)
{
	std::string select_stmt = "SELECT name FROM sqlite_master WHERE type='table' and name not like 'sqlite_%';";
	int ret = SQLITE_OK;

	adnd::templates::metadata::query_manager::iterator it = begin(select_stmt);
	adnd::templates::metadata::query_manager::iterator eit = end();
	std::string stmt;
	std::list<std::string> tables;
	while (it != eit)
	{
		std::string table = std::string(it.data<std::string>(1, ""));
		tables.push_back(table);
		++it;
	}

	for (auto t : tables)
	{
		stmt = (dropTables) ? "DROP TABLE IF EXISTS " + t + ";" : "DELETE FROM " + t + ";";
		if (!execute(stmt.c_str()))
		{
			ret = SQLITE_ERROR;
		}
	}
	return ret == SQLITE_OK;
}

bool adnd::templates::metadata::query_manager::execute(const char* sqlScript)
{
	char *zErrMsg = nullptr;
	int ret = sqlite3_exec(m_dbConnection, sqlScript, nullptr, 0, &zErrMsg);

	if (ret != SQLITE_OK)
	{
		error(zErrMsg);
		sqlite3_free(zErrMsg);
		return false;
	}
	return true;
}

sqlite3_stmt* adnd::templates::metadata::query_manager::get_prepared_statement(const adnd::templates::metadata::query_manager::query_id& stmtId)
{
	prepared_statement_set::iterator it = m_preparedStatements.find(stmtId);
	if (it == m_preparedStatements.end())
	{
		error("Invalid prepared statement id");
		return false;
	}
	return m_preparedStatements[stmtId];
}

bool adnd::templates::metadata::query_manager::execute(const adnd::templates::metadata::query_manager::query_id& stmtId)
{
	sqlite3_stmt *prepStmt = get_prepared_statement(stmtId);

	int ric = sqlite3_step(prepStmt);
	if (ric == SQLITE_DONE)
	{
		sqlite3_reset(prepStmt);
	}
	else if (ric != SQLITE_DONE && ric != SQLITE_ROW)
	{
		sqlite3_reset(prepStmt);
		error(sqlite3_errmsg(m_dbConnection));
		return false;
	}
	return true;
}

bool adnd::templates::metadata::query_manager::execute_script(const std::experimental::filesystem::path& scriptPath)
{
	if (!std::experimental::filesystem::exists(scriptPath))
	{
		std::string err = "Unable to open file " + scriptPath.string();
		error(err.c_str());
		return false;
	}
	std::ifstream in(scriptPath.c_str(), std::ios::in);
	std::string sqlText;

	in.seekg(0, std::ios::end);
	sqlText.reserve(static_cast<size_t>(in.tellg()));
	in.seekg(0, std::ios::beg);

	sqlText.assign((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());

	return execute(sqlText.c_str());
}

bool adnd::templates::metadata::query_manager::execute(const adnd::templates::metadata::query_manager::query_id& stmtId, adnd::templates::metadata::query_manager::iterator& itBegin, adnd::templates::metadata::query_manager::iterator& itEnd)
{
	sqlite3_stmt *prepStmt = get_prepared_statement(stmtId);

	auto b = begin(prepStmt);
	auto e = end();
	itBegin = b;
	itEnd = e;

	return true;
}

//bool adnd::templates::metadata::query_manager::execute(const adnd::templates::metadata::query_manager::query_id& stmtId, adnd::templates::metadata::query_manager::record& firstRow)
//{
//	sqlite3_stmt *prepStmt = get_prepared_statement(stmtId);
//
//	adnd::templates::metadata::query_manager::iterator itBegin = begin(prepStmt);
//	adnd::templates::metadata::query_manager::iterator itEnd = end();
//
//	if (itBegin != itEnd)
//	{
//		firstRow = *itBegin;
//	}
//
//	return sqlite3_reset(prepStmt) == SQLITE_OK;
//}

//bool adnd::templates::metadata::query_manager::execute(const std::string &stmt, adnd::templates::metadata::query_manager::record& firstRow)
//{
//	adnd::templates::metadata::query_manager::iterator itBegin = begin(stmt);
//	adnd::templates::metadata::query_manager::iterator itEnd = end();
//
//	if (itBegin != itEnd)
//	{
//		firstRow = *itBegin;
//	}
//
//	return true;
//}

//int exec_stmt_callback(void *data, int argc, char **argv, char **azColName)
//{
//	const adnd::templates::metadata::query_manager::visit_callback *f = (adnd::templates::metadata::query_manager::visit_callback*)data;
//	std::vector<std::string> r;
//	r.reserve(argc);
//	for (int i = 0; i < argc; ++i)
//	{
//		if (argv[i] == nullptr)
//			r.push_back("");
//		else
//			r.push_back(argv[i]);
//	}
//	(*f)(r);
//	return 0;
//}

//bool adnd::templates::metadata::query_manager::execute(const char* stmt, const adnd::templates::metadata::query_manager::visit_callback &f)
//{
//	char *zErrMsg = NULL;
//
//	m_lastError = "";
//	int(*cbk)(void*, int, char**, char**) = exec_stmt_callback;
//	int ret = sqlite3_exec(m_dbConnection, stmt, cbk, (void*)&f, &zErrMsg);
//
//	if (ret != SQLITE_OK)
//	{
//		error(zErrMsg);
//		sqlite3_free(zErrMsg);
//		return false;
//	}
//	return true;
//}
//
//bool adnd::templates::metadata::query_manager::execute(const std::string& stmt, const adnd::templates::metadata::query_manager::visit_callback &f)
//{
//	return execute(stmt.c_str(), f);
//}
//
//bool adnd::templates::metadata::query_manager::execute(const adnd::templates::metadata::query_manager::query_id& stmtId, const visit_callback& f)
//{
//	sqlite3_stmt *prepStmt = get_prepared_statement(stmtId);
//
//	adnd::templates::metadata::query_manager::iterator itBegin = begin(prepStmt);
//	adnd::templates::metadata::query_manager::iterator itEnd = end();
//
//	while (itBegin != itEnd)
//	{
//		f(*itBegin);
//		++itBegin;
//	}
//
//	return sqlite3_reset(prepStmt) == SQLITE_OK;
//}

adnd::templates::metadata::query_manager::query_id adnd::templates::metadata::query_manager::prepare_statement(const std::string& statement, const std::string& symbolicName)
{
	adnd::templates::metadata::query_manager::query_id id(-1);
	sqlite3_stmt *prepStmt = NULL;
	int ric = sqlite3_prepare_v2(m_dbConnection, statement.c_str(), static_cast<int>(statement.length()), &prepStmt, NULL);
	if (ric != SQLITE_OK)
	{
		error(sqlite3_errmsg(m_dbConnection));
		return id;
	}

	id = m_preparedStatements.size();
	m_preparedStatements[id] = prepStmt;
	if (!symbolicName.empty())
	{
		m_preparedStatementNames[symbolicName] = id;
	}

	return id;
}

//bool adnd::templates::metadata::query_manager::create_function(const std::string& functionName, long numArgs, void(*funct)(sqlite3_context*, int, sqlite3_value**))
//{
//	int iRes = sqlite3_create_function_v2(m_dbConnection, functionName.c_str(), numArgs, SQLITE_UTF8, this, funct, NULL, NULL, NULL);
//	if (iRes != SQLITE_OK)
//	{
//		error(sqlite3_errmsg(m_dbConnection));
//		return false;
//	}
//
//	return true;
//}

//unsigned long adnd::templates::metadata::query_manager::count_rows(const std::string &tableName)
//{
//	std::string select = "select count(*) from " + tableName + ";";
//	adnd::templates::metadata::query_manager::record r;
//	execute(select, r);
//	return adnd::utils::parse<unsigned long>(r[0].c_str(), r[0].length(), 0);
//}

//bool adnd::templates::metadata::query_manager::save(const std::string& dbDest)
//{
//	bool bRestored = false;
//	sqlite3_backup *backup = nullptr;
//	sqlite3 *dest = nullptr;
//
//	if (m_dbSource == adnd::templates::metadata::query_manager::source::file)
//		return true;
//
//	int ret = sqlite3_open(dbDest.c_str(), &dest);
//	if (ret != SQLITE_OK)
//	{
//		error(sqlite3_errstr(ret));
//		return false;
//	}
//
//	backup = sqlite3_backup_init(dest, "main", m_dbConnection, "main");
//	if (!backup)
//	{
//		error(std::string("Unable to open " + dbDest));
//		return false;
//	}
//	sqlite3_backup_step(backup, -1);
//	sqlite3_backup_finish(backup);
//
//	ret = sqlite3_errcode(dest);
//	if (ret != SQLITE_OK)
//	{
//		error(sqlite3_errstr(ret));
//		return false;
//	}
//
//	ret = sqlite3_close(dest);
//	if (ret != SQLITE_OK)
//	{
//		error(sqlite3_errstr(ret));
//		return false;
//	}
//	return true;
//}
//
//bool adnd::templates::metadata::query_manager::restore(const std::string& dbSrc)
//{
//	bool bRestored = false;
//	sqlite3_backup *backup = nullptr;
//	sqlite3 *src = nullptr;
//
//	if (m_dbSource == adnd::templates::metadata::query_manager::source::file)
//		return true;
//
//	int ret = sqlite3_open(dbSrc.c_str(), &src);
//	if (ret != SQLITE_OK)
//	{
//		error(sqlite3_errstr(ret));
//		return false;
//	}
//
//	backup = sqlite3_backup_init(m_dbConnection, "main", src, "main");
//	if (!backup)
//	{
//		error(std::string("Unable to open " + dbSrc));
//		return false;
//	}
//
//	sqlite3_backup_step(backup, -1);
//	sqlite3_backup_finish(backup);
//
//	ret = sqlite3_errcode(src);
//	if (ret != SQLITE_OK)
//	{
//		error(sqlite3_errstr(ret));
//		return false;
//	}
//
//	ret = sqlite3_close(src);
//	if (ret != SQLITE_OK)
//	{
//		error(sqlite3_errstr(ret));
//		return false;
//	}
//	return true;
//}

bool adnd::templates::metadata::query_manager::bind_parameter(const adnd::templates::metadata::query_manager::query_id& stmtId, int index, const std::time_t& value)
{
	prepared_statement_set::iterator it = m_preparedStatements.find(stmtId);
	if (it == m_preparedStatements.end())
	{
		error("Invalid prepared statement id");
		return false;
	}

	sqlite3_stmt *prepStmt = m_preparedStatements[stmtId];
	if (sqlite3_bind_int64(prepStmt, index, value) != SQLITE_OK)
	{
		error(sqlite3_errmsg(m_dbConnection));
		return false;
	}
	return true;
}

bool adnd::templates::metadata::query_manager::bind_parameter(const adnd::templates::metadata::query_manager::query_id& stmtId, int index, const std::string& value)
{
	prepared_statement_set::iterator it = m_preparedStatements.find(stmtId);
	if (it == m_preparedStatements.end())
	{
		error("Invalid prepared statement id");
		return false;
	}

	sqlite3_stmt *prepStmt = m_preparedStatements[stmtId];
	if (sqlite3_bind_text(prepStmt, index, value.c_str(), static_cast<int>(value.length()), SQLITE_TRANSIENT) != SQLITE_OK)
	{
		error(sqlite3_errmsg(m_dbConnection));
		return false;
	}
	return true;
}

bool adnd::templates::metadata::query_manager::bind_parameter(const adnd::templates::metadata::query_manager::query_id& stmtId, int index, const char* value)
{
	return bind_parameter(stmtId, index, std::string(value));
}

adnd::templates::metadata::query_manager::iterator adnd::templates::metadata::query_manager::begin(const std::string &stmt)
{
	return adnd::templates::metadata::query_manager::iterator(this, stmt);
}

adnd::templates::metadata::query_manager::iterator adnd::templates::metadata::query_manager::begin(sqlite3_stmt *stmt)
{
	return adnd::templates::metadata::query_manager::iterator(this, stmt);
}

adnd::templates::metadata::query_manager::iterator adnd::templates::metadata::query_manager::end()
{
	return adnd::templates::metadata::query_manager::iterator();
}

bool adnd::templates::metadata::query_manager::add_pragma(const std::string &key, const std::string &value)
{
	std::string pragma = "PRAGMA " + key + "=" + value;
	if (!execute(pragma.c_str()))
	{
		error(last_error().c_str());
		return false;
	}
	return true;
}

//std::string adnd::templates::metadata::query_manager::sql(const adnd::templates::metadata::query_manager::query_id& stmtId)
//{
//	prepared_statement_set::iterator it = m_preparedStatements.find(stmtId);
//	if (it == m_preparedStatements.end())
//	{
//		m_lastError = "Invalid prepared statement id";
//		return "";
//	}
//	sqlite3_stmt *prepStmt = m_preparedStatements[stmtId];
//	return sqlite3_sql(prepStmt);
//}

//bool adnd::templates::metadata::query_manager::register_function(const std::string& functionName, long numArgs, void(*func)(sqlite3_context*, int, sqlite3_value**), std::string &error)
//{
//	int res = sqlite3_create_function_v2(m_dbConnection, functionName.c_str(), numArgs, SQLITE_UTF8, this, func, NULL, NULL, NULL);
//	if (res != SQLITE_OK)
//	{
//		error = sqlite3_errmsg(m_dbConnection);
//		return false;
//	}
//	return true;
//}

const char* adnd::templates::metadata::query_manager::iterator::get_column_text(const int col) const
{
	const char *zValue = NULL;
	if (col <= 0 || col > m_numCols)
	{
		throw std::exception("Column index out of bound");
	}

	zValue = reinterpret_cast<const char*>(sqlite3_column_text(m_stmt, col - 1));
	if (!zValue)
		throw std::exception("Error retrieving data");

	return zValue;
}

adnd::templates::metadata::query_manager::iterator::iterator(adnd::templates::metadata::query_manager* const db, const std::string &stmt)
	: m_result(SQLITE_OK), m_db(db), m_numCols(0), m_stmt(nullptr)
{
	int nByte = -1;
	const char *pzTail = NULL;

	m_result = sqlite3_prepare_v2(m_db->db_connection(), stmt.c_str(), nByte, &m_stmt, &pzTail);
	if (m_result != SQLITE_OK)
		throw std::exception(sqlite3_errmsg(m_db->db_connection()));

	m_numCols = sqlite3_column_count(m_stmt);
	m_result = sqlite3_step(m_stmt);
	if (m_result != SQLITE_ROW && m_result != SQLITE_DONE)
	{
		throw std::exception(sqlite3_errmsg(m_db->db_connection()));
	}
}

adnd::templates::metadata::query_manager::iterator::iterator(adnd::templates::metadata::query_manager* const db, sqlite3_stmt *stmt)
	: m_result(SQLITE_OK), m_db(db), m_numCols(0), m_stmt(stmt)
{
	m_numCols = sqlite3_column_count(m_stmt);
	m_result = sqlite3_step(m_stmt);
	if (m_result != SQLITE_ROW && m_result != SQLITE_DONE)
	{
		throw std::exception(sqlite3_errmsg(m_db->db_connection()));
	}
}

//adnd::templates::metadata::query_manager::iterator::iterator(adnd::templates::metadata::query_manager* const db, const adnd::templates::metadata::query_manager::query_id& stmtId)
//	: m_result(SQLITE_OK), m_db(db), m_numCols(0)
//{
//	m_stmt = db->m_preparedStatements[stmtId];
//	m_numCols = sqlite3_column_count(m_stmt);
//	m_result = sqlite3_step(m_stmt);
//	//if (m_result != SQLITE_ROW && m_result != SQLITE_DONE)
//	//{
//	//	throw std::exception(sqlite3_errmsg(m_db->db_connection()));
//	//}
//}

adnd::templates::metadata::query_manager::iterator::iterator()
	: m_result(SQLITE_DONE), m_db(nullptr), m_numCols(0), m_stmt(nullptr)
{
}

adnd::templates::metadata::query_manager::iterator::~iterator()
{
	if (m_stmt)
	{
		m_result = sqlite3_reset(m_stmt);
		m_stmt = nullptr;
	}
}

adnd::templates::metadata::query_manager::iterator::operator bool() const
{
	return m_result == SQLITE_ROW;
}

bool adnd::templates::metadata::query_manager::iterator::operator==(const adnd::templates::metadata::query_manager::iterator& it) const
{
	return (m_result == it.m_result);
}

bool adnd::templates::metadata::query_manager::iterator::operator!=(const adnd::templates::metadata::query_manager::iterator& it) const
{
	return (m_result != it.m_result);
}

adnd::templates::metadata::query_manager::iterator& adnd::templates::metadata::query_manager::iterator::operator=(iterator& it)
{
	this->m_result = it.m_result;
	this->m_db = it.m_db;
	this->m_numCols = it.m_numCols;
	this->m_stmt = it.m_stmt;
	it.m_stmt = nullptr;	//takes ownership
	return (*this);
}

adnd::templates::metadata::query_manager::iterator& adnd::templates::metadata::query_manager::iterator::operator+=(const int& movement)
{
	for (int i = 0; i < movement; ++i)
	{
		m_result = sqlite3_step(m_stmt);
	}
	return (*this);
}

adnd::templates::metadata::query_manager::iterator& adnd::templates::metadata::query_manager::iterator::operator++()
{
	m_result = sqlite3_step(m_stmt);
	return (*this);
}

adnd::templates::metadata::query_manager::iterator adnd::templates::metadata::query_manager::iterator::operator++(int)
{
	auto temp(*this);
	++(*this);
	return temp;
}

//std::vector<std::string> adnd::templates::metadata::query_manager::iterator::operator*()
//{
//	std::vector<std::string> row;
//	const unsigned char *zValue = nullptr;
//	for (long col = 0; col < m_numCols; ++col)
//	{
//		zValue = sqlite3_column_text(m_stmt, col);
//		if (zValue)
//		{
//			row.push_back(std::string(reinterpret_cast<const char*>(zValue)));
//		}
//		else
//		{
//			row.push_back("");
//		}
//	}
//	return row;
//}

const std::vector<std::string> adnd::templates::metadata::query_manager::iterator::operator*() const
{
	std::vector<std::string> row;
	const unsigned char *zValue = nullptr;
	for (long col = 0; col < m_numCols; ++col)
	{
		zValue = sqlite3_column_text(m_stmt, col);
		if (zValue)
		{
			row.push_back(std::string(reinterpret_cast<const char*>(zValue)));
		}
		else
		{
			row.push_back("");
		}
	}
	return row;
}

int adnd::templates::metadata::query_manager::iterator::get_result() const
{
	return m_result;
}

//sqlite3_stmt const * adnd::templates::metadata::query_manager::iterator::statement() const
//{
//	return m_stmt;
//}

const int& adnd::templates::metadata::query_manager::iterator::columns_count() const
{
	return m_numCols;
}

const std::vector<std::string> adnd::templates::metadata::query_manager::iterator::get_caption() const
{
	std::vector<std::string> row;
	const char *zValue = NULL;
	for (long col = 0; col < m_numCols; ++col)
	{
		zValue = sqlite3_column_name(m_stmt, col);
		if (zValue)
		{
			row.push_back(std::string(reinterpret_cast<const char*>(zValue)));
		}
	}
	return row;
}

const std::string adnd::templates::metadata::query_manager::iterator::get_caption(int column) const
{
	const char *zCaption = NULL;
	if (column > m_numCols)
	{
		throw std::exception("Column index out of bound");
	}
	zCaption = sqlite3_column_name(m_stmt, column);
	if (!zCaption)
		throw std::exception("Error retrieving data");
	return std::string(zCaption);
}

//std::string adnd::templates::metadata::query_manager::iterator::sql() const
//{
//	return sqlite3_sql(m_stmt);
//}

//void adnd::templates::metadata::query_manager::query_data()

adnd::templates::wizard_spell::wizard_spell()
{
}

adnd::templates::wizard_spell::wizard_spell(const adnd::defs::spells::wizard_spell& spellId)
{
	auto res = metadata::get_instance().fetch<short, std::string, short>(metadata::query::wizard_spell_info);
	auto info = res.filter<short>(static_cast<short>(spellId)).run().first();

	m_id = static_cast<defs::spells::wizard_spell>(std::get<0>(info));
	m_name = std::get<1>(info);
	m_level = std::get<2>(info);

	auto resSchools = metadata::get_instance().fetch<short>(metadata::query::wizard_spell_schools);
	auto rs = resSchools.filter<short>(static_cast<short>(spellId)).run();
	for (auto r : rs)
	{
		m_schools.emplace(static_cast<defs::spells::school>(std::get<0>(r)));
	}
}

adnd::templates::priest_spell::priest_spell()
{
}

adnd::templates::priest_spell::priest_spell(const adnd::defs::spells::priest_spell& spellId)
{
	auto res = metadata::get_instance().fetch<short, std::string, short>(metadata::query::priest_spell_info);
	auto info = res.filter<short>(static_cast<short>(spellId)).run().first();

	m_id = static_cast<defs::spells::priest_spell>(std::get<0>(info));
	m_name = std::get<1>(info);
	m_level = std::get<2>(info);

	auto resSpheres = metadata::get_instance().fetch<short>(metadata::query::priest_spell_spheres);
	auto rs = resSpheres.filter<short>(static_cast<short>(spellId)).run();
	for (auto r : rs)
	{
		m_spheres.emplace(static_cast<defs::spells::sphere>(std::get<0>(r)));
	}
}

adnd::templates::school_of_magic::school_of_magic()
	: m_id(defs::spells::school::none)
{
}

adnd::templates::school_of_magic::school_of_magic(const defs::spells::school& id)
{
	auto res = metadata::get_instance().fetch<short, std::string, std::string, uint32_t, short, short>(metadata::query::school_of_magic_info);
	auto info = res.filter<short>(static_cast<short>(id)).run().first();

	m_id = static_cast<defs::spells::school>(std::get<0>(info));
	m_name = std::get<1>(info);
	m_alias = std::get<2>(info);
	m_wizardClassId = static_cast<defs::character_class>(std::get<3>(info));

	auto skl = static_cast<defs::character_skill>(std::get<4>(info));
	m_requisite = skills::character_skill_value(skl, std::get<5>(info));

	auto resOpposites = metadata::get_instance().fetch<short>(metadata::query::school_of_magic_opposites);
	auto opposites = resOpposites.filter<short>(static_cast<short>(id)).run();
	for (auto o : opposites)
	{
		m_opposites.emplace(static_cast<defs::spells::school>(std::get<0>(o)));
	}
}

bool adnd::templates::school_of_magic::is_included(const defs::spells::wizard_spell& id)
{
	auto t = metadata::get_instance().get_wizard_spell_template(id);
	for (auto s : t.schools())
		if (m_id == s)
			return false;

	return true;
}

bool adnd::templates::school_of_magic::is_opposite(const defs::spells::wizard_spell& id)
{
	auto t = metadata::get_instance().get_wizard_spell_template(id);
	for (auto s : t.schools())
		if (m_opposites.find(s) != m_opposites.end())
			return false;

	return true;
}

bool adnd::templates::school_of_magic::is_neutral(const defs::spells::wizard_spell& id)
{
	return !is_included(id) && !is_opposite(id);
}


adnd::templates::sphere_of_influence::sphere_of_influence()
	: m_id(defs::spells::sphere::none)
{
}

adnd::templates::sphere_of_influence::sphere_of_influence(const defs::spells::sphere& id)
{
	auto res = metadata::get_instance().fetch<short, std::string>(metadata::query::sphere_of_influence_info);
	auto info = res.filter<short>(static_cast<short>(id)).run().first();

	m_id = static_cast<defs::spells::sphere>(std::get<0>(info));
	m_name = std::get<1>(info);
}

adnd::templates::monster::monster()
	: m_id(defs::monsters::monster::none), m_isUndead(false), m_isElemental(false), m_isDragon(false),
	m_turnableAs(defs::turn_ability::turn_none), m_element(-1), m_dragonType(-1)
{
}

adnd::templates::monster::monster(const defs::monsters::monster& id)
	: m_id(id)
{
	auto res = metadata::get_instance().fetch<short, std::string, char, char, char, short, short, short, short, short, short, short, short, short, short, short, short, short, short>(metadata::query::monster_info);
	auto info = res.filter<short>(static_cast<short>(id)).run().first();

	m_name = std::get<1>(info);
	m_isUndead = std::get<2>(info) == 'Y';
	m_isElemental = std::get<3>(info) == 'Y';
	m_isDragon = std::get<4>(info) == 'Y';
	m_turnableAs = static_cast<defs::turn_ability>(std::get<5>(info));
	m_element = std::get<6>(info);
	m_dragonType = std::get<7>(info);

	m_climate = std::get<8>(info);
	m_terrain = std::get<9>(info);
	m_frequency = static_cast<defs::monsters::frequency>(std::get<10>(info));
	m_organisation = std::get<11>(info);
	m_activityCycle = std::get<12>(info);
	m_diet = std::get<13>(info);
	m_intelligence = static_cast<defs::monsters::intelligence>(std::get<14>(info));
	m_alignment = static_cast<defs::moral_alignment>(std::get<15>(info));
	m_morale = static_cast<defs::monsters::morale>(std::get<16>(info));
	m_moraleFrom = std::get<17>(info);
	m_moraleTo = std::get<18>(info);
}