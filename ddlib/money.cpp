#include "stdafx.h"

#include "money.h"
#include "exceptions.h"

#include <cstdlib>
#include <math.h>


adnd::money::amount::amount()
	: m_amount(0), m_currency(adnd::defs::coin::gold)
{
}

adnd::money::amount::amount(const defs::coin& c)
	: m_amount(0), m_currency(c)
{
}

adnd::money::amount::amount(const defs::coin& c, const uint32_t& amount)
	: m_amount(amount), m_currency(c)
{
}

adnd::money::amount::~amount()
{
}

// Amount is assumed to be expressed in the same currency as the left-side operand
adnd::money::amount& adnd::money::amount::operator= (const uint32_t amount)
{
	m_amount = amount;
	return (*this);
}

// Amount is assumed to be expressed in the same currency as the left-side operand
adnd::money::amount& adnd::money::amount::operator+= (const uint32_t amount)
{
	m_amount += amount;
	return (*this);
}

// Amount is assumed to be expressed in the same currency as the left-side operand
adnd::money::amount& adnd::money::amount::operator-= (const uint32_t amount)
{
	if (m_amount - amount < 0)
		throw std::exception("Amount not available", static_cast<int>(m_amount));

	m_amount -= amount;
	return (*this);
}

// Amount is assumed to be expressed in the same currency as the left-side operand
bool adnd::money::amount::operator== (const uint32_t& amount) const
{
	return m_amount == amount;
}

// Amount is assumed to be expressed in the same currency as the left-side operand
bool adnd::money::amount::operator< (const uint32_t& amount) const
{
	return m_amount < amount;
}

// Amount is assumed to be expressed in the same currency as the left-side operand
bool adnd::money::amount::operator<= (const uint32_t& amount) const
{
	return m_amount <= amount;
}

// Amount is assumed to be expressed in the same currency as the left-side operand
bool adnd::money::amount::operator>= (const uint32_t& amount) const
{
	return m_amount >= amount;
}

// Amount is assumed to be expressed in the same currency as the left-side operand
bool adnd::money::amount::operator> (const uint32_t& amount) const
{
	return m_amount > amount;
}

// Amount is assumed to be expressed in the same currency as the left-side operand
bool adnd::money::amount::operator!= (const uint32_t& amount) const
{
	return m_amount != amount;
}

// If expressed in a different currency, amount is adjusted accordingly
adnd::money::amount& adnd::money::amount::operator= (const amount& amount)
{
	if (m_currency == amount.currency())
		m_amount = amount;
	else
	{
		auto t = adnd::templates::metadata::get_instance().get_coin_template(amount.currency());
		m_amount = static_cast<uint32_t>(amount.value() * t.to(m_currency));
	}
	return (*this);
}

// If expressed in a different currency, amount is adjusted accordingly
adnd::money::amount& adnd::money::amount::operator+= (const amount& amount)
{
	if (m_currency == amount.currency())
		m_amount += amount;
	else
	{
		auto t = adnd::templates::metadata::get_instance().get_coin_template(amount.currency());
		m_amount += static_cast<uint32_t>(amount.value() * t.to(m_currency));
	}
	return (*this);
}

// If expressed in a different currency, amount is adjusted accordingly
adnd::money::amount& adnd::money::amount::operator-= (const amount& amount)
{
	uint32_t val = 0;
	if (m_currency == amount.currency())
		val = amount;
	else
	{
		auto t = adnd::templates::metadata::get_instance().get_coin_template(amount.currency());
		val = static_cast<uint32_t>(amount.value() * t.to(m_currency));
	}
	if (m_amount - val < 0)
		throw std::exception("Amount not available", static_cast<int>(m_amount));

	m_amount -= val;
	return (*this);
}

/**	If expressed in a different currency, amount is adjusted accordingly
  *	Comparison occurs in the least segnificant currency so as an integer conversion is always possible
  */
bool adnd::money::amount::operator== (const amount& amount) const
{
	return as<defs::coin::copper>() == amount.as<defs::coin::copper>();
}

/**	If expressed in a different currency, amount is adjusted accordingly
  *	Comparison occurs in the least segnificant currency so as an integer conversion is always possible
  */
bool adnd::money::amount::operator< (const amount& amount) const
{
	return as<defs::coin::copper>() < amount.as<defs::coin::copper>();
}

/**	If expressed in a different currency, amount is adjusted accordingly
  *	Comparison occurs in the least segnificant currency so as an integer conversion is always possible
  */
bool adnd::money::amount::operator<= (const amount& amount) const
{
	return as<defs::coin::copper>() <= amount.as<defs::coin::copper>();
}

/**	If expressed in a different currency, amount is adjusted accordingly
  *	Comparison occurs in the least segnificant currency so as an integer conversion is always possible
  */
bool adnd::money::amount::operator>= (const amount& amount) const
{
	return as<defs::coin::copper>() >= amount.as<defs::coin::copper>();
}

/**	If expressed in a different currency, amount is adjusted accordingly
  *	Comparison occurs in the least segnificant currency so as an integer conversion is always possible
  */
bool adnd::money::amount::operator> (const amount& amount) const
{
	return as<defs::coin::copper>() > amount.as<defs::coin::copper>();
}

/**	If expressed in a different currency, amount is adjusted accordingly
  *	Comparison occurs in the least segnificant currency so as an integer conversion is always possible
  */
bool adnd::money::amount::operator!= (const amount& amount) const
{
	return as<defs::coin::copper>() != amount.as<defs::coin::copper>();
}

double adnd::money::amount::as(const defs::coin& currencyTo) const
{
	auto t = adnd::templates::metadata::get_instance().get_coin_template(m_currency);
	return static_cast<uint32_t>(std::trunc(t.to(currencyTo) * m_amount));
}

uint32_t adnd::money::amount::value() const
{
	return static_cast<uint32_t>(m_amount);
}


const adnd::defs::coin& adnd::money::amount::currency() const
{
	return m_currency;
}


void adnd::money::amount::replace(const money::amount& amt)
{
	replace(amt.currency(), amt.value());
}

void adnd::money::amount::replace(const defs::coin& currency, const uint32_t& amount)
{
	m_currency = currency;
	m_amount = amount;
}




adnd::money::money_bag::money_bag()
{
	//db::sqlite_db::iterator it, end;
	//adnd::templates::metadata::get_instance().get_exchange_rates(it, end);
	//
	//while (it != end)
	//{
	//	auto currencyFrom = static_cast<defs::coin>(adnd::templates::utils::parse<uint16_t>(it, 1));
	//	auto currencyTo = static_cast<defs::coin>(adnd::templates::utils::parse<uint16_t>(it, 2));
	//	auto rate = adnd::templates::utils::parse<double>(it, 3);
	//
	//	m_rates[currencyFrom][currencyTo] = rate;
	//
	//	++it;
	//}
}

adnd::money::money_bag::~money_bag()
{
}

void adnd::money::money_bag::init()
{
	for (auto c : { defs::coin::copper, defs::coin::silver, defs::coin::electrum, defs::coin::gold, defs::coin::platinum })
	{
		m_money[c].replace(c, 0);
	}
}

adnd::money::amount& adnd::money::money_bag::get(const defs::coin& currency)
{
	if (m_money.find(currency) == m_money.end())
	{
		m_money.emplace(currency, adnd::money::amount(currency, 0));
	}
	return m_money[currency];
}

//const double& adnd::money::money_bag::rate(const defs::coin& from, const defs::coin& to)
//{
//	return m_rates[from][to];
//}


adnd::money::money_bag& adnd::money::money_bag::operator+= (const adnd::money::amount& amount)
{
	m_money[amount.currency()] += amount;
	return (*this);
}

adnd::money::money_bag& adnd::money::money_bag::operator-= (const adnd::money::amount& amt)
{
	subtract(amt.currency(), amt);
	return (*this);
}

void adnd::money::money_bag::add(const defs::coin& currency, uint32_t amt)
{
	m_money[currency] += amt;
}

void adnd::money::money_bag::add(const money::amount& amt)
{
	m_money[amt.currency()] += amt.value();
}

//void adnd::money::money_bag::add(const adnd::treasure::gem& g)
//{
//	m_gems[g.uid()] = g;
//}
//
//void adnd::money::money_bag::add(const adnd::treasure::object_of_art& g)
//{
//	m_gems[g.uid()] = g;
//}

void adnd::money::money_bag::subtract(const amount& amt)
{
	subtract(amt.currency(), amt);
}

void adnd::money::money_bag::subtract(const defs::coin& currency, uint32_t amt)
{
	if (!check_availability(amount(currency, amt)))
		throw std::exception("Not enough money", amt);

	if (m_money[currency] >= amt)
		m_money[currency] -= amt;
	else
	{	//Look for other currencies...
		
		for (auto it = m_money.begin(); it != m_money.end() && amt > 0; ++it)
		{
			auto ccyTempl = templates::metadata::get_instance().get_coin_template(it->first);
			double rate = ccyTempl.to(currency);

			if (rate <= 1) // currency of lesser value
			{
				ldiv_t res = std::ldiv(it->second.value(), static_cast<long>(1/rate));
				amt -= res.quot;
				m_money[it->first] = res.rem;
			}
			else // currency of greater value
			{
				auto ccyAmt = static_cast<uint32_t>(m_money[it->first] * rate);
				m_money[it->first] = 0; // Money turned into another currency
				if (ccyAmt >= amt)
				{
					m_money[currency] = ccyAmt - amt;
					amt = 0;
				}
				else
				{
					amt -= ccyAmt;
				}
			}
		}
	}
}

//void adnd::money::money_bag::remove(const uint32_t& gemUid)
//{
//	m_gems.erase(gemUid);
//}

void adnd::money::money_bag::normalise()
{
	for (auto ccy : { defs::coin::copper, defs::coin::silver, defs::coin::electrum, defs::coin::gold, defs::coin::platinum })
	{
		if (m_money[ccy].value() == 0)
			continue;

		auto amtList = normalise(ccy, m_money[ccy].value());
		if (!amtList.empty())
			m_money[ccy] = 0;
		for (auto& a : amtList)
		{
			m_money[a.currency()] += a.value();
		}
	}
}

std::list<adnd::money::amount> adnd::money::money_bag::normalise(const defs::coin& currency, uint32_t amt)
{
	std::list<adnd::money::amount> result;

	auto t = templates::metadata::get_instance().get_coin_template(currency);
	for (auto ccy : { defs::coin::platinum, defs::coin::gold, defs::coin::electrum, defs::coin::silver, defs::coin::copper })
	{
		double rate = t.to(ccy);
		if (rate < 1 && amt > 1/rate)
		{
			ldiv_t res = std::ldiv(amt, static_cast<long>(1/rate));
			if (res.quot > 0)
			{
				result.push_back(amount(ccy, res.quot));
				amt = res.rem;
			}
		}
	}
	if (amt > 0)
		result.push_back(amount(currency, amt));
	return result;
}

//void adnd::money::money_bag::subtract(const defs::coin& currency, uint32_t amt)
//{
//	if (!check_availability(amount(currency, amt)))
//		throw std::exception("Not enough money", amt);
//
//	if (m_money[currency] >= amt)
//		m_money[currency] -= amt;
//	else
//	{	//Look for other currencies...
//
//		auto t = templates::metadata::get_instance().get_coin_template(currency);
//		amount remaining(defs::coin::copper, amt * t.to(defs::coin::copper));
//		for (auto it = m_money.begin(); it != m_money.end() && remaining.value() > 0; ++it)
//		{
//			if (m_money[it->first].value() > 0)
//			{
//				uint32_t asCP = m_money[it->first].as(defs::coin::copper);
//
//				if (asCP >= remaining.value())
//				{
//					uint32_t diff = asCP - remaining.value();
//					auto tm = templates::metadata::get_instance().get_coin_template(it->first);
//					lldiv_t res = std::lldiv(diff, tm.to(defs::coin::copper));
//					m_money[it->first] = res.quot;
//					m_money[defs::coin::copper] += res.rem;
//					remaining = 0;
//				}
//			}
//		}
//	}
//}

//void adnd::money::money_bag::subtract(const defs::coin& currency, uint32_t amt)
//{
//	if (!check_availability(amount(currency, amt)))
//		throw std::exception("Not enough money", amt);
//
//	if (m_money[currency] >= amt)
//		m_money[currency] -= amt;
//	else
//	{	//Turn into copper pieces, subtract, then calculate the remainder
//		auto t = templates::metadata::get_instance().get_coin_template(currency);
//		amount priceCP(defs::coin::copper, amt * t.to(defs::coin::copper));
//		
//		amount availabilityCP(defs::coin::copper, 0);
//		for (auto m : m_money)
//			availabilityCP += m.second.as<defs::coin::copper>();
//
//		uint32_t remaining = availabilityCP - priceCP;
//	}
//}

const adnd::money::amount& adnd::money::money_bag::copper()
{
	return m_money[defs::coin::copper];
}

const adnd::money::amount& adnd::money::money_bag::silver()
{
	return m_money[defs::coin::silver];
}

const adnd::money::amount& adnd::money::money_bag::electrum()
{
	return m_money[defs::coin::electrum];
}

const adnd::money::amount& adnd::money::money_bag::gold()
{
	return m_money[defs::coin::gold];
}

const adnd::money::amount& adnd::money::money_bag::platinum()
{
	return m_money[defs::coin::platinum];
}

bool adnd::money::money_bag::check_availability(const amount& amt)
{
	uint32_t total = 0;
	for (auto m : m_money)
		total += m.second.as<defs::coin::copper>();

	return amt.as<defs::coin::copper>() <= total;
}

/**
	Normalise a fractional amount of money converting it into an integer amount of a lesser value currency
	(i.e. 1.5 gold pieces become 3 electrum pieces)
*/
adnd::money::amount adnd::money::normalise(const adnd::defs::coin& currency, const double& value)
{
	auto t = templates::metadata::get_instance().get_coin_template(currency);
	for (auto cny : { defs::coin::platinum, defs::coin::gold, defs::coin::electrum, defs::coin::silver, defs::coin::copper })
	{
		if (static_cast<short>(cny) < static_cast<short>(currency))
		{
			double newValue = t.to(cny) * value;
			double intPart = 0.0;
			if (modf(newValue, &intPart) == 0)
			{
				return money::amount(cny, static_cast<uint32_t>(intPart));
			}
		}
	}
	std::stringstream ss;
	ss << "Unable to normalise " << value << " " << t.get_acronym();
	throw std::exception(ss.str().c_str());
}

/**
	Splits a double value in a set of integer currency values (i.e. 1.5 gold pieces become 1 GP and 1 EP)
*/
std::map<adnd::defs::coin, adnd::money::amount> adnd::money::split(const adnd::defs::coin& currency, const double& value)
{
	double currencyValue = value;
	std::map<defs::coin, amount> result;

	if (currencyValue >= 1.0)
	{
		double intPart = 0.0;
		double fracPart = modf(currencyValue, &intPart);

		auto amt = money::amount(currency, static_cast<uint32_t>(intPart));
		result.emplace(currency, amt);
		currencyValue = fracPart;
	}

	auto t = templates::metadata::get_instance().get_coin_template(currency);
	for (auto cny : { defs::coin::platinum, defs::coin::gold, defs::coin::electrum, defs::coin::silver, defs::coin::copper })
	{
		if (static_cast<short>(cny) < static_cast<short>(currency) && currencyValue > 0.0)
		{
			currencyValue *= t.to(cny);

			double intPart = 0.0;
			double fracPart = modf(currencyValue, &intPart);
			if (intPart > 0)
			{
				auto amt = money::amount(cny, static_cast<uint32_t>(intPart));
				result.emplace(cny, amt);
			}
			currencyValue = fracPart;
		}
	}

	if (currencyValue > 0.0)
	{
		std::stringstream ss;
		ss << "Unable to split " << value << " " << t.get_acronym();
		throw std::exception(ss.str().c_str());
	}

	return result;
}
