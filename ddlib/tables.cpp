#include "tables.h"
//#include "stdafx.h"
#include "tables.h"
#include "utils.h"


#pragma once



short adnd::tables::thief_ability::ability(const adnd::defs::thief_ability& type) const
{
	if (type == defs::thief_ability::pick_pockets)
		return std::get<static_cast<int>(adnd::defs::thief_ability::pick_pockets)>(m_abilities);
	else if (type == defs::thief_ability::open_locks)
		return std::get<static_cast<int>(adnd::defs::thief_ability::open_locks)>(m_abilities);
	else if (type == defs::thief_ability::find_remove_traps)
		return std::get<static_cast<int>(adnd::defs::thief_ability::find_remove_traps)>(m_abilities);
	else if (type == defs::thief_ability::move_silently)
		return std::get<static_cast<int>(adnd::defs::thief_ability::move_silently)>(m_abilities);
	else if (type == defs::thief_ability::hide_in_shadows)
		return std::get<static_cast<int>(adnd::defs::thief_ability::hide_in_shadows)>(m_abilities);
	else if (type == defs::thief_ability::hear_noise)
		return std::get<static_cast<int>(adnd::defs::thief_ability::hear_noise)>(m_abilities);
	else if (type == defs::thief_ability::climb_walls)
		return std::get<static_cast<int>(adnd::defs::thief_ability::climb_walls)>(m_abilities);
	else if (type == defs::thief_ability::read_languages)
		return std::get<static_cast<int>(adnd::defs::thief_ability::read_languages)>(m_abilities);
	else
		throw std::exception("Invalid thief ability specified", static_cast<int>(type));
}

const adnd::tables::turn_undead& adnd::tables::turn_ability::turn(const adnd::defs::turn_ability& type) const
{
	if (type == adnd::defs::turn_ability::turn_skeleton || type == adnd::defs::turn_ability::turn_hd1)
		return std::get<static_cast<int>(adnd::defs::turn_ability::turn_skeleton)>(m_score);
	else if (type == adnd::defs::turn_ability::turn_zombie)
		return std::get<static_cast<int>(adnd::defs::turn_ability::turn_zombie)>(m_score);
	else if (type == adnd::defs::turn_ability::turn_ghoul || type == adnd::defs::turn_ability::turn_hd2)
		return std::get<static_cast<int>(adnd::defs::turn_ability::turn_ghoul)>(m_score);
	else if (type == adnd::defs::turn_ability::turn_shadow || type == adnd::defs::turn_ability::turn_hd3 || type == adnd::defs::turn_ability::turn_hd4)
		return std::get<static_cast<int>(adnd::defs::turn_ability::turn_shadow)>(m_score);
	else if (type == adnd::defs::turn_ability::turn_wight || type == adnd::defs::turn_ability::turn_hd5)
		return std::get<static_cast<int>(adnd::defs::turn_ability::turn_wight)>(m_score);
	else if (type == adnd::defs::turn_ability::turn_ghast)
		return std::get<static_cast<int>(adnd::defs::turn_ability::turn_ghast)>(m_score);
	else if (type == adnd::defs::turn_ability::turn_wraith || type == adnd::defs::turn_ability::turn_hd6)
		return std::get<static_cast<int>(adnd::defs::turn_ability::turn_wight)>(m_score);
	else if (type == adnd::defs::turn_ability::turn_mummy || type == adnd::defs::turn_ability::turn_hd7)
		return std::get<static_cast<int>(adnd::defs::turn_ability::turn_mummy)>(m_score);
	else if (type == adnd::defs::turn_ability::turn_spectre || type == adnd::defs::turn_ability::turn_hd8)
		return std::get<static_cast<int>(adnd::defs::turn_ability::turn_spectre)>(m_score);
	else if (type == adnd::defs::turn_ability::turn_vampire || type == adnd::defs::turn_ability::turn_hd9)
		return std::get<static_cast<int>(adnd::defs::turn_ability::turn_vampire)>(m_score);
	else if (type == adnd::defs::turn_ability::turn_ghost || type == adnd::defs::turn_ability::turn_hd10)
		return std::get<static_cast<int>(adnd::defs::turn_ability::turn_ghost)>(m_score);
	else if (type == adnd::defs::turn_ability::turn_lich || type == adnd::defs::turn_ability::turn_hd11_or_more)
		return std::get<static_cast<int>(adnd::defs::turn_ability::turn_lich)>(m_score);
	else if (type == adnd::defs::turn_ability::turn_special)
		return std::get<static_cast<int>(adnd::defs::turn_ability::turn_special)>(m_score);
	else
		throw std::exception("Invalid undead type specified", static_cast<int>(type));
}

std::string adnd::tables::class_info::get_long_name() const
{
	std::string val = std::string(longName, sizeof(longName));
	utils::trim(val);
	return val;
}

std::string adnd::tables::class_info::get_short_name() const
{
	std::string val = std::string(shortName, sizeof(shortName));
	utils::trim(val);
	return val;
}

std::string adnd::tables::class_info::get_acronym() const
{
	std::string val = std::string(acronym, sizeof(acronym));
	utils::trim(val);
	return val;
}

//const adnd::random::die& adnd::tables::class_info::hit_dice(const size_t& index) const
//{
//	return hitDice[index];
//}

const short& adnd::tables::saving_throws::save(const adnd::defs::saving_throws& type) const
{
	if (type == defs::saving_throws::paralysation)
		return std::get<static_cast<int>(adnd::defs::saving_throws::paralysation)>(m_savings);
	else if (type == defs::saving_throws::poison)
		return std::get<static_cast<int>(adnd::defs::saving_throws::poison)>(m_savings);
	else if (type == defs::saving_throws::death_magic)
		return std::get<static_cast<int>(adnd::defs::saving_throws::death_magic)>(m_savings);
	else if (type == defs::saving_throws::rod)
		return std::get<static_cast<int>(adnd::defs::saving_throws::rod)>(m_savings);
	else if (type == defs::saving_throws::staff)
		return std::get<static_cast<int>(adnd::defs::saving_throws::staff)>(m_savings);
	else if (type == defs::saving_throws::wand)
		return std::get<static_cast<int>(adnd::defs::saving_throws::wand)>(m_savings);
	else if (type == defs::saving_throws::petrification)
		return std::get<static_cast<int>(adnd::defs::saving_throws::petrification)>(m_savings);
	else if (type == defs::saving_throws::polymorph)
		return std::get<static_cast<int>(adnd::defs::saving_throws::polymorph)>(m_savings);
	else if (type == defs::saving_throws::breath_weapon)
		return std::get<static_cast<int>(adnd::defs::saving_throws::breath_weapon)>(m_savings);
	else if (type == defs::saving_throws::spell)
		return std::get<static_cast<int>(adnd::defs::saving_throws::spell)>(m_savings);
	else
		throw std::exception("Invalid saving throw specified", static_cast<int>(type));
}

std::string adnd::tables::class_type_info::get_description() const
{
	std::string descr(description, sizeof(description));
	utils::trim(descr);
	return descr;
}

const adnd::random::die adnd::tables::class_type_info::hit_dice() const
{
	//return random::die(hitDice[index]);
	return random::die(hitDice);
}

std::string adnd::tables::race_info::get_name() const
{
	std::string n(name, sizeof(name));
	adnd::utils::trim(n);
	return n;
}

std::string adnd::tables::race_info::get_acronym() const
{
	std::string acro(acronym, sizeof(acronym));
	adnd::utils::trim(acro);
	return acro;
}

std::string adnd::tables::coin_info::get_acronym() const
{
		std::string acro(acronym, sizeof(acronym));
		utils::trim(acro);
		return acro;
}

std::string adnd::tables::coin_info::get_description() const
{
	std::string val(description, sizeof(description));
	utils::trim(val);
	return val;
}