//#include "stdafx.h"
#include "monster.h"
#include "templates.h"

adnd::monster::monster::monster(const uint32_t& uid, const defs::monsters::monster& id, const std::string& name,
						uint32_t monsterType, const short& morale, const std::string& uniqueName)
	: m_uid(uid), m_id(id), m_name(name), m_type(monsterType), m_morale(morale), m_uniqueName(uniqueName)
{
}

adnd::defs::turn_ability adnd::monster::monster::turnable_as()
{
	auto t = templates::metadata::get_instance().get_monster_template(m_id);
	return t.turnable_as();
}

adnd::monster::monsters_den::~monsters_den()
{
	//for (auto m : m_monsters)
	//{
	//	delete m.second;
	//	m_monsters.erase(m.first);
	//}
}

adnd::monster::monster* adnd::monster::monsters_den::spawn(const defs::monsters::monster& id, const std::string& uniqueName)
{
	auto t = templates::metadata::get_instance().get_monster_template(id);
	std::string monsterName = (uniqueName.empty()) ? t.name() : uniqueName;

	uint32_t monsterType = 0;
	if (t.is_undead())
		monsterType |= type::undead;
	if (t.is_dragon())
		monsterType |= type::dragon;
	if (t.is_elemental())
		monsterType |= type::elemental;

	std::random_device rd;
	std::mt19937 generator(rd());
	std::uniform_int_distribution<short> distribution(t.morale_from(), t.morale_to());

	short morale = distribution(generator);

	monster *m = new monster(m_lastId, t.id(), t.name(), monsterType, morale, monsterName);
	m_monsters[m_lastId] = m;
	m_lastId++;

	return m;
}