#pragma once

//#include <utility>
#include <iostream>
#include <functional>
#include <cctype>
#include <sstream>
#include <iomanip>
#include <iterator>


namespace adnd
{
	namespace utils
	{
		enum class justify
		{
			left,
			right
		};

		template <class T, bool zeroTerminate = false>
		static void write(char* text, size_t textLength, const T& t, char padding = ' ', justify align = justify::right)
		{
			auto inputLength = (zeroTerminate) ? textLength - 1 : textLength;
			std::stringstream s;
			s << std::setw(inputLength) << std::setfill(padding) << ((align == justify::right) ? std::right : std::left) << t;
			memcpy_s(text, textLength, s.str().c_str(), inputLength);
			if (zeroTerminate)
				text[inputLength] = 0;
		}

		template <class T>
		static void write(std::string &text, const T& t, char padding = ' ', justify align = justify::right)
		{
			std::stringstream s;
			s << t;
			text = s.str();
		}

		static void ltrim(std::string &s)
		{
			s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int c) {return !std::isspace(c); }));
		}

		static void rtrim(std::string &s)
		{
			s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) { return !std::isspace(ch); }).base(), s.end());
		}

		static void trim(std::string &s)
		{
			ltrim(s);
			rtrim(s);
		}

		template <class T>
		static T parse(const char *ascii, size_t length, const T& defaultValue)
		{
			T val(defaultValue);
			std::string data(ascii, length);
		
			std::stringstream s(data);
			if (length > 0)
				if constexpr (std::is_convertible_v<T, std::string>)
					val = data;
				else
					s >> val;
			return val;
		}

		struct case_insensitive_char_traits : public std::char_traits<char>
		{
			static bool eq(char c1, char c2) { return toupper(c1) == toupper(c2); }
			static bool ne(char c1, char c2) { return toupper(c1) != toupper(c2); }
			static bool lt(char c1, char c2) { return toupper(c1) < toupper(c2); }

			static int compare(const char* s1, const char* s2, size_t n)
			{
				while (n-- != 0)
				{
					if (toupper(*s1) < toupper(*s2)) return -1;
					if (toupper(*s1) > toupper(*s2)) return 1;
					++s1;
					++s2;
				}
				return 0;
			}
			static const char* find(const char* s, int n, char a)
			{
				while (n-- > 0 && toupper(*s) != toupper(a))
				{
					++s;
				}
				return s;
			}
		};

		static int compare_no_case(const std::string &a, const std::string &b)
		{
			std::basic_string<char, case_insensitive_char_traits> ia = a.c_str();
			std::basic_string<char, case_insensitive_char_traits> ib = b.c_str();
			return ia.compare(ib);
		}
	};
};