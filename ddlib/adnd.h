#pragma once

#include "defs.h"						// basic definitions
#include "skills.h"						// character_skill_value
#include "random.h"						// dice and random events
#include "templates.h"					// character template classes, specifications and meta data
#include "equipment.h"					// items, weapons, ...
#include "inventory.h"					// collected items, wearables and weapons
#include "money.h"						// money bag, coins and exchange rates
#include "treasure.h"					// treasures, gems and magical items
#include "monster.h"					// monsters & Co.

#include <string>
#include <type_traits>
#include <map>
#include <bitset>
#include <cstdint>
#include <iostream>
#include <numeric>



namespace adnd
{
	namespace library
	{
		static const char* version = "0.9.3b";
		static const char* author = "Marco Carosio";
		static const char* desc = "Ddlib is an open C++ role playing games library. It aims to mimic the ruleset of Advanced Dungeons & Dragons 2nd Edition";
		static const char* first_created = "2018-07-13";
		static const char* last_commit = "2020-10-21";
		static const char* link_home = "https://gitlab.com/mcarosio/ddlib";
		static const char* link_ssh = "git@gitlab.com:mcarosio/ddlib.git";
		static const char* link_https = "https://gitlab.com/mcarosio/ddlib.git";
		static const char* link_doc = "https://gitlab.com/mcarosio/ddlib/-/wikis/home";
		static const char* link_license = "https://gitlab.com/mcarosio/ddlib/-/blob/master/LICENSE";
	}

	enum class hit_result
	{
		missed,
		hit,
		critical_hit,
		critical_failure
	};

	struct attack_result
	{
		attack_result() : hitResult(hit_result::missed), damage(0) {}

		hit_result	hitResult;
		short		damage;
	};

	class hp_sequence
	{
	public:
		hp_sequence() : m_currentHP(0), m_maxHP(0), m_constitutionAdjustment(0){}
		hp_sequence& operator+=(const short& hp)
		{
			m_hp.push_back(hp);
			m_currentHP += hp;
			m_maxHP += hp;

			return (*this);
		}

		short size() { return static_cast<short>(m_hp.size()); }
		short max();
		operator short() { return m_currentHP; }
		bool alive() { return m_currentHP > 0; }
		double health();
		void drain(const short& levels = 1);
		void heal(const short& hp);
		void wound(const short& hp);
		void raise();
		void raise_full();

		auto begin() { return m_hp.begin(); }
		auto end() { return m_hp.end(); }

		short operator[] (const size_t& idx)
		{
			if (idx < 0 || idx >= m_hp.size())
				throw std::exception("Index out of bound", static_cast<int>(idx));

			return m_hp[idx];
		}

	protected:
		std::vector<short>	m_hp;
		short				m_currentHP;
		short				m_maxHP;
		short				m_constitutionAdjustment;
	};

	class hit_points
	{
	public:
		hit_points(const defs::character_class& cls);

		void add(const defs::character_class& cls, const short& hp);
		void add(const defs::character_class& cls, const std::vector<short>& hps);
		short size(const defs::character_class& cls);
		short size() { return static_cast<short>(m_hps.size()); }
		short max();
		short current_hps();
		bool alive() { return m_currentHP > 0; }
		void drain(const short& levels = 1);

		void heal(const short& hp);
		void wound(const short& hp);
		void raise();
		void raise_full();
		double health();
		auto begin() { return m_hps.begin(); }
		auto end() { return m_hps.end(); }
		
		hp_sequence& operator[] (const defs::character_class& cls)
		{
			if (m_hps.find(cls) == m_hps.end())
				throw std::exception("Class not available", static_cast<unsigned short>(cls));
			return m_hps[cls];
		}

		short limit();
		short limit(const defs::character_class& cls);

		short get_constitution_adjustment();
		void set_constitution_adjustment(const short& constitutionAdjustment);

	protected:
		std::map<defs::character_class, hp_sequence>	m_hps;
		short											m_currentHP;
		short											m_constitutionAdjustment;

		short get_bonus_hps();
		short weighted_hp(const short& hp, const bool& firstLevel = false);
		//void add_hp(const defs::character_class& cls, const short& hp);

		bool drain_overflow();
	};

	class hand_slot
	{
	public:
		hand_slot(const defs::hand_slot_type& slotType = defs::hand_slot_type::primary) : m_type(slotType), m_item(nullptr)
		{}
		~hand_slot() {}

		hand_slot& operator= (equipment::item* item)
		{
			if (m_item != nullptr)
				delete m_item;
			m_item = item;
			return (*this);
		}

		bool busy() { return m_item != nullptr; }
		equipment::item* drop();
		equipment::item* item() { return m_item; }
		template <class _ItemType>
		_ItemType* item() { return static_cast<_ItemType*>(m_item); }

	protected:
		defs::hand_slot_type		m_type;
		equipment::item*			m_item;

	};

	class class_level
	{
	public:

		enum class experience_shift
		{
			xp_up,
			xp_down,
			level_up,
			level_down
		};

		struct level_score
		{
			level_score();
			level_score(short lvl, long xp, const defs::character_class& cls);
			level_score& operator=(level_score& p);
			short& level();
			long get_xp();
			void set_xp(const long& xpValue);

			defs::character_class& get_class();

			short& level_limit();
			double& xp_bonus();
			long compute_xp(const long& value);

		protected:
			std::pair<short, long>		m_score;
			defs::character_class		m_class;
			short						m_levelLimit;
			double						m_experienceBonus;
		};

		class_level(const defs::character_class& cls);

		class_level& operator+=(const long& xp);
		class_level& operator-=(const long& xp);
		class_level& operator=(const short& lvl);

		friend std::ostream& operator<<(std::ostream& out, class_level& lvl) 
		{
			bool first = true;
			for (auto& p : lvl.m_levels)
			{
				if (first)
				{
					out << p.second.level();
					first = false;
				}
				else
					out << "/" << p.second.level();
			}
			return out;
		}

		auto begin() { return m_levels.begin(); }
		auto end() { return m_levels.end(); }

		level_score& operator[] (const defs::character_class& cls);
		level_score& operator[] (const short& idx);
		size_t size();

	protected:
		std::map<defs::character_class, level_score>	m_levels;
		defs::character_class							m_class;

		void add_xp(const long& xp);
		short get_level(const defs::character_class& cls);
		short get_level();
		std::map<defs::character_class, short> get_levels();
		long get_xp(const defs::character_class& cls);
		long get_xp();
		void up(const short& levelIncrement);
		void down(const short& levelDecrement);
		void up(const defs::character_class& cls, const short& levelIncrement);
		void down(const defs::character_class& cls, const short& levelDecrement);

		friend class character;

	private:
		void up();
		void down();
		void up(const defs::character_class& cls, level_score& maxLevel);
		void down(const defs::character_class& cls, level_score& maxLevel);
	};

	class thief_skills
	{
	public:
		thief_skills() {}
		~thief_skills() {}

		void set_base_values(const short& pick, const short& open, const short& traps, const short& move, const short& hide, const short& hear, const short& climb, const short& read);

		//thief_skills& operator+=(const tables::thief_ability& advancement);
		void add(const short& pick, const short& open, const short& traps, const short& move, const short& hide, const short& hear, const short& climb, const short& read);
		short ability(const short& level, const defs::thief_ability& type) const;
		size_t size() { return m_advancements.size(); }
		void level_down(const short& levels);

	protected:
		std::vector<tables::thief_ability>	m_advancements;
	};

	class proficiency_slots
	{
	public:
		proficiency_slots(const defs::character_class& cls);
		~proficiency_slots() {}

		proficiency_slots& operator+=(const defs::equipment::item& item);
		bool has_free_slots(const defs::proficiency::type& type)
		{
			if (type == defs::proficiency::type::weapon)
				return m_usedSlots < m_weaponSlots.size();
			else if (type == defs::proficiency::type::non_weapon)
				return true;
			else
				return false;
		}

		defs::proficiency::level proficiency_level(const defs::equipment::item& item);
		short hit_modifier(const defs::equipment::item& item);
		short damage_modifier(const defs::equipment::item& item);

		short level_change(const short& level);

	protected:
		defs::character_class				m_class;
		std::vector<defs::equipment::item>	m_weaponSlots;
		short								m_weaponInitialSlots;
		short								m_weaponNumLevels;
		short								m_usedSlots;
		short								m_hitPenalty;
	};

	class character
	{
	public:
		character(const std::string& name, const defs::character_class& charClass, const defs::character_race& charRace,
			const defs::character_sex& sex, const defs::moral_alignment& alignment, bool maximizeHP = false,
			const defs::deities::deity& deityId = defs::deities::deity::none);
		virtual ~character(void) {}

		////////////////////////////////////////////////////////////////////////////////
		// Properties
		////////////////////////////////////////////////////////////////////////////////
		const std::string& get_name() { return m_name; }
		const defs::character_class& get_class() { return m_class.id(); }
		std::vector<defs::character_class_type> get_class_type()
		{
			return m_class.types();
		}
		const defs::character_race& get_race() { return m_race; }
		const short get_current_hp() { return m_hitPoints.current_hps(); }
		const uint16_t& height()			{ return m_height; }
		const uint16_t& weight()			{ return m_weight; }
		const uint16_t& age()				{ return m_age; }
		const uint16_t& life_expectancy()	{ return m_lifeExpectancy; }

		money::money_bag& money() { return m_moneyBag; }

		////////////////////////////////////////////////////////////////////////////////
		// Skills: get values
		////////////////////////////////////////////////////////////////////////////////
		void get_skill_values(short& strength, short& dexterity, short& constitution, short& intelligence, short& wisdom, short& charisma, short& exceptionalStrength)
		{
			strength = m_skills[defs::character_skill::strength].value();
			dexterity = m_skills[defs::character_skill::dexterity].value();
			constitution = m_skills[defs::character_skill::constitution].value();
			intelligence = m_skills[defs::character_skill::intelligence].value();
			wisdom = m_skills[defs::character_skill::wisdom].value();
			charisma = m_skills[defs::character_skill::charisma].value();

			if (strength == 18 && m_class.is_type_of(defs::character_class_type::warrior))
				exceptionalStrength = m_skills[defs::character_skill::strength].exceptional();
		}
		short strength(short& excStr);
		short dexterity();
		short constitution();
		short intelligence();
		short wisdom();
		short charisma();
		const skills::character_skill_value& strength_value();
		const skills::character_skill_value& dexterity_value();
		const skills::character_skill_value& constitution_value();
		const skills::character_skill_value& intelligence_value();
		const skills::character_skill_value& wisdom_value();
		const skills::character_skill_value& charisma_value();

		////////////////////////////////////////////////////////////////////////////////
		// Skills: set values, generate initial values
		////////////////////////////////////////////////////////////////////////////////
		bool check_minimum_requisites(const adnd::defs::character_skill& skill, const adnd::skills::character_skill_value& value);
		bool set_skill(const defs::character_skill& skill, const skills::character_skill_value& value);
		bool set_skill(const adnd::defs::character_skill& skill, const short& value);
		bool set_skill(const adnd::defs::character_skill& skill, const short& value, const short& exceptionalStrength);
		bool set_skills(const skills::character_skill_value& strength, const skills::character_skill_value& dexterity, const skills::character_skill_value& constitution,
			const skills::character_skill_value& intelligence, const skills::character_skill_value& wisdom, const skills::character_skill_value& charisma);
		bool set_skills(const skills::character_skill_set& set);
		void generate_skills(const skills::generation_method& genMethod = skills::generation_method::standard);
		
		short points()
		{
			short result = 0;
			for (auto& s : m_skills)
				result += s.second.value();
			return result;
		}

		////////////////////////////////////////////////////////////////////////////////
		// Combat
		////////////////////////////////////////////////////////////////////////////////
		bool can_use(const defs::equipment::item& item);
		void grab(const defs::hand_slot_type& handSlot, equipment::item* item);
		uint32_t release(const defs::hand_slot_type& handSlot, equipment::item*& item);
		uint32_t release(const defs::hand_slot_type& handSlot);
		short punch();
		adnd::tables::attack_rate get_attack_rate(const adnd::defs::weapons::attack_type& attackType, const defs::equipment::item& item = defs::equipment::item::none);
		attack_result attack(const defs::hand_slot_type& handSlot, const short& targetArmourClass, const short& targetDistance, const defs::weapons::target_size& targetSize = defs::weapons::target_size::small_medium);
		hit_result try_hit(const defs::hand_slot_type& handSlot, const short& targetArmourClass, const short& targetDistance);
		short initiative(const defs::hand_slot_type& handSlot);
		void swap_item(const defs::hand_slot_type& handSlot, const uint32_t& itemUid);
		defs::proficiency::level add_proficiency(const defs::equipment::item& item);

		////////////////////////////////////////////////////////////////////////////////
		// Ageing
		////////////////////////////////////////////////////////////////////////////////
		bool grow_old(const short& years);
		bool rejuvenate(const short& years);
		const adnd::defs::character_age ageing();

		////////////////////////////////////////////////////////////////////////////////
		// Health, HPs, cure/cause wounds
		////////////////////////////////////////////////////////////////////////////////
		bool alive();
		double health();
		short max_hp();
		void heal(const short& hp);
		bool wound(const short& hp);
		void raise();
		void raise_full();

		////////////////////////////////////////////////////////////////////////////////
		// Levels up/down
		////////////////////////////////////////////////////////////////////////////////
		class_level& level() { return m_level; }
		short get_level(const defs::character_class& cls) { return m_level.get_level(cls); }
		short get_level() { return m_level.get_level(); }
		std::map<defs::character_class, short> get_levels() { return m_level.get_levels(); }
		long get_xp(const defs::character_class& cls) { return m_level.get_xp(cls); }
		long get_xp() { return m_level.get_xp(); }
		void add_xp(const long& xp);

		void gain_level(const short& levelIncrement);
		void drain_level(const short& levelDecrement);
		void gain_level(const defs::character_class& cls, const short& levelIncrement);
		void drain_level(const defs::character_class& cls, const short& levelDecrement);

		////////////////////////////////////////////////////////////////////////////////
		// Abilities
		////////////////////////////////////////////////////////////////////////////////
		bool save_against(const defs::saving_throws& saveThrow);
		const tables::turn_undead turn_undead(const defs::turn_ability& type);
		bool do_thievery(const defs::thief_ability& abil);
		void improve_thief_abilities(const short& pick, const short& open, const short& traps, const short& move, const short& hide, const short& hear, const short& climb, const short& read);
		void improve_bard_abilities(const short& pick, const short& hear, const short& climb, const short& read);
		spells::spell_book& spell_book();
		spells::holy_symbol& holy_symbol();


		////////////////////////////////////////////////////////////////////////////////
		// Calculated properties
		////////////////////////////////////////////////////////////////////////////////
		short armour_class();
		short thaco();

		//void gain_experience(const long& xp);
		//void lose_experience(const long& xp);
		//void level_up(const short& levels);
		//void level_down(const short& levels);

		////////////////////////////////////////////////////////////////////////////////
		// Inventory
		////////////////////////////////////////////////////////////////////////////////
		inventory::inventory_bag& inventory_bag() { return m_inventory; }
		double encumbrance();
		short movement_rate();
		void pillage(treasure::treasure& tr);
		//bool can_afford(const defs::equipment::item& itemId);
		//equipment::item* buy(const defs::equipment::item& itemId);

		//template<class _ItemType>
		//_ItemType* buy_as(const defs::equipment::item& itemId) { return static_cast<_ItemType*>(buy(itemId)); }
		//equipment::item* sell(const uint32_t& itemUid);

		////////////////////////////////////////////////////////////////////////////////
		// Religion and deities
		////////////////////////////////////////////////////////////////////////////////
		//void set_worshipped_deity(const defs::deities::deity& deityId);
		const defs::deities::deity& deity() { return m_deity; }

	protected:
		std::string							m_name;
		//defs::character_class				m_class;
		templates::character_class_id		m_class;
		defs::character_race				m_race;
		defs::character_sex					m_sex;
		defs::moral_alignment				m_alignment;
		skills::character_skill_set			m_skills;
		hit_points							m_hitPoints;
		class_level							m_level;
		std::map<defs::hand_slot_type, hand_slot>
											m_hands;
		inventory::inventory_bag			m_inventory;
		uint16_t							m_age;
		uint16_t							m_lifeExpectancy;
		uint16_t							m_height;
		uint16_t							m_weight;
		money::money_bag					m_moneyBag;
		defs::deities::deity				m_deity;
		thief_skills						m_thiefAdvancement;
		proficiency_slots					m_weaponProficiencies;
		spells::spell_book					m_spellBook;
		spells::holy_symbol					m_holySymbol;

		////////////////////////////////////////////////////////////////////////////////
		// Class type checks
		////////////////////////////////////////////////////////////////////////////////
		bool is_warrior()	{ return m_class.is_type_of(defs::character_class_type::warrior); }
		bool is_wizard()	{ return m_class.is_type_of(defs::character_class_type::wizard); }
		bool is_priest()	{ return m_class.is_type_of(defs::character_class_type::priest); }
		bool is_rogue()		{ return m_class.is_type_of(defs::character_class_type::rogue); }

		////////////////////////////////////////////////////////////////////////////////
		// Combat
		////////////////////////////////////////////////////////////////////////////////
		bool two_weapons_attack();

		////////////////////////////////////////////////////////////////////////////////
		// Hit modifiers
		////////////////////////////////////////////////////////////////////////////////
		short hit_modifier(const defs::hand_slot_type& handSlot, const short& distance = 0);
		short hit_modifier_for_strength();
		short hit_modifier_for_dexterity();
		short hit_modifier_for_weapon(equipment::item *it);
		short hit_modifier_for_weapon_proficiency(equipment::item *it);
		short hit_modifier_for_missile_weapon_range(equipment::item *it, const short& distance);
		short hit_modifier_for_encumbrance();
		short hit_modifier_for_two_weapons_attack(const defs::hand_slot_type& handSlot);

		////////////////////////////////////////////////////////////////////////////////
		// Damage modifiers
		////////////////////////////////////////////////////////////////////////////////
		short damage_modifier_for_strength();
		//short damage_modifier_for_dexterity();
		short damage_modifier_for_weapon(equipment::item *it);
		short damage_modifier_for_weapon_proficiency(equipment::item *it);
		//short damage_modifier_for_missile_weapon_range(equipment::item *it, const short& distance);
		short damage_modifier_for_encumbrance();

		////////////////////////////////////////////////////////////////////////////////
		// Other modifiers
		////////////////////////////////////////////////////////////////////////////////
		short get_ageing_adjustment(const adnd::defs::character_skill& skill);
		void adjust_level_limits();
		void adjust_experience_bonus();
		short get_constitution_adjustment(const adnd::skills::character_skill_value& value);

		////////////////////////////////////////////////////////////////////////////////
		// Skills
		////////////////////////////////////////////////////////////////////////////////
		void get_skill_boundaries(const adnd::defs::character_skill& skill, short& minValue, short& maxValue);
		adnd::skills::character_skill_value generate_skill(const adnd::defs::character_skill& skill, const skills::generation_method& genMethod = skills::generation_method::standard, bool generateExceptionalStrength = false);
		
		////////////////////////////////////////////////////////////////////////////////
		// Health, HPs, cure/cause wounds
		////////////////////////////////////////////////////////////////////////////////
		void add_hp(const defs::character_class& cls, const short& hp);
		void add_hp(const defs::character_class& cls, const std::vector<short>& hps);
		void check_resurrection_survival();

		////////////////////////////////////////////////////////////////////////////////
		// Experience
		////////////////////////////////////////////////////////////////////////////////

		void on_xp_change(const adnd::defs::character_class& cls, const std::tuple<long, long, short, short, adnd::class_level::experience_shift> xpChanges);
		void on_xp_change(const long& xpOld, const long& xpNew, const short& levelOld, const short& levelNew, const defs::character_class& cls, const class_level::experience_shift& expShift);

		void resize_spell_book();
		void resize_holy_symbol();
	};
}