#include "stdafx.h"
#include "equipment.h"

adnd::equipment::weapon::weapon(const uint32_t& uid, const defs::equipment::item& id, const std::string& name, const defs::item_class& itemClass, const double& weight, const defs::item_hand_required& handRequired,
	const defs::weapons::size& weaponSize, const short& speedFactor, const defs::weapons::type& weaponType, const bool& requiresProjectiles,
	const defs::die_faces & faces_SM, const short& numRolls_SM, const short& baseDamage_SM, const defs::die_faces & faces_L, const short& numRolls_L, const short& baseDamage_L)
	: item(uid, id, name, itemClass, defs::item_type::weapon, weight, handRequired),
	m_weaponSize(weaponSize), m_speedFactor(speedFactor), m_weaponType(weaponType), m_requiresProjectiles(requiresProjectiles)
{
	m_faces[defs::weapons::target_size::small_medium] = faces_SM;
	m_numRolls[defs::weapons::target_size::small_medium] = numRolls_SM;
	m_baseDamage[defs::weapons::target_size::small_medium] = baseDamage_SM;
	m_faces[defs::weapons::target_size::large] = faces_SM;
	m_numRolls[defs::weapons::target_size::large] = numRolls_SM;
	m_baseDamage[defs::weapons::target_size::large] = baseDamage_SM;

	m_projectileInUse = defs::equipment::item::none;
}

void adnd::equipment::weapon::select_projectile(const defs::equipment::item& projectileId)
{
	auto t = templates::metadata::get_instance().get_weapon_template(id());
	if (!t->requires_projectiles())
		throw std::exception("This weapon does not require projectile");

	if (!t->allows_projectile(projectileId))
		throw std::exception("Selected projectile not allowed for this kind of weapon");

	m_projectileInUse = projectileId;
}

short adnd::equipment::weapon::damage(const defs::weapons::target_size& targetSize)
{
	if (!m_requiresProjectiles)
		return random::die(m_faces[targetSize]).roll<short>(m_numRolls[targetSize]) + m_baseDamage[targetSize];

	// Missile weapons deliver no damage, since it depends on the projectile in use
	return 0;
}

adnd::equipment::projectile::projectile(const uint32_t& uid, const defs::equipment::item& id, const std::string& name, const defs::item_class& itemClass, const double& weight,
	const defs::weapons::size& weaponSize, const defs::weapons::type& weaponType,
	const defs::die_faces & faces_SM, const short& numRolls_SM, const short& baseDamage_SM, const defs::die_faces & faces_L, const short& numRolls_L, const short& baseDamage_L)
	: item(uid, id, name, itemClass, defs::item_type::projectile, weight, defs::item_hand_required::none),
	m_weaponSize(weaponSize), m_weaponType(weaponType)
{
	m_faces[defs::weapons::target_size::small_medium] = faces_SM;
	m_numRolls[defs::weapons::target_size::small_medium] = numRolls_SM;
	m_baseDamage[defs::weapons::target_size::small_medium] = baseDamage_SM;
	m_faces[defs::weapons::target_size::large] = faces_SM;
	m_numRolls[defs::weapons::target_size::large] = numRolls_SM;
	m_baseDamage[defs::weapons::target_size::large] = baseDamage_SM;
}

short adnd::equipment::projectile::damage(const defs::weapons::target_size& targetSize)
{
	short damage = 0;
	auto t = templates::metadata::get_instance().get_projectile_template(id());
	damage = random::die(t->faces(targetSize)).roll<short>(t->rolls(targetSize)) + t->base_damage(targetSize);

	return damage;
}


adnd::equipment::magical_item::magical_item(const uint32_t& uid, const defs::equipment::item& id, const std::string& name,
	const defs::item_class& itemClass, const defs::item_type& itemType, const double& weight,
	const defs::item_hand_required& handRequired, const std::string& unidentifiedName,
	const defs::magical::item& magicalId, const defs::weapons::attack_type& attackType,
	const defs::weapons::group& weaponGroup, const defs::body_slot& slot)
	: item(uid, id, name, itemClass, itemType, weight, handRequired),
	m_unidentifiedName(unidentifiedName), m_identified(false), m_magicalId(magicalId),
	m_attackType(attackType), m_weaponGroup(weaponGroup), m_bodySlot(slot)
{
}

adnd::equipment::items_pool::~items_pool()
{
	for (auto& p : m_items)
		delete p.second;
}

adnd::equipment::item* adnd::equipment::items_pool::create(const defs::equipment::item& itemId, const std::string& itemName, const defs::item_class& itemClass)
{
	auto t = templates::metadata::get_instance().get_item_template(itemId);
	item *it = nullptr;

	std::string name = itemName;
	if (name.empty())
		name = t->description();

	if (t->item_type() == defs::item_type::clothing)
	{
		auto c = templates::metadata::get_instance().get_clothing_template(itemId);
		it = new adnd::equipment::clothing(m_lastId, itemId, name, itemClass, t->weight(), c->body_slot());
	}
	else if (t->item_type() == defs::item_type::daily_food_and_lodging)
	{
		it = new adnd::equipment::daily_food_and_lodging(m_lastId, itemId, name, itemClass, t->weight());
	}
	else if (t->item_type() == defs::item_type::household_provisioning)
	{
		it = new adnd::equipment::household_provisioning(m_lastId, itemId, name, itemClass, t->weight());
	}
	else if (t->item_type() == defs::item_type::service)
	{
		it = new adnd::equipment::service(m_lastId, itemId, name, itemClass, t->weight());
	}
	else if (t->item_type() == defs::item_type::transport)
	{
		it = new adnd::equipment::transport(m_lastId, itemId, name, itemClass, t->weight());
	}
	else if (t->item_type() == defs::item_type::animal)
	{
		it = new adnd::equipment::animal(m_lastId, itemId, name, itemClass, t->weight());
	}
	else if (t->item_type() == defs::item_type::tack_and_harness)
	{
		it = new adnd::equipment::tack_and_harness(m_lastId, itemId, name, itemClass, t->weight());
	}
	else if (t->item_type() == defs::item_type::miscellaneous_equipment)
	{
		it = new adnd::equipment::miscellaneous_equipment(m_lastId, itemId, name, itemClass, t->weight());
	}
	if (t->item_type() == defs::item_type::weapon)
	{
		auto w = templates::metadata::get_instance().get_weapon_template(itemId);
		it = new adnd::equipment::weapon(m_lastId, t->id(), name, t->item_class(), t->weight(),
			w->hand_required(), w->size(), w->speed(), w->weapon_type(), w->requires_projectiles(),
			w->faces(defs::weapons::target_size::small_medium), w->rolls(defs::weapons::target_size::small_medium), w->base_damage(defs::weapons::target_size::small_medium),
			w->faces(defs::weapons::target_size::large), w->rolls(defs::weapons::target_size::large), w->base_damage(defs::weapons::target_size::large));
	}
	else if (t->item_type() == defs::item_type::projectile)
	{
		auto p = templates::metadata::get_instance().get_projectile_template(itemId);
		it = new adnd::equipment::projectile(m_lastId, t->id(), name, t->item_class(), t->weight(), p->size(), p->weapon_type(),
			p->faces(defs::weapons::target_size::small_medium), p->rolls(defs::weapons::target_size::small_medium), p->base_damage(defs::weapons::target_size::small_medium),
			p->faces(defs::weapons::target_size::large), p->rolls(defs::weapons::target_size::large), p->base_damage(defs::weapons::target_size::large));
	}
	else if (t->item_type() == defs::item_type::armour)
	{
		auto a = templates::metadata::get_instance().get_armour_template(itemId);
		it = new adnd::equipment::armour(m_lastId, t->id(), name, t->item_class(), t->weight(),
			a->body_slot(), a->ac_bonus_melee(), a->ac_bonus_missile(), a->num_of_attack_protection(), a->protection(), a->allows_weapons());
	}
	else
	{
		it = new adnd::equipment::item(m_lastId, itemId, name, itemClass, t->item_type(), t->weight(), t->hand_required());
	}

	m_items[m_lastId] = it;
	m_lastId++;
	return it;
}

void adnd::equipment::items_pool::erase()
{
	m_lastId = 0;
	m_items.erase(m_items.begin(), m_items.end());
}

void adnd::equipment::items_pool::detatch(const uint32_t& uid)
{
	m_items.erase(uid);
	uint32_t lastUid = 0;
	for (auto& it : m_items)
		lastUid = std::max(lastUid, it.first);
	m_lastId = lastUid;
}