#pragma once

#include <list>


namespace adnd
{
	enum class bonus_malus_type
	{
		bonus = 0,
		malus = 1,
	};

	class bonus_malus
	{
	public:
		bonus_malus(const short& value, const bonus_malus_type& type = bonus_malus_type::bonus);
		virtual ~bonus_malus();

		operator short();
		bonus_malus_type type();

	protected:
		bonus_malus_type	m_type;
		short				m_value;
	};

	class bonus_sequence
	{
	public:
		bonus_sequence();
		virtual ~bonus_sequence();

		bonus_sequence& operator+= (const short& value);
		operator short();
		size_t size();
		void reset();

	protected:
		std::list<bonus_malus>		m_seq;
	};
}