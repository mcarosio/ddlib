#pragma once

#include "defs.h"
#include "equipment.h"
#include "money.h"
#include "treasure.h"

#include <string>
#include <map>
#include <tuple>


namespace adnd
{
	namespace market
	{
		class store
		{
		public:

			enum class budget_model
			{
				assigned,
				random,
				unlimited
			};

			// Customer side, buy from the store, sell to the store
			enum class buy_sell_side
			{
				buy,
				sell
			};

			store();
			store(const uint32_t& uid, const std::string& name = "", const budget_model& bud = budget_model::unlimited, bool buySpread = false, bool sellSpread = false);
			~store();


			////////////////////////////////////////////////////////////////////////////////
			// Properties
			////////////////////////////////////////////////////////////////////////////////
			const uint32_t& uid() const				{ return m_uid; }
			const std::string& name() const			{ return m_name; }
			const budget_model& budget() const		{ return m_budget; }
			const bool& buy_spread() const			{ return m_buySpread; }
			const bool& sell_spread() const			{ return m_sellSpread; }


			////////////////////////////////////////////////////////////////////////////////
			// Restocking
			////////////////////////////////////////////////////////////////////////////////
			template<class _ItemType>
			void add(const defs::equipment::item& itemId, const std::string& name = "")
			{
				auto t = adnd::templates::metadata::get_instance().get_item_template(itemId);
				adnd::equipment::item* it = equipment::items_pool::get_instance().create_as<_ItemType>(itemId, name, t->item_class());
				add(it);
			}

			void set_availability(const defs::coin& currency, const uint32_t& amount);
			void set_availability(const money::amount& amount);


			////////////////////////////////////////////////////////////////////////////////
			// Visit
			////////////////////////////////////////////////////////////////////////////////
			auto begin() { return m_shelf.begin(); }
			auto end() { return m_shelf.end(); }
			auto begin(const defs::equipment::item& itemId) { return std::get<0>(m_shelf[itemId]).begin(); }
			auto end(const defs::equipment::item& itemId) { return std::get<0>(m_shelf[itemId]).end(); }
			
			/**
				Returns the number of item instance of the given type in stock
			*/
			auto count(const defs::equipment::item& itemId)
			{
				return in_stock(itemId) ? std::get<0>(m_shelf[itemId]).size() : 0;;
			}

			/**
				Returns true if there is an item of the given type in stock.
				An item is in stock if there is at least an item of that type available for purchase
			*/
			bool in_stock(const defs::equipment::item& itemId)
			{
				return
						m_shelf.find(itemId) != m_shelf.end()			// Item type entry found
						&& std::get<0>(m_shelf[itemId]).size() > 0;		// Item type entry contains at least one item instance
			}

			/**
				Returns true if there is a gem of the given type in stock.
				A gem is in stock if there is at least a gem of that type available for purchase
			*/
			bool in_stock(const defs::treasure::gems& gemId)
			{
				return
					m_gemStock.find(gemId) != m_gemStock.end()			// Gem type entry found
					&& std::get<0>(m_gemStock[gemId]).size() > 0;		// Gem type entry contains at least one item instance
			}

			/**
				Returns true if there is an object of art with the same unique ID in stock
			*/
			bool in_stock(const uint32_t& objOfArtUid)
			{
				return
					m_artStock.find(objOfArtUid) != m_artStock.end();			// Gem type entry found
			}


			////////////////////////////////////////////////////////////////////////////////
			// Purchasing
			////////////////////////////////////////////////////////////////////////////////
			adnd::money::amount get_price(const defs::equipment::item& itemd, const store::buy_sell_side& side);

			bool check_availability(const money::amount& amt);
			adnd::equipment::item* buy(const defs::equipment::item& item);
			void undo_buy(adnd::equipment::item* it);
			money::amount sell(adnd::equipment::item* it);

			adnd::treasure::gem buy(const defs::treasure::gems& gemId);
			void undo_buy(const adnd::treasure::gem& g);
			money::amount sell(const adnd::treasure::gem& g);

			//adnd::treasure::object_of_art buy(const defs::treasure::o& gem);
			//void undo_buy(const adnd::treasure::gem& g);
			money::amount sell(const adnd::treasure::object_of_art& o);


		protected:
			static const double		m_buySpreadForPrice;
			static const double		m_sellSpreadForPrice;
			static const double		m_buySpreadForPriceRange;
			static const double		m_sellSpreadForPriceRange;

			uint32_t				m_uid;
			std::string				m_name;
			store::budget_model		m_budget;
			std::map<defs::equipment::item, std::tuple<std::map<uint32_t, adnd::equipment::item*>, money::amount, money::amount>>
									m_shelf;

			std::map<defs::treasure::gems, std::tuple<std::map<uint32_t, adnd::treasure::gem>, money::amount, money::amount>>
									m_gemStock;
			std::map<uint32_t, std::tuple<adnd::treasure::object_of_art, money::amount, money::amount>>
									m_artStock;

			std::map<uint32_t, defs::equipment::item>
								m_uidToId;
			bool				m_buySpread;
			bool				m_sellSpread;
			money::money_bag	m_availability;

			//double spread(double price, const buy_sell_side& side);
			money::amount get_price(const uint32_t& itemUid, const buy_sell_side& side);

			adnd::money::amount make_price_for(const defs::equipment::item& itemd, const store::buy_sell_side& side);
			adnd::money::amount make_price_for(const defs::treasure::gems& gemId, const store::buy_sell_side& side);
			void add(adnd::equipment::item* it);
			void add(const adnd::treasure::gem& g);
			void add(const adnd::treasure::object_of_art& o);
			void add_to_store(const defs::equipment::item& itemId);
			void add_to_store(const defs::treasure::gems& gemId);
			void add_to_store(const adnd::treasure::object_of_art& o);

			adnd::money::amount make_price_for(const double& minPrice, const double& maxPrice, const defs::coin& currency, const buy_sell_side& side);

		private:
		};

		class market
		{
		public:
			static market& get_instance();
			static void init()
			{
				//Ensure first initialisation
				get_instance();
			}
			market(market const&) = delete;
			void operator=(market const&) = delete;

			uint32_t create_store(const std::string& name, const store::budget_model& storeBudget = store::budget_model::unlimited, bool buySpread = false, bool sellSpread = false);

			store& operator[] (const uint32_t& uid)
			{
				return m_stores[uid];
			}
			store& operator[] (const std::string& storeName)
			{
				return m_stores[m_nameToUid[storeName]];
			}

		protected:
			std::map<uint32_t, store>			m_stores;
			std::map<std::string, uint32_t>		m_nameToUid;

		private:
			market() {}

			//static uint32_t			m_lastStoreUid;
			static bool				m_initialised;
		};
	}
}