#include "stdafx.h"
#include "adnd.h"

#include <algorithm>
#include <tuple>

adnd::character::character(const std::string& name, const defs::character_class& charClass, const defs::character_race& charRace,
			const defs::character_sex& sex, const defs::moral_alignment& alignment, bool maximizeHP,
			const defs::deities::deity& deityId)
			: m_name(name), m_class(charClass), m_race(charRace), m_sex(sex), m_alignment(alignment), m_hitPoints(charClass), m_level(charClass), m_moneyBag(),
			m_deity(deityId), m_weaponProficiencies(charClass),
			m_spellBook(charClass), m_holySymbol(charClass, m_deity)
{
	// It must be specified because deities give access to the related spell spheres
	if (m_class.is(defs::character_class::priest_of_specific_mythos) && m_deity == defs::deities::deity::none)
	{
		throw std::exception("Deity is mandatory for preists of specific mythos");
	}

	if (m_deity != defs::deities::deity::none)
	{
		auto deityTempl = templates::metadata::get_instance().get_deity_template(m_deity);
		if (!deityTempl.is_worshipped_by(alignment))
		{
			auto alignTempl = templates::metadata::get_instance().get_moral_alignment_template(alignment);
			std::stringstream ss;
			ss << deityTempl.get_name() << " does not accept " << alignTempl.get_name() << " worshippers";
			throw std::exception(ss.str().c_str(), static_cast<int>(alignment));
		}
	}

	auto raceTempl = templates::metadata::get_instance().get_race_template(charRace);
	if (!raceTempl.check_class(charClass))
		throw exceptions::class_not_allowed_for_race(charClass, charRace);

	if (m_level.size() == 0)
		throw std::exception("Invalid character class specified", static_cast<unsigned short>(charClass));

	auto classTempl = templates::metadata::get_instance().get_class_template(charClass);
	if (!classTempl.check_moral_alignment(alignment))
		throw std::exception("Moral alignment not allowed for the chosen class");

	std::map<defs::character_class, random::die> dice = classTempl.hit_dice();
	std::vector<short> hps;
	for (auto d : dice)
	{
		short hp = 0;
		if (maximizeHP)
			hp = static_cast<short>(d.second.faces());
		else
			hp = d.second.roll<short>(1);

		hps.push_back(hp);
	}
	m_hitPoints.add(charClass, hps);

	m_hands[defs::hand_slot_type::primary] = hand_slot(defs::hand_slot_type::primary);
	m_hands[defs::hand_slot_type::secondary] = hand_slot(defs::hand_slot_type::secondary);

	// Roll racial statistics (height, weight, age, ...)
	{
		short hBase = raceTempl.height_base(m_sex);
		short hVariable = raceTempl.height_dice1().roll<short>() + raceTempl.height_dice2().roll<short>();
		short wBase = raceTempl.weight_base(m_sex);
		short wVariable = raceTempl.weight_dice1().roll<short>() + raceTempl.weight_dice2().roll<short>();

		m_height = hBase + hVariable;
		m_weight = wBase + wVariable;
		m_age = raceTempl.age_starting() + raceTempl.age_dice().roll<short>();
		m_lifeExpectancy = raceTempl.age_maximum() + raceTempl.age_maximum_dice().roll<short>();
	}

	// Roll starting money
	uint32_t initialGold = (classTempl.starting_money().roll<uint32_t>() + classTempl.starting_money_base()) * classTempl.starting_money_multiplier();

	m_moneyBag.get(defs::coin::copper) = adnd::money::amount(defs::coin::copper, 0);
	m_moneyBag.get(defs::coin::silver) = adnd::money::amount(defs::coin::silver, 0);
	m_moneyBag.get(defs::coin::electrum) = adnd::money::amount(defs::coin::electrum, 0);
	m_moneyBag.get(defs::coin::gold) = adnd::money::amount(defs::coin::gold, initialGold);
	m_moneyBag.get(defs::coin::platinum) = adnd::money::amount(defs::coin::platinum, 0);

	generate_skills();

	if (classTempl.has_thief_abilities())
	{
		auto cc = classTempl.get_class_composition();
		defs::character_class cls = defs::character_class::none;
		if (cc.is(defs::character_class::thief))
			cls = defs::character_class::thief;
		else if (cc.is(defs::character_class::bard))
			cls = defs::character_class::bard;
	
		short level = m_level[cls].level();
		short skills[8];
		int index = 0;
		auto sklTempl = templates::metadata::get_instance().get_skill_template(defs::character_skill::dexterity);

		for (auto skl : { defs::thief_ability::pick_pockets, defs::thief_ability::open_locks,
						defs::thief_ability::find_remove_traps, defs::thief_ability::move_silently,
						defs::thief_ability::hide_in_shadows, defs::thief_ability::hear_noise,
						defs::thief_ability::climb_walls, defs::thief_ability::read_languages })
		{
			skills[index++] = classTempl.thievery(level).ability(skl)
						+ sklTempl.get_thieving_modifiers_per_dexterity(dexterity(), skl)
						+ raceTempl.get_thieving_modifiers_per_race(skl);
		}
		m_thiefAdvancement.set_base_values(skills[0], skills[1], skills[2], skills[3], skills[4], skills[5], skills[6], skills[7]);
	}

	if (classTempl.can_cast_wizard_spells())
	{
		m_spellBook.enabled() = true;
		m_spellBook.resize(intelligence_value(), 1);
	}
	
	if (classTempl.can_cast_priest_spells())
	{
		m_holySymbol.enabled() = true;
		m_holySymbol.resize(wisdom_value(), 1);
	}
}

void adnd::character::adjust_level_limits()
{
	auto raceTempl = adnd::templates::metadata::get_instance().get_race_template(m_race);
	if (!raceTempl.is_demihuman())
		return;

	auto limits = raceTempl.get_level_limit(m_class.id(), m_skills);
	for (auto lim : limits)
	{
		m_level[lim.first].level_limit() = lim.second;
	}
}

void adnd::character::adjust_experience_bonus()
{
	for (auto cls : m_class.classes())
	{
		auto t = adnd::templates::metadata::get_instance().get_class_template(cls);
		m_level[cls].xp_bonus() = t.experience_bonus(m_skills);
	}
}

bool adnd::character::check_minimum_requisites(const adnd::defs::character_skill& skill, const adnd::skills::character_skill_value& value)
{
	auto clsTempl = templates::metadata::get_instance().get_class_template(m_class.id());
	if (!clsTempl.check_skill(skill, value))
		throw std::exception("Skill value does not satisfy class minimum requisites");

	auto raceTempl = templates::metadata::get_instance().get_race_template(m_race);
	if (!raceTempl.check_skill(skill, value))
		throw std::exception("Skill value does not satisfy race minimum requisites");

	return true;
}

short adnd::character::get_constitution_adjustment(const adnd::skills::character_skill_value& value)
{
	auto constTempl = templates::metadata::get_instance().get_skill_template(defs::character_skill::constitution);
	auto constStat = constTempl.get_constitution_stats(value);
	return (is_warrior()) ? constStat.hitPointAdjustmentWarriors : constStat.hitPointAdjustment;
}

bool adnd::character::set_skill(const adnd::defs::character_skill& skill, const adnd::skills::character_skill_value& value)
{
	check_minimum_requisites(skill, value);

	m_skills[skill] = value;
	adjust_level_limits();
	adjust_experience_bonus();

	if (skill == defs::character_skill::constitution)
	{
		auto constAdj = get_constitution_adjustment(value);
		m_hitPoints.set_constitution_adjustment(constAdj);
	}

	if (skill == defs::character_skill::intelligence && m_spellBook.enabled())
	{
		if (m_class.is_type_of(defs::character_class_type::wizard))
		{
			auto spellCasterClass = m_class.select_class_by_type(defs::character_class_type::wizard);
			m_spellBook.resize(intelligence_value(), m_level.get_level(spellCasterClass));
		}
		else if (m_class.is(defs::character_class::bard))
		{
			m_spellBook.resize(intelligence_value(), m_level.get_level(defs::character_class::bard));
		}
	}

	if (skill == defs::character_skill::wisdom && m_holySymbol.enabled())
	{
		defs::character_class casterClass = defs::character_class::none;
		if (m_class.is(defs::character_class::cleric))
		{
			casterClass = defs::character_class::cleric;
		}
		else if (m_class.is(defs::character_class::paladin))
		{
			casterClass = defs::character_class::paladin;
		}
		else if (m_class.is(defs::character_class::ranger))
		{
			casterClass = defs::character_class::ranger;
		}
		else if (m_class.is(defs::character_class::druid))
		{
			casterClass = defs::character_class::druid;
		}
		else if (m_class.is(defs::character_class::priest_of_specific_mythos))
		{
			casterClass = defs::character_class::priest_of_specific_mythos;
		}
		m_holySymbol.resize(wisdom_value(), m_level.get_level(casterClass));
	}

	return true;
}

bool adnd::character::set_skill(const adnd::defs::character_skill& skill, const short& value)
{
	return set_skill(skill, skills::character_skill_value(skill, value));
}

bool adnd::character::set_skill(const adnd::defs::character_skill& skill, const short& value, const short& exceptionalStrength)
{
	return set_skill(skill, skills::character_skill_value(skill, value, (skill == defs::character_skill::strength) ? exceptionalStrength : 0));
}

bool adnd::character::set_skills(const skills::character_skill_set& set)
{
	return set_skills(set.strength(), set.dexterity(), set.constitution(), set.intelligence(), set.wisdom(), set.charisma());
}

bool adnd::character::set_skills(const skills::character_skill_value& strength,
								const skills::character_skill_value& dexterity,
								const skills::character_skill_value& constitution,
								const skills::character_skill_value& intelligence,
								const skills::character_skill_value& wisdom,
								const skills::character_skill_value& charisma)
{
	check_minimum_requisites(defs::character_skill::strength, strength);
	check_minimum_requisites(defs::character_skill::dexterity, dexterity);
	check_minimum_requisites(defs::character_skill::constitution, constitution);
	check_minimum_requisites(defs::character_skill::intelligence, intelligence);
	check_minimum_requisites(defs::character_skill::wisdom, wisdom);
	check_minimum_requisites(defs::character_skill::charisma, charisma);

	m_skills[defs::character_skill::strength] = strength;
	m_skills[defs::character_skill::dexterity] = dexterity;
	m_skills[defs::character_skill::constitution] = constitution;
	m_skills[defs::character_skill::intelligence] = intelligence;
	m_skills[defs::character_skill::wisdom] = wisdom;
	m_skills[defs::character_skill::charisma] = charisma;

	auto constAdj = get_constitution_adjustment(constitution);
	m_hitPoints.set_constitution_adjustment(constAdj);

	adjust_level_limits();
	adjust_experience_bonus();

	resize_spell_book();
	resize_holy_symbol();

	return true;
}

void adnd::character::generate_skills(const skills::generation_method& genMethod)
{
	adnd::skills::character_skill_value str = generate_skill(defs::character_skill::strength, genMethod, is_warrior());
	adnd::skills::character_skill_value dex = generate_skill(defs::character_skill::dexterity, genMethod);
	adnd::skills::character_skill_value con = generate_skill(defs::character_skill::constitution, genMethod);
	adnd::skills::character_skill_value itl = generate_skill(defs::character_skill::intelligence, genMethod);
	adnd::skills::character_skill_value wis = generate_skill(defs::character_skill::wisdom, genMethod);
	adnd::skills::character_skill_value cha = generate_skill(defs::character_skill::charisma, genMethod);

	m_skills[defs::character_skill::strength] = str;
	m_skills[defs::character_skill::dexterity] = dex;
	m_skills[defs::character_skill::constitution] = con;
	m_skills[defs::character_skill::intelligence] = itl;
	m_skills[defs::character_skill::wisdom] = wis;
	m_skills[defs::character_skill::charisma] = cha;

	adjust_level_limits();
	adjust_experience_bonus();

	// When constitution value is applied to the character, the HP modifier changes
	auto st = templates::metadata::get_instance().get_skill_template(defs::character_skill::constitution);
	auto conStats = st.get_constitution_stats(constitution_value());
	short newHitPointAdjustment = (is_warrior()) ? conStats.hitPointAdjustmentWarriors : conStats.hitPointAdjustment;
	m_hitPoints.set_constitution_adjustment(newHitPointAdjustment);
}

adnd::skills::character_skill_value adnd::character::generate_skill(const adnd::defs::character_skill& skill, const skills::generation_method& genMethod, bool generateExceptionalStrength)
{
	short minValue = 3;
	short maxValue = 18;
	get_skill_boundaries(skill, minValue, maxValue);

	skills::skill_generator skGen(genMethod);
	short value = skGen.generate(minValue, maxValue);

	// Race modifier applied here
	auto raceTempl = templates::metadata::get_instance().get_race_template(m_race);
	value += raceTempl.get_skill_modifier(skill);

	return adnd::skills::character_skill_value(skill, value);
}

void adnd::character::get_skill_boundaries(const adnd::defs::character_skill& skill, short& minValue, short& maxValue)
{
	short minClassValue = 3, maxClassValue = 18;
	short minRaceValue = 3, maxRaceValue = 18;
	auto raceTempl = templates::metadata::get_instance().get_race_template(m_race);
	raceTempl.get_skill_boundaries(skill, minRaceValue, maxRaceValue);

	auto clsTempl = templates::metadata::get_instance().get_class_template(m_class.id());
	short minClVal = 3, maxClVal = 18;
	clsTempl.get_skill_boundaries(skill, minClVal, maxClVal);
	minClassValue = std::max<short>(minClVal, minClassValue);
	maxClassValue = std::min<short>(maxClVal, maxClassValue);
	
	minValue = std::max<short>(minClassValue, minRaceValue);
	maxValue = std::min<short>(maxClassValue, maxRaceValue);
}

short adnd::character::armour_class()
{
	short armourClass = adnd::defs::base_armour_class;
	auto st = templates::metadata::get_instance().get_skill_template(defs::character_skill::dexterity);
	auto dexStats = st.get_dexterity_stats(dexterity_value());

	auto raceTempl = adnd::templates::metadata::get_instance().get_race_template(m_race);
	short acMalusForEncumbrance = 0;
	short movRate = movement_rate();

	// AC reduces in case of heavy encumbrance
	if (movRate == 1)
		acMalusForEncumbrance = 3;
	else if (movRate <= raceTempl.base_movement_rate() / 3)
		acMalusForEncumbrance = 1;

	return armourClass + dexStats.defensiveAdjustment + acMalusForEncumbrance;
}

void adnd::character::on_xp_change(const adnd::defs::character_class& cls, const std::tuple<long, long, short, short, adnd::class_level::experience_shift> xpChanges)
{
	auto xpOld = std::get<0>(xpChanges);
	auto xpNew = std::get<1>(xpChanges);
	auto levelOld = std::get<2>(xpChanges);
	auto levelNew = std::get<3>(xpChanges);
	auto expShift = std::get<4>(xpChanges);
	on_xp_change(xpOld, xpNew, levelOld, levelNew, cls, expShift);
}

void adnd::character::on_xp_change(const long& xpOld, const long& xpNew, const short& levelOld, const short& levelNew, const defs::character_class& cls,
									const class_level::experience_shift& expShift)
{
	auto t = templates::metadata::get_instance().get_class_template(cls);
	if (expShift == class_level::experience_shift::level_up)
	{
		for (short l = levelOld; l < levelNew; ++l)
		{
			add_hp(cls, t.hit_dice()[cls].roll<short>());
			// Thief skills improvement is managed by the end user
		}
	}
	else if (expShift == class_level::experience_shift::level_down)
	{
		m_hitPoints[cls].drain(levelOld - levelNew);
		m_thiefAdvancement.level_down(levelOld - levelNew);
	}

	if (adnd::configuration::get_instance().get(defs::config::optional::weapon_proficiency))
		m_weaponProficiencies.level_change(levelNew);

	resize_spell_book();
	resize_holy_symbol();
}

void adnd::character::resize_spell_book()
{
	if (m_class.is_type_of(defs::character_class_type::wizard))
	{
		auto cls = m_class.select_class_by_type(defs::character_class_type::wizard);
		m_spellBook.resize(intelligence_value(), m_level.get_level(cls));
	}
	if (m_class.is(defs::character_class::bard))
	{
		m_spellBook.resize(intelligence_value(), m_level.get_level(defs::character_class::bard));
	}
}

void adnd::character::resize_holy_symbol()
{
	if (m_class.is_type_of(defs::character_class_type::priest))
	{
		auto cls = m_class.select_class_by_type(defs::character_class_type::priest);
		m_holySymbol.resize(wisdom_value(), m_level.get_level(cls));
	}
	if (m_class.is(defs::character_class::ranger))
	{
		m_holySymbol.resize(wisdom_value(), m_level.get_level(defs::character_class::ranger));
	}
}

void adnd::character::grab(const adnd::defs::hand_slot_type& handSlot, adnd::equipment::item* item)
{
	hand_slot& otherHand = (handSlot == adnd::defs::hand_slot_type::primary) ? m_hands[adnd::defs::hand_slot_type::secondary] : m_hands[adnd::defs::hand_slot_type::primary];

	if (m_hands[handSlot].busy())
		throw std::exception("This hand is busy");
	else if (item->item_type() == defs::item_type::weapon
		&& otherHand.busy() && otherHand.item<adnd::equipment::item>()->item_type() == defs::item_type::weapon
		&& !(is_warrior() || is_rogue()))
		throw std::exception("Cannot handle two weapons simulateously");
	else if (handSlot == adnd::defs::hand_slot_type::secondary && item->hand_required() == adnd::defs::item_hand_required::primary)
		throw std::exception("Cannot hold this item");
	else if (handSlot == adnd::defs::hand_slot_type::secondary && item->hand_required() == adnd::defs::item_hand_required::both)
		throw std::exception("Cannot hold this item");
	else if (handSlot == adnd::defs::hand_slot_type::secondary && otherHand.busy())
		throw std::exception("Cannot hold this item");
	else if (handSlot == adnd::defs::hand_slot_type::primary && item->hand_required() == defs::item_hand_required::both && otherHand.busy())
		throw std::exception("Cannot hold this item");

	if (!can_use(item->id()))
	{
		std::string message = item->name() + " not allowed for this class";
		throw std::exception(message.c_str());
	}

	m_hands[handSlot] = item;
}

bool adnd::character::can_use(const defs::equipment::item& item)
{
	auto t = templates::metadata::get_instance().get_class_template(m_class.id());
	return t.can_use(item);
}

uint32_t adnd::character::release(const defs::hand_slot_type& handSlot, equipment::item*& item)
{
	item = m_hands[handSlot].drop();
	return item->uid();
}

uint32_t adnd::character::release(const defs::hand_slot_type& handSlot)
{
	equipment::item *it = m_hands[handSlot].drop();
	return it->uid();
}

short adnd::character::punch()
{
	auto st = templates::metadata::get_instance().get_skill_template(defs::character_skill::strength);
	auto damageAdjustment = st.get_strength_stats(strength_value()).damageAdjustment;
	return random::die(defs::die_faces::four).roll<short>(1) + damageAdjustment;
}

adnd::tables::attack_rate adnd::character::get_attack_rate(const adnd::defs::weapons::attack_type& attackType, const defs::equipment::item& item)
{
	defs::equipment::item itemInUse = item;

	if (item == defs::equipment::item::none)
	{
		itemInUse = m_hands[defs::hand_slot_type::primary].item<adnd::equipment::item>()->id();
	}

	// Check if weapon proficiency optional rule is in use
	if (adnd::configuration::get_instance().get(defs::config::optional::weapon_proficiency))
	{
		defs::proficiency::level lvl = m_weaponProficiencies.proficiency_level(itemInUse);
		if (lvl == defs::proficiency::level::specialised)
		{
/*
//	auto stmt = m_db.resolve_by_name(metadata::_stmt::specialist_attacks_per_round);
//
//	adnd::templates::metadata::query_manager::iterator it, end;
//	if (!m_db.execute(stmt, it, end))
//		throw std::exception("Unable to retrieve specialist attacks per round");
//
//	std::map<short, adnd::tables::specialist_attacks_per_round> results;
//	while (it != end)
//	{
//		adnd::tables::specialist_attacks_per_round info;
//
//		info.level = metadata::parse<short>(it, 1);
//		info.meleeWeapon.attacks = metadata::parse<short>(it, 2);
//		info.meleeWeapon.rounds = metadata::parse<short>(it, 3);
//		info.lightBow.attacks = metadata::parse<short>(it, 4);
//		info.lightBow.rounds = metadata::parse<short>(it, 5);
//		info.heavyBow.attacks = metadata::parse<short>(it, 6);
//		info.heavyBow.rounds = metadata::parse<short>(it, 7);
//		info.thrownDagger.attacks = metadata::parse<short>(it, 8);
//		info.thrownDagger.rounds = metadata::parse<short>(it, 9);
//		info.thrownDart.attacks = metadata::parse<short>(it, 10);
//		info.thrownDart.rounds = metadata::parse<short>(it, 11);
//		info.otherMissile.attacks = metadata::parse<short>(it, 12);
//		info.otherMissile.rounds = metadata::parse<short>(it, 13);
//
//		results[info.level] = info;
//		++it;
//	}
//
//	auto attacks = results.lower_bound(level);
//	adnd::tables::rate_of_fire rate{ 1,1 };
//
//	if (attackType == defs::weapons::attack_type::melee || attackType == defs::weapons::attack_type::mount)
//		rate = attacks->second.meleeWeapon;
//	else if (attackType == defs::weapons::attack_type::light_bow)
//			rate = attacks->second.lightBow;
//	else if (attackType == defs::weapons::attack_type::heavy_bow)
//		rate = attacks->second.heavyBow;
//	else if (attackType == defs::weapons::attack_type::thrown_dagger)
//		rate = attacks->second.thrownDagger;
//	else if (attackType == defs::weapons::attack_type::thrown_dart)
//		rate = attacks->second.thrownDart;
//	else if (attackType == defs::weapons::attack_type::missile)
//		rate = attacks->second.otherMissile;
//	else
//	{
//		adnd::tables::attack_rate result{ defs::weapons::attack_type::none, level, rate.attacks, rate.rounds };
//		return result;
//	}
//
//	adnd::tables::attack_rate result{ attackType, level, rate.attacks, rate.rounds };
//	return result;*/

			std::tuple<short, short, short, short, short, short, short, short, short, short, short, short, short> attacks;
			auto results = templates::metadata::get_instance().fetch<short, short, short, short, short, short, short, short, short, short, short, short, short>(templates::metadata::query::specialist_attacks_per_round).run();
			for (auto r : results)
			{
				short level = std::get<0>(r);
				if (level <= m_level.get_level())
				{
					attacks = r;
				}
			}

			adnd::tables::attack_rate rate;
			short level = std::get<0>(attacks);
			if (attackType == defs::weapons::attack_type::melee || attackType == defs::weapons::attack_type::mount)
				rate = { attackType, level, std::get<1>(attacks), std::get<2>(attacks) };
			else if (attackType == defs::weapons::attack_type::light_bow)
				rate = { attackType, level, std::get<3>(attacks), std::get<4>(attacks) };
			else if (attackType == defs::weapons::attack_type::heavy_bow)
				rate = { attackType, level, std::get<5>(attacks), std::get<6>(attacks) };
			else if (attackType == defs::weapons::attack_type::thrown_dagger)
				rate = { attackType, level, std::get<7>(attacks), std::get<8>(attacks) };
			else if (attackType == defs::weapons::attack_type::thrown_dart)
				rate = { attackType, level, std::get<9>(attacks), std::get<10>(attacks) };
			else if (attackType == defs::weapons::attack_type::missile)
				rate = { attackType, level, std::get<11>(attacks), std::get<12>(attacks) };
			else
			{
				rate = { defs::weapons::attack_type::none, level, 1, 1 };
			}

			return rate;
			//if (rate.type != defs::weapons::attack_type::none)
			//	return specAttacks;
		}
	}

	// returns the default attack rate for the class/level
	auto clsId = m_class.select_class_by_type(defs::character_class_type::warrior);
	auto t = adnd::templates::metadata::get_instance().get_class_template(clsId);
	return t.get_attack_rate(attackType, m_level.get_level(clsId));
}

adnd::attack_result adnd::character::attack(const defs::hand_slot_type& handSlot, const short& targetArmourClass, const short& targetDistance, const defs::weapons::target_size& targetSize)
{
	attack_result attackResult;

	attackResult.hitResult = try_hit(handSlot, targetArmourClass, targetDistance);
	if (attackResult.hitResult == hit_result::missed || attackResult.hitResult == hit_result::critical_failure)
		return attackResult;

	if (!m_hands[handSlot].busy())
	{
		attackResult.damage = punch();
		return attackResult;
	}
		
	equipment::item *it = m_hands[handSlot].item();
	if (it == nullptr || it->item_type() != defs::item_type::weapon)
		//Attack with improper weapon (unusual situation)
		return attack_result();

	auto wt = templates::metadata::get_instance().get_weapon_template(it->id());
	equipment::weapon *w = dynamic_cast<equipment::weapon*>(it);

	//attackResult.damage = w->damage(targetSize);
	if (!wt->requires_projectiles())
	{
		//Melee weapon, strength modifier applies
		attackResult.damage = w->damage(targetSize) + damage_modifier_for_strength();
	}
	else
	{
		//Missile weapon, no modifier for dexterity applies
		auto projType = w->projectile_in_use();
		
		if (projType == defs::equipment::item::none)
			throw std::exception("No projectile selected for this weapon");

		adnd::equipment::projectile* projectile = nullptr;
		try
		{
			projectile = m_inventory.find_as<adnd::equipment::projectile>(projType);
		}
		catch (std::exception&)
		{
			throw std::exception("Not enough projectiles", static_cast<int>(projType));
		}

		attackResult.damage = projectile->damage(targetSize);
		m_inventory.remove(projectile->uid());
	}

	attackResult.damage += damage_modifier_for_weapon(it)
		+ damage_modifier_for_weapon_proficiency(it)
		+ damage_modifier_for_encumbrance();

	if (attackResult.hitResult == hit_result::critical_hit)
	{
		attack_result ar = attack(handSlot, targetArmourClass, targetDistance, targetSize);
		attackResult.hitResult = ar.hitResult;
		attackResult.damage += ar.damage;
	}

	return attackResult;
}

short adnd::character::hit_modifier(const defs::hand_slot_type& handSlot, const short& distance)
{
	if (!m_hands[handSlot].busy())
	{
		// The character has no weapons at hand, try punch

		if (distance > 3)
			// Target unreachable
			throw exceptions::target_out_of_range(defs::equipment::item::none, distance);
		
		//Punch attack, only strength modifier applies
		return hit_modifier_for_strength();
	}

	// The character attacks using a weapon
	equipment::weapon *it = m_hands[handSlot].item<equipment::weapon>();
	if (it == nullptr || it->item_type() != defs::item_type::weapon)
		//Attack with improper weapon (unusual situation)
		return 0;

	short hitModifier = 0;
	auto wt = templates::metadata::get_instance().get_weapon_template(it->id());
	if (wt->requires_projectiles())
	{
		//Missile weapon, dexterity/range modifiers apply
		hitModifier = hit_modifier_for_dexterity() + hit_modifier_for_missile_weapon_range(it, distance);
	}
	else if (wt->attack_type() == defs::weapons::attack_type::melee)
	{
		if (distance > 3)
		{
			auto resMissileRangeModif = templates::metadata::get_instance().fetch<int, short, short, short, short, short, short, short, short, short, short, short>(templates::metadata::query::missile_range_modifiers);
			auto modifs = resMissileRangeModif.filter<uint32_t>(static_cast<uint32_t>(it->id())).run();

			if (!modifs.has_values())
			{
				// Melee weapon, np missile weapon ranges available, target is out of range
				throw exceptions::target_out_of_range(defs::equipment::item::none, distance);
			}
			
			// Melee weapon, but missile weapon range is available, the weapon can be throws at the target
			hitModifier = hit_modifier_for_dexterity() + hit_modifier_for_missile_weapon_range(it, distance);
			return hitModifier;
		}
		//Melee weapon, strength modifier applies
		hitModifier = hit_modifier_for_strength();
	}
	else
		throw exceptions::target_out_of_range(m_hands[handSlot].item()->id(), distance);

	hitModifier += hit_modifier_for_weapon(it)
				+ hit_modifier_for_weapon_proficiency(it)
				+ hit_modifier_for_encumbrance();

	if (is_warrior() || is_rogue())
		hitModifier += hit_modifier_for_two_weapons_attack(handSlot);
	else
		throw std::exception("Unable to handle two weapons simultaneously");

	return hitModifier;
}

short adnd::character::hit_modifier_for_strength()
{
	auto st = templates::metadata::get_instance().get_skill_template(defs::character_skill::strength);
	auto strStats = st.get_strength_stats(strength_value());
	return strStats.hitModifier;
}

short adnd::character::hit_modifier_for_dexterity()
{
	auto st = templates::metadata::get_instance().get_skill_template(defs::character_skill::dexterity);
	auto dexStats = st.get_dexterity_stats(dexterity_value());
	return dexStats.missileAttackAdjustment;
}

short adnd::character::hit_modifier_for_weapon(equipment::item *it)
{	
	equipment::weapon *w = dynamic_cast<equipment::weapon*>(it);
	return w->bonus_malus();
}

short adnd::character::hit_modifier_for_weapon_proficiency(equipment::item *it)
{
	if (!adnd::configuration::get_instance().get(defs::config::optional::weapon_proficiency))
		return 0;

	return m_weaponProficiencies.hit_modifier(it->id());
}

short adnd::character::hit_modifier_for_missile_weapon_range(equipment::item *it, const short& distance)
{
	auto t = templates::metadata::get_instance().get_weapon_template(it->id());
	
	short hitModifier = 0;
	if (t->requires_projectiles())
	{
		auto profLevel = m_weaponProficiencies.proficiency_level(it->id());
		hitModifier = t->distance_modifier(distance, profLevel);
	}
	
	return hitModifier;
}

short adnd::character::hit_modifier_for_encumbrance()
{
	auto t = adnd::templates::metadata::get_instance().get_race_template(m_race);
	short hitModifier = 0;
	short movRate = movement_rate();

	if (movRate == 1)
		hitModifier = -4;
	else if (movRate <= t.base_movement_rate() / 3)
		hitModifier = -2;
	else if (movRate <= t.base_movement_rate() * 2 / 3)
		hitModifier = -1;

	return hitModifier;
}

short adnd::character::hit_modifier_for_two_weapons_attack(const defs::hand_slot_type& handSlot)
{
	short hitModifier = 0;
	if (two_weapons_attack())
	{
		if (m_class.is(defs::character_class::ranger))
			hitModifier = 0;
		else
			hitModifier = (handSlot == defs::hand_slot_type::primary) ? -2 : -4;

		auto st = templates::metadata::get_instance().get_skill_template(defs::character_skill::dexterity);
		auto stats = st.get_dexterity_stats(dexterity_value());
		hitModifier = std::min<short>(hitModifier + stats.reactionAdjustment, 0);
	}
	return hitModifier;
}

short adnd::character::damage_modifier_for_strength()
{
	auto st = templates::metadata::get_instance().get_skill_template(defs::character_skill::strength);
	auto strStats = st.get_strength_stats(strength_value());
	return strStats.damageAdjustment;
}

//short adnd::character::damage_modifier_for_dexterity()
//{
//	return 0;
//}

short adnd::character::damage_modifier_for_weapon(equipment::item *it)
{
	equipment::weapon *w = dynamic_cast<equipment::weapon*>(it);
	return w->bonus_malus();
}

short adnd::character::damage_modifier_for_weapon_proficiency(equipment::item *it)
{
	if (!adnd::configuration::get_instance().get(defs::config::optional::weapon_proficiency))
		return 0;

	return m_weaponProficiencies.damage_modifier(it->id());
}

//short adnd::character::damage_modifier_for_missile_weapon_range(equipment::item *it, const short& distance)
//{
//	return 0;
//}

short adnd::character::damage_modifier_for_encumbrance()
{
	return 0;
}


adnd::hit_result adnd::character::try_hit(const defs::hand_slot_type& handSlot, const short& targetArmourClass, const short& targetDistance)
{
	equipment::item *primaryItem = m_hands[defs::hand_slot_type::primary].item<equipment::item>();
	equipment::item *secondaryItem = m_hands[defs::hand_slot_type::secondary].item<equipment::item>();
	equipment::item *w = (handSlot == defs::hand_slot_type::primary) ? primaryItem : secondaryItem;

	if (m_hands[handSlot].busy() && w->item_type() != adnd::defs::item_type::weapon)
	{
		std::stringstream ss;
		ss << "Unable to deliver attack using " << w->name();
		throw std::exception(ss.str().c_str());
	}

	short hitModifier = hit_modifier(handSlot, targetDistance);
	short thacoValue = thaco();

	auto roll = random::die(defs::die_faces ::twenty).roll();

	if (roll == static_cast<short>(defs::die_faces ::twenty))
		return hit_result::critical_hit;
	else if (roll == 1)
			return hit_result::critical_failure;
	else if (roll + hitModifier >= thacoValue - targetArmourClass)
		return adnd::hit_result::hit;
	else
		return hit_result::missed;
}

short adnd::character::initiative(const defs::hand_slot_type& handSlot)
{
	adnd::random::die_roll r(adnd::defs::die_faces ::ten, 1);
	short initiativeResult = r.roll<short>();

	initiativeResult += m_hands[handSlot].item()->initialive_modifier();

	return initiativeResult;
}

void adnd::character::swap_item(const defs::hand_slot_type& handSlot, const uint32_t& itemUid)
{
	auto it = m_inventory.remove(itemUid);
	auto itemInUse = m_hands[handSlot].drop();
	m_hands[handSlot] = it;
	m_inventory += it;
}

adnd::defs::proficiency::level adnd::character::add_proficiency(const defs::equipment::item& item)
{
	if (!adnd::configuration::get_instance().get(defs::config::optional::weapon_proficiency))
		return defs::proficiency::level::none;

	auto currentProfLevel = m_weaponProficiencies.proficiency_level(item);
	if (currentProfLevel == defs::proficiency::level::proficient
		&& (m_class.is_multiclass() || !m_class.is(defs::character_class::fighter)))
	{
		/*
		In one way, a weapon specialist is like a wizard specialist.
		The specialization requires a single-minded dedication and training.
		Thus, multi-class characters cannot use weapon specialization; it is available only to single-class fighters
		*/
		auto t = templates::metadata::get_instance().get_class_template(m_class.id());
		std::stringstream ss;
		ss << t.get_long_name() << " cannot get weapon specialisation (only single-class fighters can)";
		throw std::exception(ss.str().c_str());
		
	}

	m_weaponProficiencies += item;
	return m_weaponProficiencies.proficiency_level(item);
}

bool adnd::character::two_weapons_attack()
{
	equipment::item *primaryItem = m_hands[defs::hand_slot_type::primary].item<equipment::item>();
	equipment::item *secondaryItem = m_hands[defs::hand_slot_type::secondary].item<equipment::item>();

	return m_hands[defs::hand_slot_type::primary].busy()
			&& m_hands[defs::hand_slot_type::secondary].busy()
			&& primaryItem->item_type() == adnd::defs::item_type::weapon
			&& secondaryItem->item_type() == adnd::defs::item_type::weapon
			&& primaryItem->hand_required() != defs::item_hand_required::both;
}

//const adnd::tables::attack_rate& adnd::character::get_attack_rate(const defs::weapons::attack_type& attackType)
//{
//	auto t = adnd::templates::metadata::get_instance().get_class_template(m_class);
//	return t.get_attack_rate(m_class, level().get_level(), attackType);
//}

//short adnd::character::thaco()
//{
//	short thaco = 20;
//
//	for (auto lvl : m_level)
//	{
//		db::sqlite_db::record r;
//		adnd::templates::metadata::get_instance().get_thaco_info(lvl.first, r);
//
//		auto score = adnd::templates::utils::parse<short>(r[0]);
//		auto factor = adnd::templates::utils::parse<short>(r[1]);
//
//		short t = 20 - (lvl.second.level() - 1 / score) * factor;
//
//		thaco = std::min<short>(t, thaco);
//	}
//
//	return thaco;
//}

short adnd::character::thaco()
{
	short thaco = defs::base_thaco;

	for (auto lvl : m_level)
	{
		auto t = adnd::templates::metadata::get_instance().get_class_template(lvl.first);
		thaco = std::min<short>(thaco, t.get_thaco(lvl.second.level()));
	}

	return thaco;
}

//bool adnd::character::can_afford(const defs::equipment::item& itemId)
//{
//	auto t = templates::metadata::get_instance().get_item_template(itemId);
//	auto cost = money::amount(t->min_cost_currency(), t->min_cost_amount());
//
//	return m_moneyBag.check_availability(cost);
//}
//
//adnd::equipment::item* adnd::character::buy(const defs::equipment::item& itemId)
//{
//	adnd::equipment::item* it = nullptr;
//	if (can_afford(itemId))
//	{
//		auto& pool = adnd::equipment::items_pool::get_instance();
//		auto t = templates::metadata::get_instance().get_item_template(itemId);
//		it = pool.create(t->id(), t->description(), t->item_class());
//
//		money::amount price(t->min_cost_currency(), t->min_cost_amount());
//		m_moneyBag.subtract(price);
//		m_inventory += it;
//	}
//	return it;
//}
//
//adnd::equipment::item* adnd::character::sell(const uint32_t& itemUid)
//{
//	auto it = m_inventory.remove(itemUid);
//	auto t = templates::metadata::get_instance().get_item_template(it->id());
//
//	money::amount price(t->min_cost_currency(), t->min_cost_amount());
//	m_moneyBag += price;
//	return it;
//}

short adnd::character::strength(short& excStr)
{
	if (is_warrior())
		excStr = m_skills[defs::character_skill::strength].exceptional();
	return m_skills[defs::character_skill::strength].value();
}

short adnd::character::dexterity()
{
	return m_skills[defs::character_skill::dexterity].value();
}

short adnd::character::constitution()
{
	return m_skills[defs::character_skill::constitution].value();
}

short adnd::character::intelligence()
{
	return m_skills[defs::character_skill::intelligence].value();
}

short adnd::character::wisdom()
{
	return m_skills[defs::character_skill::wisdom].value();
}

short adnd::character::charisma()
{
	return m_skills[defs::character_skill::charisma].value();
}

double adnd::character::encumbrance()
{
	if (!adnd::configuration::get_instance().get(defs::config::optional::encumbrance))
		return 0;

	double weight = 2.5;	// Take into account base encumbrance for clothes
	for (auto h : m_hands)
		weight += (h.second.item()) ? h.second.item()->weight() : 0.0;

	weight += m_inventory.weight();

	return weight;
}

short adnd::character::movement_rate()
{
	auto t = adnd::templates::metadata::get_instance().get_race_template(m_race);
	return t.get_movement_rate(m_skills.strength(), encumbrance());
}

void adnd::character::pillage(treasure::treasure& tr)
{
	treasure::treasure_pool::get_instance().detatch(tr.uid());

	for (auto c : tr.coins())
	{
		m_moneyBag += tr.coins(c);
	}

	for (auto g : tr.gems())
	{
		m_inventory.add(g);
	}

	for (auto o : tr.objects_of_art())
	{
		m_inventory.add(o);
	}
}

//void adnd::character::set_worshipped_deity(const defs::deities::deity& deityId)
//{
//	auto t = templates::metadata::get_instance().get_deity_template(deityId);
//	if (!t.is_worshipped_by(m_alignment))
//		throw std::exception("Deity not allowed for moral alignment", static_cast<int>(m_alignment));
//
//	m_deity = deityId;
//}

const adnd::skills::character_skill_value& adnd::character::strength_value()
{
	return m_skills[defs::character_skill::strength];
}

const adnd::skills::character_skill_value& adnd::character::dexterity_value()
{
	return m_skills[defs::character_skill::dexterity];
}

const adnd::skills::character_skill_value& adnd::character::constitution_value()
{
	return m_skills[defs::character_skill::constitution];
}

const adnd::skills::character_skill_value& adnd::character::intelligence_value()
{
	return m_skills[defs::character_skill::intelligence];
}

const adnd::skills::character_skill_value& adnd::character::wisdom_value()
{
	return m_skills[defs::character_skill::wisdom];
}

const adnd::skills::character_skill_value& adnd::character::charisma_value()
{
	return m_skills[defs::character_skill::charisma];
}

short adnd::character::get_ageing_adjustment(const adnd::defs::character_skill& skill)
{
	auto r = templates::metadata::get_instance().fetch<uint16_t, short, short, short, short, short, short>(templates::metadata::query::ageing_modifiers);
	auto modifResult = r.filter<short>(static_cast<short>(ageing())).run();

	if (!modifResult.has_values())
		return 0;

	auto modif = modifResult.first();
	if (skill == defs::character_skill::strength)
		return std::get<1>(modif);
	else if (skill == defs::character_skill::dexterity)
		return std::get<2>(modif);
	else if (skill == defs::character_skill::constitution)
		return std::get<3>(modif);
	else if (skill == defs::character_skill::intelligence)
		return std::get<4>(modif);
	else if (skill == defs::character_skill::wisdom)
		return std::get<5>(modif);
	else
		return std::get<6>(modif);
}	

bool adnd::character::grow_old(const short& years)
{
	auto pre = templates::metadata::get_instance().fetch<uint16_t, short, short, short, short, short, short>(templates::metadata::query::ageing_modifiers);
	auto rPre = pre.filter<short>(static_cast<short>(ageing())).run();

	if (rPre.has_values())
	{
		auto prevModif = rPre.first();
		m_skills[defs::character_skill::strength] -= std::get<1>(prevModif);
		m_skills[defs::character_skill::dexterity] -= std::get<2>(prevModif);
		m_skills[defs::character_skill::constitution] -= std::get<3>(prevModif);
		m_skills[defs::character_skill::intelligence] -= std::get<4>(prevModif);
		m_skills[defs::character_skill::wisdom] -= std::get<5>(prevModif);
		m_skills[defs::character_skill::charisma] -= std::get<6>(prevModif);
	}

	m_age += years;

	auto post = templates::metadata::get_instance().fetch<uint16_t, short, short, short, short, short, short>(templates::metadata::query::ageing_modifiers);
	auto rPost = post.filter<short>(static_cast<short>(ageing())).run();

	if (rPost.has_values())
	{
		auto postModif = rPost.first();

		m_skills[defs::character_skill::strength] += std::get<1>(postModif);
		m_skills[defs::character_skill::dexterity] += std::get<2>(postModif);
		m_skills[defs::character_skill::constitution] += std::get<3>(postModif);
		m_skills[defs::character_skill::intelligence] += std::get<4>(postModif);
		m_skills[defs::character_skill::wisdom] += std::get<5>(postModif);
		m_skills[defs::character_skill::charisma] += std::get<6>(postModif);
	}

	return m_age > m_lifeExpectancy;
}

bool adnd::character::rejuvenate(const short& years)
{
	auto pre = templates::metadata::get_instance().fetch<uint16_t, short, short, short, short, short, short>(templates::metadata::query::ageing_modifiers);
	auto prevModif = pre.filter<short>(static_cast<short>(ageing())).run().first();

	m_skills[defs::character_skill::strength] -= std::get<1>(prevModif);
	m_skills[defs::character_skill::dexterity] -= std::get<2>(prevModif);
	m_skills[defs::character_skill::constitution] -= std::get<3>(prevModif);
	m_skills[defs::character_skill::intelligence] -= std::get<4>(prevModif);
	m_skills[defs::character_skill::wisdom] -= std::get<5>(prevModif);
	m_skills[defs::character_skill::charisma] -= std::get<6>(prevModif);
	
	m_age -= years;

	auto post = templates::metadata::get_instance().fetch<uint16_t, short, short, short, short, short, short>(templates::metadata::query::ageing_modifiers);
	auto postModif = post.filter<short>(static_cast<short>(ageing())).run().first();

	m_skills[defs::character_skill::strength] += std::get<1>(postModif);
	m_skills[defs::character_skill::dexterity] += std::get<2>(postModif);
	m_skills[defs::character_skill::constitution] += std::get<3>(postModif);
	m_skills[defs::character_skill::intelligence] += std::get<4>(postModif);
	m_skills[defs::character_skill::wisdom] += std::get<5>(postModif);
	m_skills[defs::character_skill::charisma] += std::get<6>(postModif);

	return m_age >= 0;
}

const adnd::defs::character_age adnd::character::ageing()
{
	auto t = adnd::templates::metadata::get_instance().get_race_template(m_race);
	if (m_age < t.middle_age())
		return defs::character_age::young;
	else if (m_age < t.old_age())
		return defs::character_age::middle_age;
	else if (m_age < t.venerable_age())
		return defs::character_age::old;
	else
		return defs::character_age::venerable;
}

bool adnd::character::save_against(const defs::saving_throws& saveThrow)
{
	short bestScore = 20;
	for (auto& lvl : m_level)
	{
		auto t = templates::metadata::get_instance().get_class_template(lvl.first);
		auto st = t.saving_throws(lvl.second.level());
		
		short score = st.save(saveThrow);
		bestScore = std::min<short>(score, bestScore);
	}

	auto roll = random::die(defs::die_faces ::twenty).roll();

	return roll >= bestScore;
}

const adnd::tables::turn_undead adnd::character::turn_undead(const defs::turn_ability& type)
{
	auto clsTempl = adnd::templates::metadata::get_instance().get_class_template(m_class.id());
	adnd::tables::turn_undead tu;

	if (!clsTempl.can_turn_undead())
	{
		if (adnd::configuration::get_instance().get(defs::config::optional::exception_on_turn_undead))
		{
			std::stringstream ss;
			ss << get_name() << " cannot turn undead";
			throw std::exception(ss.str().c_str());
		}
		else
			return tables::turn_undead(defs::turn_effect::no_effect, 0);
	}

	if (m_class.is(defs::character_class::paladin) && get_level() >= 3)
		tu = clsTempl.turn(get_level() - 2, type);
	else
		tu = clsTempl.turn(get_level(), type);

	return tu;
}

bool adnd::character::do_thievery(const adnd::defs::thief_ability& abil)
{
	short score = 0;	//Such score means no chance to succede, since every roll will fail against it

	for (auto cls : m_class.classes())
	{
		auto t = templates::metadata::get_instance().get_class_template(cls);
		if (t.has_thief_abilities())
		{
			score = m_thiefAdvancement.ability(m_level[cls].level(), abil);
		}
	}
	//If the character has thief abilities, a valid score has been selected at this point

	auto roll = random::die(defs::die_faces ::ten).roll() * 10 + random::die(defs::die_faces ::ten).roll();

	// Max the ability score if it exceeds 95%
	return roll <= std::min<short>(score, 95);
}

void adnd::character::improve_thief_abilities(const short& pick, const short& open, const short& traps, const short& move, const short& hide, const short& hear, const short& climb, const short& read)
{
	short scoreTotal = pick + open + traps + move + hide + hear + climb + read;

	if (!m_class.is(defs::character_class::thief))
	{
		auto p = templates::metadata::get_character_type_and_id(m_class.id());
		auto r = templates::metadata::get_instance().fetch<uint32_t, uint16_t, uint16_t, std::string, std::string, std::string, char>(templates::metadata::query::class_info);
		auto info = r.filter<uint16_t>(p.classType).filter<uint32_t>(p.classId).run().first();
		
		std::stringstream ss;
		ss << std::get<3>(info) << " unable to improve thief abilities";
		throw std::exception(ss.str().c_str());
	}

	defs::character_class cls = defs::character_class::thief;

	if (m_level[cls].level() > 1 && m_level[cls].level() <= m_thiefAdvancement.size())
	{
		throw std::exception("Higher level is required to further improve thief abilities", static_cast<int>(m_thiefAdvancement.size()));
	}

	// Level = 1 or there is room for improvements
	short scoreSumLimit = 0;
	short scoreLimit = 0;

	if (m_level[cls].level() == 1)
	{
		scoreSumLimit = 60;
		scoreLimit = 30;
	}
	else
	{
		scoreSumLimit = 30;
		scoreLimit = 15;
	}

	if (scoreTotal != scoreSumLimit)
	{
		std::stringstream ss;
		ss << "Thief scores advancement must sum up to " << scoreSumLimit;
		throw std::exception(ss.str().c_str(), scoreSumLimit);
	}

	for (auto& abil : { pick, open, traps, move, hide, hear, climb, read })
	{
		if (abil < 0 || abil > scoreLimit)
		{
			std::stringstream ss;
			ss << "Each advancement score must be between 0 and " << scoreLimit;
			throw std::exception(ss.str().c_str(), abil);
		}
	}
			
	m_thiefAdvancement.add(pick, open, traps, move, hide, hear, climb, read);
}

void adnd::character::improve_bard_abilities(const short& pick, const short& hear, const short& climb, const short& read)
{
	short scoreTotal = pick + hear + climb + read;

	if (!m_class.is(defs::character_class::bard))
	{
		auto p = templates::metadata::get_character_type_and_id(m_class.id());
		auto r = templates::metadata::get_instance().fetch<uint32_t, uint16_t, uint16_t, std::string, std::string, std::string, char>(templates::metadata::query::class_info);
		auto info = r.filter<uint16_t>(p.classType).filter<uint32_t>(p.classId).run().first();

		std::stringstream ss;
		ss << std::get<3>(info) << " unable to improve thief abilities";
		throw std::exception(ss.str().c_str());
	}

	defs::character_class cls = defs::character_class::bard;

	if (m_level[cls].level() > 1 && m_level[cls].level() <= m_thiefAdvancement.size())
	{
		throw std::exception("Higher level is required to further improve thief abilities", static_cast<int>(m_thiefAdvancement.size()));
	}

	// Level = 1 or there is room for improvements
	short scoreSumLimit = 0;
	short scoreLimit = 0;
	if (m_level[cls].level() == 1)
	{
		scoreSumLimit = 20;
		scoreLimit = 20;
	}
	else
	{
		scoreSumLimit = 15;
		scoreLimit = 15;
	}

	if (scoreTotal != scoreSumLimit)
	{
		std::stringstream ss;
		ss << "Thief scores advancement must sum up to " << scoreSumLimit;
		throw std::exception(ss.str().c_str(), scoreSumLimit);
	}

	for (auto& abil : { pick, hear, climb, read })
	{
		if (abil < 0 || abil > scoreLimit)
		{
			std::stringstream ss;
			ss << "Each advancement score must be between 0 and " << scoreLimit;
			throw std::exception(ss.str().c_str(), abil);
		}
	}

	m_thiefAdvancement.add(pick, 0, 0, 0, 0, hear, climb, read);
}

adnd::spells::spell_book& adnd::character::spell_book()
{
	auto t = templates::metadata::get_instance().get_class_template(m_class.id());
	if (!t.can_cast_wizard_spells())
	{
		std::stringstream ss;
		ss << t.get_long_name() << " has no spell book";
		throw std::exception(ss.str().c_str());
	}
	return m_spellBook;
}

adnd::spells::holy_symbol& adnd::character::holy_symbol()
{
	auto t = templates::metadata::get_instance().get_class_template(m_class.id());
	if (!t.can_cast_priest_spells())
	{
		std::stringstream ss;
		ss << t.get_long_name() << " has no holy symbol";
		throw std::exception(ss.str().c_str());
	}
	return m_holySymbol;
}

bool adnd::character::alive()
{
	if (m_age > m_lifeExpectancy)
		return false;
	else
		return m_hitPoints.alive();
}

void adnd::character::add_hp(const defs::character_class& cls, const short& hp)
{
	m_hitPoints.add(cls, hp);
}

void adnd::character::add_hp(const defs::character_class& cls, const std::vector<short>& hps)
{
	m_hitPoints.add(cls, hps);
}

short adnd::character::max_hp()
{
	return m_hitPoints.max();
}

void adnd::character::add_xp(const long& xp)
{
	auto oldLevels = get_levels();
	std::map<defs::character_class, long> oldXPs;
	for (auto l : oldLevels)
	{
		oldXPs[l.first] = get_xp(l.first);
	}

	m_level.add_xp(xp);

	auto newLevels = get_levels();
	for (auto l : newLevels)
	{
		if (newLevels[l.first] > oldLevels[l.first])
			on_xp_change(oldXPs[l.first], get_xp(l.first), oldLevels[l.first], newLevels[l.first], l.first, class_level::experience_shift::level_up);
	}
}

void adnd::character::gain_level(const short& levelIncrement)
{
	auto oldLevels = get_levels();
	std::map<defs::character_class, long> oldXPs;
	for (auto l : oldLevels)
	{
		oldXPs[l.first] = get_xp(l.first);
	}

	m_level.up(levelIncrement);

	auto newLevels = get_levels();
	for (auto l : newLevels)
	{
		if (newLevels[l.first] > oldLevels[l.first])
			on_xp_change(oldXPs[l.first], get_xp(l.first), oldLevels[l.first], newLevels[l.first], l.first, class_level::experience_shift::level_up);
	}
}

void adnd::character::drain_level(const short& levelDecrement)
{
	auto oldLevels = get_levels();
	std::map<defs::character_class, long> oldXPs;
	for (auto l : oldLevels)
	{
		oldXPs[l.first] = get_xp(l.first);
	}

	m_level.down(levelDecrement);

	auto newLevels = get_levels();
	for (auto l : newLevels)
	{
		if (newLevels[l.first] > oldLevels[l.first])
			on_xp_change(oldXPs[l.first], get_xp(l.first), oldLevels[l.first], newLevels[l.first], l.first, class_level::experience_shift::level_down);
	}
}

void adnd::character::gain_level(const defs::character_class& cls, const short& levelIncrement)
{
	auto oldLevel = get_level(cls);
	auto oldXP = get_xp(cls);

	m_level.up(cls, levelIncrement);

	on_xp_change(oldXP, get_xp(cls), oldLevel, oldLevel + levelIncrement, cls, class_level::experience_shift::level_up);
}

void adnd::character::drain_level(const defs::character_class& cls, const short& levelDecrement)
{
	auto oldLevel = get_level(cls);
	auto oldXP = get_xp(cls);

	m_level.down(cls, levelDecrement);

	on_xp_change(oldXP, get_xp(cls), oldLevel, oldLevel - levelDecrement, cls, class_level::experience_shift::level_down);
}

void adnd::character::heal(const short& hp)
{
	m_hitPoints.heal(hp);
}

bool adnd::character::wound(const short& hp)
{
	/*In addition to dying when hit points reach 0, a character also runs the risk of dying
	abruptly when he suffers massive amounts of damage. A character who suffers 50 or
	more points of damage from a single attack must roll a successful saving throw vs. death,
	or he dies*/
	if (hp >= defs::critical_damage && m_hitPoints.current_hps() - hp > 0)
	{
		if (!save_against(defs::saving_throws::death_magic))
			m_hitPoints.wound(m_hitPoints.current_hps());
	}
	else
	{
		m_hitPoints.wound(hp);
	}
	return alive();
}

void adnd::character::check_resurrection_survival()
{
	if (m_skills[defs::character_skill::constitution].value() == 1)
	{
		// When the character's Constitution drops to 0, that character can no longer be raised
		// He is permanently removed from play
		std::stringstream ss;
		ss << get_name() << " did not survive resurrection";
		throw std::exception(ss.str().c_str());
	}

	auto st = templates::metadata::get_instance().get_skill_template(defs::character_skill::constitution);
	auto constStats = st.get_constitution_stats(constitution_value());

	random::die d(defs::die_faces::ten);
	auto prob = d.roll() + d.roll() * 10;
	if (prob > constStats.resurrectionSurvival)
	{
		std::stringstream ss;
		ss << get_name() << " did not survive resurrection";
		throw std::exception(ss.str().c_str());
	}

	// A character restored to life in this way has his Constitution permanently lowered by 1 point
	m_skills[defs::character_skill::constitution] = m_skills[defs::character_skill::constitution].value() - 1;
}

void adnd::character::raise()
{
	check_resurrection_survival();
	m_hitPoints.raise();
}

void adnd::character::raise_full()
{
	check_resurrection_survival();
	m_hitPoints.raise_full();
}

double adnd::character::health()
{
	if (m_age > m_lifeExpectancy)
		return 0.0;
	else
		return m_hitPoints.health();
}

//void adnd::character::gain_experience(const long& xp)
//{
//	level().add_xp(xp);
//}
//
//void adnd::character::lose_experience(const long& xp)
//{
//	level().add_xp(-xp);
//}
//
//void adnd::character::level_up(const short& levels)
//{
//	level().up(levels);
//}
//
//void adnd::character::level_down(const short& levels)
//{
//	level().down(levels);
//}

void adnd::hp_sequence::drain(const short& levels)
{
	if (levels >= static_cast<short>(m_hp.size()))
	{
		m_maxHP = 0;
		m_currentHP = 0;
		m_hp.erase(m_hp.begin(), m_hp.end());
	}
	else
	{
		size_t newSize = m_hp.size() - levels;
		m_currentHP = 0;
		for (size_t s = 0; s < newSize; ++s)
			m_currentHP += m_hp[s];
		for (size_t s = newSize; s < m_hp.size(); ++s)
			m_maxHP -= m_hp[s];
		m_hp.resize(newSize);
	}
}

short adnd::hp_sequence::max()
{
	return m_maxHP;
}

double adnd::hp_sequence::health()
{
	if (m_maxHP == 0)
		return 0.0;
	else
		return m_currentHP / static_cast<double>(m_maxHP);
}

void adnd::hp_sequence::heal(const short& hp)
{
	if (!alive())
		throw std::exception("Unable to heal a dead character, resurrection required");

	if (m_currentHP + hp > m_maxHP)
		m_currentHP = m_maxHP;
	else
		m_currentHP += hp;
}

void adnd::hp_sequence::wound(const short& hp)
{
	if (m_currentHP - hp < 0)
		m_currentHP = 0;
	else
		m_currentHP -= hp;
}

void adnd::hp_sequence::raise()
{
	if (alive())
		throw std::exception("Resurrection has no effect on a living character");

	m_currentHP = 1;
}

void adnd::hp_sequence::raise_full()
{
	if (alive())
		throw std::exception("Resurrection has no effect on a living character");

	if (m_maxHP == 0)
		throw std::exception("Life energy was completely drained, resurrection has no effect");

	m_currentHP = m_maxHP;
}

//void adnd::hp_sequence::set_constitution_adjustment(const short & constitutionAdjustment)
//{
//	for (int posHP = 0; posHP < m_hp.size(); ++posHP)
//	{
//		m_hp[posHP] -= m_constitutionAdjustment;
//	}
//	m_constitutionAdjustment = constitutionAdjustment;
//	for (int posHP = 0; posHP < m_hp.size(); ++posHP)
//	{
//		m_hp[posHP] += m_constitutionAdjustment;
//	}
//}

adnd::hit_points::hit_points(const defs::character_class& cls) : m_currentHP(0)
{
	auto t = templates::metadata::get_instance().get_class_template(cls);
	auto cc = t.get_class_composition();
	for (auto& c : cc.classes())
	{
		m_hps.emplace(c, hp_sequence());
	}
}

void adnd::hit_points::drain(const short& levels)
{
	if (levels == 0)
		return;

	short maxSize = 0;
	short totSize = 0;
	hp_sequence* seq = nullptr;
	for (auto& h : m_hps)
	{
		if (h.second.size() > maxSize)
		{
			maxSize = h.second.size();
			seq = &h.second;
		}
		totSize += h.second.size();
	}

	if (totSize - levels < static_cast<short>(m_hps.size()))
	{
		// Energy drain exceed the total number of levels. The character dies...
		/*
			According to the rules:
			If a character is drained to 0 level but still retains hit points (i.e., he is still alive),
			that character's adventuring career is over. He cannot regain levels and has lost all benefits
			of a character class. The adventurer has become an ordinary person

			Ddlib does not take into account characters of level 0, in this case they automatically die...
		*/
		m_currentHP = 0;
		for (auto& h : m_hps)
			m_hps[h.first].drain(m_hps[h.first].size());
	}
	else
	{
		// HP given for that level are lost...
		short hp = seq[seq->size() - 1];
		short hpDrained = weighted_hp(hp);
		seq->drain(1);
		m_currentHP = std::max(m_currentHP - hpDrained, 1);

		drain(levels - 1);
	}
}

/**
Adds a single hit point value to the specified base class (no multiclass is allowed).
The input HP value is the result of a die roll, no constitution bonus needs to be specified
*/
void adnd::hit_points::add(const defs::character_class& cls, const short& hp)
{
	if (hp < 0)
		throw std::exception("HP value must be greater than zero");

	auto t = templates::metadata::get_instance().get_class_template(cls);
	auto cc = t.get_class_composition();

	if (cc.is_multiclass())
		throw std::exception("Unable to add a single hit point value to a multiclass character");

	auto d = t.hit_dice()[cls];
	if (hp > static_cast<short>(d.faces()))
		throw std::exception("HP value cannot exceed the maximum HD value for the class");

	m_hps[cls] += hp;

	m_currentHP += weighted_hp(hp);
}

short adnd::hit_points::weighted_hp(const short& hp, const bool& firstLevel)
{
	short divisor = static_cast<short>(m_hps.size());
	short weightedHP = std::max(std::div(hp, divisor).quot, 1);

	if (firstLevel)
		weightedHP += m_constitutionAdjustment;
	else
		weightedHP += std::div(m_constitutionAdjustment, divisor).quot;

	return weightedHP;
}

/**
	Notify the character is dead by energy drain.
	They aren't simply dead, their life energy has been completely drained.
	If the character is restored, it starts over from the 1sh level
*/
bool adnd::hit_points::drain_overflow()
{
	short sum = 0;
	for (auto& h : m_hps)
		sum += h.second.size();
	return m_currentHP == 0 && sum == 0;
}

/**
Adds a set of hit point values to the specified base class.
If multiclass is specified, the values are assigned in order to each base class.
The current HP score increment equals the sum of the HPs divided by the number of base classes,
fraction rounded down, plus the constitution bonus.
Otherwise, values are assigned in order to the same base class.
Each input HP value is the result of a die roll, no constitution bonus needs to be specified
*/
void adnd::hit_points::add(const defs::character_class& cls, const std::vector<short>& hps)
{
	auto t = templates::metadata::get_instance().get_class_template(cls);
	auto cc = t.get_class_composition();
	if (!cc.is_multiclass())
	{
		for (size_t sz = 0; sz < hps.size(); ++sz)
		{
			add(cls, hps[sz]);
		}
		return;
	}

	short sz = static_cast<short>(hps.size());
	if (sz % cc.classes().size() != 0)
		throw std::exception("HP sequence does not match the size of the multiclass type");

	size_t index = 0;
	auto dice = t.hit_dice();
	short hpIncrement = 0;
	for (auto& d : dice)
	{
		m_hps[d.first] += hps[index];
		hpIncrement += hps[index];
		++index;
	}
	m_currentHP += weighted_hp(hpIncrement, true);
}
//void adnd::hit_points::add(const defs::character_class& cls, const std::vector<short>& hps)
//{
//	auto t = templates::metadata::get_instance().get_class_template(cls);
//	auto cc = t.get_class_composition();
//	if (!cc.is_multiclass())
//	{
//		for (size_t sz = 0; sz < hps.size(); ++sz)
//		{
//			add(cls, hps[sz]);
//		}
//		return;
//	}
//
//	if (hps.size() % cc.classes().size() != 0)
//		throw std::exception("HP sequence does not match the size of the multiclass type");
//
//	size_t index = 0;
//	short _hp = 0;
//	std::vector<double> ratios;
//	ratios.resize(cc.classes().size());
//
//	auto dice = t.hit_dice();
//	for (auto& d : dice)
//	{
//		_hp += hps[index];
//		ratios[index] = hps[index];
//		++index;
//	}
//	for (auto& r : ratios)
//		r = r / _hp;
//
//	short sz = 0;
//	for (auto& d : dice)
//	{
//		m_hps[d.first] += std::max<short>(static_cast<short>(hps[sz] * ratios[sz]), 1);
//		++sz;
//	}
//	m_currentHP += _hp / sz;
//}

short adnd::hit_points::size(const defs::character_class& cls)
{
	short sz = 0;
	if (cls == defs::character_class::none)
		sz = m_hps.begin()->second.size();
	else
		sz = m_hps[cls].size();
	return sz;
}

/**
Returns the maximum HPs available for a character when completely healthy.
The figure sums up to the total dice rolled when advancing in experience
*/
short adnd::hit_points::max()
{
	short _max = 0;

	//Sum first level scores
	for (auto& h : m_hps)
		if (h.second.size() == 0)
			return 0;	// Energy completely drained in one of the base classes. The character is dead
		else
			_max += h.second[0];

	//Divide by number of base classes, plus whole constitution bonus
	short divisor = static_cast<short>(m_hps.size());
	_max = std::max(std::div(_max, divisor).quot, 1) + m_constitutionAdjustment;

	//Successive levels are computed individually
	short partialMax = 0;
	for (auto& h : m_hps)
	{
		for (size_t i=1; i<h.second.size(); ++i)
		{
			auto val = std::div(h.second[i], divisor).quot;
			partialMax += std::max(val, 1) + std::div(m_constitutionAdjustment, divisor).quot;
		}
	}
	return _max + partialMax;
}

/**
Returns the sum of the die rolls for each base class, adjusted by the costitution bonus
*/
short adnd::hit_points::current_hps()
{
	return m_currentHP;
}

void adnd::hit_points::heal(const short& hp)
{
	if (!alive())
		throw std::exception("Unable to heal a dead character, resurrection required");

	if (m_currentHP + hp > max())
		m_currentHP = max();
	else
		m_currentHP += hp;
}

void adnd::hit_points::wound(const short& hp)
{
	if (m_currentHP - hp < 0)
		m_currentHP = 0;
	else
		m_currentHP -= hp;
}

void adnd::hit_points::raise()
{
	if (alive())
		throw std::exception("Resurrection has no effect on a living character");

	if (drain_overflow())
		throw std::exception("Life energy was completely drained, resurrection has no effect");

	m_currentHP = 1;
}

void adnd::hit_points::raise_full()
{
	if (alive())
		throw std::exception("Resurrection has no effect on a living character");

	// Does restoration (priests' 7th level spell) overcome this limit?
	if (max() == 0)
		throw std::exception("Life energy was completely drained, resurrection has no effect");

	m_currentHP = max();
}

double adnd::hit_points::health()
{
	auto maxHP = max();
	if (maxHP == 0)
		return 0.0;
	else
		return m_currentHP / static_cast<double>(maxHP);
}

/**
Returns the maximum amount of HPs possible for a character
*/
short adnd::hit_points::limit()
{
	if (drain_overflow())
		return 0;

	short _limit = 0;

	//Sum first level scores
	for (auto& h : m_hps)
	{
		auto t = templates::metadata::get_instance().get_class_template(h.first);
		auto faces = static_cast<short>(t.hit_dice()[h.first].faces());
		_limit += faces;
	}

	//Divide by number of base classes, plus whole constitution bonus
	short divisor = static_cast<short>(m_hps.size());
	_limit = std::max(std::div(_limit, divisor).quot, 1) + m_constitutionAdjustment;

	//Successive levels are computed individually
	short partialLimit = 0;
	short constAdj = std::div(m_constitutionAdjustment, divisor).quot;
	for (auto& h : m_hps)
	{
		auto t = templates::metadata::get_instance().get_class_template(h.first);
		auto faces = static_cast<short>(t.hit_dice()[h.first].faces());

		auto val = std::div(faces, divisor).quot + constAdj;
		partialLimit += val * (h.second.size() - 1);
	}
	return std::max(0, _limit + partialLimit);
}

/**
Returns the maximum amount of HPs possible for a character given a base class component.
For multiclass characters, it represents one of the base classes.
For monoclass characters, it is equivalent to call 'limit()'
*/
short adnd::hit_points::limit(const defs::character_class& cls)
{
	auto t = templates::metadata::get_instance().get_class_template(cls);

	auto d = t.hit_dice()[cls];
	short _limit = static_cast<short>(d.faces()) * m_hps[cls].size();

	return std::div(_limit, static_cast<short>(m_hps.size())).quot;
}

/**
Returns the total extra HP for constitution bonus/malus
*/
short adnd::hit_points::get_bonus_hps()
{
	/*
	The character's hit points are the average of all his Hit Dice rolls. When the character is
	first created, the player rolls hit points for each class separately, totals them up, then
	divides by the number of dice rolled (round fractions down). Any Constitution bonus is
	then added to the character's hit points.
	Later the character is likely to gain levels in different classes at different times. When
	this happens, roll the appropriate Hit Die and divide the result by the number of classes
	the character has (round fractions down, but a Hit Die never yields less than 1 hit point).
	The character's Constitution bonus is split between his classes
	*/

	// Constitution bonus in fully applied at 1st level
	short bonusHP = m_constitutionAdjustment;
	short divisor = static_cast<short>(m_hps.size());

	/*
	Then, for each further level then 1st, the bonus is divided by the number of base classes
	(fraction rounded down)
	*/
	for (auto& h : m_hps)
	{
		auto _bonus = std::div(m_constitutionAdjustment, divisor).quot;
		bonusHP += _bonus * (h.second.size() - 1); // 1st level excluded
	}
	return bonusHP;
}

/**
Returns the bonus apllied to each die roll for HPs
*/
short adnd::hit_points::get_constitution_adjustment()
{
	return m_constitutionAdjustment;
}

void adnd::hit_points::set_constitution_adjustment(const short & constitutionAdjustment)
{
	if (m_constitutionAdjustment != constitutionAdjustment)
	{
		short divisor = static_cast<short>(m_hps.size());

		auto prevBonus = get_bonus_hps();
		m_constitutionAdjustment = constitutionAdjustment;
		auto currentBonus = get_bonus_hps();

		// When first initialised, bonus does not apply
		if (m_currentHP > 0)
			m_currentHP = m_currentHP - prevBonus + currentBonus;
	}
}

adnd::equipment::item* adnd::hand_slot::drop()
{
	if (m_item == nullptr)
		std::exception("No items to drop");

	equipment::item* it = m_item;
	m_item = nullptr;
	return it;
}


adnd::class_level::class_level(const defs::character_class& cls)
	: m_class(cls)
{
	auto t = templates::metadata::get_instance().get_class_template(cls);
	auto cc = t.get_class_composition();

	for (auto& c : cc.classes())
	{
		m_levels[c].level() = 1;
		m_levels[c].set_xp(0);
		m_levels[c].get_class() = c;
	}
}

/**
If the experience amount is negative, the experience is subtracted
*/
void adnd::class_level::add_xp(const long& xp)
{
	long _xp = xp / static_cast<long>(m_levels.size());
	for (auto& p : m_levels)
	{
		long xpNew = static_cast<long>(p.second.get_xp() + _xp * (1.0 + ((_xp > 0) ? p.second.xp_bonus() : 0)));
		if (xpNew < 0)
			throw std::exception("Experience cannot be negative", xpNew);

		auto t = adnd::templates::metadata::get_instance().get_class_template(p.first);

		short levelNew = t.get_level(xpNew);
		//if (p.second.level() != levelNew)
		//	result[p.first] = std::make_tuple(p.second.get_xp(), xpNew, p.second.level(), levelNew, (p.second.level() < levelNew) ? experience_shift::level_up : experience_shift::level_down);
		//else if (p.second.get_xp() != xpNew)
		//	result[p.first] = std::make_tuple(p.second.get_xp(), xpNew, p.second.level(), levelNew, (p.second.get_xp() < xpNew) ? experience_shift::xp_up : experience_shift::xp_down);
		p.second.level() = levelNew;
		p.second.set_xp(xpNew);
	}
}

adnd::class_level& adnd::class_level::operator+=(const long& xp)
{
	add_xp(xp);
	return (*this);
}

adnd::class_level& adnd::class_level::operator-=(const long& xp)
{
	add_xp(-xp);
	return (*this);
}

adnd::class_level& adnd::class_level::operator=(const short& lvl)
{
	/*set_level<defs::character_class::fighter>(lvl);
	set_level<defs::character_class::ranger>(lvl);
	set_level<defs::character_class::paladin>(lvl);
	set_level<defs::character_class::mage>(lvl);

	set_level<defs::character_class::abjurer>(lvl);
	set_level<defs::character_class::conjurer>(lvl);
	set_level<defs::character_class::diviner>(lvl);
	set_level<defs::character_class::enchanter>(lvl);
	set_level<defs::character_class::illusionist>(lvl);
	set_level<defs::character_class::invoker>(lvl);
	set_level<defs::character_class::necromancer>(lvl);
	set_level<defs::character_class::transmuter>(lvl);

	set_level<defs::character_class::cleric>(lvl);
	set_level<defs::character_class::druid>(lvl);
	set_level<defs::character_class::preist_of_specific_mythos>(lvl);
	set_level<defs::character_class::thief>(lvl);
	set_level<defs::character_class::bard>(lvl);*/
	return (*this);
}

short adnd::class_level::get_level(const defs::character_class& cls)
{
	if (m_levels.find(cls) == m_levels.end())
	{
		return 0;
	}
	return m_levels[cls].level();
}

short adnd::class_level::get_level()
{
	return m_levels.begin()->second.level();
}

std::map<adnd::defs::character_class, short> adnd::class_level::get_levels()
{
	std::map<adnd::defs::character_class, short> levels;
	for (auto l : m_levels)
	{
		levels[l.first] = l.second.level();
	}
	return levels;
}

long adnd::class_level::get_xp(const defs::character_class& cls)
{
	return m_levels[cls].get_xp();
}

long adnd::class_level::get_xp()
{
	long total = 0;
	for (auto l : m_levels)
	{
		total += l.second.get_xp();
	}
	return total;
}

adnd::class_level::level_score& adnd::class_level::operator[] (const defs::character_class& cls)
{
	return m_levels[cls];
}

adnd::class_level::level_score& adnd::class_level::operator[] (const short& idx)
{
	if (idx < 0 || idx >= static_cast<short>(m_levels.size()))
		throw std::exception("Invalid index", idx);

	auto l = m_levels.begin();
	for (int i = 0; i < idx; ++i)
		++l;
	return l->second;
}

size_t adnd::class_level::size()
{
	return m_levels.size();
}

void adnd::class_level::up(const short& levelIncrement)
{
	for (short l = 0; l < levelIncrement; ++l)
		up();
}

void adnd::class_level::down(const short& levelDecrement)
{
	for (short l = 0; l < levelDecrement; ++l)
		down();
}

void adnd::class_level::up(const defs::character_class& cls, const short& levelIncrement)
{
	if (m_levels.find(cls) == m_levels.end())
		throw std::exception("Invalid class specified", static_cast<int>(cls));

	level_score& lvl = m_levels[cls];
	for (short l = 0; l < levelIncrement; ++l)
		up(cls, lvl);
}

void adnd::class_level::down(const defs::character_class& cls, const short& levelDecrement)
{
	level_score& lvl = m_levels[cls];
	for (short l = 0; l < levelDecrement; ++l)
		down(cls, lvl);
}

void adnd::class_level::up()
{
	//Start by choosing the first class as the one to advance in
	defs::character_class chosenClassId = m_levels.begin()->first;
	level_score chosenClassLevel = m_levels.begin()->second;

	for (auto& l : m_levels)
	{
		auto currClsTempl = templates::metadata::get_instance().get_class_template(l.second.get_class());
		auto currClsNextLevelXP = currClsTempl.get_xp(l.second.level() + 1, l.first) - l.second.get_xp();

		auto chosenClsTempl = templates::metadata::get_instance().get_class_template(chosenClassId);
		auto chosenClsNextLevelXP = chosenClsTempl.get_xp(chosenClassLevel.level() + 1, chosenClassId) - chosenClassLevel.get_xp();

		// Choose the one requiring less experience
		if (currClsNextLevelXP < chosenClsNextLevelXP)
		{
			chosenClassId = l.first;
			chosenClassLevel = l.second;
		}
	}

	//chosenClassLevel = lowest level class, the one chosen to advance
	auto t = adnd::templates::metadata::get_instance().get_class_template(m_class);
	short newLevelScore = chosenClassLevel.level() + 1;
	long newLevelXp = t.get_xp(newLevelScore, chosenClassId);

	m_levels[chosenClassId].level() = newLevelScore;
	m_levels[chosenClassId].set_xp(newLevelXp);
}

void adnd::class_level::down()
{
	//Start by choosing the first class as the one to drain energy from
	defs::character_class chosenClassId = m_levels.begin()->first;
	level_score chosenClassLevel = m_levels.begin()->second;

	for (auto& l : m_levels)
	{
		auto currClsTempl = templates::metadata::get_instance().get_class_template(l.second.get_class());
		auto currClsNextLevelXP = currClsTempl.get_xp(l.second.level() + 1, l.first) - l.second.get_xp();
		
		auto chosenClsTempl = templates::metadata::get_instance().get_class_template(chosenClassId);
		auto chosenClsNextLevelXP = chosenClsTempl.get_xp(chosenClassLevel.level() + 1, chosenClassId) - chosenClassLevel.get_xp();

		if (l.second.level() > chosenClassLevel.level())
		{
			// Choose the highest
			chosenClassId = l.first;
			chosenClassLevel = l.second;
		}
		else if (currClsNextLevelXP > chosenClsNextLevelXP)
		{
			// Choose the one requiring less experience
			chosenClassId = l.first;
			chosenClassLevel = l.second;
		}
	}

	down(chosenClassId, chosenClassLevel);
}

void adnd::class_level::up(const defs::character_class& cls, level_score& lvl)
{
	auto t = adnd::templates::metadata::get_instance().get_class_template(cls);
	short newLevelScore = lvl.level() + 1;
	long newLevelXp = t.get_xp(newLevelScore, cls);

	m_levels[cls].level() = newLevelScore;
	m_levels[cls].set_xp(newLevelXp);
}

void adnd::class_level::down(const defs::character_class& cls, level_score& lvl)
{
	short newLevelScore = lvl.level() - 1;
	if (newLevelScore <= 0)
		throw std::exception("Level cannot be zero or below", newLevelScore);

	auto t = adnd::templates::metadata::get_instance().get_class_template(cls);
	/*
	The character's experience points drop to halfway between the minimum needed
	for his new (post - drain) level and the minimum needed for the next level
	above his new level
	*/
	auto l1 = t.get_xp(newLevelScore, cls);
	auto l2 = t.get_xp(newLevelScore + 1, cls);
	long newLevelXp = std::ldiv(l1 + l2, 2).quot;

	m_levels[cls].level() = newLevelScore;
	m_levels[cls].set_xp(newLevelXp);
}



adnd::class_level::level_score::level_score()
	: m_score(std::make_pair<short, long>(1, 0)), m_experienceBonus(0.0)
{
}

adnd::class_level::level_score::level_score(short lvl, long xp, const defs::character_class& cls)
	: m_score(std::pair<short, long>(lvl, xp)), m_class(cls), m_experienceBonus(0.0)
{
}

adnd::class_level::level_score& adnd::class_level::level_score::operator=(level_score& p)
{
	level() = p.level();
	m_score.second = p.get_xp();
	return (*this);
}

short& adnd::class_level::level_score::level()
{
	return m_score.first;
}

long adnd::class_level::level_score::get_xp()
{
	return m_score.second;
}

void adnd::class_level::level_score::set_xp(const long& xpValue)
{
	m_score.second = xpValue;
}

adnd::defs::character_class& adnd::class_level::level_score::get_class()
{
	return m_class;
}

short& adnd::class_level::level_score::level_limit()
{
	return m_levelLimit;
}

double& adnd::class_level::level_score::xp_bonus()
{
	return m_experienceBonus;
}

long adnd::class_level::level_score::compute_xp(const long& value)
{
	return static_cast<long>(value + m_experienceBonus * value);
}

void adnd::thief_skills::set_base_values(const short& pick, const short& open, const short& traps, const short& move, const short& hide, const short& hear, const short& climb, const short& read)
{
	if (size() > 0)
		throw std::exception("Unable to change base skill values");

	add(pick, open, traps, move, hide, hear, climb, read);
}

//adnd::thief_skills& adnd::thief_skills::operator+=(const adnd::tables::thief_ability& advancement)
//{
//	m_advancements.push_back(advancement);
//}

void adnd::thief_skills::add(const short& pick, const short& open, const short& traps, const short& move, const short& hide, const short& hear, const short& climb, const short& read)
{
	m_advancements.push_back(tables::thief_ability(pick, open, traps, move, hide, hear, climb, read));

	//// Check if any abilities exceed the 95% limit
	//for (auto sklType : { defs::thief_ability::pick_pockets, defs::thief_ability::open_locks,
	//				defs::thief_ability::find_remove_traps, defs::thief_ability::move_silently,
	//				defs::thief_ability::hide_in_shadows, defs::thief_ability::hear_noise,
	//				defs::thief_ability::climb_walls, defs::thief_ability::read_languages })
	//{
	//	if (ability(m_advancements.size(), sklType) > 95)
	//		throw std::exception("Thief ability score cannot exceed 95%");
	//}
}

short adnd::thief_skills::ability(const short& level, const defs::thief_ability& type) const
{
	short val = 0;
	short lvl = 0;
	for (auto it = m_advancements.begin(); it != m_advancements.end() && lvl < level; ++it, ++lvl)
	{
		val += it->ability(type);
	}
	return val;
}

void adnd::thief_skills::level_down(const short& levels)
{
	if (m_advancements.size() > levels)
		m_advancements.resize(m_advancements.size() - levels);
	else
		m_advancements.resize(0);
}

adnd::proficiency_slots::proficiency_slots(const defs::character_class& cls)
	: m_class(cls), m_usedSlots(0)
{
	auto t = templates::metadata::get_instance().get_class_template(cls);
	auto cc = t.get_class_composition();

	uint16_t weaponInitialScore = 0;
	uint16_t weaponNumLevels = 0;
	short hitPenalty = 0;
	uint16_t nonWeaponInitialScore = 0;
	uint16_t nonWeaponNumLevels = 0;

	for (auto ct : cc.types())
	{
		auto result = templates::metadata::get_instance().fetch<uint16_t, uint16_t, short, uint16_t, uint16_t>(templates::metadata::query::proficiency_slots);

		auto r = result.filter<short>(static_cast<short>(ct)).run().first();

		uint16_t wInitialScore = std::get<0>(r);
		if (wInitialScore > weaponInitialScore)
		{
			weaponInitialScore = wInitialScore;
			weaponNumLevels = std::get<1>(r);
			hitPenalty = std::get<2>(r);
		}
		
		short nwInitialScore = std::get<3>(r);
		if (nwInitialScore > nonWeaponInitialScore)
		{
			nonWeaponInitialScore = nwInitialScore;
			nonWeaponNumLevels = std::get<4>(r);
		}
	}
	
	m_weaponInitialSlots = weaponInitialScore;
	m_weaponSlots.resize(weaponInitialScore);
	m_weaponNumLevels = weaponNumLevels;
	m_hitPenalty = hitPenalty;
}

adnd::proficiency_slots& adnd::proficiency_slots::operator+=(const defs::equipment::item& item)
{
	auto t = templates::metadata::get_instance().get_item_template(item);
	if (t->item_type() != defs::item_type::weapon)
	{
		std::stringstream ss;
		ss << "Unable to gain weapon proficiency on " << t->description();
		throw std::exception(ss.str().c_str());
	}

	if (!has_free_slots(defs::proficiency::type::weapon))
		throw std::exception("No weapon proficiency slots left");

	bool found = false;
	for (auto s : m_weaponSlots)
	{
		if (s == item && m_class != defs::character_class::fighter)
			throw std::exception("Only fighters can further specialise with a weapon");
	}

	m_weaponSlots[m_usedSlots] = item;
	++m_usedSlots;

	return (*this);
}

adnd::defs::proficiency::level adnd::proficiency_slots::proficiency_level(const defs::equipment::item& item)
{
	auto t = templates::metadata::get_instance().get_item_template(item);
	if (t->item_type() != defs::item_type::weapon)
		return defs::proficiency::level::none;

	short count = 0;
	for (auto s : m_weaponSlots)
	{
		if (s == item)
			++count;
	}

	if (count == 0)
		return defs::proficiency::level::none;
	else if (count == 1)
		return defs::proficiency::level::proficient;
	else if (count == 2 && item != defs::equipment::item::composite_long_bow && item != defs::equipment::item::composite_short_bow
			&& item != defs::equipment::item::long_bow && item != defs::equipment::item::short_bow)
		return defs::proficiency::level::specialised;
	else if (count == 2 && (item == defs::equipment::item::composite_long_bow || item == defs::equipment::item::composite_short_bow
			|| item == defs::equipment::item::long_bow || item == defs::equipment::item::short_bow))
		return defs::proficiency::level::proficient;
	else
		return defs::proficiency::level::specialised;
}

short adnd::proficiency_slots::hit_modifier(const defs::equipment::item& item)
{
	auto t = templates::metadata::get_instance().get_item_template(item);
	if (t->item_type() != defs::item_type::weapon)
	{
		std::stringstream ss;
		ss << "No hit modifier for " << t->description();
		throw std::exception(ss.str().c_str());
	}

	short modifier = 0;
	auto wt = templates::metadata::get_instance().get_weapon_template(item);
	if (wt->attack_type() == defs::weapons::attack_type::melee)
	{
		adnd::defs::proficiency::level profLvl = proficiency_level(item);
		if (profLvl == defs::proficiency::level::none)
		{
			/*
			When a character uses a weapon that is similar to a weapon he is proficient with,
			his attack penalty is only one-half the normal amount (rounded up)
			*/
			bool found = false;
			for (auto it = m_weaponSlots.begin(); it != m_weaponSlots.end() && !found; ++it)
			{
				auto _t = templates::metadata::get_instance().get_weapon_template(*it);
				if (_t->weapon_group() == wt->weapon_group())
				{
					auto v = std::div(m_hitPenalty, 2);
					modifier = v.quot + v.rem;
					found = true;
				}
			}
			if (!found) // no proficiency in similar weapons... full penalty is applied
				modifier = m_hitPenalty;
		}
		else if (profLvl == defs::proficiency::level::proficient)
			modifier = 0;
		else if (profLvl == defs::proficiency::level::specialised)
			modifier = 1;
	}

	return modifier;
}

short adnd::proficiency_slots::damage_modifier(const defs::equipment::item& item)
{
	auto t = templates::metadata::get_instance().get_item_template(item);
	if (t->item_type() != defs::item_type::weapon)
	{
		std::stringstream ss;
		ss << "No damage modifier for " << t->description();
		throw std::exception(ss.str().c_str());
	}

	short modifier = 0;
	auto wt = templates::metadata::get_instance().get_weapon_template(item);
	if (wt->attack_type() == defs::weapons::attack_type::melee)
	{
		adnd::defs::proficiency::level profLvl = proficiency_level(item);
		if (profLvl == defs::proficiency::level::specialised)
			modifier = 2;
	}
	return modifier;
}

short adnd::proficiency_slots::level_change(const short& level)
{
	div_t res = std::div(level, m_weaponNumLevels);
	short slots = res.quot + m_weaponInitialSlots;
	short diff = slots - static_cast<short>(m_weaponSlots.size());
	m_weaponSlots.resize(slots);
	return diff;
}